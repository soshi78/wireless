﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblWorkType = New System.Windows.Forms.Label()
        Me.lblMAC = New System.Windows.Forms.Label()
        Me.lblSerialNo = New System.Windows.Forms.Label()
        Me.lblPID = New System.Windows.Forms.Label()
        Me.lblModuleSerialNo = New System.Windows.Forms.Label()
        Me.lblInspectionDate = New System.Windows.Forms.Label()
        Me.lblOperatorID = New System.Windows.Forms.Label()
        Me.BarcodeReader = New System.IO.Ports.SerialPort(Me.components)
        Me.lblInstructionDisplay = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblPaper = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblOrder = New System.Windows.Forms.Label()
        Me.lblOKWork = New System.Windows.Forms.Label()
        Me.lblQuantity = New System.Windows.Forms.Label()
        Me.PSoC = New System.IO.Ports.SerialPort(Me.components)
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lblNgSampleChk = New System.Windows.Forms.Label()
        Me.lblOkSampleChk = New System.Windows.Forms.Label()
        Me.lblInspectionCounter = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.lblJudge = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.lblCycleTime = New System.Windows.Forms.Label()
        Me.lblTiltle = New System.Windows.Forms.Label()
        Me.lblInspectionStartTime = New System.Windows.Forms.Label()
        Me.lblCycleTimeMonitor = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.TM_ManualMode = New System.Windows.Forms.ToolStripMenuItem()
        Me.PSoCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CameraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WebBrowserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblInsResult16 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.lblInsResult15 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.lblInsResult14 = New System.Windows.Forms.Label()
        Me.lblInsResult10 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.lblInsResult13 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.lblInsResult12 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.lblInsResult11 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblInsResult9 = New System.Windows.Forms.Label()
        Me.lblInsResult8 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.lblInsResult7 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.lblInsResult6 = New System.Windows.Forms.Label()
        Me.lblInsResult5 = New System.Windows.Forms.Label()
        Me.lblInsResult4 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblInsResult3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblInsResult2 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.lblInsResult1 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.lblInsResult0 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.lblWarningComment = New System.Windows.Forms.Label()
        Me.btnManuLEDChk = New System.Windows.Forms.Button()
        Me.lblCCounterComment = New System.Windows.Forms.Label()
        Me.lblWCCounter = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblIOUCount = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lblCvtCount = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.Location = New System.Drawing.Point(863, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(333, 30)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "製品品番"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.Location = New System.Drawing.Point(864, 280)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(310, 28)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "MACアドレス"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.Location = New System.Drawing.Point(1174, 280)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(188, 28)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "シリアルNo"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.Location = New System.Drawing.Point(1174, 224)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(188, 28)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "無線プロダクトID"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label6.Location = New System.Drawing.Point(864, 224)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(310, 28)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "無線モジュール検査シリアルNo"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label7.Location = New System.Drawing.Point(863, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(167, 30)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "検査年月日"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label8.Location = New System.Drawing.Point(1196, 26)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(166, 30)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "社員番号"
        '
        'lblWorkType
        '
        Me.lblWorkType.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblWorkType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWorkType.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblWorkType.Location = New System.Drawing.Point(863, 126)
        Me.lblWorkType.Name = "lblWorkType"
        Me.lblWorkType.Size = New System.Drawing.Size(333, 30)
        Me.lblWorkType.TabIndex = 8
        '
        'lblMAC
        '
        Me.lblMAC.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblMAC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMAC.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblMAC.Location = New System.Drawing.Point(864, 308)
        Me.lblMAC.Name = "lblMAC"
        Me.lblMAC.Size = New System.Drawing.Size(310, 28)
        Me.lblMAC.TabIndex = 9
        '
        'lblSerialNo
        '
        Me.lblSerialNo.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblSerialNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSerialNo.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblSerialNo.Location = New System.Drawing.Point(1174, 308)
        Me.lblSerialNo.Name = "lblSerialNo"
        Me.lblSerialNo.Size = New System.Drawing.Size(188, 28)
        Me.lblSerialNo.TabIndex = 10
        '
        'lblPID
        '
        Me.lblPID.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblPID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPID.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblPID.Location = New System.Drawing.Point(1174, 252)
        Me.lblPID.Name = "lblPID"
        Me.lblPID.Size = New System.Drawing.Size(188, 28)
        Me.lblPID.TabIndex = 11
        '
        'lblModuleSerialNo
        '
        Me.lblModuleSerialNo.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblModuleSerialNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblModuleSerialNo.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblModuleSerialNo.Location = New System.Drawing.Point(864, 252)
        Me.lblModuleSerialNo.Name = "lblModuleSerialNo"
        Me.lblModuleSerialNo.Size = New System.Drawing.Size(310, 28)
        Me.lblModuleSerialNo.TabIndex = 13
        '
        'lblInspectionDate
        '
        Me.lblInspectionDate.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblInspectionDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInspectionDate.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInspectionDate.Location = New System.Drawing.Point(863, 56)
        Me.lblInspectionDate.Name = "lblInspectionDate"
        Me.lblInspectionDate.Size = New System.Drawing.Size(167, 30)
        Me.lblInspectionDate.TabIndex = 14
        '
        'lblOperatorID
        '
        Me.lblOperatorID.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblOperatorID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblOperatorID.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblOperatorID.Location = New System.Drawing.Point(1196, 56)
        Me.lblOperatorID.Name = "lblOperatorID"
        Me.lblOperatorID.Size = New System.Drawing.Size(166, 30)
        Me.lblOperatorID.TabIndex = 15
        '
        'BarcodeReader
        '
        '
        'lblInstructionDisplay
        '
        Me.lblInstructionDisplay.BackColor = System.Drawing.Color.White
        Me.lblInstructionDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInstructionDisplay.Font = New System.Drawing.Font("メイリオ", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInstructionDisplay.Location = New System.Drawing.Point(4, 85)
        Me.lblInstructionDisplay.Name = "lblInstructionDisplay"
        Me.lblInstructionDisplay.Size = New System.Drawing.Size(853, 156)
        Me.lblInstructionDisplay.TabIndex = 16
        Me.lblInstructionDisplay.Text = "instructionDisplay" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "*"
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label9.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label9.Location = New System.Drawing.Point(1196, 95)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(166, 31)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "帳票No"
        '
        'lblPaper
        '
        Me.lblPaper.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblPaper.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPaper.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblPaper.Location = New System.Drawing.Point(1196, 126)
        Me.lblPaper.Name = "lblPaper"
        Me.lblPaper.Size = New System.Drawing.Size(166, 30)
        Me.lblPaper.TabIndex = 19
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label10.Location = New System.Drawing.Point(863, 156)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(167, 30)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "受注数量"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label11.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label11.Location = New System.Drawing.Point(1030, 156)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(166, 30)
        Me.Label11.TabIndex = 21
        Me.Label11.Text = "適合品数"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label12.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label12.Location = New System.Drawing.Point(1196, 156)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(166, 30)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "残数"
        '
        'lblOrder
        '
        Me.lblOrder.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblOrder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblOrder.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblOrder.Location = New System.Drawing.Point(863, 186)
        Me.lblOrder.Name = "lblOrder"
        Me.lblOrder.Size = New System.Drawing.Size(167, 30)
        Me.lblOrder.TabIndex = 23
        '
        'lblOKWork
        '
        Me.lblOKWork.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblOKWork.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblOKWork.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblOKWork.Location = New System.Drawing.Point(1030, 186)
        Me.lblOKWork.Name = "lblOKWork"
        Me.lblOKWork.Size = New System.Drawing.Size(166, 30)
        Me.lblOKWork.TabIndex = 24
        '
        'lblQuantity
        '
        Me.lblQuantity.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblQuantity.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblQuantity.Location = New System.Drawing.Point(1196, 186)
        Me.lblQuantity.Name = "lblQuantity"
        Me.lblQuantity.Size = New System.Drawing.Size(166, 30)
        Me.lblQuantity.TabIndex = 25
        '
        'PSoC
        '
        Me.PSoC.BaudRate = 38400
        Me.PSoC.PortName = "COM7"
        Me.PSoC.ReadBufferSize = 100
        Me.PSoC.ReadTimeout = 200
        Me.PSoC.WriteBufferSize = 100
        Me.PSoC.WriteTimeout = 200
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label22.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label22.Location = New System.Drawing.Point(1028, 26)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(168, 30)
        Me.Label22.TabIndex = 68
        Me.Label22.Text = "検査総数"
        '
        'lblNgSampleChk
        '
        Me.lblNgSampleChk.BackColor = System.Drawing.Color.White
        Me.lblNgSampleChk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNgSampleChk.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblNgSampleChk.Location = New System.Drawing.Point(1163, 497)
        Me.lblNgSampleChk.Name = "lblNgSampleChk"
        Me.lblNgSampleChk.Size = New System.Drawing.Size(186, 30)
        Me.lblNgSampleChk.TabIndex = 69
        Me.lblNgSampleChk.Text = "不適合ワークS**"
        Me.lblNgSampleChk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblOkSampleChk
        '
        Me.lblOkSampleChk.BackColor = System.Drawing.Color.White
        Me.lblOkSampleChk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblOkSampleChk.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblOkSampleChk.Location = New System.Drawing.Point(993, 497)
        Me.lblOkSampleChk.Name = "lblOkSampleChk"
        Me.lblOkSampleChk.Size = New System.Drawing.Size(181, 30)
        Me.lblOkSampleChk.TabIndex = 70
        Me.lblOkSampleChk.Text = "適合ワークS**"
        Me.lblOkSampleChk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblInspectionCounter
        '
        Me.lblInspectionCounter.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblInspectionCounter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInspectionCounter.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInspectionCounter.Location = New System.Drawing.Point(1028, 56)
        Me.lblInspectionCounter.Name = "lblInspectionCounter"
        Me.lblInspectionCounter.Size = New System.Drawing.Size(168, 30)
        Me.lblInspectionCounter.TabIndex = 71
        '
        'Label28
        '
        Me.Label28.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label28.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label28.Location = New System.Drawing.Point(993, 467)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(356, 30)
        Me.Label28.TabIndex = 74
        Me.Label28.Text = "日常点検"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label29
        '
        Me.Label29.BackColor = System.Drawing.Color.Transparent
        Me.Label29.Font = New System.Drawing.Font("メイリオ", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label29.Location = New System.Drawing.Point(363, 334)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(225, 30)
        Me.Label29.TabIndex = 76
        Me.Label29.Text = "検査工程"
        '
        'lblJudge
        '
        Me.lblJudge.BackColor = System.Drawing.Color.White
        Me.lblJudge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblJudge.Font = New System.Drawing.Font("メイリオ", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblJudge.Location = New System.Drawing.Point(501, 584)
        Me.lblJudge.Name = "lblJudge"
        Me.lblJudge.Size = New System.Drawing.Size(486, 120)
        Me.lblJudge.TabIndex = 77
        Me.lblJudge.Text = "総合判定"
        Me.lblJudge.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label31
        '
        Me.Label31.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label31.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label31.Location = New System.Drawing.Point(993, 527)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(356, 30)
        Me.Label31.TabIndex = 78
        Me.Label31.Text = "検査開始時刻"
        '
        'lblCycleTime
        '
        Me.lblCycleTime.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblCycleTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCycleTime.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblCycleTime.Location = New System.Drawing.Point(993, 587)
        Me.lblCycleTime.Name = "lblCycleTime"
        Me.lblCycleTime.Size = New System.Drawing.Size(356, 30)
        Me.lblCycleTime.TabIndex = 79
        Me.lblCycleTime.Text = "サイクルタイム"
        '
        'lblTiltle
        '
        Me.lblTiltle.BackColor = System.Drawing.Color.Silver
        Me.lblTiltle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTiltle.Font = New System.Drawing.Font("メイリオ", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblTiltle.Location = New System.Drawing.Point(4, 26)
        Me.lblTiltle.Name = "lblTiltle"
        Me.lblTiltle.Size = New System.Drawing.Size(853, 59)
        Me.lblTiltle.TabIndex = 82
        Me.lblTiltle.Text = "EX600無線ユニット検査治具(Z16-010)"
        Me.lblTiltle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblInspectionStartTime
        '
        Me.lblInspectionStartTime.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblInspectionStartTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInspectionStartTime.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInspectionStartTime.Location = New System.Drawing.Point(993, 557)
        Me.lblInspectionStartTime.Name = "lblInspectionStartTime"
        Me.lblInspectionStartTime.Size = New System.Drawing.Size(356, 30)
        Me.lblInspectionStartTime.TabIndex = 83
        '
        'lblCycleTimeMonitor
        '
        Me.lblCycleTimeMonitor.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblCycleTimeMonitor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCycleTimeMonitor.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblCycleTimeMonitor.Location = New System.Drawing.Point(993, 617)
        Me.lblCycleTimeMonitor.Name = "lblCycleTimeMonitor"
        Me.lblCycleTimeMonitor.Size = New System.Drawing.Size(356, 30)
        Me.lblCycleTimeMonitor.TabIndex = 84
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TM_ManualMode})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1354, 26)
        Me.MenuStrip1.TabIndex = 86
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'TM_ManualMode
        '
        Me.TM_ManualMode.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PSoCToolStripMenuItem, Me.CameraToolStripMenuItem, Me.WebBrowserToolStripMenuItem})
        Me.TM_ManualMode.Name = "TM_ManualMode"
        Me.TM_ManualMode.Size = New System.Drawing.Size(116, 22)
        Me.TM_ManualMode.Text = "マニュアルモード"
        '
        'PSoCToolStripMenuItem
        '
        Me.PSoCToolStripMenuItem.Name = "PSoCToolStripMenuItem"
        Me.PSoCToolStripMenuItem.Size = New System.Drawing.Size(149, 22)
        Me.PSoCToolStripMenuItem.Text = "PSoC"
        '
        'CameraToolStripMenuItem
        '
        Me.CameraToolStripMenuItem.Name = "CameraToolStripMenuItem"
        Me.CameraToolStripMenuItem.Size = New System.Drawing.Size(149, 22)
        Me.CameraToolStripMenuItem.Text = "Camera"
        '
        'WebBrowserToolStripMenuItem
        '
        Me.WebBrowserToolStripMenuItem.Name = "WebBrowserToolStripMenuItem"
        Me.WebBrowserToolStripMenuItem.Size = New System.Drawing.Size(149, 22)
        Me.WebBrowserToolStripMenuItem.Text = "WebBrowser"
        '
        'lblInsResult16
        '
        Me.lblInsResult16.BackColor = System.Drawing.Color.White
        Me.lblInsResult16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult16.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult16.Location = New System.Drawing.Point(739, 545)
        Me.lblInsResult16.Name = "lblInsResult16"
        Me.lblInsResult16.Size = New System.Drawing.Size(248, 30)
        Me.lblInsResult16.TabIndex = 241
        '
        'Label71
        '
        Me.Label71.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label71.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label71.Location = New System.Drawing.Point(498, 545)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(242, 30)
        Me.Label71.TabIndex = 242
        Me.Label71.Text = "MAC シリアルNo読込み"
        '
        'Label69
        '
        Me.Label69.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label69.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label69.Location = New System.Drawing.Point(498, 515)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(242, 30)
        Me.Label69.TabIndex = 240
        Me.Label69.Text = "FeRAM読込み確認"
        '
        'lblInsResult15
        '
        Me.lblInsResult15.BackColor = System.Drawing.Color.White
        Me.lblInsResult15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult15.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult15.Location = New System.Drawing.Point(739, 515)
        Me.lblInsResult15.Name = "lblInsResult15"
        Me.lblInsResult15.Size = New System.Drawing.Size(248, 30)
        Me.lblInsResult15.TabIndex = 239
        '
        'Label67
        '
        Me.Label67.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label67.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label67.Location = New System.Drawing.Point(498, 485)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(242, 30)
        Me.Label67.TabIndex = 238
        Me.Label67.Text = "FeRAM書込みプロテクト"
        '
        'lblInsResult14
        '
        Me.lblInsResult14.BackColor = System.Drawing.Color.White
        Me.lblInsResult14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult14.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult14.Location = New System.Drawing.Point(739, 485)
        Me.lblInsResult14.Name = "lblInsResult14"
        Me.lblInsResult14.Size = New System.Drawing.Size(248, 30)
        Me.lblInsResult14.TabIndex = 237
        '
        'lblInsResult10
        '
        Me.lblInsResult10.BackColor = System.Drawing.Color.White
        Me.lblInsResult10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult10.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult10.Location = New System.Drawing.Point(739, 368)
        Me.lblInsResult10.Name = "lblInsResult10"
        Me.lblInsResult10.Size = New System.Drawing.Size(248, 30)
        Me.lblInsResult10.TabIndex = 236
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label14.Location = New System.Drawing.Point(498, 368)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(242, 30)
        Me.Label14.TabIndex = 235
        Me.Label14.Text = "ソフトウェアVer書込み"
        '
        'Label63
        '
        Me.Label63.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label63.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label63.Location = New System.Drawing.Point(498, 455)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(242, 30)
        Me.Label63.TabIndex = 234
        Me.Label63.Text = "シリアルNo書込み"
        '
        'lblInsResult13
        '
        Me.lblInsResult13.BackColor = System.Drawing.Color.White
        Me.lblInsResult13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult13.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult13.Location = New System.Drawing.Point(739, 455)
        Me.lblInsResult13.Name = "lblInsResult13"
        Me.lblInsResult13.Size = New System.Drawing.Size(248, 30)
        Me.lblInsResult13.TabIndex = 233
        '
        'Label59
        '
        Me.Label59.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label59.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label59.Location = New System.Drawing.Point(498, 425)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(242, 30)
        Me.Label59.TabIndex = 232
        Me.Label59.Text = "MACアドレス書込み"
        '
        'lblInsResult12
        '
        Me.lblInsResult12.BackColor = System.Drawing.Color.White
        Me.lblInsResult12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult12.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult12.Location = New System.Drawing.Point(739, 425)
        Me.lblInsResult12.Name = "lblInsResult12"
        Me.lblInsResult12.Size = New System.Drawing.Size(248, 30)
        Me.lblInsResult12.TabIndex = 231
        '
        'Label57
        '
        Me.Label57.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label57.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label57.Location = New System.Drawing.Point(498, 398)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(242, 30)
        Me.Label57.TabIndex = 230
        Me.Label57.Text = "無線PID書込み"
        '
        'lblInsResult11
        '
        Me.lblInsResult11.BackColor = System.Drawing.Color.White
        Me.lblInsResult11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult11.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult11.Location = New System.Drawing.Point(739, 398)
        Me.lblInsResult11.Name = "lblInsResult11"
        Me.lblInsResult11.Size = New System.Drawing.Size(248, 30)
        Me.lblInsResult11.TabIndex = 229
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label30.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label30.Location = New System.Drawing.Point(6, 676)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(237, 28)
        Me.Label30.TabIndex = 228
        Me.Label30.Text = "NP40Ver"
        '
        'Label26
        '
        Me.Label26.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label26.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label26.Location = New System.Drawing.Point(6, 648)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(237, 28)
        Me.Label26.TabIndex = 227
        Me.Label26.Text = "RX62Ver"
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label24.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label24.Location = New System.Drawing.Point(6, 620)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(237, 28)
        Me.Label24.TabIndex = 226
        Me.Label24.Text = "RX210Ver"
        '
        'lblInsResult9
        '
        Me.lblInsResult9.BackColor = System.Drawing.Color.White
        Me.lblInsResult9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult9.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult9.Location = New System.Drawing.Point(243, 676)
        Me.lblInsResult9.Name = "lblInsResult9"
        Me.lblInsResult9.Size = New System.Drawing.Size(252, 28)
        Me.lblInsResult9.TabIndex = 225
        '
        'lblInsResult8
        '
        Me.lblInsResult8.BackColor = System.Drawing.Color.White
        Me.lblInsResult8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult8.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult8.Location = New System.Drawing.Point(243, 648)
        Me.lblInsResult8.Name = "lblInsResult8"
        Me.lblInsResult8.Size = New System.Drawing.Size(252, 28)
        Me.lblInsResult8.TabIndex = 224
        '
        'Label48
        '
        Me.Label48.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label48.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label48.Location = New System.Drawing.Point(6, 592)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(489, 28)
        Me.Label48.TabIndex = 223
        Me.Label48.Text = "ソフトウェアVer読込み"
        '
        'lblInsResult7
        '
        Me.lblInsResult7.BackColor = System.Drawing.Color.White
        Me.lblInsResult7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult7.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult7.Location = New System.Drawing.Point(243, 620)
        Me.lblInsResult7.Name = "lblInsResult7"
        Me.lblInsResult7.Size = New System.Drawing.Size(252, 28)
        Me.lblInsResult7.TabIndex = 222
        '
        'Label40
        '
        Me.Label40.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label40.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label40.Location = New System.Drawing.Point(6, 564)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(237, 28)
        Me.Label40.TabIndex = 221
        Me.Label40.Text = "WEBソフトウェア書込み"
        '
        'lblInsResult6
        '
        Me.lblInsResult6.BackColor = System.Drawing.Color.White
        Me.lblInsResult6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult6.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult6.Location = New System.Drawing.Point(243, 564)
        Me.lblInsResult6.Name = "lblInsResult6"
        Me.lblInsResult6.Size = New System.Drawing.Size(252, 28)
        Me.lblInsResult6.TabIndex = 220
        '
        'lblInsResult5
        '
        Me.lblInsResult5.BackColor = System.Drawing.Color.White
        Me.lblInsResult5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult5.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult5.Location = New System.Drawing.Point(243, 536)
        Me.lblInsResult5.Name = "lblInsResult5"
        Me.lblInsResult5.Size = New System.Drawing.Size(252, 28)
        Me.lblInsResult5.TabIndex = 219
        '
        'lblInsResult4
        '
        Me.lblInsResult4.BackColor = System.Drawing.Color.White
        Me.lblInsResult4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult4.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult4.Location = New System.Drawing.Point(243, 508)
        Me.lblInsResult4.Name = "lblInsResult4"
        Me.lblInsResult4.Size = New System.Drawing.Size(252, 28)
        Me.lblInsResult4.TabIndex = 218
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label17.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label17.Location = New System.Drawing.Point(6, 480)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(237, 28)
        Me.Label17.TabIndex = 217
        Me.Label17.Text = "COM確認"
        '
        'lblInsResult3
        '
        Me.lblInsResult3.BackColor = System.Drawing.Color.White
        Me.lblInsResult3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult3.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult3.Location = New System.Drawing.Point(243, 480)
        Me.lblInsResult3.Name = "lblInsResult3"
        Me.lblInsResult3.Size = New System.Drawing.Size(252, 28)
        Me.lblInsResult3.TabIndex = 216
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 452)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(237, 28)
        Me.Label5.TabIndex = 215
        Me.Label5.Text = "過電流確認"
        '
        'lblInsResult2
        '
        Me.lblInsResult2.BackColor = System.Drawing.Color.White
        Me.lblInsResult2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult2.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult2.Location = New System.Drawing.Point(243, 452)
        Me.lblInsResult2.Name = "lblInsResult2"
        Me.lblInsResult2.Size = New System.Drawing.Size(252, 28)
        Me.lblInsResult2.TabIndex = 214
        '
        'Label90
        '
        Me.Label90.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label90.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label90.Location = New System.Drawing.Point(6, 536)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(237, 28)
        Me.Label90.TabIndex = 213
        Me.Label90.Text = "EX600無線検査"
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label36.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label36.Location = New System.Drawing.Point(6, 424)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(237, 28)
        Me.Label36.TabIndex = 212
        Me.Label36.Text = "FeRAM初期値書込み"
        '
        'lblInsResult1
        '
        Me.lblInsResult1.BackColor = System.Drawing.Color.White
        Me.lblInsResult1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult1.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult1.Location = New System.Drawing.Point(243, 424)
        Me.lblInsResult1.Name = "lblInsResult1"
        Me.lblInsResult1.Size = New System.Drawing.Size(252, 28)
        Me.lblInsResult1.TabIndex = 211
        '
        'Label49
        '
        Me.Label49.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label49.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label49.Location = New System.Drawing.Point(6, 368)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(237, 56)
        Me.Label49.TabIndex = 210
        Me.Label49.Text = "無線モジュール検査" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "シリアルNo読取り"
        '
        'lblInsResult0
        '
        Me.lblInsResult0.BackColor = System.Drawing.Color.White
        Me.lblInsResult0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblInsResult0.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblInsResult0.Location = New System.Drawing.Point(243, 368)
        Me.lblInsResult0.Name = "lblInsResult0"
        Me.lblInsResult0.Size = New System.Drawing.Size(252, 56)
        Me.lblInsResult0.TabIndex = 209
        Me.lblInsResult0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label34
        '
        Me.Label34.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label34.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label34.Location = New System.Drawing.Point(6, 508)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(237, 28)
        Me.Label34.TabIndex = 208
        Me.Label34.Text = "EX600製品通常検査"
        '
        'lblWarningComment
        '
        Me.lblWarningComment.BackColor = System.Drawing.Color.White
        Me.lblWarningComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWarningComment.Font = New System.Drawing.Font("メイリオ", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblWarningComment.Location = New System.Drawing.Point(5, 244)
        Me.lblWarningComment.Name = "lblWarningComment"
        Me.lblWarningComment.Size = New System.Drawing.Size(853, 30)
        Me.lblWarningComment.TabIndex = 244
        '
        'btnManuLEDChk
        '
        Me.btnManuLEDChk.Font = New System.Drawing.Font("メイリオ", 14.25!)
        Me.btnManuLEDChk.Location = New System.Drawing.Point(993, 650)
        Me.btnManuLEDChk.Name = "btnManuLEDChk"
        Me.btnManuLEDChk.Size = New System.Drawing.Size(356, 52)
        Me.btnManuLEDChk.TabIndex = 248
        Me.btnManuLEDChk.Text = "LED目視検査モード"
        Me.btnManuLEDChk.UseVisualStyleBackColor = True
        '
        'lblCCounterComment
        '
        Me.lblCCounterComment.BackColor = System.Drawing.Color.White
        Me.lblCCounterComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCCounterComment.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblCCounterComment.Location = New System.Drawing.Point(5, 277)
        Me.lblCCounterComment.Name = "lblCCounterComment"
        Me.lblCCounterComment.Size = New System.Drawing.Size(853, 59)
        Me.lblCCounterComment.TabIndex = 249
        Me.lblCCounterComment.Text = "ワークコネクタ/IOユニット/コネクタ変換基板 の使用回数を超えています。" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "該当品の交換、カウンタリセットを製造技術担当者に連絡してください。"
        '
        'lblWCCounter
        '
        Me.lblWCCounter.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblWCCounter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWCCounter.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblWCCounter.Location = New System.Drawing.Point(1177, 372)
        Me.lblWCCounter.Name = "lblWCCounter"
        Me.lblWCCounter.Size = New System.Drawing.Size(175, 30)
        Me.lblWCCounter.TabIndex = 251
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label16.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label16.Location = New System.Drawing.Point(993, 372)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(184, 30)
        Me.Label16.TabIndex = 250
        Me.Label16.Text = "ワークコネクタ"
        '
        'lblIOUCount
        '
        Me.lblIOUCount.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblIOUCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblIOUCount.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblIOUCount.Location = New System.Drawing.Point(1177, 432)
        Me.lblIOUCount.Name = "lblIOUCount"
        Me.lblIOUCount.Size = New System.Drawing.Size(175, 30)
        Me.lblIOUCount.TabIndex = 253
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label19.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label19.Location = New System.Drawing.Point(993, 432)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(184, 30)
        Me.Label19.TabIndex = 252
        Me.Label19.Text = "IOユニット"
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label20.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label20.Location = New System.Drawing.Point(1177, 402)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(175, 30)
        Me.Label20.TabIndex = 255
        '
        'lblCvtCount
        '
        Me.lblCvtCount.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblCvtCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCvtCount.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblCvtCount.Location = New System.Drawing.Point(993, 402)
        Me.lblCvtCount.Name = "lblCvtCount"
        Me.lblCvtCount.Size = New System.Drawing.Size(184, 30)
        Me.lblCvtCount.TabIndex = 254
        Me.lblCvtCount.Text = "コネクタ変換基板"
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label23.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label23.Location = New System.Drawing.Point(993, 342)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(359, 30)
        Me.Label23.TabIndex = 256
        Me.Label23.Text = "コネクタ使用回数"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1354, 705)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.lblCvtCount)
        Me.Controls.Add(Me.lblIOUCount)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.lblWCCounter)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.lblCCounterComment)
        Me.Controls.Add(Me.btnManuLEDChk)
        Me.Controls.Add(Me.lblWarningComment)
        Me.Controls.Add(Me.lblInsResult16)
        Me.Controls.Add(Me.Label71)
        Me.Controls.Add(Me.Label69)
        Me.Controls.Add(Me.lblInsResult15)
        Me.Controls.Add(Me.Label67)
        Me.Controls.Add(Me.lblInsResult14)
        Me.Controls.Add(Me.lblInsResult10)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label63)
        Me.Controls.Add(Me.lblInsResult13)
        Me.Controls.Add(Me.Label59)
        Me.Controls.Add(Me.lblInsResult12)
        Me.Controls.Add(Me.Label57)
        Me.Controls.Add(Me.lblInsResult11)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.lblInsResult9)
        Me.Controls.Add(Me.lblInsResult8)
        Me.Controls.Add(Me.Label48)
        Me.Controls.Add(Me.lblInsResult7)
        Me.Controls.Add(Me.Label40)
        Me.Controls.Add(Me.lblInsResult6)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.lblInsResult3)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblInsResult2)
        Me.Controls.Add(Me.Label90)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.lblInsResult1)
        Me.Controls.Add(Me.Label49)
        Me.Controls.Add(Me.lblInsResult0)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.lblCycleTimeMonitor)
        Me.Controls.Add(Me.lblInspectionStartTime)
        Me.Controls.Add(Me.lblTiltle)
        Me.Controls.Add(Me.lblCycleTime)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.lblJudge)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.lblInspectionCounter)
        Me.Controls.Add(Me.lblOkSampleChk)
        Me.Controls.Add(Me.lblNgSampleChk)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.lblQuantity)
        Me.Controls.Add(Me.lblOKWork)
        Me.Controls.Add(Me.lblOrder)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblPaper)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lblInstructionDisplay)
        Me.Controls.Add(Me.lblOperatorID)
        Me.Controls.Add(Me.lblInspectionDate)
        Me.Controls.Add(Me.lblModuleSerialNo)
        Me.Controls.Add(Me.lblPID)
        Me.Controls.Add(Me.lblSerialNo)
        Me.Controls.Add(Me.lblMAC)
        Me.Controls.Add(Me.lblWorkType)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.lblInsResult4)
        Me.Controls.Add(Me.lblInsResult5)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.Text = "lblConnectorWarning"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents lblWorkType As Label
    Friend WithEvents lblMAC As Label
    Friend WithEvents lblSerialNo As Label
    Friend WithEvents lblPID As Label
    Friend WithEvents lblModuleSerialNo As Label
    Friend WithEvents lblInspectionDate As Label
    Friend WithEvents lblOperatorID As Label
    Friend WithEvents BarcodeReader As IO.Ports.SerialPort
    Friend WithEvents lblInstructionDisplay As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents lblPaper As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents lblOrder As Label
    Friend WithEvents lblOKWork As Label
    Friend WithEvents lblQuantity As Label
    Friend WithEvents PSoC As IO.Ports.SerialPort
    Friend WithEvents Label22 As Label
    Friend WithEvents lblNgSampleChk As Label
    Friend WithEvents lblOkSampleChk As Label
    Friend WithEvents lblInspectionCounter As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents lblJudge As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents lblCycleTime As Label
    Friend WithEvents lblTiltle As Label
    Friend WithEvents lblInspectionStartTime As Label
    Friend WithEvents lblCycleTimeMonitor As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents TM_ManualMode As ToolStripMenuItem
    Friend WithEvents PSoCToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CameraToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lblInsResult16 As Label
    Friend WithEvents Label71 As Label
    Friend WithEvents Label69 As Label
    Friend WithEvents lblInsResult15 As Label
    Friend WithEvents Label67 As Label
    Friend WithEvents lblInsResult14 As Label
    Friend WithEvents lblInsResult10 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents lblInsResult13 As Label
    Friend WithEvents Label59 As Label
    Friend WithEvents lblInsResult12 As Label
    Friend WithEvents Label57 As Label
    Friend WithEvents lblInsResult11 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents lblInsResult9 As Label
    Friend WithEvents lblInsResult8 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents lblInsResult7 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents lblInsResult6 As Label
    Friend WithEvents lblInsResult5 As Label
    Friend WithEvents lblInsResult4 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents lblInsResult3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents lblInsResult2 As Label
    Friend WithEvents Label90 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents lblInsResult1 As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents lblInsResult0 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents lblWarningComment As Label
    Friend WithEvents btnManuLEDChk As Button
    Friend WithEvents WebBrowserToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lblCCounterComment As Label
    Friend WithEvents lblWCCounter As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents lblIOUCount As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents lblCvtCount As Label
    Friend WithEvents Label23 As Label
End Class
