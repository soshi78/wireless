﻿Option Explicit On

Imports OpenCvSharp
Imports Sna.NoWire

Public Class frmMain

    '-----バーコードパターン-----
    Private Const REG_BARCODE_PATTERN_BC1 As String = "\d{4}\$.{25}"
    Private Const REG_BARCODE_PATTERN_BC2 As String = "a[+-]?\d{7}a"
    Private Const REG_BARCODE_PATTERN_ID As String = "a\d{6}a" '作業者ID;前後のa＋6連続の数字

    Private Const REG_BARCODE_PATTERN_PID As String = "^([0-9A-F]{2}-){3}[0-9A-F]{2}$" '製品ID'修正予定
    Private Const REG_BARCODE_PATTERN_MAC As String = "^([0-9A-F]{2}-){5}[0-9A-F]{2}$" 'MACアドレス
    Private Const REG_BARCODE_PATTERN_MASTER As String = "^([0-9A-F]{2}-){5}[0-9A-F]{2}\r\n([0-9A-F]{2}-){3}[0-9A-F]{2}$"
    Private Const REG_BARCODE_PATTERN_SLAVE As String = "^([0-9A-F]{2}-){3}[0-9A-F]{2}$"
    Private Const REG_BARCODE_PATTERN_SAMPLE As String = "-(OK|NG)?SAMPLE" '-SAMPLE,-OKSAMPLE,-NGSAMPLEにマッチ

    'ファイルパス
    Private Const pathSettingFolder As String = "\TestData\品番リスト\Data\"
    Private Const pathInspectionCounter As String = "\TestData\検査回数\" '検査回数
    Private Const pathConverterCounter As String = "\TestData\検査回数\converter_counter.txt" 'コネクタ変換基板抜入回数
    Private Const pathIOUnitCounter As String = "\TestData\検査回数\iounit_counter.txt" 'IOユニット抜入回数
    Private Const pathWorkConnectorCounter As String = "\TestData\検査回数\work_connector_counter.txt" 'ワークコネクタ（上部クランプコネクタ）抜入回数
    Private Const pathConverterCounterLimit As String = "\TestData\検査回数\limit_converter_counter.txt" 'コネクタ変換基板抜入限度回数
    Private Const pathIOUnitCounterLimit As String = "\TestData\検査回数\limit_iounit_counter.txt" 'IOユニット抜入限度回数
    Private Const pathWorkConnectorCounterLimit As String = "\TestData\検査回数\limit_work_connector_counter.txt" 'ワークコネクタ（上部クランプコネクタ）抜入限度回数
    Private Const pathPaperFolder As String = "\TestData\帳票\"
    Private Const pathProgramNumber As String = "\TestData\設備プログラム管理番号\"
    Private Const pathInitFeRAMFolder As String = "\TestData\FeRAM初期値\"
    Private Const pathDailyCheck As String = "\TestData\日常点検\"
    Private Const pathSerialNoFolder As String = "\TestData\シリアルNo\"
    Private Const pathErrorCode As String = "\TestData\error_code.txt"
    Private Const pathPinUpper As String = "\TestData\治具設定\pin_upper_setting.txt"
    Private Const pathConnecter As String = "\TestData\治具設定\connecter_setting.txt"
    Private Const pathPlc As String = "\TestData\治具設定\plc_setting.txt"
    Private Const pathLEDChkSetting As String = "\TestData\LED確認\LED_Chk_Setting.txt"
    '外部ストレージ(管理PC,NAS)
    Private Const pathControlPC As String = "C:\Z1501401\TestData\検査履歴\EX600_wireless\" 'todo:変更'管理PC確認用
    Private Const pathNAS As String = "C:\Z1501402\TestData\検査履歴\EX600_wireless\" 'todo:変更'NAS確認用
    Private Const pathHistoryModuleInspection As String = "C:\Z1501401\TestData\検査履歴\EX600_wireless\Z16-00801\" 'todo:変更'管理PCモジュール検査履歴保存フォルダ
    Private Const pathHistory As String = "C:\Z1501401\TestData\検査履歴\EX600_wireless\Z16-01001\" 'todo:変更'管理PC履歴保存フォルダ
    Private Const pathHistoryNAS As String = "C:\Z1501402\TestData\検査履歴\EX600_wireless\Z16-01001\" 'todo:変更'NAS履歴保存フォルダ

    'Primary Setup Tool
    Private Const pathPstBatBrowse As String = "\TestData\PST\bat_files\pstBROWSE.bat"
    Private Const pathPstBatName As String = "\TestData\PST\bat_files\pstNAME.bat"
    Private Const pathPstBatIP As String = "\TestData\PST\bat_files\pstIP.bat"
    Private Const pathPstBatReset As String = "\TestData\PST\bat_files\pstReset.bat"
    Private Const pathPstResultBrowse As String = "\TestData\PST\results\BrowseResult.txt"
    Private Const pathPstResultNAME As String = "\TestData\PST\results\NAMEResult.txt"
    Private Const pathPstResultIP As String = "\TestData\PST\results\IPResult.txt"
    Private Const pathPstResultReset As String = "\TestData\PST\results\ResetResult.txt"

    'ソリューションファイルパス
    Private PathSlnFile As String = System.IO.Path.GetDirectoryName(
                                    System.IO.Path.GetDirectoryName(
                                    System.IO.Path.GetDirectoryName(Application.StartupPath)))


    'PSoC通信部設定メンバ---------------------------------------
    Dim Inrxbuff(10) As Byte                'RS232C(IN)受信データ
    Dim Outrxbuff(3) As Byte                'RS232C(OUT)受信データ
    Dim Rxflg As Boolean = False            '受信完了フラグ
    Dim RS232CIOTIMEOUT As Integer = 5000   '読込み待ち時間(5秒)
    Dim RS232CIORETRYCOUNT As Integer = &H2 '再読み込み回数
    Dim Buff(15) As Byte

    'PSoC通信出力用データ---------------------------------------
    Dim Port0outdata As Byte = &H0 'PSoCポート1出力データ
    Dim Port1outdata As Byte = &H0 'PSoCポート2出力データ
    Dim Port2outdata As Byte = &H0 'PSoCポート2出力データ
    Dim Port3outdata As Byte = &H0 'PSoCポート3出力データ
    Dim Port4outdata As Byte = &H0 'PSoCポート4出力データ


    '設備立ち上げ日
    Private Const startUpDate As String = "2017/04/01"

    'ベリファイ用の書き込みデータ配列
    '通常メモリエリア用（開5定義のチェックサムが16バイト目に来るので列数は15でOK）
    Dim normalAreaMemoryArr()() As Byte 'FormLoadで初期化する


    '制御用メモリエリア用
    Dim controlAreaMemoryArr()() As Byte 'FormLoadで初期化する


    'Encoding
    Private enc_sj As System.Text.Encoding = System.Text.Encoding.GetEncoding("shift_jis")

    'SMC固有のMACアドレスNo.
    Private Const SMC_MAC_ID As String = "00-23-C6"

    '検査履歴の項目
    Dim historyItem As String = String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}," &
                                              "{10},{11},{12},{13},{14},{15},{16},{17},{18},{19}," &
                                              "{20},{21},{22},{23},{24},{25},{26},{27},{28},{29}," &
                                              "{30},{31},{32},{33}",
                                              "検査時刻",
                                              "工程",
                                              "社員番号",
                                              "製品品番",
                                              "日常点検",
                                              "帳票No",
                                              "残数",
                                              "総合判定",
                                              "不適合工程",
                                              "タクトタイム",
                                              "MACアドレス",
                                              "シリアルNo",
                                              "無線PID",
                                              "フォームウェアバージョン（全体）",
                                              "フォームウェアバージョン（RX210）",
                                              "フォームウェアバージョン（RX62）",
                                              "フォームウェアバージョン（NP40）",
                                              "無線モジュール検査シリアルNo",
                                              "【無線モジュール検査シリアル番号読込み】",
                                              "【FeRAM初期値書込み】",
                                              "【過電流確認】",
                                              "【COM確認】",
                                              "【通常製品検査】",
                                              "【無線通信検査】",
                                              "【WEBソフトウェア書込み】",
                                              "【ソフトウェアバージョン読込み】",
                                              "【ソフトウェアバージョン書込み】]",
                                              "【無線PID書込み】",
                                              "【EtherNetMACアドレス書込み】",
                                              "【EtherNetシリアルNo書込み】",
                                              "【FeRAMグループ0書込みプロテクト】",
                                              "【FeRAM全体読込み確認】",
                                              "【MAC_シリアルNo読込み】",
                                              "開発出荷品フラグ")




    'バーコード読み取り状態
    Enum BarcodeReadStep As Integer
        IDRead  '1.社員番号バーコードを読むステップ
        BC1Read '2.バーコード1(残数$生産品番)読むステップ
        BC2Read '3.帳票No.を読むステップ
        PID_MACRead 'PID,MACを読むステップ
        SetupDone '検査工程開始可能な状態
    End Enum





    '品番パラメータ
    Structure InspectionParams
        Dim DailyChk As String() '日常点検
        Dim ProductType As String() '製品種別1,2
        Dim FormwareVersion As String() '全体ver,RX210ver,RX62ver.NP40ver
        Dim PinUpperJudge As String() 'ピン治具上部判定
        Dim PLCJudge As String() 'PLC判別
        Dim PinBoardJudge As String() 'ピン変換基板判別
        Dim FeRAMWriteInitVal As String() '[FeRAM初期値書込み]
        Dim Overcurrent As String() '過電流確認
        Dim ComChk As String() 'COM確認
        Dim PInspection As String() '[通常製品検査]
        Dim LEDChk1 As String() '[LED確認(製品検査)],flg,autoCheck
        Dim ReadNModuleSeraialNo As String() '[無線モジュールシリアル番号読込み]
        Dim NNowireCom As String() '[無線通信検査]
        Dim PST As String() '[PST]'primary setup tool設定
        Dim WEBSoftware As String() '[WEBソフトウェア書込み]
        Dim ReadSoftwareVer As String() '[ソフトウェアバージョン読込み]
        Dim WriteSoftVerFeRAM As String() '[ソフトウェアバージョンFeRAM書込み]
        Dim WritePID As String() '[無線PID書込み]
        Dim WriteMAC As String() '[EtherNetMACアドレス書込み]
        Dim WriteSerial As String() '[EtherNetシリアルNo書込み]
        Dim FeRAMProtect As String() '[FeRAMプロテクト]
        Dim ReadFeRAM As String() '[FeRAM読込み確認]
        Dim ReadMACSerialNo As String() '[MAC_SerialNo読込み]

    End Structure

    ''' <summary>
    ''' 品番リストから読みこんだパラメータを格納
    ''' </summary>
    Dim Params As New InspectionParams

    'LED確認(画像処理)パラメータ
    Structure LED_Chk_Setting
        Dim TH_V As String() '明度閾値
        Dim TH_S As String() '彩度閾値
        Dim LED_Area_MAX As String() 'LEDラベル面積(MAX)
        Dim LED_Area_MIN As String() 'LEDラベル面積(MIN)
        Dim TH_H_RED_MAX As String() '赤色色相(MAX)
        Dim TH_H_RED_MIN As String() '赤色色相(MIN)
        Dim TH_H_GREEN_MAX As String() '緑色色相(MAX)
        Dim TH_H_GREEN_MIN As String() '緑色色相(MIN)
    End Structure

    ''' <summary>
    ''' LED確認のパラメータを格納
    ''' </summary>
    Dim chkLED_Setting As New LED_Chk_Setting

    Structure ColorTH
        Dim Red_Max As Integer
        Dim Red_Min As Integer
        Dim Green_Max As Integer
        Dim Green_Min As Integer
    End Structure

    'LED色相閾値
    Dim LEDColorTh As New ColorTH

    'LED色
    Enum LED_COLOR
        RED
        ORANGE
        GREEN
        OFF
        OTHER
    End Enum

    Dim barcodeStr As String = String.Empty 'バーコード受信用
    Dim errorProcessName As String = String.Empty '検査不適合工程名を保持 
    Dim errorProcessContent As String = String.Empty 'エラー表示用の文字列
    Dim regexReplace As New Text.RegularExpressions.Regex(REG_BARCODE_PATTERN_SAMPLE) 'サンプルバーコードの品番以外の部分を除去する用
    Dim isReInspection As Boolean = False '検査ワークが再検査品かどうか
    Dim isNgWorkReInspection As Boolean = False '検査不合格品再検査かどうか
    Dim hasSerialNo As Boolean = False '検査ワークにシリアル番号が既に割当てられているかどうか
    Dim isManuLEDChkMode As Boolean = False 'LED目視検査モードフラグ
    Dim preIncrementedSerialNo As String = String.Empty 'インクリメント前のシリアルNo(シリアルNoのベリファイ用)
    Dim isDiv5Product As Boolean = False '開発5部出荷品フラグ(検査履歴なしで初期値以外のPID,MACの書込みあり),(PID書込み以降で例外などが発生した場合検査治具を使用しても同様の状態になり得るので注意)

    Dim wcLimit As ULong = 0 'ワークコネクタ使用限度回数
    Dim cvtLimit As ULong = 0 'コネクタ変換基板使用限度回数
    Dim iouLimit As ULong = 0 'IOユニット使用限度回数

    '検査時間計測用ファイルの項目
    Dim insTimeItem As String = String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}",
                                              "モジュールシリアル番号読込み",
                                              "FeRAM初期値書込み",
                                              "過電流検知",
                                              "COM出力確認",
                                              "製品検査（テストプログラム）",
                                              "製品検査（LED確認）",
                                              "無線通信検査",
                                              "WEB書込み",
                                              "ソフトVer読込み",
                                              "ソフトVer書込み",
                                              "PID書込み",
                                              "MAC書込み",
                                              "シリアルNo書込み",
                                              "FeRAM書込みプロテクト",
                                              "FeRAM読込み確認",
                                              "MAC_シリアルNo確認",
                                              "サイクルタイム")
    Dim insTimeContent As String = String.Empty
    '初期化
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        '画面を最大で表示
        Me.Show()
        Me.WindowState = FormWindowState.Maximized

        'フォームサイズの変更を不可能にする
        Me.FormBorderStyle = FormBorderStyle.FixedSingle
        '二重起動防止
        If Diagnostics.Process.GetProcessesByName(Diagnostics.Process.GetCurrentProcess.ProcessName).Length > 1 Then
            MessageBox.Show("二重起動禁止です。" & ControlChars.Cr &
                            "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End If

        'システムの時刻取得
        Dim today As String = DateTime.Now.ToString("yyyy/MM/dd")
        If today.CompareTo(startUpDate) < 0 Then
            MessageBox.Show(String.Format("パソコンの日付が{0}になっています。" & ControlChars.Cr &
                                          "装置製造年月日{1}より過去になっています。" & ControlChars.Cr &
                                          "製造技術担当者に連絡してください。" & ControlChars.Cr &
                                          "ソフトを終了します。", today, startUpDate),
                                          "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End If




        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '通常エリアベリファイ用変数初期化
        normalAreaMemoryArr = New Byte(26)() {}
        For i As Integer = 0 To 26
            normalAreaMemoryArr(i) = New Byte(15) {}
        Next
        '制御エリアベリファイ用変数初期化
        controlAreaMemoryArr = New Byte(4)() {}
        For i As Integer = 0 To 4
            controlAreaMemoryArr(i) = New Byte(15) {}
        Next
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



        Me.Show()

        With BarcodeReader
            If Not .IsOpen() Then
                Try
                    .PortName = "COM4"
                    .BaudRate = 1152000
                    .Parity = IO.Ports.Parity.None
                    .DataBits = 8
                    .StopBits = IO.Ports.StopBits.One
                    .ReadBufferSize = 4096
                    .WriteBufferSize = 2048
                    .ReadTimeout = 200
                    .WriteTimeout = 200
                    .NewLine = vbCr
                    .DiscardNull = False
                    .ParityReplace = 0
                    .Open()
                Catch ex As Exception
                    MessageBox.Show(ex.Message & ControlChars.NewLine &
                                    "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                    "ソフトを終了します。", "確認事項",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error)
                    'Me.Close()
                End Try
            End If
        End With

        With PSoC
            If Not .IsOpen() Then
                Try
                    .PortName = "COM7"
                    .BaudRate = 38400                 '通信速度
                    .Parity = IO.Ports.Parity.None    'パリティ
                    .DataBits = 8                     'データ長
                    .StopBits = IO.Ports.StopBits.One 'ストップビット長
                    .ReadBufferSize = 100             '受信バッファサイズ(バイト単位)
                    .WriteBufferSize = 100            '送信バッファサイズ(バイト単位)
                    .ReadTimeout = 200                 '受信待ちタイムアウト(ms単位)
                    .WriteTimeout = 200                '送信完了タイムアウト(ms単位)
                    .Open()
                    .DiscardInBuffer()                '受信バッファ初期化
                    .DiscardOutBuffer()               '送信バッファ初期化
                Catch ex As Exception
                    MessageBox.Show(ex.Message & ControlChars.NewLine &
                                    "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                    "ソフトを終了します。", "確認事項",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error)
                    Me.Close()
                End Try
            End If
        End With

        'ラベル表示の初期化
        lblInstructionDisplay.Text = String.Empty
        lblMAC.Text = String.Empty
        lblModuleSerialNo.Text = String.Empty
        lblOKWork.Text = String.Empty
        lblOperatorID.Text = String.Empty
        lblOrder.Text = String.Empty
        lblPaper.Text = String.Empty
        lblPID.Text = String.Empty
        lblQuantity.Text = String.Empty
        lblSerialNo.Text = String.Empty
        lblWorkType.Text = String.Empty


        '検査プログラム番号の読込み
        Try
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}program_number.txt", PathSlnFile, pathProgramNumber), enc_sj)
                Me.Text = sr.ReadLine()
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            MessageBox.Show("検査プログラム番号が読み込ません" & ControlChars.CrLf &
                            "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                            "ソフトを終了します。", "確認事項",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error)
            Me.Close()
        End Try

        '検査年月日の読込み
        lblInspectionDate.Text = Date.Now.ToString()

        '検査回数の読み出し
        Try
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}counter.txt", PathSlnFile, pathInspectionCounter), enc_sj)
                lblInspectionCounter.Text = sr.ReadLine()
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            MessageBox.Show("検査回数が読み込めません" & ControlChars.CrLf &
                            "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                            "ソフトを終了します。", "確認事項",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error)
            Me.Close()
        End Try

        'ワークコネクタ、コネクタ変換基板、IOユニット抜入回数の読み出し
        Dim wcCount As String = String.Empty '回数表示ラベルを指定する
        Dim cvtCount As String = String.Empty '
        Dim iouCount As String = String.Empty '
        If Not loadCounter(PathSlnFile & pathWorkConnectorCounter, wcCount) Then
            MessageBox.Show("ワークコネクタ使用回数が読み込めません" & ControlChars.CrLf &
                            "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                            "ソフトを終了します。", "確認事項",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error)
            Me.Close()
        End If
        If Not loadCounter(PathSlnFile & pathConverterCounter, cvtCount) Then
            MessageBox.Show("コネクタ変換基板使用回数が読み込めません" & ControlChars.CrLf &
                            "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                            "ソフトを終了します。", "確認事項",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error)
            Me.Close()
        End If
        If Not loadCounter(PathSlnFile & pathIOUnitCounter, iouCount) Then
            MessageBox.Show("IOユニット使用回数が読み込めません" & ControlChars.CrLf &
                            "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                            "ソフトを終了します。", "確認事項",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error)
            Me.Close()
        End If
        lblWCCounter.Text = wcCount
        lblCvtCount.Text = cvtCount
        lblIOUCount.Text = iouCount

        'コネクタ使用限度回数の読込み
        'ワークコネクタ
        Try
            Using sr As IO.StreamReader = New IO.StreamReader(PathSlnFile & pathWorkConnectorCounterLimit, enc_sj)
                Dim tmpStr = sr.ReadLine()
                wcLimit = Convert.ToUInt64(tmpStr, 10)
            End Using

        Catch ex As Exception
            MessageBox.Show("ワークコネクタ使用限度回数が読み込めません" & ControlChars.CrLf &
                           "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                           "ソフトを終了します。", "確認事項",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error)
            Me.Close()
        End Try
        'コネクタ変換基板
        Try
            Using sr As IO.StreamReader = New IO.StreamReader(PathSlnFile & pathConverterCounterLimit, enc_sj)
                Dim tmpStr = sr.ReadLine()
                cvtLimit = Convert.ToUInt64(tmpStr, 10)
            End Using
        Catch ex As Exception
            MessageBox.Show("コネクタ変換基板使用限度回数が読み込めません" & ControlChars.CrLf &
                           "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                           "ソフトを終了します。", "確認事項",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error)
            Me.Close()
        End Try
        'IOユニット
        Try
            Using sr As IO.StreamReader = New IO.StreamReader(PathSlnFile & pathIOUnitCounterLimit, enc_sj)
                Dim tmpStr = sr.ReadLine()
                iouLimit = Convert.ToUInt64(tmpStr, 10)
            End Using
        Catch ex As Exception
            MessageBox.Show("IOユニット使用限度回数が読み込めません" & ControlChars.CrLf &
                           "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                           "ソフトを終了します。", "確認事項",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error)
            Me.Close()
        End Try

        'ControlPCとNASの存在を確認
        chkPcNas()


        'パソリの通信を切断、初期化
        Sna.NoWire.Nfc_disconnect()

        If Not readLEDChkSetting() Then
            MessageBox.Show("画像処理設定ファイルが読み込めません。" & ControlChars.CrLf &
                        "ソフトを終了します。", "確認事項",
                        MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End If
        main()

    End Sub


    Private Sub main()


        Dim bcReadStep As BarcodeReadStep = BarcodeReadStep.IDRead
        Dim bc As String = String.Empty 'バーコード文字列

        'IFBOX
        Dim bytDataTest As Byte = 0
        USBIO.Pin_Config(bytDataTest)
        USBIO.Pin_Upper(bytDataTest)
        USBIO.PLC_Byte(bytDataTest)

RESTART:
        '初期化
        Sna.NoWire.Nfc_disconnect()
        isReInspection = False
        isNgWorkReInspection = False
        hasSerialNo = False
        isDiv5Product = False
        btnManuLEDChk.Enabled = True 'LED目視検査モードボタン使用の初期化

        insTimeContent = String.Empty '検証、時間計測用

        'PLC,HUB,EX260に電源を入れる
        If Not USBIO.AC_Relay(1) Then
            PSoCError()
        End If

        If Not USBIO.Hub_Pow(1) Then
            PSoCError()
        End If

        If Not USBIO.EX260_Pow(1) Then
            PSoCError()
        End If

        Do 'メインループ

            bc = String.Empty '初期化
            barcodeStr = String.Empty

            'マニュアルモード制限（バーコードを読む前のみ使用許可）
            If bcReadStep = BarcodeReadStep.IDRead Then
                MenuStrip1.Enabled = True
            Else
                MenuStrip1.Enabled = False
            End If
            'バーコード読込み工程ごとのメッセージ
            Select Case bcReadStep
                Case BarcodeReadStep.IDRead
                    lblInstructionDisplay.Text = "社員番号バーコードを読み込んでください。"
                Case BarcodeReadStep.BC1Read
                    lblInstructionDisplay.Text = "バーコード①を読み込んでください。" & ControlChars.NewLine & "社員番号の変更が可能です。"
                Case BarcodeReadStep.BC2Read
                    lblInstructionDisplay.Text = "バーコード②を読み込んでください" & ControlChars.NewLine & "社員番号の変更が可能です。"
                Case BarcodeReadStep.PID_MACRead
                    lblInstructionDisplay.Text = "製品銘板バーコードを読み込んでください。" & ControlChars.NewLine & "社員番号の変更が可能です。"
                Case BarcodeReadStep.SetupDone
                    lblInstructionDisplay.Text = "検査を行うワークをセットし、カバーを閉じてください。" & ControlChars.NewLine &
                                                ControlChars.NewLine &
                                                 "検査を開始する場合はスタートSWを押してください。" & ControlChars.NewLine &
                                                 "再度製品銘板を読み込む場合にはブレークSWを押してください。" & ControlChars.NewLine &
                                                 "LED検査を目視で行う場合は[LED目視検査モード]ボタンを押してください。"
            End Select

            'バーコード読込み待ち
            Do While barcodeStr.Length = 0

                If bcReadStep = BarcodeReadStep.SetupDone Then
                    Dim bytData As Byte 'スイッチ状態のデータ格納
                    Dim isWorksetOnce As Boolean = False 'ワークが一度セットされたか、セットされてからワーク検知OFFの場合にはポップアップ
                    Do

                        'ワーククランプ前にカバーを閉じた場合にはポップアップ
                        'クランプ検知がないため、ワークセンサで代用
                        If Not USBIO.Reserve0_In(bytData) Then
                            PSoCError()
                        End If
                        If bytData = 0 Then
                            Dim bytWorkSensData As Byte

                            If Not USBIO.Work_Sens(bytWorkSensData) Then
                                PSoCError()
                            End If

                            If bytWorkSensData <> 0 Then
                                MessageBox.Show("ワークをクランプしてからカバーを閉じてください。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Continue Do
                            End If

                        End If

                        '--------------------------------------------------
                        'ブレークSW->データマトリクスの読み直し
                        '--------------------------------------------------
                        If Not USBIO.Break_SW(bytData) Then
                            PSoCError()
                        End If

                        If bytData = 0 Then

                            Dim serialNoStr As String = loadSerialNo(Params.WriteSerial(1), Params.WriteSerial(2), Params.WriteSerial(3))
                            If serialNoStr Is Nothing Then
                                MessageBox.Show("シリアル番号ファイルが読み込めません。" & ControlChars.CrLf &
                                "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                "ソフトを終了します。", "確認事項",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error)
                                Me.Close()
                            End If

                            Sna.NoWire.Nfc_disconnect()

                            lblSerialNo.Text = serialNoStr

                            bcReadStep = BarcodeReadStep.PID_MACRead
                            lblPID.Text = String.Empty
                            lblMAC.Text = String.Empty
                            GoTo RESTART
                        End If


                        '--------------------------------------------------
                        'スタートSW->検査開始
                        '--------------------------------------------------
                        If Not USBIO.Start_SW(bytData) Then
                            PSoCError()
                        End If

                        If bytData = 0 Then

                            Dim bytWorkSensData As Byte
                            Dim bytReserveIn0Data As Byte
                            'ワーク検知
                            If Not USBIO.Work_Sens(bytWorkSensData) Then
                                PSoCError()
                            End If
                            'アクリルカバー開閉検知
                            If Not USBIO.Reserve0_In(bytReserveIn0Data) Then
                                PSoCError()
                            End If
                            'ワークがセット済みand(アクリルカバーが閉じているorLED目視検査モード)場合
                            If bytWorkSensData = 0 AndAlso (bytReserveIn0Data = 0 OrElse isManuLEDChkMode) Then
                                '検査中にLED目視検査モードの変更はさせない
                                btnManuLEDChk.Enabled = False
                                Exit Do
                            ElseIf bytWorkSensData <> 0 Then
                                MessageBox.Show("ワークがセットされていません" & ControlChars.NewLine &
                                                "ワークをセットしてください。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                GoTo RESTART
                            ElseIf bytReserveIn0Data <> 0 Then
                                MessageBox.Show("アクリルカバーが閉じられていません" & ControlChars.NewLine &
                                                "カバーを閉じてください。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                GoTo RESTART
                            End If

                        End If


                        '一度ワークをセットした後、ワーク検知がオフになったらメッセージ
                        '検査し忘れ防止
                        If Not USBIO.Work_Sens(bytData) Then
                            PSoCError()
                        End If

                        If bytData = 0 Then
                            isWorksetOnce = True
                        Else
                            If isWorksetOnce Then
                                MessageBox.Show("検査を開始せずにワークを取り外しました。",
                                               "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                        End If



                        My.Application.DoEvents()
                    Loop

                    '検査スタート(=検査開始時刻)

                    Dim inspectionStartTime As Date = Date.Now
                    Dim cycleTimeSW As Stopwatch = New Stopwatch()

                    cycleTimeSW.Start()
                    lblInspectionStartTime.Text = inspectionStartTime.ToString("yy/MM/dd HH:mm:ss")


                    '日常点検状態のチェック
                    '適合サンプル
                    If doneDailyChk2(Params.DailyChk(2)) Then
                        lblOkSampleChk.BackColor = Color.Lime
                    Else
                        lblOkSampleChk.BackColor = Color.Red
                    End If

                    '不適合サンプル
                    If doneDailyChk2(Params.DailyChk(3)) Then
                        lblNgSampleChk.BackColor = Color.Lime
                    Else
                        lblNgSampleChk.BackColor = Color.Red
                    End If

                    '残数チェック
                    If Integer.Parse(lblQuantity.Text) <= 0 Then
                        '残数が0の際には検査させずバーコード①読込みへ戻る'仕様変更2017/7/12

                        Dim tmpAns As DialogResult = MessageBox.Show("生産が完了している帳票No.です。" & ControlChars.Cr &
                                                                "バーコード①読込みへ戻ります。",
                                                                "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        lblWorkType.Text = String.Empty
                        lblOKWork.Text = String.Empty
                        lblOrder.Text = String.Empty
                        lblQuantity.Text = String.Empty
                        lblPaper.Text = String.Empty
                        lblMAC.Text = String.Empty
                        lblPID.Text = String.Empty
                        lblSerialNo.Text = String.Empty
                        bcReadStep = BarcodeReadStep.BC1Read
                        GoTo RESTART


                        'Dim tmpAns As DialogResult = MessageBox.Show("残数が0ですがよろしいですか？" & ControlChars.Cr &
                        '                                        "[はい]を押すとパスワード入力後、検査を続行します(残数は記録されません)。" & ControlChars.Cr &
                        '                                        "[いいえ]を押すとバーコード①読込みへ戻ります。",
                        '                                        "確認事項", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
                        'If tmpAns = DialogResult.Yes Then
                        '    'パスワードボックスの表示
                        '    PasswordInput.Show()
                        '    Dim result As Boolean = False

                        '    Do
                        '        If Not PasswordInput.Visible Then
                        '            result = PasswordInput.getPassState()
                        '            'MessageBox.Show(result.ToString())
                        '            Exit Do
                        '        End If
                        '        My.Application.DoEvents()
                        '    Loop
                        '    'パスワード入力失敗or入力しない場合
                        '    If Not result Then
                        '        lblWorkType.Text = String.Empty
                        '        lblOKWork.Text = String.Empty
                        '        lblOrder.Text = String.Empty
                        '        lblQuantity.Text = String.Empty
                        '        lblPaper.Text = String.Empty
                        '        lblMAC.Text = String.Empty
                        '        lblPID.Text = String.Empty
                        '        lblSerialNo.Text = String.Empty
                        '        bcReadStep = BarcodeReadStep.BC1Read
                        '        GoTo RESTART
                        '    End If

                        'End If


                        'If tmpAns = DialogResult.No Then
                        '    lblWorkType.Text = String.Empty
                        '    lblOKWork.Text = String.Empty
                        '    lblOrder.Text = String.Empty
                        '    lblQuantity.Text = String.Empty
                        '    lblPaper.Text = String.Empty
                        '    lblMAC.Text = String.Empty
                        '    lblPID.Text = String.Empty
                        '    lblSerialNo.Text = String.Empty
                        '    bcReadStep = BarcodeReadStep.BC1Read
                        '    GoTo RESTART
                        'End If

                    End If

                    '------------------------------------------------------------------------------------------
                    '注意事項の更新
                    '------------------------------------------------------------------------------------------

                    '日常点検
                    If lblOkSampleChk.BackColor <> Color.Lime OrElse lblNgSampleChk.BackColor <> Color.Lime Then
                        lblWarningComment.Text = "日常点検が完了していません。"
                    Else
                        lblWarningComment.Text = String.Empty
                    End If

                    'シリアル番号
                    If lblSerialNo.Text Like "**FFFFFF" Then
                        lblWarningComment.Text &= "シリアル番号の残りがありません。"
                        MessageBox.Show("シリアル番号が" & lblSerialNo.Text & "です。" & ControlChars.CrLf &
                                        "開発5部に連絡してください。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.Close()
                    ElseIf lblSerialNo.Text Like "**FFFF**" Then
                        lblWarningComment.Text &= "シリアル番号が残りわずかです。"
                    End If

                    If lblWarningComment.Text <> String.Empty Then
                        lblWarningComment.BackColor = Color.Yellow
                    Else
                        lblWarningComment.BackColor = Color.White
                    End If

                    '検査回数のインクリメント
                    plusInspectionCount()



                    'ワークコネクタ、コネクタ変換基板、IOユニット抜入回数のインクリメント、表示の更新
                    'ワークコネクタの挿抜はマスタユニット検査時のみ
                    If Params.ProductType(4) = "1" Then
                        If Not incrementCounter(PathSlnFile & pathWorkConnectorCounter, lblWCCounter) Then
                            MessageBox.Show("ワークコネクタ使用回数の更新ができません。" & ControlChars.CrLf &
                                "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                "ソフトを終了します。", "確認事項",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error)
                            Me.Close()
                        End If
                    End If

                    If Not incrementCounter(PathSlnFile & pathConverterCounter, lblCvtCount) Then
                        MessageBox.Show("コネクタ変換基板使用回数の更新ができません。" & ControlChars.CrLf &
                                "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                "ソフトを終了します。", "確認事項",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error)
                        Me.Close()
                    End If
                    If Not incrementCounter(PathSlnFile & pathIOUnitCounter, lblIOUCount) Then
                        MessageBox.Show("IOユニット使用回数の更新ができません。" & ControlChars.CrLf &
                                "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                "ソフトを終了します。", "確認事項",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error)
                        Me.Close()
                    End If

                    '---------------------------------------------------------------------------------------
                    'ワークコネクタ、コネクタ変換基板、IOユニット抜入回数が上限の場合にメッセージ表示
                    'リセットする場合は製造技術者に連絡
                    '---------------------------------------------------------------------------------------
                    'ラベル表示のコネクタ使用回数を数値変換
                    Dim currentWCC As ULong = 0
                    Dim currentCvtC As ULong = 0
                    Dim currentIOUC As ULong = 0
                    Try
                        currentWCC = Convert.ToUInt64(lblWCCounter.Text)
                        currentCvtC = Convert.ToUInt64(lblCvtCount.Text)
                        currentIOUC = Convert.ToUInt64(lblIOUCount.Text)
                    Catch ex As Exception
                        MessageBox.Show("コネクタ使用回数の更新ができません。" & ControlChars.CrLf &
                               "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                               "ソフトを終了します。", "確認事項",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Error)
                        Me.Close()
                    End Try

                    Dim isCountOver As Boolean = False
                    Dim targetConnectors As String = String.Empty '交換対象コネクタ
                    If currentWCC > wcLimit Then
                        targetConnectors &= "ワークコネクタ/"
                        isCountOver = True
                    End If
                    If currentCvtC > cvtLimit Then
                        targetConnectors &= "コネクタ変換基板/"
                        isCountOver = True
                    End If
                    If currentIOUC > iouLimit Then
                        targetConnectors &= "IOユニット/"
                        isCountOver = True
                    End If

                    If isCountOver Then
                        '最後の"/"を取り除く
                        lblCCounterComment.Text = targetConnectors.Substring(0, targetConnectors.Length - 1) & " の使用回数を超えています。" & ControlChars.CrLf &
                                                 "該当品の交換、カウンタリセットを製造技術担当者に連絡してください。"
                        lblCCounterComment.BackColor = Color.Red
                    Else
                        lblCCounterComment.BackColor = Color.White
                    End If



                    '帳票の新規作成
                    Try
                            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}{2}.txt", PathSlnFile, pathPaperFolder, lblPaper.Text), False, enc_sj)
                                '帳票フォーマット
                                '時刻,製品品番,受注数,適合品数,残数
                                sw.WriteLine(String.Format("{0},{1},{2},{3},{4}",
                                                       inspectionStartTime.ToString(),
                                                       lblWorkType.Text,
                                                       lblOrder.Text,
                                                       lblOKWork.Text,
                                                       lblQuantity.Text))
                            End Using
                        Catch ex As Exception
                            writeErrorLog(ex)
                            MessageBox.Show("帳票ファイルの作成ができません。" & ControlChars.NewLine &
                                        "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                        "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Me.Close()
                        End Try





                        '-----------------------------------------------------------
                        '
                        '検査スタート
                        '
                        '-----------------------------------------------------------

                        '不適合工程名の初期化
                        errorProcessName = String.Empty
                        'エラー工程メッセージの初期化
                        errorProcessContent = String.Empty

                        '検査前にパソリの通信状態を初期化
                        Sna.NoWire.Nfc_disconnect()

                        '--------------------------------------------
                        '検査前チェック
                        '--------------------------------------------
                        'ピン治具上部判定
                        If Not USBIO.Pin_Upper(bytData) Then
                            PSoCError()
                        End If

                        If bytData <> Convert.ToByte(Params.PinUpperJudge(1), 16) Then
                            MessageBox.Show("使用するピン治具が異なります。" & ControlChars.CrLf &
                                    "プログラムを終了します。。" & ControlChars.CrLf &
                                    "検査機種：" & lblWorkType.Text & ControlChars.CrLf &
                                    "登録機種:" & readJigSetting("pin_upper", Params.PinUpperJudge(1)), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Me.Close()
                        End If
                        'ピン変換基板判定
                        If Not USBIO.Pin_Config(bytData) Then
                            PSoCError()
                        End If

                        If bytData <> Convert.ToByte(Params.PinBoardJudge(1), 16) Then
                            MessageBox.Show("使用するピン変換基板が異なります。" & ControlChars.CrLf &
                                    "プログラムを終了します。。" & ControlChars.CrLf &
                                    "検査機種：" & lblWorkType.Text & ControlChars.CrLf &
                                    "登録機種:" & readJigSetting("connecter", Params.PinBoardJudge(1)), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Me.Close()
                        End If

                        'PLC判別
                        If Not USBIO.PLC_Byte(bytData) Then
                            PSoCError()
                        End If

                        If bytData <> Convert.ToByte(Params.PLCJudge(1), 16) Then
                            MessageBox.Show("使用するPLCが異なります。" & ControlChars.CrLf &
                                    "プログラムを終了します。。" & ControlChars.CrLf &
                                    "検査機種：" & lblWorkType.Text & ControlChars.CrLf &
                                    "登録機種:" & readJigSetting("plc", Params.PLCJudge(1)), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Me.Close()
                        End If


                        '------------------------------------------
                        '           検査関数の呼び出し
                        '       isJudge:検査結果総合判定用
                        '------------------------------------------
                        Dim isJudge As Boolean = If(inspection(isReInspection), True, False)


                        '製品ワークの電源をOFFにする
                        If Not USBIO.EX600_Pow(0) Then
                            PSoCError()
                        End If

                        'NFC通信を切断
                        Sna.NoWire.Nfc_disconnect()


                        '検査前チェック（PID,MACチェックでエラーの場合にはID銘板読込みへ戻る）
                        If Not isJudge Then
                            If errorProcessName.IndexOf("検査前チェック") >= 0 Then

                                lblMAC.Text = String.Empty
                                lblPID.Text = String.Empty
                                lblModuleSerialNo.Text = String.Empty
                                lblInspectionStartTime.Text = String.Empty

                                bcReadStep = BarcodeReadStep.PID_MACRead
                                GoTo RESTART
                            End If
                        End If



                        '検査時間の反映
                        cycleTimeSW.Stop()
                        lblCycleTimeMonitor.Text = cycleTimeSW.ElapsedMilliseconds.ToString("000000")
                        '検証用
                        Try

                            insTimeContent &= "," & cycleTimeSW.ElapsedMilliseconds.ToString("0.0")

                            If Not IO.File.Exists(String.Format("{0}\inspection_time.txt", PathSlnFile)) Then
                                Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}\inspection_time.txt", PathSlnFile), False, enc_sj)
                                    sw.WriteLine(insTimeItem)
                                End Using
                            End If

                            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}\inspection_time.txt", PathSlnFile), True, enc_sj)
                                sw.WriteLine(insTimeContent)
                            End Using
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try

                        '日常点検ワークかどうかで場合わけ（残数と点検結果更新で処理が異なる）

                        '日常点検の結果を更新
                        If Params.DailyChk(1) = "2" Then
                        Dim expectedResult As Boolean = If(Params.DailyChk(4) = "1", True, False) '品番リスト変更によりindex:3->4(2017/7/10)
                        If isJudge <> expectedResult Then
                            '日常点検結果不一致(OKサンプル->NG判定, NGサンプル->OK判定)
                            updateDailyCheck2(lblWorkType.Text, Params.DailyChk(4), isJudge)


                                MessageBox.Show("日常点検ワーク異常です。" & ControlChars.NewLine &
                                            "整合技術担当者に連絡してください。" & ControlChars.NewLine &
                                            "点検ワーク：" & If(expectedResult, "適合ワーク", "不適合ワーク") & ControlChars.NewLine &
                                            "検査結果：" & If(isJudge, "適合", "不適合"), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Else
                            '検査結果と想定するサンプルの結果が一致->サンプル確認完了フラグをセット
                            updateDailyCheck2(lblWorkType.Text, Params.DailyChk(4), isJudge)
                        End If
                        End If

                        '日常点検結果を反映
                        '適合サンプル
                        If doneDailyChk2(Params.DailyChk(2)) Then
                            lblOkSampleChk.BackColor = Color.Lime
                        Else
                            lblOkSampleChk.BackColor = Color.Red
                        End If

                        '不適合サンプル
                        If doneDailyChk2(Params.DailyChk(3)) Then
                            lblNgSampleChk.BackColor = Color.Lime
                        Else
                            lblNgSampleChk.BackColor = Color.Red
                        End If
                        Me.Refresh()
                        '------------------------------------------------------------------------------------------
                        '注意事項の更新
                        '------------------------------------------------------------------------------------------
                        '日常点検
                        If lblOkSampleChk.BackColor <> Color.Lime OrElse lblNgSampleChk.BackColor <> Color.Lime Then
                            lblWarningComment.Text = "日常点検が完了していません。"
                        Else
                            lblWarningComment.Text = String.Empty
                        End If

                        'シリアル番号
                        If lblSerialNo.Text Like "**FFFF**" Then
                            lblWarningComment.Text &= "シリアル番号が残りわずかです。"
                        End If
                        If lblWarningComment.Text <> String.Empty Then
                            lblWarningComment.BackColor = Color.Yellow
                        Else
                            lblWarningComment.BackColor = Color.White
                        End If

                        '日常点検ワークなら適合数,残数はそのまま
                        '日常点検ワーク検査時以外(通常ワーク)
                        If Params.DailyChk(1) <> "2" Then
                            '検査合格
                            If isJudge Then
                                lblOKWork.Text = (Integer.Parse(lblOKWork.Text) + 1).ToString() '適合数更新
                                '再検査でないまたはNG品再検査で合格なら残数-1(再検査の場合は残数変化なし)
                                '仕様：再検査不適合、適合を繰り返すと残数が減る
                                If Not isReInspection OrElse isNgWorkReInspection Then
                                    lblQuantity.Text = (Integer.Parse(lblQuantity.Text) - 1).ToString()
                                    If Integer.Parse(lblQuantity.Text) <= 0 Then
                                        lblQuantity.Text = "0"
                                    End If
                                End If
                            End If
                        End If


                        '帳票ファイルの更新など
                        Try
                            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}{2}.txt", PathSlnFile, pathPaperFolder, lblPaper.Text), False, enc_sj)
                                sw.WriteLine(String.Format("{0},{1},{2},{3},{4}",
                                                       inspectionStartTime,
                                                       lblWorkType.Text,
                                                       lblOrder.Text,
                                                       lblOKWork.Text,
                                                       lblQuantity.Text))
                            End Using
                        Catch ex As Exception
                            writeErrorLog(ex)
                            MessageBox.Show("帳票ファイルの書き込みができません。")
                            Me.Close()
                        End Try

                        '管理PCとNASの存在を確認
                        chkPcNas()


                        '検査履歴ファイル(再検査時は追加書込み)
                        '再検査品(isReInspection=true)ならapppendモードで書き込み
                        Try
                            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}${2}.txt",
                                                                                    pathHistory,
                                                                                    lblMAC.Text,
                                                                                    lblPID.Text),
                                                                                    isReInspection, enc_sj)

                                '初回書込みの時には項目名を入れる
                                If Not isReInspection Then
                                    sw.WriteLine(historyItem)
                                End If

                                sw.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}," &
                                          "{10},{11},{12},{13},{14},{15},{16},{17},{18},{19}," &
                                          "{20},{21},{22},{23},{24},{25},{26},{27},{28},{29}," &
                                          "{30},{31},{32},{33}",
                                          inspectionStartTime,
                                          "無線ユニット検査",'固定値
                                          lblOperatorID.Text,
                                          lblWorkType.Text,
                                          If(lblOkSampleChk.BackColor = Color.Lime AndAlso lblNgSampleChk.BackColor = Color.Lime, "日常点検完了", "日常点検未完了"),
                                          lblPaper.Text,
                                          lblQuantity.Text,
                                          If(isJudge, "適合", "不適合"),
                                          If(isJudge, String.Empty, errorProcessName & "_" & errorProcessContent),'不適合工程名_エラー内容
                                          lblCycleTimeMonitor.Text,
                                          lblMAC.Text,
                                          If(lblInsResult(13).BackColor = Color.Lime OrElse (isReInspection OrElse isDiv5Product), lblSerialNo.Text, String.Empty),'シリアルNo書込み合格なら記録,再検査品・開発出荷品は必ずシリアル番号を記録する
                                          lblPID.Text,
                                          If(isJudge, Params.FormwareVersion(1), String.Empty),'全て検査に通っているなら品番リストのVer
                                          lblInsResult(7).Text,
                                          lblInsResult(8).Text,
                                          lblInsResult(9).Text,
                                          lblModuleSerialNo.Text,
                                          lblInsResult(0).Text,
                                          lblInsResult(1).Text,
                                          lblInsResult(2).Text,
                                          lblInsResult(3).Text,
                                          lblInsResult(4).Text,
                                          lblInsResult(5).Text,
                                          lblInsResult(6).Text,
                                          If(lblInsResult(7).BackColor = Color.Lime AndAlso lblInsResult(8).BackColor = Color.Lime AndAlso lblInsResult(9).BackColor = Color.Lime,
                                          "合格",
                                          If(lblInsResult(7).BackColor = Color.Red OrElse lblInsResult(8).BackColor = Color.Red OrElse lblInsResult(9).BackColor = Color.Red, "不合格", String.Empty)),
                                          lblInsResult(10).Text,
                                          lblInsResult(11).Text,
                                          lblInsResult(12).Text,
                                          lblInsResult(13).Text,
                                          lblInsResult(14).Text,
                                          lblInsResult(15).Text,
                                          lblInsResult(16).Text,
                                          isDiv5Product)
                            End Using
                        Catch ex As Exception
                            writeErrorLog(ex)
                            MessageBox.Show("検査履歴ファイルの書き込みができません。" & ControlChars.NewLine &
                                    "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                    "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Me.Close()
                        End Try

                        '履歴ファイルをNASへ上書きコピーする
                        Try
                            IO.File.Copy(String.Format("{0}{1}${2}.txt", pathHistory, lblMAC.Text, lblPID.Text),
                                 String.Format("{0}{1}${2}.txt", pathHistoryNAS, lblMAC.Text, lblPID.Text), True)
                        Catch ex As Exception
                            writeErrorLog(ex)
                            MessageBox.Show("NASにデータの転送ができません。" & ControlChars.NewLine &
                                    "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                    "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Me.Close()
                        End Try

                        '一括検査履歴を保存する(追加書込み)
                        '不合格工程などのチェック用
                        Try
                            '一括履歴ファイルが存在しているかチェック->なければ項目を最初の行へ挿入
                            Dim hasHistory As Boolean = True

                            If IO.File.Exists(String.Format("{0}製品検査一括履歴(div5).txt", pathHistory)) Then
                                hasHistory = True
                            Else
                                hasHistory = False
                            End If

                            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}製品検査一括履歴(div5).txt", pathHistory), True, enc_sj)

                                '初回書込みの時には項目名を入れる
                                If Not hasHistory Then
                                    sw.WriteLine(historyItem)
                                End If

                                sw.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}," &
                                      "{10},{11},{12},{13},{14},{15},{16},{17},{18},{19}," &
                                      "{20},{21},{22},{23},{24},{25},{26},{27},{28},{29}," &
                                      "{30},{31},{32},{33}",
                                      inspectionStartTime,
                                      "無線ユニット検査",'固定値
                                      lblOperatorID.Text,
                                      lblWorkType.Text,
                                      If(lblOkSampleChk.BackColor = Color.Lime AndAlso lblNgSampleChk.BackColor = Color.Lime, "日常点検完了", "日常点検未完了"),
                                      lblPaper.Text,
                                      lblQuantity.Text,
                                      If(isJudge, "適合", "不適合"),
                                      If(isJudge, String.Empty, errorProcessName & "_" & errorProcessContent),'不適合工程名_エラー内容
                                      lblCycleTimeMonitor.Text,
                                      lblMAC.Text,
                                      If(lblInsResult(13).BackColor = Color.Lime OrElse (isReInspection OrElse isDiv5Product), lblSerialNo.Text, String.Empty),'シリアルNo書込み合格なら記録
                                      lblPID.Text,
                                      If(isJudge, Params.FormwareVersion(1), String.Empty),'全て検査に通っているなら品番リストのVer,todo:変更余地あり
                                      lblInsResult(7).Text,
                                      lblInsResult(8).Text,
                                      lblInsResult(9).Text,
                                      lblModuleSerialNo.Text,
                                      lblInsResult(0).Text,
                                      lblInsResult(1).Text,
                                      lblInsResult(2).Text,
                                      lblInsResult(3).Text,
                                      lblInsResult(4).Text,
                                      lblInsResult(5).Text,
                                      lblInsResult(6).Text,
                                      If(lblInsResult(7).BackColor = Color.Lime AndAlso lblInsResult(8).BackColor = Color.Lime AndAlso lblInsResult(9).BackColor = Color.Lime,
                                     "合格",
                                     If(lblInsResult(7).BackColor = Color.Red OrElse lblInsResult(8).BackColor = Color.Red OrElse lblInsResult(9).BackColor = Color.Red, "不合格", String.Empty)),
                                      lblInsResult(10).Text,
                                      lblInsResult(11).Text,
                                      lblInsResult(12).Text,
                                      lblInsResult(13).Text,
                                      lblInsResult(14).Text,
                                      lblInsResult(15).Text,
                                      lblInsResult(16).Text,
                                      isDiv5Product)
                            End Using
                        Catch ex As Exception
                            writeErrorLog(ex)
                            MessageBox.Show("検査履歴ファイルの書き込みができません。" & ControlChars.NewLine &
                                    "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                    "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Me.Close()
                        End Try
                        'NASに作成した一括履歴ファイルをコピーする
                        Try
                            IO.File.Copy(String.Format("{0}製品検査一括履歴(div5).txt", pathHistory),
                                 String.Format("{0}製品検査一括履歴(div5).txt", pathHistoryNAS), True)
                        Catch ex As Exception
                            writeErrorLog(ex)
                            MessageBox.Show("NASにデータの転送ができません。" & ControlChars.NewLine &
                                    "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                    "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Me.Close()
                        End Try
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        '日常点検の場合には通常の日常点検履歴フォルダにもファイルをコピーする(日常点検履歴の参照をしやすくするため)
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        If Params.DailyChk(1) <> "2" Then
                            Try
                                IO.File.Copy(String.Format("{0}{1}${2}.txt", pathHistory, lblMAC.Text, lblPID.Text),
                                     String.Format("{0}日常点検履歴\{1}${2}.txt", pathHistory, lblMAC.Text, lblPID.Text), True)

                                IO.File.Copy(String.Format("{0}{1}${2}.txt", pathHistory, lblMAC.Text, lblPID.Text),
                                     String.Format("{0}日常点検履歴\{1}${2}.txt", pathHistoryNAS, lblMAC.Text, lblPID.Text), True)

                            Catch ex As Exception
                                writeErrorLog(ex)
                                MessageBox.Show("日常点検履歴データの転送ができません。" & ControlChars.NewLine &
                                        "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                        "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                Me.Close()
                            End Try
                        End If


                    '履歴保存終了後に表示ラベルのシリアル番号を更新する

                    lblSerialNo.Text = loadSerialNo(Params.WriteSerial(1), Params.WriteSerial(2), Params.WriteSerial(3))

                        cycleTimeSW.Stop()
                        lblCycleTimeMonitor.Text = cycleTimeSW.ElapsedMilliseconds.ToString("") & "ミリ秒"
                        cycleTimeSW.Reset()

                        '検査結果の表示
                        If isJudge Then
                            lblJudge.Text = "適合"
                            lblJudge.BackColor = Color.Lime
                            OKBuzzer()
                        Else
                            lblJudge.Text = "不適合"
                            lblJudge.BackColor = Color.Red
                            NGBuzzer()
                        End If


                        lblInstructionDisplay.Text = "検査" & lblJudge.Text & "です。" & "ワークを取り外してください。" & ControlChars.NewLine &
                                                 "ワークを取り外すと、判定欄がリセットされます。"
                        'ワーク検知がOFFになったら戻る
                        Do
                            Dim bytWorkSens As Byte = 0
                            If Not USBIO.Work_Sens(bytWorkSens) Then
                                PSoCError()
                            End If
                            If bytWorkSens = 1 Then
                                Exit Do
                            End If
                            My.Application.DoEvents()
                        Loop



                        '量産ワークの場合はID銘版読み込みに戻り次のワークの検査へ
                        bcReadStep = BarcodeReadStep.PID_MACRead
                        '[検査結果のクリア]
                        lblJudge.Text = String.Empty
                        lblJudge.BackColor = Color.White
                        lblCycleTimeMonitor.Text = String.Empty
                        lblInspectionStartTime.Text = String.Empty

                        '[製品情報ラベルをクリアする]
                        lblModuleSerialNo.Text = String.Empty
                        'lblSerialNo.Text = String.Empty'シリアル番号はクリアしない
                        lblMAC.Text = String.Empty
                        lblPID.Text = String.Empty
                        '[検査工程毎の結果のクリア]
                        For i As Integer = 0 To 16
                            lblInsResult(i).Text = String.Empty
                            lblInsResult(i).BackColor = Color.White
                        Next

                        '日常点検ワーク
                        If Params.DailyChk(1) = "2" Then
                            '日常点検後はバーコード①読込みに戻る
                            bcReadStep = BarcodeReadStep.BC1Read
                            '品番情報などのクリア
                            lblWorkType.Text = String.Empty
                            lblQuantity.Text = String.Empty
                            lblOKWork.Text = String.Empty
                            lblOrder.Text = String.Empty
                            lblPaper.Text = String.Empty
                        End If

                        GoTo RESTART
                        My.Application.DoEvents()

                    End If

                    My.Application.DoEvents()
            Loop

            'バーコード文字列が割り込みで変化すると困るので別変数に代入しておく
            bc = barcodeStr.Substring(0, barcodeStr.Length - 1) '<Cr>の除去

            '作業者ID(a######a)はいつでも読み込みOK
            If System.Text.RegularExpressions.Regex.IsMatch(bc, REG_BARCODE_PATTERN_ID) Then
                lblOperatorID.Text = bc.Substring(1, bc.Length - 2)
            End If

            Select Case bcReadStep
                Case BarcodeReadStep.IDRead
                    '作業者IDが入力されていなければ次のステップへは進めない
                    If lblOperatorID.Text Like "######" Then
                        bcReadStep = BarcodeReadStep.BC1Read
                    Else
                        Continue Do
                    End If

                Case BarcodeReadStep.BC1Read
                    '受注数$製品品番バーコードを読む
                    If Not System.Text.RegularExpressions.Regex.IsMatch(bc, REG_BARCODE_PATTERN_BC1) Then
                        Continue Do
                    End If

                    '品番リストファイルが見つからない、パラメータが読み込めないなど
                    If Not dealBC1(bc) Then
                        Continue Do
                    End If
                    bcReadStep = BarcodeReadStep.BC2Read

                Case BarcodeReadStep.BC2Read
                    '【バーコード①】受注数＄製品品番バーコードを読む
                    If System.Text.RegularExpressions.Regex.IsMatch(bc, REG_BARCODE_PATTERN_BC1) Then

                        '品番変更
                        If Not dealBC1(bc) Then
                            'MessageBox.Show("Error: dealBC1_bc2step")
                            'Me.Close()
                            Continue Do
                        End If

                        bcReadStep = BarcodeReadStep.BC2Read
                    End If

                    '【バーコード②】帳票Noを読む
                    If System.Text.RegularExpressions.Regex.IsMatch(bc, REG_BARCODE_PATTERN_BC2) Then

                        Dim paperStr As String()
                        bc = bc.Substring(1, bc.Length - 2)

                        '帳票が存在しない場合は仮の値を入れ次のステップへ進む。帳票は後で作成する
                        If Not IO.File.Exists(String.Format("{0}{1}{2}.txt", PathSlnFile, pathPaperFolder, bc)) Then
                            lblPaper.Text = bc
                            lblOKWork.Text = "0"
                            lblQuantity.Text = lblOrder.Text
                            bcReadStep = BarcodeReadStep.PID_MACRead
                            Continue Do
                        End If

                        '帳票チェック
                        '2年経過した古い帳票は上書きする
                        '帳票が存在する場合には登録品番と検査品番の一致を確認
                        '帳票パラメータ読込み
                        Try
                            Using sr As New IO.StreamReader(String.Format("{0}{1}{2}.txt", PathSlnFile, pathPaperFolder, bc), enc_sj)
                                paperStr = sr.ReadLine().Split(","c)
                                '帳票Noのフォーマット
                                '日付,品番,受注数,適合数,残数
                            End Using
                        Catch ex As Exception
                            writeErrorLog(ex)
                            MessageBox.Show("古い帳票の更新ができません。" & ControlChars.NewLine &
                                            "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                            "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Me.Close()
                        End Try


                        '2年経過チェック
                        '古い帳票は上書きして[新しい]帳票を作成(日付,品番,受注数(=残数),適合品数(=0),残数)
                        If Date.Now.Date > Date.Parse(paperStr(0)).AddYears(2).Date Then
                            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}{2}.txt", PathSlnFile, pathPaperFolder, bc), False, enc_sj)
                                sw.WriteLine(String.Format("{0},{1},{2},{3},{4}", Now.Date, lblWorkType.Text, lblQuantity.Text, "0", lblQuantity.Text))
                            End Using
                            '新しい帳票の値を読込みラベルを更新
                            Using sr As New IO.StreamReader(String.Format("{0}{1}{2}.txt", PathSlnFile, pathPaperFolder, bc), enc_sj)
                                paperStr = sr.ReadLine().Split(","c)
                            End Using
                        End If


                        '登録品番と検査品番の一致チェック
                        If paperStr(1).Trim() <> lblWorkType.Text Then
                            MessageBox.Show("読み込んだ帳票Noがすでに別の機種で登録されています。" & ControlChars.NewLine &
                                            "製造技術担当者に連絡して帳票Noの記録を削除してください。" & ControlChars.NewLine &
                                            "ソフトを終了します。" & ControlChars.NewLine &
                                            "読み込んだ品番:" & lblWorkType.Text & ControlChars.NewLine &
                                            "登録されている品番" & paperStr(1), "確認事項", MessageBoxButtons.OK)
                            Me.Close()
                        End If


                        'ラベルに表示
                        lblPaper.Text = bc
                        lblOrder.Text = paperStr(2)
                        lblOKWork.Text = paperStr(3)
                        lblQuantity.Text = paperStr(4)
                        bcReadStep = BarcodeReadStep.PID_MACRead
                    End If




                Case BarcodeReadStep.PID_MACRead
                    '段取り終了（検査開始スイッチを押すまではバーコードの再読み込み可能）

                    '------------------
                    '再検査フラグの初期化
                    '-------------------
                    isReInspection = False


                    '受注数＄製品品番バーコードを読む
                    If System.Text.RegularExpressions.Regex.IsMatch(bc, REG_BARCODE_PATTERN_BC1) Then
                        Dim tmpAns As Long = MessageBox.Show("生産機種の切替を行いますか？" & ControlChars.NewLine &
                                                             "【はい】を押すと生産機種を変更します。" & ControlChars.NewLine &
                                                             "【いいえ】を押すと何も変更されません。", "確認事項", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
                        '品番変更なし
                        If tmpAns = DialogResult.No Then
                            Continue Do
                        End If
                        '品番変更
                        If Not dealBC1(bc) Then
                            'MessageBox.Show("error:dealBC1_pidStep")
                        End If
                        bcReadStep = BarcodeReadStep.BC2Read
                        Continue Do
                    End If

                    '帳票Noを読む
                    If System.Text.RegularExpressions.Regex.IsMatch(bc, REG_BARCODE_PATTERN_BC2) Then

                        '品番チェック
                        Dim tmpAns As Long = MessageBox.Show("バーコード②の再読込みは禁止です。" & ControlChars.NewLine &
                                                             "【はい】を押すとバーコード①読込みへ戻ります。" & ControlChars.NewLine &
                                                             "【いいえ】を押すと何も変更されません。", "確認事項", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
                        If tmpAns = DialogResult.No Then
                            Continue Do
                        End If

                        '読み込んだ情報をクリアしてBC1読み込みへ戻る
                        lblOKWork.Text = String.Empty
                        lblOrder.Text = String.Empty
                        lblQuantity.Text = String.Empty
                        lblWorkType.Text = String.Empty
                        lblPaper.Text = String.Empty
                        bcReadStep = BarcodeReadStep.BC1Read
                        Continue Do
                    End If

                    'マスターユニットのバーコードを読んだ場合
                    If System.Text.RegularExpressions.Regex.IsMatch(bc, REG_BARCODE_PATTERN_MASTER) Then

                        Dim tmpBCData As String() = Split(bc, ControlChars.CrLf) '[MAC CrLf PID]
                        Dim MAC = tmpBCData(0)
                        Dim PID = tmpBCData(1)

                        'ID銘板が不適切な場合はID銘板読み取り処理へ戻る
                        If Not dealPIDData(PID) Then
                            Continue Do
                        End If
                        If Not dealMACData(MAC) Then
                            Continue Do
                        End If

                        If Not IO.File.Exists(String.Format("{0}{1}${2}.txt", pathHistory, MAC, PID)) Then
                            'PID,MAC単体の重複チェック
                            Dim pFiles As String() = IO.Directory.GetFiles(pathHistory, "*$" & PID & "*", IO.SearchOption.TopDirectoryOnly)
                            Dim mFiles As String() = IO.Directory.GetFiles(pathHistory, "*" & MAC & "$*", IO.SearchOption.TopDirectoryOnly)
                            If pFiles.Count > 0 Then
                                MessageBox.Show("PIDが重複しています。" & ControlChars.CrLf &
                                            "製造技術担当者に連絡してください。" & ControlChars.CrLf &
                                            "重複しているPID：" & PID, "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Continue Do
                            End If
                            If mFiles.Count > 0 Then
                                MessageBox.Show("MACが重複しています。" & ControlChars.CrLf &
                                            "製造技術担当者に連絡してください。" & ControlChars.CrLf &
                                            "重複しているMAC：" & MAC, "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Continue Do
                            End If

                        End If

                        '履歴ファイルの検索
                        If IO.File.Exists(String.Format("{0}{1}${2}.txt", pathHistory, MAC, PID)) Then

                            'PID,MAC単体の重複チェック
                            '検査履歴がある場合にはPID,MAC1つずつヒットする
                            Dim pFiles As String() = IO.Directory.GetFiles(pathHistory, "*$" & PID & "*", IO.SearchOption.TopDirectoryOnly)
                            Dim mFiles As String() = IO.Directory.GetFiles(pathHistory, "*" & MAC & "$*", IO.SearchOption.TopDirectoryOnly)
                            If pFiles.Count > 1 Then
                                MessageBox.Show("PIDが重複しています。" & ControlChars.CrLf &
                                            "製造技術担当者に連絡してください。" & ControlChars.CrLf &
                                            "重複しているPID：" & PID, "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Continue Do
                            End If
                            If mFiles.Count > 1 Then
                                MessageBox.Show("MACが重複しています。" & ControlChars.CrLf &
                                            "製造技術担当者に連絡してください。" & ControlChars.CrLf &
                                            "重複しているMAC：" & MAC, "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Continue Do
                            End If

                            '履歴ファイル内の製品品番と一致をチェック
                            '履歴ファイルからシリアル番号を読み出す
                            Try
                                '検査履歴ファイルの中身を調べる
                                Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}${2}.txt", pathHistory, MAC, PID), enc_sj)
                                    Dim tmpStrArry As String() = sr.ReadLine().Split(","c)
                                    Dim workTypeIndex = Array.IndexOf(tmpStrArry, "製品品番")
                                    Dim serialNoIndex = Array.IndexOf(tmpStrArry, "シリアルNo")
                                    Dim serialNoList As List(Of String) = New List(Of String) 'シリアル番号の一致確認用リスト
                                    Dim judgeIndex = Array.IndexOf(tmpStrArry, "総合判定")
                                    Dim judgeContent As String = String.Empty

                                    '履歴ファイルフォーマットに問題がある場合(履歴ファイルの破損など)
                                    If workTypeIndex < 0 Then
                                        MessageBox.Show("検査履歴中に【製品品番】の項目が見つかりません" & ControlChars.NewLine &
                                                            "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                                            "読込みファイル：" & String.Format("{0}{1}${2}.txt", pathHistory, MAC, PID),
                                                            "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        'Me.Close()
                                        lblPID.Text = String.Empty
                                        lblMAC.Text = String.Empty
                                        Continue Do
                                    End If

                                    If serialNoIndex < 0 Then
                                        MessageBox.Show("検査履歴中に【シリアルNo】の項目が見つかりません" & ControlChars.NewLine &
                                                            "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                                            "読込みファイル：" & String.Format("{0}{1}${2}.txt", pathHistory, MAC, PID),
                                                            "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        'Me.Close()
                                        lblPID.Text = String.Empty
                                        lblMAC.Text = String.Empty
                                        Continue Do
                                    End If

                                    If judgeIndex < 0 Then
                                        MessageBox.Show("検査履歴中に【総合判定】の項目が見つかりません" & ControlChars.NewLine &
                                                            "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                                            "読込みファイル：" & String.Format("{0}{1}${2}.txt", pathHistory, MAC, PID),
                                                            "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        lblPID.Text = String.Empty
                                        lblMAC.Text = String.Empty
                                        'Me.Close()
                                        Continue Do
                                    End If


                                    Dim workInHistory As String = lblWorkType.Text '履歴内のワーク名を保持

                                    Do While sr.Peek() >= 0
                                        tmpStrArry = sr.ReadLine().Split(","c)
                                        '製品品番が履歴内で全て同じかを確認
                                        If tmpStrArry(workTypeIndex) <> lblWorkType.Text Then
                                            workInHistory = tmpStrArry(workTypeIndex)
                                        End If

                                        'シリアルNoが記録されている場合にリストに追加（後で一致確認のため）
                                        If tmpStrArry(serialNoIndex) Like "????????" Then
                                            serialNoList.Add(tmpStrArry(serialNoIndex))
                                        End If
                                        '総合判定は最終行の判定を使用
                                        '不適合→適合(再検査)→不適合(返却品)などもあり得るので
                                        judgeContent = tmpStrArry(judgeIndex)

                                        My.Application.DoEvents()
                                    Loop
                                    '製品品番が履歴内で全て同じかを確認
                                    If tmpStrArry(workTypeIndex) <> lblWorkType.Text Then
                                        MessageBox.Show("検査ワークの製品品番と履歴ファイルに登録されている製品品番が異なります。" & ControlChars.Cr &
                                                                "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                                                "検査ワークの製品品番： " & lblWorkType.Text & ControlChars.CrLf &
                                                                "登録されている製品品番：" & workInHistory,
                                                                "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        lblPID.Text = String.Empty
                                        lblMAC.Text = String.Empty
                                        Continue Do
                                    End If


                                    'シリアル番号のチェック
                                    If serialNoList.Count > 0 Then
                                        For i As Integer = 0 To serialNoList.Count - 1
                                            If serialNoList.Item(i) <> serialNoList.First() Then
                                                MessageBox.Show("履歴中にシリアル番号が複数存在します。" & ControlChars.Cr &
                                                                    "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                                                    "読込みファイル：" & String.Format("{0}{1}${2}.txt", pathHistory, MAC, PID),
                                                                    "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                                'Me.Close()
                                                lblPID.Text = String.Empty
                                                lblMAC.Text = String.Empty
                                                Continue Do
                                            End If
                                        Next
                                    End If


                                    'シリアル番号書込前に不合格なワークを再検査した場合
                                    If serialNoList.Count <= 0 Then
                                        lblSerialNo.Text = loadSerialNo(Params.WriteSerial(1), Params.WriteSerial(2), Params.WriteSerial(3))
                                        'シリアル番号書込完了品の再検査をした場合
                                    ElseIf serialNoList.First() Like "????????" Then
                                        hasSerialNo = True
                                        '履歴ファイル内のシリアル番号の確認
                                        If serialNoList.First.Substring(0, 2) <> Params.WriteSerial(3) Then
                                            MessageBox.Show(String.Format("シリアルNoの型式が異なります。" & ControlChars.CrLf &
                                          "ファイルが壊れている可能性があります。" & ControlChars.CrLf &
                                          "読み込んだシリアルNo:{0}" & ControlChars.CrLf &
                                          "登録されているシリアルNoの型式:{1}******", serialNoList.First, Params.WriteSerial(3)), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                            'Me.Close()
                                            lblPID.Text = String.Empty
                                            lblMAC.Text = String.Empty
                                            Continue Do
                                        End If

                                        lblSerialNo.Text = serialNoList.First()
                                    End If

                                    '最後の履歴の総合判定が不適合ならNG品の再検査(検査適合時には残数を減らす)
                                    If judgeContent <> "適合" Then
                                        isNgWorkReInspection = True
                                    End If

                                    Me.Refresh()
                                End Using
                            Catch ex As Exception
                                writeErrorLog(ex)
                                Me.Close()
                            End Try




                            '異なるワークに同一銘板を貼付する可能性があるのでモジュール検査シリアル番号をチェック
                            '通常生産かつ適合品再検査
                            If Params.DailyChk(1) <> "2" AndAlso Not isNgWorkReInspection Then
                                Dim tmpAns As Long = MessageBox.Show(String.Format("検査済みワークです。" & ControlChars.Cr &
                                                       "再検査を行いますか。" & ControlChars.Cr &
                                                       "【はい】を押すと再検査を行います。。" & ControlChars.Cr &
                                                       "【いいえ】を押すとID銘板読込み処理へ戻ります。"),
                                                       "確認事項", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

                                If tmpAns = DialogResult.No Then
                                    bcReadStep = BarcodeReadStep.PID_MACRead
                                    lblMAC.Text = String.Empty
                                    lblPID.Text = String.Empty
                                    '最新のシリアル番号を表示し直す
                                    Dim serialNoStr As String = loadSerialNo(Params.WriteSerial(1), Params.WriteSerial(2), Params.WriteSerial(3))

                                    If serialNoStr Is Nothing Then
                                        MessageBox.Show("シリアル番号ファイルを読み込めません。" & ControlChars.Cr &
                                                        "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                                        "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        Me.Close()
                                    End If
                                    lblSerialNo.Text = serialNoStr
                                    Continue Do
                                End If

                                If tmpAns = DialogResult.Yes Then
                                    'do nothing
                                End If

                            End If



                            lblPID.Text = PID
                            lblMAC.Text = MAC
                            '再検査フラグの設定(初期化処理に注意)
                            isReInspection = True


                        End If

                        bcReadStep = BarcodeReadStep.SetupDone
                        Continue Do
                    End If

                    'スレーブユニットのバーコードを読んだ場合
                    If System.Text.RegularExpressions.Regex.IsMatch(bc, REG_BARCODE_PATTERN_SLAVE) Then
                        'バーコード：[PID]
                        Dim PID = bc
                        Dim MAC = String.Empty 'ダミーの値(マスタユニットと同じように表記するため)

                        If Not dealPIDData(PID) Then
                            Continue Do
                        End If



                        '履歴ファイルの検索
                        If IO.File.Exists(String.Format("{0}{1}${2}.txt", pathHistory, MAC, PID)) Then


                            Try
                                '検査履歴ファイルの中身を調べる
                                Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}${2}.txt", pathHistory, MAC, PID), enc_sj)
                                    Dim tmpStrArry As String() = sr.ReadLine().Split(","c)
                                    Dim workTypeIndex = Array.IndexOf(tmpStrArry, "製品品番")
                                    Dim judgeIndex = Array.IndexOf(tmpStrArry, "総合判定")
                                    Dim judgeContent As String = String.Empty

                                    '履歴ファイルフォーマットに問題
                                    If workTypeIndex < 0 Then
                                        MessageBox.Show("検査履歴中に【製品品番】の項目が見つかりません" & ControlChars.NewLine &
                                                            "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                                            "読込みファイル：" & String.Format("{0}{1}${2}.txt", pathHistory, MAC, PID),
                                                            "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        'Me.Close()
                                        lblPID.Text = String.Empty
                                        lblMAC.Text = String.Empty
                                        Continue Do
                                    End If

                                    If judgeIndex < 0 Then
                                        MessageBox.Show("検査履歴中に【総合判定】の項目が見つかりません" & ControlChars.NewLine &
                                                            "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                                            "読込みファイル：" & String.Format("{0}{1}${2}.txt", pathHistory, MAC, PID),
                                                            "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        'Me.Close()
                                        lblPID.Text = String.Empty
                                        lblMAC.Text = String.Empty
                                        Continue Do
                                    End If

                                    Dim workInHistory As String = lblWorkType.Text '履歴内のワーク名を保持

                                    Do While sr.Peek() >= 0
                                        tmpStrArry = sr.ReadLine().Split(","c)
                                        '製品品番が履歴内で全て同じかを確認
                                        If tmpStrArry(workTypeIndex) <> lblWorkType.Text Then
                                            workInHistory = tmpStrArry(workTypeIndex)
                                        End If
                                        '履歴内検査結果を確認(直前の結果)
                                        judgeContent = tmpStrArry(judgeIndex)

                                        My.Application.DoEvents()
                                    Loop

                                    '製品品番が履歴内で全て同じかを確認
                                    If workInHistory <> lblWorkType.Text Then
                                        MessageBox.Show("検査ワークの製品品番と履歴ファイルに登録されている製品品番が異なります。" & ControlChars.Cr &
                                                                "検査ワークの製品品番： " & lblWorkType.Text & ControlChars.CrLf &
                                                                "登録されている製品品番：" & workInHistory,
                                                                "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        'Me.Close()
                                        lblPID.Text = String.Empty
                                        lblMAC.Text = String.Empty
                                        Continue Do
                                    End If


                                    '最後の履歴の総合判定
                                    If judgeContent <> "適合" Then
                                        isNgWorkReInspection = True 'NGワークの再検査
                                    End If

                                    Me.Refresh()
                                End Using
                            Catch ex As Exception
                                writeErrorLog(ex)
                                Me.Close()
                            End Try


                            '通常生産品かつ合格品の再検査の場合
                            If Params.DailyChk(1) <> "2" AndAlso Not isNgWorkReInspection Then
                                Dim tmpAns As Long = MessageBox.Show(String.Format("検査済みワークです。" & ControlChars.Cr &
                                                                                "再検査を行いますか。" & ControlChars.Cr &
                                                                                "[はい]を押すと再検査を行います。。" & ControlChars.Cr &
                                                                                "[いいえ]を押すとID銘板読込み処理へ戻ります。"),
                                                                                "確認事項", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

                                If tmpAns = DialogResult.No Then
                                    bcReadStep = BarcodeReadStep.PID_MACRead
                                    lblMAC.Text = String.Empty
                                    lblPID.Text = String.Empty
                                    Continue Do
                                End If

                                If tmpAns = DialogResult.Yes Then
                                    'do nothing
                                End If
                            End If

                            lblPID.Text = PID
                            lblMAC.Text = MAC
                            '再検査フラグの設定(初期化処理に注意)
                            isReInspection = True

                        End If

                    End If
                    lblPID.Text = bc
                    bcReadStep = BarcodeReadStep.SetupDone



                Case BarcodeReadStep.SetupDone
                    'do nothing
                Case Else
                    'do nothing

            End Select

            My.Application.DoEvents()
        Loop


    End Sub

    '読込みが発生次第barcodeStrの値が変更される(バーコードの読込み)
    Private Sub BacodeReader1_DataReceived(ByVal sender As System.Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles BarcodeReader.DataReceived
        barcodeStr = BarcodeReader.ReadExisting() 'データマトリックスの内容がMAC<Cr>PID<Cr>のためReadLineからReadExistingに変更
        BarcodeReader.DiscardInBuffer()
    End Sub


    ''' <summary>
    ''' PSoC通信エラー時にメッセージを表示してプログラムを終了する
    ''' </summary>
    ''' <returns></returns>
    Private Function PSoCError() As Boolean

        MessageBox.Show("PSoC通信異常発生" & ControlChars.CrLf &
                        "ソフトを終了します。", "確認事項",
                        MessageBoxButtons.OK, MessageBoxIcon.Error)

        Me.Close()
        Return True
    End Function

    ''' <summary>
    ''' workTypeの日常点検の有無を確認(点検日と最終点検日が異なる場合はフラグリセット)
    ''' </summary>
    ''' <param name="workType"></param>
    ''' <returns>
    ''' true->日常点検完了(合格、不合格サンプル両方),false->日常点検未完了
    ''' </returns>
    Private Function doneDailyCheck(ByVal workType As String, ByRef okFlg As String, ByRef ngFlg As String) As Boolean

        Dim okSampleChkFlg As String = "0"
        Dim ngSampleChkFlg As String = "0"
        Dim tmpDate As String = String.Empty
        Dim okSampleData As String = String.Empty
        Dim ngSampleData As String = String.Empty
        '読込みファイル名を量産ワークと同一にするため、-(OK|NG)SAMPLEの文字を除外する
        workType = regexReplace.Replace(workType, "")
        Try
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}{2}.txt", PathSlnFile, pathDailyCheck, workType), enc_sj)

                '[日常点検ファイルのフォーマット]
                '日付
                'OKサンプル,点検フラグ
                'NGサンプル,点検フラグ
                tmpDate = sr.ReadLine() '日付データ
                okSampleData = sr.ReadLine() 'okサンプル確認,フラグ
                ngSampleData = sr.ReadLine() 'ngサンプル確認,フラグ
            End Using

            '最終点検日の日付が現在と異なる場合はフラグリセット
            If Date.Now.Date <> Date.Parse(tmpDate).Date Then
                Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}{2}.txt", PathSlnFile, pathDailyCheck, workType), False, enc_sj)
                    sw.WriteLine(Date.Now)
                    sw.WriteLine("合格サンプル確認,0")
                    sw.WriteLine("不合格サンプル確認,0")
                End Using

                okSampleChkFlg = "0"
                okFlg = okSampleChkFlg

                ngSampleChkFlg = "0"
                ngFlg = ngSampleChkFlg

                Return False
            End If

            okSampleChkFlg = okSampleData.Split(","c)(1)
            okFlg = okSampleChkFlg

            ngSampleChkFlg = ngSampleData.Split(","c)(1)
            ngFlg = ngSampleChkFlg


            If okSampleChkFlg = "1" AndAlso ngSampleChkFlg = "1" Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            writeErrorLog(ex)
            MessageBox.Show("日常点検ファイル確認エラー" & ControlChars.NewLine &
                            "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End Try

        Return False
    End Function

    ''' <summary>
    ''' 日常点検の結果を更新
    ''' </summary>
    ''' <param name="workType">製品品番</param>
    ''' <param name="sampleType">サンプルの種類（適合、不適合サンプル）</param>
    ''' <param name="flg">セットするフラグtrue->日常点検完了,false->日常点検未完了</param>
    ''' <returns></returns>
    Private Function updateDailyCheck(ByVal workType As String, ByVal sampleType As String, ByVal flg As Boolean) As Boolean

        Dim tmpOkVal As String = String.Empty '合格サンプルのフラグを格納
        Dim tmpNGVal As String = String.Empty '不合格サンプルのフラグ格納

        Try
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}{2}.txt", PathSlnFile, pathDailyCheck, workType), enc_sj)
                '変更前の値読み込み
                Do While sr.Peek() >= 0
                    Dim tmpStrArry As String() = sr.ReadLine().Split(","c)
                    If tmpStrArry(0).IndexOf("合格サンプル確認") = 0 Then
                        tmpOkVal = tmpStrArry(1)
                    End If

                    If tmpStrArry(0).IndexOf("不合格サンプル確認") = 0 Then
                        tmpNGVal = tmpStrArry(1)
                    End If

                    My.Application.DoEvents()
                Loop

            End Using

            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}{2}.txt", PathSlnFile, pathDailyCheck, workType), False, enc_sj)

                Dim dailyCheckFlg As Integer = 0
                If flg Then
                    dailyCheckFlg = 1
                Else
                    dailyCheckFlg = 0
                End If

                '日付更新
                sw.WriteLine(Date.Now)
                '合格サンプル確認結果更新
                If sampleType = "1" Then '合格サンプル
                    sw.WriteLine(String.Format("合格サンプル確認,{0}", dailyCheckFlg.ToString()))
                    sw.WriteLine(String.Format("不合格サンプル確認,{0}", tmpNGVal.ToString()))
                    lblOkSampleChk.BackColor = If(dailyCheckFlg = 1, Color.Lime, Color.Red)
                    '不合格サンプル確認結果更新
                ElseIf sampleType = "0" Then
                    sw.WriteLine(String.Format("合格サンプル確認,{0}", tmpOkVal.ToString()))
                    sw.WriteLine(String.Format("不合格サンプル確認,{0}", dailyCheckFlg.ToString()))
                    lblNgSampleChk.BackColor = If(dailyCheckFlg = 1, Color.Lime, Color.Red)
                Else
                    '不正な値が入った場合には値の変更を行わない
                    sw.WriteLine(String.Format("合格サンプル確認,{0}", tmpOkVal.ToString()))
                    sw.WriteLine(String.Format("不合格サンプル確認,{0}", tmpNGVal.ToString()))
                    Return False
                End If

            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            MessageBox.Show("日常点検ファイルの更新ができません。" & ControlChars.NewLine &
                            "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try

        Return True
    End Function
    ''' <summary>
    ''' 日常点検確認（1サンプル毎）
    ''' </summary>
    ''' <param name="sampleName"></param>
    ''' <returns></returns>
    Private Function doneDailyChk2(ByVal sampleName As String) As Boolean


        Dim tmpDate As String = String.Empty
        Dim tmpResult As String()

        Try
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}{2}.txt", PathSlnFile, pathDailyCheck, sampleName), enc_sj)
                tmpDate = sr.ReadLine() '日付データ
                tmpResult = sr.ReadLine().Split(","c) 'サンプルワーク確認結果
            End Using

            '最終点検日の日付が現在と異なる場合はフラグリセット
            If Date.Now.Date <> Date.Parse(tmpDate).Date Then
                Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}{2}.txt", PathSlnFile, pathDailyCheck, sampleName), False, enc_sj)
                    sw.WriteLine(Date.Now)
                    sw.WriteLine("サンプルワーク確認,0")
                End Using

                Return False
            End If


            If tmpResult(1) = "1" Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            writeErrorLog(ex)
            MessageBox.Show("日常点検ファイル確認エラー" & ControlChars.NewLine &
                            "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End Try

    End Function

    ''' <summary>
    ''' 日常点検履歴の更新
    ''' </summary>
    ''' <param name="sampleName">更新するサンプルワーク履歴ファイルパス</param>
    ''' <param name="sampleType">サンプルワークの種類(適合、不適合ワーク)</param>
    ''' <param name="insResult">検査結果</param>
    ''' <returns></returns>
    Private Function updateDailyCheck2(ByVal sampleName As String, ByVal sampleType As String, ByVal insResult As Boolean) As Boolean
        Try
            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}{2}.txt", PathSlnFile, pathDailyCheck, sampleName), False, enc_sj)
                sw.WriteLine(Date.Now)
                '想定結果=検査結果なら確認結果を1として更新、それ以外は0
                Dim flg As String = If(insResult, "1", "0")
                sw.Write("サンプルワーク確認,{0}", If(sampleType = flg, "1", "0"))
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            MessageBox.Show("日常点検ファイルの更新ができません。" & ControlChars.NewLine &
                           "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try

        Return True
    End Function

    ''' <summary>
    ''' 品番パラメータ読込み
    ''' </summary>
    ''' <param name="workType"></param>
    ''' <returns></returns>
    Private Function readSettingParams(ByVal workType As String) As Boolean

        Try
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}{2}.txt", PathSlnFile, pathSettingFolder, workType), enc_sj)
                Do While sr.Peek() > 0
                    Dim tmpStr As String = sr.ReadLine()


                    If tmpStr.IndexOf("[製品種別]") >= 0 Then
                        Params.ProductType = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[日常点検確認]") >= 0 Then
                        Params.DailyChk = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[フォームウェアバージョン]") >= 0 Then
                        Params.FormwareVersion = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[ピン治具上部判定]") >= 0 Then
                        Params.PinUpperJudge = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[PLC判別]") >= 0 Then
                        Params.PLCJudge = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[ピン変換基板判別]") >= 0 Then
                        Params.PinBoardJudge = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[無線モジュール検査シリアル番号読込み]") >= 0 Then
                        Params.ReadNModuleSeraialNo = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[FeRAM初期値書込み]") >= 0 Then
                        Params.FeRAMWriteInitVal = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[過電流確認]") >= 0 Then
                        Params.Overcurrent = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[COM確認]") >= 0 Then
                        Params.ComChk = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[通常製品検査]") >= 0 Then
                        Params.PInspection = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[LED確認1]") >= 0 Then
                        Params.LEDChk1 = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[無線通信検査]") >= 0 Then
                        Params.NNowireCom = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[PST]") >= 0 Then
                        Params.PST = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[WEBソフトウェア書込み]") >= 0 Then
                        Params.WEBSoftware = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[ソフトウェアバージョン読込み]") >= 0 Then
                        Params.ReadSoftwareVer = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[ソフトウェアバージョンFeRAM書込み]") >= 0 Then
                        Params.WriteSoftVerFeRAM = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[無線PID書込み]") >= 0 Then
                        Params.WritePID = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[EtherNetMACアドレス書込み]") >= 0 Then
                        Params.WriteMAC = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[EtherNetシリアルNo書込み]") >= 0 Then
                        Params.WriteSerial = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[FeRAMプロテクト]") >= 0 Then
                        Params.FeRAMProtect = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[FeRAM読込み確認]") >= 0 Then
                        Params.ReadFeRAM = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[MAC_シリアルNo読込み]") >= 0 Then
                        Params.ReadMACSerialNo = tmpStr.Split(","c)
                    End If

                    My.Application.DoEvents()
                Loop
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            'MessageBox.Show("品番リストファイルの読込みに失敗しました。")
            'プログラムは終了しない
            Return False
        End Try

        Return True
    End Function

    ''' <summary>
    ''' 検査回数を更新、保存ファイルの更新
    ''' </summary>
    ''' <returns></returns>
    Private Function plusInspectionCount() As Boolean
        Try
            'オーバーフローの可能性を減らすためULong型にする
            Dim tmpCount As ULong = 0
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}counter.txt", PathSlnFile, pathInspectionCounter), enc_sj)
                If Not UInt64.TryParse(sr.ReadLine(), tmpCount) Then
                    MessageBox.Show("検査回数を数値に変換できません。" & ControlChars.NewLine &
                                    "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Me.Close()
                    Return False
                End If
            End Using

            tmpCount += Convert.ToUInt64(1)

            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}counter.txt", PathSlnFile, pathInspectionCounter), False, enc_sj)
                sw.WriteLine(tmpCount.ToString())
                lblInspectionCounter.Text = tmpCount.ToString()
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            MessageBox.Show("検査回数ファイルの更新に失敗しました。" & ControlChars.NewLine &
                            "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End Try
        Return True
    End Function
    ''' <summary>
    ''' コネクタ抜入回数ファイルの読み出し
    ''' </summary>
    ''' <param name="path"></param>
    ''' <param name="data"></param>
    ''' <returns></returns>
    Private Function loadCounter(ByVal path As String, ByRef data As String) As Boolean
        Try
            Using sr As IO.StreamReader = New IO.StreamReader(path, enc_sj)
                data = sr.ReadLine()
                If Not IsNumeric(data) Then
                    Return False
                End If

                If Convert.ToUInt64(data) >= 0 Then
                    Return False
                End If

            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            Return False
        End Try
        Return True
    End Function
    ''' <summary>
    ''' コネクタ抜入回数のインクリメント
    ''' </summary>
    ''' <param name="path"></param>
    ''' <returns></returns>
    Private Function incrementCounter(ByVal path As String, ByRef lbl As Label) As Boolean
        Dim count As ULong = 0
        Dim strCount As String = String.Empty

        If Not loadCounter(path, strCount) Then
            Return False
        End If

        Try
            count = Convert.ToUInt64(strCount, 10) + Convert.ToUInt64(1)
            Using sw As IO.StreamWriter = New IO.StreamWriter(path, False, enc_sj)
                sw.WriteLine(count)
            End Using
            lbl.Text = count.ToString()
        Catch ex As Exception
            writeErrorLog(ex)
            Return True
        End Try

        Return True
    End Function
    ''' <summary>
    ''' コネクタ抜入回数のリセット
    ''' </summary>
    ''' <param name="path"></param>
    ''' <returns></returns>
    Private Function resetCounter(ByVal path As String) As Boolean
        Try
            Using sw As IO.StreamWriter = New IO.StreamWriter(path, False, enc_sj)
                sw.WriteLine("0")
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            Return False
        End Try


        Return True
    End Function

    ''' <summary>
    ''' 製品検査のエラーコードの内容文字列を取得
    ''' </summary>
    ''' <param name="errorCode"></param>
    ''' <returns></returns>
    Private Function errorCodeContent(ByVal errorCode As String) As String

        Dim tmpStrArry As String()
        Try
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}", PathSlnFile, pathErrorCode), enc_sj)
                Do While sr.Peek() > 0
                    tmpStrArry = sr.ReadLine().Split(","c)
                    If tmpStrArry(0) = errorCode Then
                        Return tmpStrArry(1)
                    End If
                    My.Application.DoEvents()
                Loop
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            MessageBox.Show("製品検査のエラーコードを取得できません。" & ControlChars.NewLine &
                            "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return Nothing
        End Try

        Return Nothing

    End Function

    ''' <summary>
    ''' ピン治具、ピン変換基板、PLCの対応文字列を返す
    ''' </summary>
    ''' <param name="type"></param>
    ''' <param name="code"></param>
    ''' <returns></returns>
    Private Function readJigSetting(ByVal type As String, ByVal code As String) As String

        Dim tmpStrArry As String()
        Dim filePath As String

        Try
            Select Case type
                Case "pin_upper"
                    filePath = PathSlnFile & pathPinUpper
                Case "connecter"
                    filePath = PathSlnFile & pathConnecter
                Case "plc"
                    filePath = PathSlnFile & pathPlc
                Case Else
                    Return Nothing
            End Select

            Using sr As IO.StreamReader = New IO.StreamReader(filePath, enc_sj)
                Do While sr.Peek() > 0
                    tmpStrArry = sr.ReadLine().Split(","c)
                    If tmpStrArry(0) = code Then
                        Return tmpStrArry(1)
                    End If
                    My.Application.DoEvents()
                Loop

                Return "登録されていない機種です"

            End Using

        Catch ex As Exception
            writeErrorLog(ex)
            MessageBox.Show("治具設定ファイルの読み込みができません。" & ControlChars.NewLine &
                            "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return Nothing
        End Try

        Return Nothing
    End Function


    ''' <summary>
    ''' 同じMACが今までに使用されているか判定
    ''' </summary>
    ''' <param name="pid"></param>
    ''' <param name="mac"></param>
    ''' <returns>true:同一のMACが存在,false:同一のMACは存在しない</returns>
    Private Function chkMACOverlap(ByVal pid As String, ByVal mac As String) As Boolean
        'MACの重複検索
        Dim overlappingMAC As IEnumerable(Of String) =
                            IO.Directory.EnumerateFiles(String.Format("{0}", pathHistory),
                                                        String.Format("{0}$*", mac), IO.SearchOption.TopDirectoryOnly)
        For Each f As String In overlappingMAC
            MessageBox.Show(String.Format("MACが重複しています。" & ControlChars.Cr &
                                                          "製造技術担当者に連絡してください。" & ControlChars.Cr &
                                                          "検査ワークのMAC：{0}" & ControlChars.Cr &
                                                          "検査済ワークの履歴ファイル：{1}", mac, IO.Path.GetFileName(f)),
                                                          "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return True
        Next
        Return False
    End Function
    ''' <summary>
    ''' 同じPIDが今までに使用されているか判定
    ''' </summary>
    ''' <param name="pid"></param>
    ''' <param name="mac"></param>
    ''' <returns>true:同一のPIDが存在,false:同一のPIDは存在しない</returns>
    Private Function chkPIDOverlap(ByVal pid As String, ByVal mac As String) As Boolean
        'PIDの重複検索
        Dim overlappingPID As IEnumerable(Of String) =
            IO.Directory.EnumerateFiles(String.Format("{0}", pathHistory),
                                        String.Format("*${0}*", pid), IO.SearchOption.TopDirectoryOnly)
        For Each f As String In overlappingPID
            MessageBox.Show(String.Format("PIDが重複しています。" & ControlChars.Cr &
                                          "製造技術担当者に連絡してください。" & ControlChars.Cr &
                                          "検査ワークのPID：{0}" & ControlChars.Cr &
                                          "検査済ワークの履歴ファイル：{1}", pid, IO.Path.GetFileName(f)),
                                          "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return True
        Next

        Return False
    End Function



    'プログラム終了時の処理
    Private Sub Form1_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        'todo:PSoCなどのクローズ処理を追加【随時】
        Try
            offOutputPSoC() 'PSoC出力を全てオフ
            BarcodeReader.Close()
            PSoC.Close()
        Catch ex As Exception
            writeErrorLog(ex)
            End
        End Try

        End
    End Sub


    '----------------------------------------------------------------------------------------
    '                   NFC関連関数
    '----------------------------------------------------------------------------------------

    ''' <summary>
    ''' パソリとの通信確認
    ''' パソリとの通信前に必要
    ''' </summary>
    ''' <returns></returns>
    Private Function isConnectNFC() As Boolean

        'エラーが発生（回線切断状態でdisconnect->false）
        'If Not Sna.NoWire.Nfc_disconnect() Then
        '    Return False
        'End If

        If Not Sna.NoWire.Nfc_connect() Then
            Sna.NoWire.Nfc_disconnect()
            Return False
        End If

        If Not Sna.NoWire.Nfc_req() Then
            Sna.NoWire.Nfc_disconnect()
            Return False
        End If

        Return True

    End Function
    ''' <summary>
    ''' パソリとの通信確認（リトライ処理あり）
    ''' </summary>
    ''' <returns></returns>
    Private Function retryIsConnectNFC() As Boolean
        Dim retryNum As Integer = 3

        For i As Integer = 0 To retryNum - 1
            If isConnectNFC() Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary>
    ''' 無線モジュール検査シリアル番号読み込み
    ''' </summary>
    ''' <returns>
    ''' Nfc_rf_readに成功すれば8桁のシリアル番号(文字列)を返す
    ''' </returns>
    Private Function readInspectionSerialNo() As String

        Dim SerialNo As String = String.Empty '無線モジュール検査シリアル番号

        '通信確認
        If Not retryIsConnectNFC() Then
            Return Nothing
        End If


        Dim rxdata As Byte() = retryNfcReadLine(&H80)
        If rxdata Is Nothing Then
            'MessageBox.Show("error nfcReadLine")
            Return Nothing
        End If
        '無線モジュールシリアル番号の取得
        For i As Integer = 0 To 3
            SerialNo = SerialNo & rxdata(i).ToString("X2")
        Next

        Return SerialNo

    End Function

    ''' <summary>
    ''' NFC通信での指定ブロック（16バイト読み込み）
    ''' 16バイト目はチェックサム用のため有効データ長は15バイト
    ''' </summary>
    ''' <param name="block"></param>
    ''' <returns></returns>
    Private Function nfcReadLine(ByVal block As UInt16) As Byte()

        trans_mode = False

        '通信確認
        If Not isConnectNFC() Then
            Return Nothing
        End If

        '読み込み
        If Not Sna.NoWire.Nfc_rf_read(Convert.ToUInt16(block)) Then
            Dim err As Boolean() = Sna.NoWire.ErrorStatus
            Return Nothing
        End If
        '有効データの抽出
        If Not Sna.NoWire.Nfc_rf_DealRecvBuffer() Then
            Return Nothing
        End If

        Dim data(Sna.NoWire.RecvData.Length - 1) As Byte

        '値のみコピーしてreturn
        'return RecvDataだとRecvDataへの参照が帰る？
        For i As Integer = 0 To Sna.NoWire.RecvData.Length - 1
            data(i) = Sna.NoWire.RecvData(i)
        Next

        Return data

    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="block"></param>
    ''' <returns></returns>
    Private Function retryNfcReadLine(ByVal block As UInt16) As Byte()
        Dim retryNum As Integer = 3
        For i As Integer = 0 To retryNum - 1
            Dim data As Byte() = nfcReadLine(block)
            If data Is Nothing Then
                Continue For
            Else
                Return data
            End If
        Next
        Return Nothing
    End Function

    ''' <summary>
    ''' トンネルリード用
    ''' </summary>
    ''' <param name="block"></param>
    ''' <param name="length"></param>
    ''' <returns></returns>
    Private Function nfcTunnelReadLine(ByVal block As Integer, ByVal length As Byte) As Byte()

        '通信確認
        If Not isConnectNFC() Then
            Return Nothing
        End If

        Dim readLength As Byte = 16 '暗号化のため、16の倍数でないとだめ
        Do While readLength < length
            Try
                readLength += Convert.ToByte(16)
            Catch ex As Exception
                'overflowなど
                MessageBox.Show(ex.Message)
                Return Nothing
            End Try
            My.Application.DoEvents()
        Loop


        If Not Sna.NoWire.nfc_tunnel_read(block, readLength) Then
            Return Nothing
        End If

        '有効データ抽出
        If Not Sna.NoWire.Nfc_tunnel_DealRecvBuffer() Then
            Return Nothing
        End If
        '値のみコピーしてreturn
        Dim data(Sna.NoWire.RecvTunnelData.Length - 1) As Byte
        For i As Integer = 0 To Sna.NoWire.RecvTunnelData.Length - 1
            data(i) = Sna.NoWire.RecvTunnelData(i)
        Next

        Return data

    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="block"></param>
    ''' <param name="length"></param>
    ''' <returns></returns>
    Private Function retryNfcTunnelReadLine(ByVal block As Integer, ByVal length As Byte) As Byte()
        Dim retryNum As Integer = 3
        For i As Integer = 0 To retryNum - 1
            Dim data As Byte() = nfcTunnelReadLine(block, length)
            If data Is Nothing Then
                Continue For
            Else
                Return data
            End If
        Next
        Return Nothing
    End Function

    Private Function nfcWriteLine(ByVal block As UInt16, ByVal data As Byte()) As Boolean
        '通信確認
        If Not isConnectNFC() Then Return False

        If Not Sna.NoWire.Nfc_rf_write(block, data) Then
            Return False
        End If

        Return True '書き込み成功

    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="block"></param>
    ''' <param name="data"></param>
    ''' <returns></returns>
    Private Function retryNfcWriteLine(ByVal block As UInt16, ByVal data As Byte()) As Boolean
        Dim retryNum As Integer = 3
        For i As Integer = 0 To retryNum - 1
            If nfcWriteLine(block, data) Then
                Return True
            End If
        Next
        Return False
    End Function
    ''' <summary>
    ''' 無線モジュール検査完了フラグの読み込み
    ''' </summary>
    ''' <returns>
    ''' 読み込んだ各フラグの値の配列
    ''' </returns>
    Private Function readModuleInspectionFlag() As Byte()

        '通信確認
        If Not retryIsConnectNFC() Then
            Return Nothing
        End If

        'モジュール検査フラグデータ読み込み
        Dim tmp = retryNfcReadLine(&H0)

        If tmp Is Nothing Then
            Return Nothing
        End If

        Dim flgData As Byte = tmp(&HC) 'フラグデータのアドレス(0x000C)

        '各ビット情報抽出
        Dim flg(7) As Byte
        'flg(i):iビット目のビット
        For i As Integer = 0 To 7
            flg(i) = Convert.ToByte((flgData \ Convert.ToByte(2 ^ i)) And &H1)
        Next

        Return flg

    End Function

    Private Function retryReadModuleInspectionFlag() As Byte()

        Dim retryNum As Integer = 3
        For i As Integer = 0 To retryNum - 1
            Dim tmpData As Byte() = readModuleInspectionFlag()
            If tmpData IsNot Nothing Then
                Return tmpData
            End If
        Next

        Return Nothing
    End Function

    ''' <summary>
    ''' 検査モードの読み出し
    ''' 2ビット(0:出荷検査モード, 1:通常モード, 2:テストモード,　3:デバッグモード)
    ''' データエリア1の[b5b4]
    ''' </summary>
    ''' <returns></returns>
    Private Function readInspectionMode() As Byte

        '通信確認
        If Not retryIsConnectNFC() Then
            Return Nothing
        End If

        'モジュール検査フラグデータ読み込み
        Dim tmp = retryNfcReadLine(&H0)
        Dim flgData As Byte = tmp(&HC) 'フラグデータのアドレス(0x000C)

        '各ビット情報抽出(1バイト分)
        Dim flg(7) As Byte
        'flg(i):iビット目のビット
        For i As Integer = 0 To flg.Length - 1
            flg(i) = Convert.ToByte((flgData \ Convert.ToByte(2 ^ i)) And &H1)
        Next

        '5ビット目,4ビット目を取り出し2進数に変換
        Dim mode As Byte = Convert.ToByte((flg(5).ToString & flg(4).ToString()), 2)

        Return mode

    End Function



    ''' <summary>
    ''' モード、製品検査完了フラグの読み込み
    ''' </summary>
    ''' <returns></returns>
    Private Function readModeFlag() As Byte()
        If Not retryIsConnectNFC() Then
            Return Nothing
        End If

        'モジュール検査フラグデータ読み込み
        Dim tmp = retryNfcReadLine(&H0)
        Dim flgData As Byte = tmp(4) 'フラグデータのアドレス(0x0004)

        '各ビット情報抽出
        Dim flg(7) As Byte

        For i As Integer = 0 To 7
            flg(i) = Convert.ToByte((flgData \ Convert.ToByte(2 ^ i)) And &H1)
        Next

        Return flg

    End Function

    ''' <summary>
    ''' 8ビットのフラグデータ配列を値に変換
    ''' </summary>
    ''' <param name="flgs">
    ''' flgs(i):iビット目のフラグ
    ''' </param>
    ''' <returns></returns>
    Private Function eightBitFlgsToByteNum(ByVal flgs As Byte()) As Byte

        'todo:8bitを超える場合には8bitまでの値で計算するように変更する？
        If flgs.Length <> 8 Then
            Return Nothing
        End If

        Dim data As String = String.Empty
        For i As Integer = flgs.Length - 1 To 0 Step -1
            If Not (flgs(i) = 1 OrElse flgs(i) = 0) Then
                Return Nothing
            End If
            data &= flgs(i)
        Next

        Return Convert.ToByte(data, 2)

    End Function

    ''' <summary>
    ''' 指定位置のフラグをONまたはOFFした際の値を返す
    ''' </summary>
    ''' <param name="flgIndex">
    ''' 1バイト中のフラグを操作する位置
    ''' flg(i):iビット目のフラグ
    ''' </param>
    ''' <param name="flg">
    ''' フラグのOn(true)またはOFF(false)の指定
    ''' </param>
    ''' <returns>
    ''' フラグ変更後の数値0-255
    ''' </returns>
    Private Function calcFlgNum(ByVal num As Byte, ByVal flgIndex As Byte, ByVal flg As Boolean) As Byte

        If flgIndex < 1 AndAlso flgIndex > 8 Then
            Return Nothing
        End If

        Dim val As Byte = 1
        val = val << (flgIndex - 1) 'flgIndexビット目のみ1で他は0

        If flg Then 'flg->On
            Return val Or num

        Else 'flg->OFF
            val = Not val 'flgIndexビット目が0で他は1
            Return val And num
        End If

    End Function


    ''' <summary>
    ''' モジュール検査フラグデータの書き込み
    ''' </summary>
    ''' <param name="flgIndex"></param>
    ''' <returns></returns>
    Private Function writeModuleInspectionFlag(ByVal flgIndex As Byte, ByVal flg As Boolean) As Boolean

        Dim readData As Byte() = nfcReadLine(&H0)
        If readData Is Nothing Then
            Return False
        End If
        readData(flgIndex) = calcFlgNum(readData(flgIndex), flgIndex, flg) '書込みフラグを考慮した値に更新

        If Not retryNfcWriteLine(&H0, readData) Then
            Return False
        End If

        Return True '書き込み正常終了

    End Function

    ''' <summary>
    ''' 2つのbyte配列データが最初からX文字一致するかを計算
    ''' </summary>
    ''' <param name="data1"></param>
    ''' <param name="data2"></param>
    ''' <returns></returns>
    Private Function verifyLineData(ByVal data1 As Byte(), ByVal data2 As Byte()) As Integer

        'データ長の短い方に合わせる
        Dim len As Integer = Math.Min(data1.Length, data2.Length)
        Dim counter As Integer = 0

        For i As Integer = 0 To len - 1
            '値が異なれば異なるところまでの長さまでは一致している
            If data1(i) <> data2(i) Then
                Return counter
            End If
            counter += 1
        Next

        Return counter

    End Function


    ''' <summary>
    ''' 通常エリア、制御エリアに分けてFeRAM初期値を読み込む
    ''' ベリファイ用のグローバル変数に値は格納する
    ''' </summary>
    ''' <param name="initFile"></param>
    ''' <returns></returns>
    Private Function readFeRAMInitFile(ByVal initFile As String) As Boolean

        Dim memBlockNum As Integer = 27

        '初期値ファイルから読み込んだ値でFeRAM初期化
        Using sr = New IO.StreamReader(initFile, enc_sj)
            Try

                Dim counter As Integer = 0
                Do While sr.Peek() >= 0

                    Dim tmpStrArray As String() = Split(sr.ReadLine(), ",")
                    Dim writeData As Byte() = New Byte(tmpStrArray.Length - 1) {}

                    For i As Integer = 0 To tmpStrArray.Length - 1
                        writeData(i) = Convert.ToByte(tmpStrArray(i), 16)

                        If counter < memBlockNum Then
                            normalAreaMemoryArr(counter)(i) = writeData(i)
                        End If

                        If counter >= memBlockNum Then
                            controlAreaMemoryArr(counter - 27)(i) = writeData(i)
                        End If

                    Next
                    counter += 1

                    My.Application.DoEvents()
                Loop

            Catch ex As Exception
                MessageBox.Show(ex.Message)
                Return False
            End Try
        End Using

        Return True
    End Function

    ''' <summary>
    '''NFCの通常メモリエリア用配列に新たに値を書き込む
    ''' </summary>
    ''' <param name="addHead"></param>
    ''' <param name="data"></param>
    ''' <returns></returns>
    Private Function renewNormalMemDataArray(ByVal addHead As UInt16, ByVal data As Byte()) As Boolean

        'addHeadは16進数を想定
        Dim row As Integer = addHead \ 16
        Dim col As Integer = addHead Mod 16

        If row > 26 Then
            'MessageBox.Show("invalid  vlue:row")
            Return False
        End If

        If col + data.Length > 16 Then
            'MessageBox.Show("invalid vlue:col")
            Return False
        End If

        For i As Integer = 0 To data.Length - 1
            Try
                'normalAreaMemory(row, col + i) = data(i)
                normalAreaMemoryArr(row)(col + i) = data(i)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
                Return False
            End Try
        Next

        Return True
    End Function
    ''' <summary>
    ''' NFCの通常メモリエリア用配列に新たに値を書き込む（単点）
    ''' </summary>
    ''' <param name="addHead"></param>
    ''' <param name="singleData"></param>
    ''' <returns></returns>
    Private Function renewNormalMemDataArray(ByVal addHead As UInt16, ByVal singleData As Byte) As Boolean

        'addHeadは16進数を想定
        Dim row As Integer = addHead \ 16
        Dim col As Integer = addHead Mod 16

        If row > 26 Then
            'MessageBox.Show("invalid  vlue:row")
            Return False
        End If

        Try
            normalAreaMemoryArr(row)(col) = singleData
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
        End Try

        Return True
    End Function

    ''' <summary>
    ''' NFCの制御メモリエリア用配列に新たに値を書き込む
    ''' </summary>
    ''' <param name="addHead"></param>
    ''' <param name="data"></param>
    ''' <returns></returns>
    Private Function renewControlMemDataArray(ByVal addHead As UInt16, ByVal data As Byte()) As Boolean

        'addHeadは16進数を想定
        Dim row As Integer = addHead \ 16
        Dim col As Integer = addHead Mod 16

        If row < 27 OrElse row > 32 Then
            'MessageBox.Show("invalid value:row")
            Return False
        End If

        'checksumが不要な分、1バイトだけ有効データ長が長い
        If col + data.Length > 16 Then
            'MessageBox.Show("invalid value:col")
        End If

        For i As Integer = 0 To data.Length - 1
            Try
                controlAreaMemoryArr(row - 27)(col + i) = data(i)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
                Return False
            End Try
        Next
        Return True
    End Function

    ''' <summary>
    ''' NFCの制御メモリエリア用配列に新たに値を書き込む（単点）
    ''' </summary>
    ''' <param name="addHead"></param>
    ''' <param name="singleData"></param>
    ''' <returns></returns>
    Private Function renewControlMemDataArray(ByVal addHead As UInt16, ByVal singleData As Byte) As Boolean

        'addHeadは16進数を想定
        Dim row As Integer = addHead \ 16
        Dim col As Integer = addHead Mod 16

        If row < 27 OrElse row > 32 Then
            'MessageBox.Show("invalid value:row")
            Return False
        End If

        Try
            controlAreaMemoryArr(row - 27)(col) = singleData
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
        End Try

        Return True
    End Function

    ''' <summary>
    ''' ブロック0の書込みプロテクトを解除する（再検査用）
    ''' </summary>
    ''' <returns></returns>
    Private Function resetFeRAMProtect() As Boolean
        lblInstructionDisplay.Text = "書込みプロテクト解除中"
        Me.Refresh()

        trans_mode = False

        If Not retryIsConnectNFC() Then
            errorProcessContent = "NFC接続エラー"
            Return False
        End If
        'ブロック0の読込み確認
        Dim block0Data As Byte() = retryNfcReadLine(&H0)
        If block0Data Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        '読み出しの時にも必要？ないとエラー
        trans_mode = True
        'システムエリアデータの読み出し
        If Not Nfc_rf_read(&H1F0) Then
            Return False
        End If
        If Not Nfc_rf_DealRecvBuffer_Without_Check() Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        Dim systemAreaData As Byte() = New Byte(RecvData.Length - 1) {}
        For i As Integer = 0 To systemAreaData.Length - 1
            systemAreaData(i) = RecvData(i)
        Next

        'データの書き換え
        systemAreaData(0) = 0
        systemAreaData(4) = 0

        trans_mode = True
        If Not Nfc_rf_write_normal_system(&H1F0, systemAreaData) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If
        trans_mode = False
        'ベリファイ
        Dim verifyData As Byte() = New Byte(RecvData.Length - 1) {}
        For i As Integer = 0 To verifyData.Length - 1
            verifyData(i) = RecvData(i)
        Next
        '配列長チェック
        If systemAreaData.Length <> verifyData.Length Then
            errorProcessContent = "NFCベリファイエラー"
            Return False
        End If
        'データ一致チェック
        For i As Integer = 0 To systemAreaData.Length - 1
            If systemAreaData(i) <> verifyData(i) Then
                errorProcessContent = "NFCベリファイエラー"
                Return False
            End If
        Next

        Return True
    End Function

    Private Function retryResetFeRAMProtect() As Boolean
        Dim retryNum As Integer = 5
        '検査開始後、最初の工程のためここで
        'NFCが接続できない場合にタイムアウトで打ち切る
        Dim stpW As Stopwatch = New Stopwatch()
        stpW.Start()

        For i As Integer = 0 To retryNum - 1

            If stpW.ElapsedMilliseconds > 10000 Then
                errorProcessContent = "書き込みプロテクト解除タイムアウト"
                Return False
            End If

            If resetFeRAMProtect() Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Function readFeRAMPID(ByRef pid As Byte()) As Boolean

        'PIDフォーマット**-**-**-**
        '配列長が4以上必要
        If pid.Length < 4 Then
            Return False
        End If

        Sna.NoWire.Nfc_disconnect()
        wait(100)

        '暗号化モード使用
        Sna.NoWire.trans_mode = False

        If Not retryIsConnectNFC() Then
            Return False
        End If

        'PID読込み
        Dim tmpPID As Byte() = retryNfcReadLine(&H0)

        If tmpPID Is Nothing Then
            Return False
        End If

        If tmpPID.Length < 4 Then
            Return False
        End If

        For i As Integer = 0 To 3
            pid(i) = tmpPID(i)
        Next

        Return True

    End Function

    ''' <summary>
    ''' byte()形式のPID,MAC配列を文字列配列(**-**-**-**)に変換する
    ''' </summary>
    ''' <param name="pid_mac"></param>
    ''' <returns></returns>
    Private Function makeStr_PID_MAC(ByVal pid_mac As Byte()) As String

        Dim tmpStr As String = String.Empty

        For i As Integer = 0 To pid_mac.Length - 1

            If i >= pid_mac.Length - 1 Then
                tmpStr = tmpStr & pid_mac(i).ToString("X2")
            Else
                tmpStr = tmpStr & pid_mac(i).ToString("X2") & "-"
            End If
        Next
        Return tmpStr
    End Function

    Private Function readFeRAMMAC(ByRef mac As Byte()) As Boolean
        'MACフォーマット**-**-**-**-**-**
        '配列長が6以上必要
        If mac.Length < 6 Then
            Return False
        End If

        Dim tmpMAC As Byte() = retryNfcReadLine(&H0)

        If tmpMAC Is Nothing Then
            Return False
        End If

        If tmpMAC.Length < 6 Then
            Return False
        End If
        Try
            For i As Integer = 0 To 5
                mac(i) = tmpMAC(i + 5)
            Next
        Catch ex As Exception
            Return False
        End Try


        Return True
    End Function
    ''' <summary>
    ''' 初期値書込みファイルからPID初期値を取得
    ''' </summary>
    ''' <param name="filePath"></param>
    ''' <param name="pid"></param>
    ''' <returns></returns>
    Private Function loadInitPID(ByVal filePath As String, ByRef pid As Byte()) As Boolean

        If pid.Length < 4 Then
            Return False
        End If

        Try
            Dim tmpStr As String()
            Using sr As New IO.StreamReader(filePath, enc_sj)
                tmpStr = sr.ReadLine().Split(","c)
                For i As Integer = 0 To 3
                    pid(i) = Convert.ToByte(tmpStr(i), 16)
                Next
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            Return False
        End Try
        Return True
    End Function


    ''' <summary>
    ''' WEB書込み確認
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function chkWebWrite(ByVal flg As String, ByVal reinspectionFlg As Boolean, ByVal sourceFolder As String, ByVal pathCompImg As String, ByVal ftpAddress As String, ByVal httpAddress As String, ByVal pathFList As String) As Boolean
        '検査ワークの電源投入後ftpサーバに接続できるようになるまで時間がかかるので必要ならwaitを入れる
        'lblInstructionDisplay.Text = "WEBソフトウェア書込み:FTPサーバ立ち上がり待ち"

        lblInstructionDisplay.Text = "WEB書込み確認中"
        Me.Refresh()


        If flg <> "1" Then
            '該当する検査工程のラベルの色を灰色にする
            lblInsResult(6).Text = String.Empty
            lblInsResult(6).BackColor = Color.Gray
            Return True
        End If

        'FTPサーバ立ち上がり待ち
        wait(4000)

        'WEB書込み確認
        Dim pathDataForWeb As String = String.Format("{0}\{1}", PathSlnFile, sourceFolder)

        '転送元フォルダのファイルを確認する
        '元ファイルフォルダのファイル、フォルダを抽出して
        'ファイル一覧と比較
        Dim pathFileList As String = String.Format("{0}\{1}", PathSlnFile, pathFList)
        Dim sourceFileList As List(Of String) = New List(Of String)
        'ファイル一覧を取得
        Using sr As New IO.StreamReader(pathFileList, enc_sj)
            Do While sr.Peek() >= 0
                sourceFileList.Add(sr.ReadLine())
                My.Application.DoEvents()
            Loop
        End Using

        Dim sourceFiles As String() = IO.Directory.GetFiles(pathDataForWeb)
        Dim sourceFolders As String() = IO.Directory.GetDirectories(pathDataForWeb)
        Dim filesNum As Integer = 0
        Dim folderNum As Integer = 0
        '一致ファイル数カウント
        For Each f In sourceFiles
            If sourceFileList.IndexOf(IO.Path.GetFileName(f)) >= 0 Then
                filesNum += 1
            End If
        Next
        For Each f In sourceFolders
            If sourceFileList.IndexOf(IO.Path.GetFileName(f)) >= 0 Then
                folderNum += 1
            End If
        Next

        If folderNum + filesNum <> sourceFileList.Count Then
            errorProcessContent = "ファイル数,フォルダ数不一致"
            Return False
        End If


        'FTPサーバー内ファイルリスト取得
        Dim uriFileList As New Uri(ftpAddress)
        Dim ftpReqFileList As Net.FtpWebRequest = CType(Net.WebRequest.Create(uriFileList), Net.FtpWebRequest)
        ftpReqFileList.Proxy = Nothing
        ftpReqFileList.Credentials = New Net.NetworkCredential("", "")
        ftpReqFileList.Method = Net.WebRequestMethods.Ftp.ListDirectoryDetails
        ftpReqFileList.KeepAlive = False
        ftpReqFileList.UsePassive = True

        'リモートサーバに接続できない->FTPサーバの立ち上がりを待つ必要がある
        Dim fileList As New List(Of String)

        '検査ワーク内のファイルリストを取得
        Try
            Dim ftpResFileList As Net.FtpWebResponse = CType(ftpReqFileList.GetResponse(), Net.FtpWebResponse)
            Using sr As New IO.StreamReader(ftpResFileList.GetResponseStream(), enc_sj)
                Dim res As String = String.Empty
                While sr.Peek() >= 0
                    Dim tmpStrArry = Split(sr.ReadLine(), " ")
                    res = tmpStrArry(tmpStrArry.Length - 1)
                    'If InStr(res, ".") > 0 Then
                    '    fileList.Add(res)
                    'End If
                    fileList.Add(IO.Path.GetFileName(res))
                End While
            End Using
            ftpResFileList.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            writeErrorLog(ex)
            errorProcessContent = "FTPサーバ接続エラー"
            Return False
        End Try

        'FTPサーバ内にフォルダを作成
        'すでにフォルダが存在する場合にはエラーとなるので場合分けを追加
        Try
            For Each fl As String In IO.Directory.GetDirectories(pathDataForWeb)

                Dim dirName As String = IO.Path.GetFileName(fl) 'フォルダ名
                Dim uriMakeDir As New Uri(ftpAddress & dirName) 'フォルダパス

                '転送フォルダが存在する場合にディレクトリを作成するとエラーとなるので回避
                If fileList.IndexOf(dirName) >= 0 Then
                    'フォルダが存在する場合にはディレクトリ作成を行わない
                    Continue For
                End If

                Dim ftpReqMakeDir As Net.FtpWebRequest = CType(Net.WebRequest.Create(uriMakeDir), Net.FtpWebRequest)
                ftpReqMakeDir.Credentials = New Net.NetworkCredential("", "")
                ftpReqMakeDir.Method = Net.WebRequestMethods.Ftp.MakeDirectory

                Dim ftpResMakeDir As Net.FtpWebResponse = CType(ftpReqMakeDir.GetResponse(), Net.FtpWebResponse)
                ftpResMakeDir.Close()
            Next
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "FTPサーバディレクトリ作成エラー"
            Return False
        End Try


        '指定のファイルをアップロード
        Try
            For Each fl As String In IO.Directory.GetFiles(pathDataForWeb, "*", IO.SearchOption.AllDirectories)
                Dim uriUpload As New Uri(ftpAddress & IO.Path.GetFileName(fl))
                Dim ftpReqUp As Net.FtpWebRequest = CType(Net.WebRequest.Create(uriUpload), Net.FtpWebRequest)
                ftpReqUp.Credentials = New Net.NetworkCredential("", "")
                ftpReqUp.Method = Net.WebRequestMethods.Ftp.UploadFile
                ftpReqUp.KeepAlive = False
                ftpReqUp.UseBinary = False
                ftpReqUp.UsePassive = True

                Dim reqStrm As IO.Stream = ftpReqUp.GetRequestStream()
                Dim fs As New IO.FileStream(fl, IO.FileMode.Open, IO.FileAccess.Read)
                Dim buffer(1023) As Byte
                While True
                    Dim readSize As Integer = fs.Read(buffer, 0, buffer.Length)
                    If readSize = 0 Then
                        Exit While
                    End If
                    reqStrm.Write(buffer, 0, readSize)
                End While
                fs.Close()
                reqStrm.Close()

                Dim ftpResUp As Net.FtpWebResponse = CType(ftpReqUp.GetResponse(), Net.FtpWebResponse)
                ftpResUp.Close()
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            writeErrorLog(ex)
            errorProcessContent = "FTPサーバファイルアップロードエラー"
            Return False
        End Try


        'ファイルの存在を確認
        Try
            Dim uriFTPFileList As New Uri(ftpAddress)
            'FtpWebRequestの作成
            Dim ftpReq As Net.FtpWebRequest = CType(Net.WebRequest.Create(uriFTPFileList), Net.FtpWebRequest)
            ftpReq.Method = Net.WebRequestMethods.Ftp.ListDirectoryDetails
            ftpReq.KeepAlive = False '要求完了後に接続を閉じる
            ftpReq.UsePassive = False 'passiveモードを無効

            'FtpWebResponseを取得
            Dim ftpRes As Net.FtpWebResponse = CType(ftpReq.GetResponse(), Net.FtpWebResponse)
            'FTPサーバより送信させたデータを取得
            Dim sr As New IO.StreamReader(ftpRes.GetResponseStream())

            Dim fileListWeb As String() = IO.Directory.GetFiles(pathDataForWeb, "*",
                                                        IO.SearchOption.AllDirectories)

            Dim dirListWeb As String() = IO.Directory.GetDirectories(pathDataForWeb, "*",
                                                        IO.SearchOption.AllDirectories)

            Dim fileListFtp As List(Of String) = New List(Of String)
            Do While sr.Peek() >= 0
                Dim file As String() = sr.ReadLine().Split(" "c)
                Dim fileName As String = file(UBound(file))
                fileListFtp.Add(fileName)
                My.Application.DoEvents()
            Loop

            'ファイル存在チェック
            For Each fw In fileListWeb
                Dim fileName As String = IO.Path.GetFileName(fw)
                If fileListFtp.IndexOf(fileName) >= 0 Then
                    'MessageBox.Show(fileListFtp.Item(fileListFtp.IndexOf(fileName)) & "_____" & fileName)
                Else
                    'MessageBox.Show("ファイルの一部が不足しています。")
                    errorProcessContent = "転送ファイル不一致エラー"
                    Return False
                End If
            Next
            'フォルダ存在チェック
            For Each dw In dirListWeb
                Dim dirName As String = IO.Path.GetFileName(dw)
                If fileListFtp.IndexOf(dirName) >= 0 Then
                    'MessageBox.Show(fileListFtp.Item(fileListFtp.IndexOf(dirName)) & "_____" & dirName)
                Else
                    'MessageBox.Show("フォルダの一部が不足しています。")
                    errorProcessContent = "作成フォルダ不一致エラー"
                    Return False
                End If
            Next
            sr.Close()
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try



        'URLをIEで開く
        frmWebBrowser.WindowState = FormWindowState.Maximized 'フォーム画面の最大化
        frmWebBrowser.WebBrowser1.Navigate(New Uri(httpAddress))
        frmWebBrowser.Focus()
        frmWebBrowser.Visible = True
        frmWebBrowser.Show()

        '表示まで待機
        Do
            If frmWebBrowser.WebBrowser1.ReadyState = WebBrowserReadyState.Complete AndAlso
                    frmWebBrowser.WebBrowser1.IsBusy = False Then
                wait(3000) 'toppage->wireless systemへと変わるのに時間がかかるのでwait
                Exit Do
            End If
            My.Application.DoEvents()
        Loop

        frmWebBrowser.Focus() 'プリントスクリーンでWebBrowserの画面を確実に撮るため

        Dim bmpImg As New Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height)
        Dim g As Graphics = Graphics.FromImage(bmpImg)
        g.CopyFromScreen(New Drawing.Point(0, 0), New Drawing.Point(0, 0), bmpImg.Size)
        g.Dispose()

        Dim img As New Mat()

        Dim correctImg As New Mat(String.Format("{0}\{1}", PathSlnFile, pathCompImg))
        Dim resultImg As New Mat()
        img = Extensions.BitmapConverter.ToMat(bmpImg)
        Cv2.CvtColor(img, img, ColorConversionCodes.BGRA2BGR)


        'wait(3000)
        '指定矩形の範囲のみ画素値の比較
        For j As Integer = 0 To Convert.ToInt32(3 * img.Height / 4) 'タスクバーの部分は除外する必要がある
            For i As Integer = Convert.ToInt32(img.Width / 4) To Convert.ToInt32(3 * img.Width / 4)
                For k As Integer = 0 To 2
                    If img.At(Of Vec3b)(j, i).Item(k) <> correctImg.At(Of Vec3b)(j, i).Item(k) Then
                        errorProcessContent = "比較画像不一致エラー"
                        frmWebBrowser.Visible = False
                        frmWebBrowser.Close()
                        Return False
                    End If
                Next
            Next
        Next

        frmWebBrowser.Visible = False
        frmWebBrowser.Close()


        Me.Show()

        'WEB書込み画面の確認
        lblInsResult(6).Text = "合格"
        lblInsResult(6).BackColor = Color.Lime
        Me.Refresh()
        Return True

    End Function

    ''' <summary>
    ''' FTPサーバ内の1階層分のファイルリストを取得する
    ''' </summary>
    ''' <param name="url">接続先FTPサーバアドレス</param>
    ''' <param name="result">取得ファイル一覧</param>
    ''' <returns></returns>
    Private Function ftpList(ByVal url As String, ByRef result As List(Of String)) As Boolean

        'FTPサーバー内ファイルリスト取得
        Dim uriFileList As New Uri(url)
        Dim ftpReqFileList As Net.FtpWebRequest = CType(Net.WebRequest.Create(uriFileList), Net.FtpWebRequest)
        ftpReqFileList.Proxy = Nothing
        ftpReqFileList.Credentials = New Net.NetworkCredential("", "")
        ftpReqFileList.Method = Net.WebRequestMethods.Ftp.ListDirectory
        ftpReqFileList.KeepAlive = False
        ftpReqFileList.UsePassive = True

        'リモートサーバに接続できない->FTPサーバの立ち上がりを待つ必要がある
        Dim fileList As New List(Of String)

        '検査ワーク内のファイルリストを取得0
        Try
            Dim ftpResFileList As Net.FtpWebResponse = CType(ftpReqFileList.GetResponse(), Net.FtpWebResponse)
            Using sr As New IO.StreamReader(ftpResFileList.GetResponseStream(), enc_sj)
                wait(20) 'ファイルのリストが完全に取得できないことがあるので追加_2017/06/27
                Dim res As String = String.Empty
                While sr.Peek() >= 0
                    Dim tmpStrArry = Split(sr.ReadLine(), " ")
                    res = tmpStrArry(tmpStrArry.Length - 1)
                    'If InStr(res, ".") > 0 Then
                    '    fileList.Add(res)
                    'End If
                    fileList.Add(IO.Path.GetFileName(res))
                End While
            End Using
            ftpResFileList.Close()
        Catch wex As System.Net.WebException 'Exception
            'applicationフォルダでLISTコマンドを実行するとエラーが発生(仕様)
            '[directory does not exist]
            If Not url.Contains("application") Then
                Return False
            End If
            'MessageBox.Show(wex.Message)
        Catch ex As Exception
            Return False
        End Try


        For Each f In fileList
            result.Add(url & f.ToString())
        Next


        Return True
    End Function

    ''' <summary>
    ''' WEB書込み確認検査(FFFTP使用)
    ''' </summary>
    ''' <param name="flg">検査実行フラグ</param>
    ''' <param name="batPath">FFFTP起動バッチファイルパス</param>
    ''' <param name="cmpImgPath">画面比較画像ファイルパス</param>
    ''' <param name="ftpAd">FTPアドレス</param>
    ''' <param name="httpAd">ログインHTTPアドレス</param>
    ''' <param name="fileSourcePath">転送元ファイルパス</param>
    ''' <param name="sFileNum">転送元ファイル数</param>
    ''' <param name="sFolderNum">転送元フォルダ数</param>
    ''' <returns></returns>
    Private Function chkWebWrite2(ByVal flg As String, ByVal batPath As String, ByVal cmpImgPath As String, ByVal ftpAd As String, ByVal httpAd As String, ByVal fileSourcePath As String, ByVal sFileNum As String, sFolderNum As String) As Boolean



        If flg <> "1" Then
            '該当する検査工程のラベルの色を灰色にする
            lblInsResult(6).Text = String.Empty
            lblInsResult(6).BackColor = Color.Gray
            Return True
        End If

        wait(2000) 'FTPサーバ立ち上がり待ち

        lblInstructionDisplay.Text = "WEBソフトウェア書込み確認中"
        Me.Refresh()
        '----------------------------------------------------------
        'FTP接続確認
        '----------------------------------------------------------
        Dim urlTest As String = ftpAd
        Dim listTest As List(Of String) = New List(Of String)
        If Not ftpList(urlTest, listTest) Then
            'MessageBox.Show("connection error")
            errorProcessContent = "FTP接続確認"
            Return False
        End If


        '----------------------------------------------------------
        'ファイル転送(FFFTPミラーリング、バッチ処理)
        '----------------------------------------------------------
        lblInstructionDisplay.Text = "WEBソフトウェアコンテンツ転送中"
        Me.Refresh()

        'バッチファイル実行
        Try
            Using proCmd As New Diagnostics.Process()
                '実行バッチファイル名
                proCmd.StartInfo.FileName = String.Format("{0}\{1}", PathSlnFile, batPath)
                'シェル機能設定
                proCmd.StartInfo.UseShellExecute = False
                'コンソールウィンドウ設定
                proCmd.StartInfo.CreateNoWindow = True
                '標準出力リダイレクト
                proCmd.StartInfo.RedirectStandardOutput = True
                '外部アプリケーション実行
                proCmd.Start()
                'プロセス終了まで待機(最大300秒間)
                proCmd.WaitForExit(300000)
                'プロセス終了確認
                If Not proCmd.HasExited Then
                    'MessageBox.Show("未完了")
                    errorProcessContent = "ミラーリング(バッチ処理)未完了"
                    Return False
                End If

                proCmd.Close()

            End Using
        Catch ex As Exception
            'MessageBox.Show(ex.Message)
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try


        '----------------------------------------------------------
        '転送ファイルの一致確認
        '----------------------------------------------------------
        lblInstructionDisplay.Text = "WEBソフトウェアコンテンツ確認中"
        Me.Refresh()

        wait(2000)
        Try


            'FTPサーバファイル・フォルダリスト取得
            Dim ftpResult As List(Of String) = New List(Of String)
            Dim url As String = ftpAd
            ftpList(url, ftpResult)
            'ftpサーバルートフォルダにあるフォルダを取得
            Dim folderList = From f In ftpResult Where IO.Path.GetFileName(f).IndexOf(".") < 0
            wait(100)

            '第一階層のファイルを入れる
            Dim ftpResult2ndDir As List(Of String) = New List(Of String)
            For Each f In ftpResult
                ftpResult2ndDir.Add(f)
            Next
            wait(100)

            '第二階層のファイル・フォルダを検索
            For Each fl In folderList
                If Not ftpList(fl & "/", ftpResult2ndDir) Then
                    'MessageBox.Show("")
                    'エラー情報の追記
                    errorProcessContent = "LIST:" & fl
                    Return False
                End If
                'MessageBox.Show(fl.ToString())
            Next
            wait(100)

            '第二階層までのファイルを取得
            Dim fileList As List(Of String) = New List(Of String)
            For Each f In ftpResult2ndDir
                If IO.Path.GetFileName(f).IndexOf(".") >= 0 Then
                    fileList.Add(f)
                End If
            Next
            wait(100)


            '転送元ファイルを取得
            '転送元ファイル数・フォルダ数確認
            Dim sourcePath As String = String.Format("{0}\{1}", PathSlnFile, fileSourcePath)
            Dim sFiles As String() = IO.Directory.GetFiles(sourcePath, "*", IO.SearchOption.AllDirectories)
            Dim sFolders As String() = IO.Directory.GetDirectories(sourcePath, "*", IO.SearchOption.AllDirectories)

            Dim fileNum As Integer
            Dim folderNum As Integer

            If Not Integer.TryParse(sFileNum, fileNum) Then
                errorProcessContent = "ファイル数パラメータ異常:" & sFileNum
                Return False
            End If

            If Not Integer.TryParse(sFolderNum, folderNum) Then
                errorProcessContent = "フォルダ数パラメータ異常：" & sFolderNum
                Return False
            End If

            If sFiles.Length <> fileNum Then
                errorProcessContent = "転送元ファイル数不一致_" & sFiles.Length & "_" & fileNum
                Return False
            End If

            If sFolders.Length <> folderNum Then
                errorProcessContent = "転送元フォルダ数不一致_" & sFolders.Length & "_" & folderNum
                Return False
            End If

            Dim count As Integer = 0
            For Each f In sFiles
                Dim tmpStr As String = url & f.Remove(0, sourcePath.Length + 1)
                tmpStr = tmpStr.Replace("\", "/")
                If Not fileList.Contains(tmpStr) Then
                    'MessageBox.Show(f.ToString())
                    errorProcessContent = "フォルダファイル一致確認:" & tmpStr
                    Return False
                End If
                count += 1
            Next
        Catch ex As Exception
            'MessageBox.Show(ex.Message)
            errorProcessContent = "例外発生"
            Return False
        End Try
        wait(100)

        '-----------------------------------------------------------------
        '画面確認
        '----------------------------------------------------------------
        lblInstructionDisplay.Text = "WEBソフトウェア画面確認中"
        Me.Refresh()

        'URLをIEで開く
        Dim httpAddress As String = httpAd
        frmWebBrowser.WindowState = FormWindowState.Maximized

        frmWebBrowser.WebBrowser1.Navigate(New Uri(httpAddress))
        frmWebBrowser.Focus()
        frmWebBrowser.Visible = True
        frmWebBrowser.Show()
        '表示まで待機
        Dim browserTO As Stopwatch = New Stopwatch()
        browserTO.Start()
        Do

            If browserTO.ElapsedMilliseconds > 5000 Then
                errorProcessContent = "ブラウザ立ち上げタイムアウト"
                Return False
            End If

            If frmWebBrowser.WebBrowser1.ReadyState = WebBrowserReadyState.Complete AndAlso
                    frmWebBrowser.WebBrowser1.IsBusy = False Then
                wait(5000) 'toppage->wireless systemへと変わるのに時間がかかるので
                Exit Do
            End If
            My.Application.DoEvents()
        Loop

        frmWebBrowser.Focus() 'プリントスクリーンでWEBBROWSEの画面を確実にとるため

        Dim bmpImg As New Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height)
        Dim g As Graphics = Graphics.FromImage(bmpImg)
        g.CopyFromScreen(New Drawing.Point(0, 0), New Drawing.Point(0, 0), bmpImg.Size)
        g.Dispose()

        Dim img As New Mat()


        Dim correctImg As New Mat(String.Format("{0}\{1}", PathSlnFile, cmpImgPath))
        Dim resultImg As New Mat()
        img = Extensions.BitmapConverter.ToMat(bmpImg)
        Cv2.CvtColor(img, img, ColorConversionCodes.BGRA2BGR)


        'wait(3000)
        '画像比較（黒くマスクした部分を除く）
        For j As Integer = 0 To img.Height - 1 'タスクバーの部分は除外する必要がある
            For i As Integer = img.Width To img.Width - 1

                '黒くマスクした部分
                Dim val As Integer = 0
                For k As Integer = 0 To 2
                    val += correctImg.At(Of Vec3b)(j, i).Item(k)
                Next
                '(i,j)の画素値が黒(0,0,0)なら画素値の比較を行わない
                If val = 0 Then
                    Continue For
                End If

                For k As Integer = 0 To 2

                    If img.At(Of Vec3b)(j, i).Item(k) <> correctImg.At(Of Vec3b)(j, i).Item(k) Then
                        frmWebBrowser.Visible = False
                        frmWebBrowser.Close()
                        'MessageBox.Show("画像不一致")
                        errorProcessContent = "画像不一致"
                        Return False
                    End If
                Next
            Next
        Next
        ''指定矩形の範囲のみ画素値の比較
        'For j As Integer = 0 To Convert.ToInt32(3 * img.Height / 4) 'タスクバーの部分は除外する必要がある
        '    For i As Integer = Convert.ToInt32(img.Width / 4) To Convert.ToInt32(3 * img.Width / 4)
        '        For k As Integer = 0 To 2
        '            If img.At(Of Vec3b)(j, i).Item(k) <> correctImg.At(Of Vec3b)(j, i).Item(k) Then
        '                frmWebBrowser.Visible = False
        '                frmWebBrowser.Close()
        '                'MessageBox.Show("画像不一致")
        '                errorProcessContent = "画像不一致"
        '                Return False
        '            End If
        '        Next
        '    Next
        'Next


        frmWebBrowser.Visible = False
        frmWebBrowser.Close()

        Me.Show()

        'WEB書込み画面の確認
        lblInsResult(6).Text = "合格"
        lblInsResult(6).BackColor = Color.Lime
        Me.Refresh()
        Return True

        Return True
    End Function

    ''' <summary>
    ''' PST Browseコマンド実行
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function pstBrowse(ByVal flg As String) As Boolean
        'PROFINET以外の時は実行不要
        If flg <> "1" Then
            Return True
        End If

        Dim browseCommand As String = String.Format("s7wnpstx -BROWSE", PathSlnFile, pathPstBatBrowse)
        Dim resultStr As String = String.Empty 'コンソール出力結果保持

        'バッチファイル作成
        Try
            Using sw As New IO.StreamWriter(String.Format("{0}{1}", PathSlnFile, pathPstBatBrowse), False, enc_sj)
                sw.WriteLine(browseCommand)
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            Return False
        End Try

        'バッチファイル確認
        Try
            Using sr As New IO.StreamReader(String.Format("{0}{1}", PathSlnFile, pathPstBatBrowse), enc_sj)
                Dim tmpStr As String = sr.ReadLine()
                If tmpStr <> browseCommand Then
                    errorProcessContent = "バッチファイル確認(BROWSE)"
                    Return False
                End If
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            Return False
        End Try

        'バッチファイル実行
        Try
            Using proCmd As New Diagnostics.Process()
                '実行バッチファイル名
                proCmd.StartInfo.FileName = String.Format(String.Format("{0}{1}", PathSlnFile, pathPstBatBrowse))
                'シェル機能設定
                proCmd.StartInfo.UseShellExecute = False
                'コンソールウィンドウ設定
                proCmd.StartInfo.CreateNoWindow = True
                '標準出力リダイレクト
                proCmd.StartInfo.RedirectStandardOutput = True
                '外部アプリケーション実行
                proCmd.Start()

                '標準出力読み取り
                resultStr = proCmd.StandardOutput.ReadToEnd()
                'プロセス終了まで待機(最大5秒間)
                proCmd.WaitForExit(5000)

                'プロセス終了確認
                If Not proCmd.HasExited Then
                    errorProcessContent = "プロセス終了確認(BROWSE)"
                    Return False
                End If

                proCmd.Close()

            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチファイル処理結果を保存
        Try
            Using sw As New IO.StreamWriter(String.Format("{0}{1}", PathSlnFile, pathPstResultBrowse))
                sw.WriteLine(resultStr)
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        Return True

    End Function

    ''' <summary>
    ''' 指定したターゲットのMAC,IP,DeviceNameを取得
    ''' </summary>
    ''' <param name="targetType"></param>
    ''' <param name="targetMAC"></param>
    ''' <param name="targetIP"></param>
    ''' <param name="targetName"></param>
    ''' <returns></returns>
    Private Function getPstBrowseResult(ByVal targetType As String, ByRef targetMAC As String, ByRef targetIP As String, ByRef targetName As String) As Boolean

        Dim isTagrget As Boolean = False 'デバイスが見つかるかチェック

        Try
            Using sr As New IO.StreamReader(String.Format("{0}{1}", PathSlnFile, pathPstResultBrowse))
                Dim tmpStr As String
                Do

                    If sr.EndOfStream Then
                        Exit Do
                    End If

                    tmpStr = sr.ReadLine()

                    If tmpStr.IndexOf(targetType) >= 0 Then

                        isTagrget = True

                        Dim tmpStrArry As String() = tmpStr.Split(":"c)
                        '結果の項目は
                        'Type : MAC : IP : Name
                        If tmpStrArry.Length < 3 Then
                            errorProcessContent = "BROWSEコマンド結果フォーマットエラー"
                            Return False
                        End If

                        targetMAC = tmpStrArry(1).Trim()
                        targetIP = tmpStrArry(2).Trim()
                        targetName = tmpStrArry(3).Trim()

                    End If

                    My.Application.DoEvents()
                Loop
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try


        If Not isTagrget Then
            errorProcessContent = "デバイスが見つかりません"
        End If
        Return isTagrget

    End Function
    ''' <summary>
    ''' PST NAMEコマンド(デバイスネーム変更)
    ''' </summary>
    ''' <param name="MAC"></param>
    ''' <param name="deviceName"></param>
    ''' <returns></returns>
    Private Function pstName(ByVal MAC As String, ByVal deviceName As String) As Boolean
        'NAMEコマンド
        Dim nameCommand As String = String.Format("s7wnpstx {0} -NAME={1}", MAC, deviceName)
        Dim resultStr As String = String.Empty

        'バッチファイル作成
        Try
            Using sw As New IO.StreamWriter(String.Format("{0}{1}", PathSlnFile, pathPstBatName), False, enc_sj)
                sw.WriteLine(nameCommand)
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチファイル確認
        Try
            Using sr As New IO.StreamReader(String.Format("{0}{1}", PathSlnFile, pathPstBatName), enc_sj)
                Dim tmpStr As String = sr.ReadLine()
                If tmpStr <> nameCommand Then
                    errorProcessContent = "バッチファイル確認(NAME)"
                    Return False
                End If
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチファイル実行
        Try
            Using proCmd As New Diagnostics.Process()
                '実行バッチファイル名
                proCmd.StartInfo.FileName = String.Format(String.Format("{0}{1}", PathSlnFile, pathPstBatName))
                'シェル機能設定
                proCmd.StartInfo.UseShellExecute = False
                'コンソールウィンドウ設定
                proCmd.StartInfo.CreateNoWindow = True
                '標準出力リダイレクト
                proCmd.StartInfo.RedirectStandardOutput = True
                '外部アプリケーション実行
                proCmd.Start()

                '標準出力読み取り
                resultStr = proCmd.StandardOutput.ReadToEnd()
                'プロセス終了まで待機(最大10秒間)
                proCmd.WaitForExit(10000)

                'プロセス終了確認
                If Not proCmd.HasExited Then
                    errorProcessContent = "プロセス終了確認(NAME)"
                    Return False
                End If

                proCmd.Close()

            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチファイル処理結果を保存
        Try
            Using sw As New IO.StreamWriter(String.Format("{0}{1}", PathSlnFile, pathPstResultNAME))
                sw.WriteLine(resultStr)
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチ処理結果を確認
        Try
            Using sr As New IO.StreamReader(String.Format("{0}{1}", PathSlnFile, pathPstResultNAME))
                Do
                    If sr.EndOfStream Then
                        Exit Do
                    End If

                    Dim tmpStr As String = sr.ReadLine()

                    If tmpStr.IndexOf("name:") >= 0 Then
                        Dim tmpStrArry As String() = tmpStr.Split(":"c)
                        'name: ***
                        If tmpStrArry.Length < 1 Then
                            Return False
                        End If
                        'device name が所定の値であるか確認
                        If tmpStrArry(1).Trim() <> deviceName Then
                            errorProcessContent = "バッチ結果確認(デバイスネーム)"
                            Return False
                        End If

                    End If

                    '最終行のコマンド成功文字列を確認
                    If tmpStr.IndexOf("Parameters successfully transmitted") >= 0 Then
                        errorProcessContent = "バッチ結果確認(コマンド成功文字列(NAME))"
                        Return True
                    End If

                    My.Application.DoEvents()
                Loop
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        Return False

    End Function

    ''' <summary>
    ''' PST IPアドレス設定
    ''' </summary>
    ''' <param name="MAC"></param>
    ''' <param name="IP"></param>
    ''' <param name="subnetmask"></param>
    ''' <returns></returns>
    Private Function pstIP(ByVal MAC As String, ByVal IP As String, ByVal subnetmask As String) As Boolean
        'IP設定コマンド
        Dim ipCommand As String = String.Format("s7wnpstx {0} {1} {2}", MAC, IP, subnetmask)
        Dim resultStr As String = String.Empty

        'バッチファイル作成
        Try
            Using sw As New IO.StreamWriter(String.Format("{0}{1}", PathSlnFile, pathPstBatIP), False, enc_sj)
                sw.WriteLine(ipCommand)
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチファイル確認
        Try
            Using sr As New IO.StreamReader(String.Format("{0}{1}", PathSlnFile, pathPstBatIP), enc_sj)
                Dim tmpStr As String = sr.ReadLine()
                If tmpStr <> ipCommand Then
                    errorProcessContent = "バッチファイル確認(IP)"
                    Return False
                End If
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチファイル実行
        Try
            Using proCmd As New Diagnostics.Process()
                '実行バッチファイル名
                proCmd.StartInfo.FileName = String.Format(String.Format("{0}{1}", PathSlnFile, pathPstBatIP))
                'シェル機能設定
                proCmd.StartInfo.UseShellExecute = False
                'コンソールウィンドウ設定
                proCmd.StartInfo.CreateNoWindow = True
                '標準出力リダイレクト
                proCmd.StartInfo.RedirectStandardOutput = True
                '外部アプリケーション実行
                proCmd.Start()

                '標準出力読み取り
                resultStr = proCmd.StandardOutput.ReadToEnd()
                'プロセス終了まで待機(最大10秒間)
                proCmd.WaitForExit(10000)

                'プロセス終了確認
                If Not proCmd.HasExited Then
                    errorProcessContent = "プロセス終了確認(IP)"
                    Return False
                End If

                proCmd.Close()

            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチファイル処理結果を保存
        Try
            Using sw As New IO.StreamWriter(String.Format("{0}{1}", PathSlnFile, pathPstResultIP))
                sw.WriteLine(resultStr)
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチ処理結果を確認
        Try
            Using sr As New IO.StreamReader(String.Format("{0}{1}", PathSlnFile, pathPstResultIP))
                Do
                    If sr.EndOfStream Then
                        Exit Do
                    End If

                    Dim tmpStr As String = sr.ReadLine()

                    '最終行のコマンド成功文字列を確認
                    If tmpStr.IndexOf("Parameters successfully transmitted") >= 0 Then
                        errorProcessContent = "コマンド成功文字列確認(IP)"
                        Return True
                    End If

                    My.Application.DoEvents()
                Loop
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        Return False
    End Function

    ''' <summary>
    ''' PST RESETコマンド
    ''' </summary>
    ''' <param name="mac"></param>
    ''' <returns></returns>
    Private Function pstReset(ByVal mac As String) As Boolean
        'IP設定コマンド
        Dim resetCommand As String = String.Format("s7wnpstx {0} -RESET", mac)
        Dim resultStr As String = String.Empty

        'バッチファイル作成
        Try
            Using sw As New IO.StreamWriter(String.Format("{0}{1}", PathSlnFile, pathPstBatReset), False, enc_sj)
                sw.WriteLine(resetCommand)
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチファイル確認
        Try
            Using sr As New IO.StreamReader(String.Format("{0}{1}", PathSlnFile, pathPstBatReset), enc_sj)
                Dim tmpStr As String = sr.ReadLine()
                If tmpStr <> resetCommand Then
                    errorProcessContent = "バッチファイル確認(RESET)"
                    Return False
                End If
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチファイル実行
        Try
            Using proCmd As New Diagnostics.Process()
                '実行バッチファイル名
                proCmd.StartInfo.FileName = String.Format(String.Format("{0}{1}", PathSlnFile, pathPstBatReset))
                'シェル機能設定
                proCmd.StartInfo.UseShellExecute = False
                'コンソールウィンドウ設定
                proCmd.StartInfo.CreateNoWindow = True
                '標準出力リダイレクト
                proCmd.StartInfo.RedirectStandardOutput = True
                '外部アプリケーション実行
                proCmd.Start()

                '標準出力読み取り
                resultStr = proCmd.StandardOutput.ReadToEnd()
                'プロセス終了まで待機(最大10秒間)
                proCmd.WaitForExit(10000)

                'プロセス終了確認
                If Not proCmd.HasExited Then
                    errorProcessContent = "バッチファイル終了確認(RESET)"
                    Return False
                End If

                proCmd.Close()

            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチファイル処理結果を保存
        Try
            Using sw As New IO.StreamWriter(String.Format("{0}{1}", PathSlnFile, pathPstResultReset))
                sw.WriteLine(resultStr)
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        'バッチ処理結果を確認
        Try
            Using sr As New IO.StreamReader(String.Format("{0}{1}", PathSlnFile, pathPstResultReset))
                Do
                    If sr.EndOfStream Then
                        Exit Do
                    End If

                    Dim tmpStr As String = sr.ReadLine()

                    '最終行のコマンド成功文字列を確認
                    If tmpStr.IndexOf("Parameters successfully transmitted") >= 0 Then
                        errorProcessContent = "コマンド成功文字列確認(RESET)"
                        Return True
                    End If

                    My.Application.DoEvents()
                Loop
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            errorProcessContent = "例外発生"
            Return False
        End Try

        Return False
    End Function

    '-----------------------------------------------------------------------------------
    '
    '                      検査準備段階用の検査関数
    '
    '-----------------------------------------------------------------------------------


    Private Function offOutputPSoC() As Boolean
        If Not Master_Pow(0) Then Return False

        If Not Hub_Pow(0) Then Return False

        If Not Reserve0_Out(0) Then Return False

        If Not AC_Relay(0) Then Return False

        If Not EX260_Pow(0) Then Return False

        If Not EX600_Pow(0) Then Return False

        If Not Slave_Pow(0) Then Return False

        If Not Reserve1_Out(0) Then Return False

        If Not Buzzer(0) Then Return False

        If Not SI_change170V(0) Then Return False

        If Not SI_change240V(0) Then Return False

        If Not SI_change223V(0) Then Return False

        If Not Reserve2_Out(0) Then Return False

        If Not Reserve3_Out(0) Then Return False

        If Not Reserve4_Out(0) Then Return False

        Return True
    End Function

    ''' <summary>
    ''' 無線モジュールシリアル番号読込み
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function insReadMlodueSerialNo(ByVal flg As String, ByVal reinspectionFlg As Boolean) As Boolean

        lblInstructionDisplay.Text = "無線モジュールシリアル番号読込み"

        If flg <> "1" Then
            '検査不要
            lblInsResult(0).Text = String.Empty
            lblInsResult(0).BackColor = Color.Gray
            Return True
        End If

        '無線モジュールシリアル番号の読み込み(string)
        Dim iSerialNo As String = readInspectionSerialNo()

        '読み込み失敗時==Nothing
        If iSerialNo Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        '適正な値か判定(0x00000000は不正な値)
        If iSerialNo = "00000000" Then
            errorProcessContent = "無線モジュールシリアル番号エラー：00000000"
            Return False
        End If


        '[無線モジュール検査の完了フラグ読み込み]
        Dim ModuleDoneFlgs As Byte() = retryReadModuleInspectionFlag()

        If ModuleDoneFlgs Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        If ModuleDoneFlgs(0) <> 1 Then
            errorProcessContent = "無線モジュール検査未完了エラー" & "_フラグ" & ModuleDoneFlgs(0)
            Return False
        End If

        '--------------------------------------------------------------------------------------
        '無線モジュール検査の履歴確認はしない_2017/7/12
        'サンプルワークなどのモジュール検査シリアル番号が検査履歴にないため（開発５部出荷品）
        '--------------------------------------------------------------------------------------
        '前工程(無線モジュール検査)検査履歴を調べる
        'If Not IO.File.Exists(String.Format("{0}{1}.txt", pathHistoryModuleInspection, iSerialNo)) Then
        '    errorProcessContent = "無線モジュール検査履歴ファイルなし" & ":" & String.Format("{0}{1}{2}.txt", PathSlnFile, pathHistoryModuleInspection, iSerialNo)
        '    Return False
        'End If

        ''無線モジュール検査履歴の[総合判定]の場所
        'Dim judgeIndex As Integer = -1
        'Try
        '    Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}.txt", pathHistoryModuleInspection, iSerialNo), enc_sj)
        '        Dim tmpStrArry As String() = sr.ReadLine().Split(","c)
        '        judgeIndex = Array.IndexOf(tmpStrArry, "総合判定")

        '        If judgeIndex < 0 Then
        '            errorProcessContent = "無線モジュール検査履歴ファイルフォーマット([総合判定]なし)"
        '            Return False
        '        End If
        '        '最新の検査結果が合格かどうかをチェックする
        '        While sr.Peek() >= 0
        '            tmpStrArry = sr.ReadLine().Split(","c)
        '            My.Application.DoEvents()
        '        End While

        '        If tmpStrArry(judgeIndex) <> "合格" Then
        '            errorProcessContent = "無線モジュール検査不合格品"
        '            Return False
        '        End If

        '    End Using
        'Catch ex As Exception
        '    writeErrorLog(ex)
        '    errorProcessContent = "例外発生"
        '    Return False
        'End Try

        '再検査品の場合は検査履歴のモジュール検査シリアル番号が読み出した値と一致するか確認
        If reinspectionFlg Then
            '無線モジュール検査履歴の[無線モジュールシリアル番号]の場所
            Dim noIndex As Integer = -1
            Try
                Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}${2}.txt", pathHistory, lblMAC.Text, lblPID.Text), enc_sj)
                    Dim tmpStrArry As String() = sr.ReadLine().Split(","c)
                    Dim tmpSerialNo As String = String.Empty '検査履歴内のモジュールシリアル番号

                    noIndex = Array.IndexOf(tmpStrArry, "無線モジュール検査シリアルNo")

                    If noIndex < 0 Then
                        errorProcessContent = "無線モジュール検査履歴ファイルフォーマット([無線モジュール検査シリアルNo]なし)"
                        Return False
                    End If
                    '最新の検査結果を取得
                    While sr.Peek() >= 0
                        tmpStrArry = sr.ReadLine().Split(","c)
                        If tmpStrArry(noIndex) <> String.Empty Then
                            tmpSerialNo = tmpStrArry(noIndex)
                        End If
                        My.Application.DoEvents()
                    End While

                    '検査不合格により履歴の番号が空白の場合があるので除外
                    If tmpStrArry(noIndex) <> iSerialNo AndAlso tmpStrArry(noIndex) <> String.Empty Then
                        errorProcessContent = String.Format("無線モジュールシリアル番号不一致(ワーク:{0}_検査履歴:{1})", iSerialNo, tmpSerialNo)
                        MessageBox.Show("読み出したシリアル番号と検査履歴内のシリアル番号が異なります。" & ControlChars.CrLf &
                                        "ID銘板が重複している可能性があります。" & ControlChars.CrLf &
                                        "読み出したシリアル番号：" & iSerialNo & ControlChars.CrLf &
                                        "検査履歴内シリアル番号：" & tmpSerialNo,
                                        "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return False
                    End If

                End Using
            Catch ex As Exception
                writeErrorLog(ex)
                errorProcessContent = "例外発生"
                Return False
            End Try
        End If


        lblModuleSerialNo.Text = iSerialNo
        lblInsResult(0).Text = "合格" 'iSerialNo
        lblInsResult(0).BackColor = Color.Lime

        Return True
    End Function

    ''' <summary>
    ''' FeRAM初期値書込み(ブロックH0~H1A0,システムメモリエリアには書込みしない)
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <param name="workType"></param>
    ''' <returns></returns>
    Private Function insFeRAMInitWrite(ByVal flg As String, ByVal workType As String, ByVal reInspectionFlag As Boolean) As Boolean

        lblInstructionDisplay.Text = "FeRAM初期値書込み"

        If flg <> "1" Then
            lblInsResult(1).Text = String.Empty
            lblInsResult(1).BackColor = Color.Gray
            Return True
        End If

        '->検査ワーク電源をOFFにする
        If Not USBIO.EX600_Pow(0) Then
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If

        If Not retryIsConnectNFC() Then
            errorProcessContent = "NFC接続エラー"
            Return False
        End If


        '->初期値読み込み
        If Not readFeRAMInitFile(String.Format("{0}{1}{2}.txt", PathSlnFile, pathInitFeRAMFolder, workType)) Then
            errorProcessContent = "NFC初期値書込みエラー"
            Return False
        End If


        '->書込み情報の更新(無線モジュールシリアル番号、タグ情報(製品品番)、検査モード)

        '無線モジュールシリアル番号
        Dim NWMSerialNo As Byte() = New Byte((lblModuleSerialNo.Text.Length \ 2) - 1) {}
        For i As Integer = 0 To NWMSerialNo.Length - 1
            NWMSerialNo(i) = Convert.ToByte(lblModuleSerialNo.Text.Substring(i * 2, 2), 16)
        Next

        If Not renewNormalMemDataArray(&H80, NWMSerialNo) Then
            errorProcessContent = "NFC初期値書込みエラー"
            Return False
        End If


        'タグ情報(製品品番)
        Dim tagInfo As Byte() = New Byte(15) {}

        '再検査の場合には元のタグ情報をいれる
        If reInspectionFlag Then
            tagInfo = retryNfcReadLine(&H20)
            If tagInfo Is Nothing Then
                errorProcessContent = "NFC初期値書込みエラー"
                Return False
            End If

            '初回の検査ではデフォルト値（製品品番）
        Else
            For i As Integer = 0 To tagInfo.Length - 1
                '製品品番：15文字で末尾に0x00:NULLが入る->tagInfo(14) == 0(配列初期値=0のままでOK)
                'If i > workType.Length - 1 Then
                '    Exit For
                'End If
                'tagInfo(i) = Convert.ToByte(Asc(workType.Substring(i, 1))) '1文字ずつ製品品番をアスキーコードに変換(書込みのため)
                tagInfo(i) = normalAreaMemoryArr(2)(i)
            Next
        End If


        If Not renewNormalMemDataArray(&H20, tagInfo) Then
            errorProcessContent = "NFC初期値書込みエラー"
            Return False
        End If


        '->EX600製品検査モード選択(0x0011, 0x55:EX600製品検査, 0xAA:無線製品検査, 0x00:通常モード)
        If Not renewNormalMemDataArray(&HB, &H55) Then
            errorProcessContent = "NFC初期値書込みエラー"
            Return False
        End If


        '->初期値データの書き込み
        '通常メモリエリア
        For i As Integer = 0 To 26 '全ての通常メモリエリアに書込みを行う
            '0x080:無線モジュールシリアル番号の書き込みを追加(初期化用ファイルで上書きしてしまうのを防止)
            If Not retryNfcWriteLine(Convert.ToUInt16(16 * i), normalAreaMemoryArr(i)) Then
                errorProcessContent = "NFC書込みエラー"
                Return False
            End If
        Next

        '->ベリファイ
        '通常メモリエリア
        For i As Integer = 0 To 26
            '2つの配列の頭から一致が15未満->エラー(有効データ長:15, 16番目はチェックサムなので考慮しない)

            Dim verifyData As Byte() = nfcReadLine(Convert.ToUInt16(16 * i))
            If verifyData Is Nothing Then
                errorProcessContent = "NFC読込みエラー"
                Return False
            End If

            If verifyLineData(normalAreaMemoryArr(i), verifyData) < 15 Then
                errorProcessContent = "ベリファイエラー"
                Return False
            End If
        Next
        lblInsResult(1).Text = "合格"
        lblInsResult(1).BackColor = Color.Lime
        Me.Refresh()
        Return True

    End Function

    ''' <summary>
    ''' 検査準備
    ''' </summary>
    ''' <returns></returns>
    Private Function setupInspection(ByVal reinspectionFlg As Boolean) As Boolean

        Dim swModuleRead As Stopwatch = New Stopwatch()
        swModuleRead.Start()

        '各検査前に検査状態（過電流ワーク検知などを確認）
        Dim insState As String = String.Empty
        If Not chkInspectionState(insState) Then
            lblInsResult(0).BackColor = Color.Red
            lblInsResult(0).Text = "不合格:" & insState
            errorProcessName = "検査前確認"
            errorProcessContent = insState
            Return False
        End If


        Sna.NoWire.trans_mode = False

        '書込みプロテクト後にエラーが発生し履歴が保存されなかったときのために
        '再検査でなくともプロテクトの解除をかける
        If Not retryResetFeRAMProtect() Then
            lblInsResult(0).BackColor = Color.Red
            lblInsResult(0).Text = "不合格"
            errorProcessName = "書込みプロテクト解除初期化"
            Return False
        End If

        'PID,MACのチェック
        'ワークFeRAM内のPID,MACをバーコードを比較
        Dim bytPID As Byte() = New Byte(3) {}
        Dim strPID As String = String.Empty
        Dim bytInitPID As Byte() = New Byte(3) {}
        Dim strInitPID As String = String.Empty

        If Not readFeRAMPID(bytPID) Then
            MessageBox.Show("ワークFeRAM内のPIDが読み込めません" & ControlChars.NewLine &
                            "ID銘板読込みに戻ります。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            errorProcessName = "検査前チェック"
            errorProcessContent = "PID読込み"
            Return False
        End If

        '初期値ファイルのPIDを取得
        If Not loadInitPID(String.Format("{0}{1}{2}.txt", PathSlnFile, pathInitFeRAMFolder, Params.FeRAMWriteInitVal(2)), bytInitPID) Then
            MessageBox.Show("初期値書込ファイルを読み込めません" & ControlChars.NewLine &
                            "ID銘板読込みに戻ります。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            errorProcessName = "検査前チェック"
            errorProcessContent = "初期値ファイル読込み"
            Return False
        End If

        strPID = makeStr_PID_MAC(bytPID)
        strInitPID = makeStr_PID_MAC(bytInitPID)

        '開発5部出荷品フラグを初期化する
        isDiv5Product = False

        'バーコードと読みこんだ値を比較
        '未検査状態では初期値（全て0）なので除外
        If strPID <> lblPID.Text Then
            If strPID <> "00-00-00-00" AndAlso strPID <> strInitPID Then
                MessageBox.Show("ID銘板と異なるPIDが書き込まれています。" & ControlChars.NewLine &
                                "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                "ID銘板読込みに戻ります。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                errorProcessName = "検査前チェック"
                errorProcessContent = "ID銘板と異なるPID"
                Return False
            End If

            'ID銘板と同じPIDかつ初期値以外のPIDが書き込まれた上で検査履歴に履歴が無い->開発出荷品
        Else

            If strPID <> "00-00-00-00" AndAlso strPID <> strInitPID Then
                If Not IO.File.Exists(String.Format("{0}\{1}${2}.txt", pathHistory, lblMAC.Text, lblPID.Text)) Then
                    'Dim tmpAns As DialogResult = MessageBox.Show("開発5部出荷品です。" & ControlChars.NewLine &
                    '                                            "開発担当者に連絡してください。" & ControlChars.NewLine &
                    '                "                           ID銘板読込みに戻ります。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)

                    isDiv5Product = True

                    Dim tmpAns As DialogResult = MessageBox.Show("開発5部出荷品です。" & ControlChars.NewLine &
                                                                "[はい]を押すとID銘板読込みに戻ります。" & ControlChars.NewLine &
                                                                "[いいえ]を押すと検査を続行します。",
                                                                "確認事項", MessageBoxButtons.YesNo, MessageBoxIcon.Error)
                    If tmpAns = DialogResult.Yes Then
                        errorProcessName = "検査前チェック"
                        errorProcessContent = "開発5部出荷品"
                        Return False
                    Else
                        'do nothing
                    End If

                End If
            End If
        End If



        'マスタユニットの場合にはMACのチェックも行う
        If Params.ProductType(4) = "1" Then
            Dim bytMAC As Byte() = New Byte(5) {}
            Dim strMAC As String = String.Empty
            If Not readFeRAMMAC(bytMAC) Then
                MessageBox.Show("ワークFeRAM内のMACが読み込めません" & ControlChars.NewLine &
                                "ソフトを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                errorProcessName = "検査前チェック"
                errorProcessContent = "MAC読込み"
                Return False
            End If
            strMAC = makeStr_PID_MAC(bytMAC)
            If strMAC <> lblMAC.Text Then
                If strMAC <> "00-00-00-00-00-00" Then
                    MessageBox.Show("ID銘板と異なるMACが書き込まれています。" & ControlChars.NewLine &
                                    "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                    "ID銘板読込みに戻ります。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    errorProcessName = "検査前チェック"
                    errorProcessContent = "ID銘板と異なるMAC"
                    Return False
                End If
                '初期値以外のPIDが書き込まれた上で検査履歴に履歴が無い->開発出荷品
            Else
                If strMAC <> "00-00-00-00-00-00" Then
                    If Not IO.File.Exists(String.Format("{0}\{1}${2}.txt", pathHistory, lblMAC.Text, lblPID.Text)) Then

                        'Dim tmpAns As DialogResult = MessageBox.Show("開発5部出荷品です。" & ControlChars.NewLine &
                        '                                            "開発担当者に連絡してください。" & ControlChars.NewLine &
                        '                "                           ID銘板読込みに戻ります。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)

                        'PIDのところで開発５部出荷品判定している場合にはメッセージ表示しない
                        If Not isDiv5Product Then
                            isDiv5Product = True
                            Dim tmpAns As DialogResult = MessageBox.Show("開発5部出荷品です。" & ControlChars.NewLine &
                                                                "[はい]を押すとID銘板読込みに戻ります。" & ControlChars.NewLine &
                                                                "[いいえ]を押すと検査を続行します。",
                                                                "確認事項", MessageBoxButtons.YesNo, MessageBoxIcon.Error)
                            If tmpAns = DialogResult.Yes Then
                                errorProcessName = "検査前チェック"
                                errorProcessContent = "開発5部出荷品"
                                Return False
                            Else
                                'do nothing
                            End If
                        End If
                    End If
                End If
            End If
        End If

        '再検査品の場合はここでシリアル番号保持するためを読み出しておく
        '開発５部出荷品の検査の場合もシリアル番号を読み出す
        If (reinspectionFlg OrElse isDiv5Product) AndAlso Params.ProductType(4) = "1" Then

            Dim tmpByt As Byte()
            If Not retryIsConnectNFC() Then
                Return False
            End If

            tmpByt = retryNfcReadLine(&H0)
            If tmpByt Is Nothing Then
                Return False
            End If

            '通常モードか確認
            '&H4のb5,b4 == 01 -> 通常モード
            If (tmpByt(4) And &H10) <> &H0 Then
                '電源を入れてシリアル番号を読む
                If Not USBIO.EX600_Pow(1) Then
                    Return False
                End If

                wait(1000)

                Dim bytSerialNo As Byte() = New Byte(3) {}
                If Not readSerailNo(bytSerialNo) Then
                    Return False
                End If

                Dim tmpSerialNo As String = String.Empty
                For i As Integer = 0 To bytSerialNo.Length - 1
                    tmpSerialNo &= bytSerialNo(i).ToString("X2")
                Next

                '2017_0627追加
                '通常モード変更後、シリアル番号書込み前で不合格になった場合には
                'シリアル番号が書かれていないのでチェックする

                '製品品番に対応したシリアル番号(頭2文字は製品ごとの値)が書かれているかをチェックする
                Dim correctSerial As String = loadSerialNo(Params.WriteSerial(1), Params.WriteSerial(2), Params.WriteSerial(3))
                If tmpSerialNo.Substring(0, 2) = correctSerial.Substring(0, 2) Then
                    hasSerialNo = True
                    lblSerialNo.Text = tmpSerialNo
                End If

                If Not USBIO.EX600_Pow(0) Then
                    Return False
                End If
                wait(100)

            Else
                '直前の検査で通常モード以外の時ID書込み工程より前に不合格の場合
                '履歴ファイルをチェックした段階で検査履歴内のシリアル番号を持ってくる
                'シリアル番号を書き込む前で不合格なら通常のシリアル番号読込み
            End If
        End If

        Sna.NoWire.trans_mode = False

        '無線モジュールシリアル番号読込み
        If Not insReadMlodueSerialNo(Params.ReadNModuleSeraialNo(1), reinspectionFlg) Then
            lblInsResult(0).Text = "不合格"
            lblInsResult(0).BackColor = Color.Red
            errorProcessName = "無線モジュールシリアル番号読込み"
            Return False
        End If
        swModuleRead.Stop()
        insTimeContent &= swModuleRead.ElapsedMilliseconds.ToString("0.0")

        Dim swFeRAMInit As Stopwatch = New Stopwatch()
        swFeRAMInit.Start()

        'FeRAM初期値書込
        If Not insFeRAMInitWrite(Params.FeRAMWriteInitVal(1), Params.FeRAMWriteInitVal(2), reinspectionFlg) Then

            lblInsResult(1).Text = "不合格"
            lblInsResult(1).BackColor = Color.Red
            errorProcessName = "FeRAM初期値書込み"
            Return False
        End If

        swFeRAMInit.Stop()
        insTimeContent &= "," & swFeRAMInit.ElapsedMilliseconds.ToString("0.0")

        Return True
    End Function



    '--------------------------------------------------------
    '
    '               製品検査用関数
    '
    '--------------------------------------------------------

    ''' <summary>
    ''' 治具ユニットの電源を切替える
    ''' </summary>
    ''' <returns></returns>
    Private Function insUnitPower(ByVal unitType As String, ByVal val As Byte) As Boolean
        'マスターユニットかどうかの判別(master unit flag)

        Dim powerVal As Byte = Convert.ToByte(If(val = 1, 1, 0))

        If unitType = "1" Then
            If Not USBIO.Master_Pow(powerVal) Then
                Return False
            End If

        Else
            If Not USBIO.Slave_Pow(powerVal) Then
                Return False
            End If
        End If
        Return True
    End Function


    ''' <summary>
    ''' EX600通常検査（テストプログラム）
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function insEX600Nomal(ByVal flg As String) As Boolean
        '--------------------------------------------
        '               製品検査
        'No.7[FeRAM検査]製品側で検査
        'No.8[アドレス通信検査]製品側で検査
        'No.9[内部通信検査]製品側で検査
        'No.10[バルブ短絡検査]製品側で検査
        'No.11[バルブ出力検査]製品側で検査
        'No.12[電源異常検出検査]製品側で検査
        'No.13[フィールドバス検査]製品側で検査
        '--------------------------------------------

        If flg <> "1" Then
            lblInsResult(4).Text = String.Empty
            lblInsResult(4).BackColor = Color.Gray
            Return True
        End If

        lblInstructionDisplay.Text = "製品検査中" & ControlChars.CrLf & "ブレークSWを押すと検査を中止します。"

        Dim seg7Data As Byte = &H0 '7seg出力格納（最新の値）
        Dim lastSeg7Data As Byte = &H0 '7seg出力格納（前回の値）
        Dim seg7Timer As Stopwatch = New Stopwatch()
        Dim wait7Seg As Integer = 1000 '7セグ値を一定と見なす閾値
        Dim inspectionStepNo As Integer = 0 '検査ステップ番号
        Dim breakSWData As Byte 'ブレークSWの値格納
        Dim busoutChk As Boolean = False 'BUSOUT確認の合否
        Dim busoutTimer As Stopwatch = New Stopwatch() 'BUSOUT確認用タイムアウト計測
        Dim busoutTO As Integer = 30000 'BUSOUT確認タイムアウト閾値[ms]
        Dim stwSeg7TO As Stopwatch = New Stopwatch() '７セグ確認のタイムアウト計測
        Dim seg7TOVal As Integer = 60000 '7セグ確認検査タイムアウト閾値[ms]

        'FB通信タイムアウト、BUSOUT確認タイムアウト解消のため
        '検証用
        If Not USBIO.AC_Relay(0) Then
            lblInsResult(4).Text = "不合格"
            errorProcessContent = "PSoC通信エラー"
            errorProcessName = "通常製品検査"
            Return False
        End If
        wait(500)
        If Not USBIO.AC_Relay(1) Then
            lblInsResult(4).Text = "不合格"
            errorProcessContent = "PSoC通信エラー"
            errorProcessName = "通常製品検査"
            Return False
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''

        seg7Timer.Start()
        stwSeg7TO.Start()

        '初期化
        If Not USBIO.Seg7(seg7Data) Then
            Return False
        End If

        Do
            'タイムアウト判定
            If stwSeg7TO.ElapsedMilliseconds > seg7TOVal Then
                lblInsResult(4).Text = "不合格:タイムアウト"
                errorProcessContent = "通常製品検査タイムアウト"
                errorProcessName = "通常製品検査タイムアウト"
                Return False
            End If

            '7セグの値更新、前回の値保持
            lastSeg7Data = seg7Data
            If Not USBIO.Seg7(seg7Data) Then
                Return False
            End If


            If seg7Data <> lastSeg7Data Then

                Dim highVal As Byte = If(seg7Data > lastSeg7Data, seg7Data, lastSeg7Data)
                Dim lowVal As Byte = If(seg7Data <= lastSeg7Data, seg7Data, lastSeg7Data)

                '[エラーコードXX:左側は0~A(LED検査時のみA),右側はA~F]
                'フィールドバス検査からLED検査の9Aを除くためlowVal<=8
                '7SEG消灯後の0も除く

                If highVal >= &HA AndAlso lowVal <= &H8 AndAlso lowVal >= &H0 Then
                    Dim errorCode As String = lowVal.ToString("X") & highVal.ToString("X")
                    'ラベルにエラーコードの表示
                    lblInsResult(4).Text = "不合格:" & errorCode
                    lblInsResult(4).BackColor = Color.Red
                    errorProcessContent = errorCode & errorCodeContent(errorCode)
                    Return False
                Else
                    '通常の検査ステップの切り替わり(e.g. "56", "9A"など)
                    'do nothing
                End If

                lastSeg7Data = seg7Data
                seg7Timer.Reset() '7セグ値が一定時間変化しないかを見ているので変化した際にはタイマーリセット
                seg7Timer.Start()
                Continue Do
            End If

            'BUSOUT確認(予備入力への入力待ち)
            Dim bytBusout As Byte
            If Not USBIO.Reserve1_In(bytBusout) Then
                errorProcessContent = "PSoC通信エラー"
                Return False
            End If

            If bytBusout = 0 Then
                busoutChk = True 'BUSOUT確認OK
            End If


            '一定時間同じ7Seg値
            If seg7Timer.ElapsedMilliseconds > wait7Seg Then
                inspectionStepNo = seg7Data
                lblInsResult(4).Text = inspectionStepNo.ToString("X2")
                seg7Timer.Restart()

                'フィールドバス検査
                'BUSOUT確認の時間計測
                If seg7Data >= 9 Then
                    busoutTimer.Start()
                End If

                If seg7Data >= &HA Then
                    If busoutChk OrElse Params.ProductType(4) <> "1" Then
                        Me.Refresh()
                        Exit Do
                    Else
                        'BUSOUTが確認できない場合
                        'タイムアウト処理
                        If busoutTimer.ElapsedMilliseconds > busoutTO Then
                            lblInsResult(4).Text = "不合格:BUSOUT確認"
                            errorProcessContent = "BUSOUT未確認"
                            Return False
                        End If
                    End If

                End If

            End If


            'ブレークボタンでの検査中断
            If Not USBIO.Break_SW(breakSWData) Then
                Return False
            End If
            If breakSWData = 0 Then
                lblInsResult(4).Text = "不合格:検査中断"
                errorProcessContent = "検査中断"
                Return False
            End If


            My.Application.DoEvents()
        Loop
        'LED確認検査に続くのでここではラベル表示の更新はしない
        Return True
    End Function

    ''' <summary>
    ''' EX600通常検査LED確認
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <param name="ipFlg"></param>
    ''' <param name="unitType"></param>
    ''' <returns></returns>
    Private Function insEX600LEDChk(ByVal flg As String, ByVal ipFlg As String, ByVal unitType As String, ByVal colorPatten As String) As Boolean
        'マスターとスレーブで処理を分岐

        'Dim colorPatternList As List(Of LED_COLOR) = New List(Of LED_COLOR)
        Dim colorPatternList As List(Of LED_COLOR) = makeColorPattern(colorPatten)

        '検査省略
        If flg <> "1" Then
            lblInsResult(4).Text = String.Empty
            lblInsResult(4).BackColor = Color.Gray
            Return True
        End If

        '目視確認(startSW->OK, breakSW->NG)
        If ipFlg <> "1" Then
            lblInstructionDisplay.Text = "LED確認(目視確認)" & ControlChars.CrLf &
                                         "全てのLEDが点灯することを確認してください。" & ControlChars.CrLf &
                                         "合格の場合はスタートSWを押して下さい" & ControlChars.CrLf &
                                         "不合格の場合はブレークSWを押して下さい。"

            Do

                lblInstructionDisplay.Text = "LEDの確認を行ってください。" & ControlChars.CrLf &
                                             "合格ならStartSWを押してください。" & ControlChars.CrLf &
                                             "不合格ならBreakSWを押してください。"

                Dim byteDataSSW As Byte
                Dim byteDataBSW As Byte
                If Not USBIO.Start_SW(byteDataSSW) Then
                    errorProcessContent = "PSoC通信エラー"
                    Return False
                End If

                If byteDataSSW = 0 Then
                    lblInsResult(4).Text = "合格"
                    lblInsResult(4).BackColor = Color.Lime
                    Me.Refresh()
                    Return True
                End If

                If Not USBIO.Break_SW(byteDataBSW) Then
                    errorProcessContent = "PSoC通信エラー"
                    Return False
                End If

                If byteDataBSW = 0 Then
                    lblInsResult(4).Text = "不合格"
                    lblInsResult(4).BackColor = Color.Red
                    errorProcessContent = "LED確認（目視）"
                    Me.Refresh()
                    Return False
                End If

                My.Application.DoEvents()
            Loop

            '-----------------------------------------------------------------------------
            'シャッター速度の都合上、LEDラベル文字が見えないためカメラ越しの目視はしない
            '-----------------------------------------------------------------------------

            'Using cap As VideoCapture = New VideoCapture(0)
            '    cap.Set(CaptureProperty.FrameWidth, 640)
            '    cap.Set(CaptureProperty.FrameHeight, 480)
            '    cap.Set(CaptureProperty.Fps, 30)
            '    Dim img As Mat = New Mat()

            '    Dim winSizeW As Integer = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width
            '    Dim winSizeH As Integer = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height
            '    Dim taskbarSizeH As Integer = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height - System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height

            '    Cv2.NamedWindow("img", WindowMode.AutoSize)
            '    Cv2.MoveWindow("img", winSizeW - cap.FrameWidth, winSizeH - cap.FrameHeight - taskbarSizeH)

            '    Do

            '        lblInstructionDisplay.Text = "LEDの確認を行ってください。" & ControlChars.CrLf &
            '                                     "合格ならStartSWを押してください。" & ControlChars.CrLf &
            '                                     "不合格ならBreakSWを押してください。"

            '        cap.Read(img)
            '        Cv2.ImShow("img", img)
            '        Cv2.WaitKey(10)


            '        Dim byteDataSSW As Byte
            '        Dim byteDataBSW As Byte
            '        If Not USBIO.Start_SW(byteDataSSW) Then
            '            errorProcessContent = "PSoC通信エラー"
            '            Return False
            '        End If

            '        If byteDataSSW = 0 Then
            '            lblInsResult(4).Text = "合格"
            '            lblInsResult(4).BackColor = Color.Lime
            '            Cv2.DestroyWindow("img")
            '            Me.Refresh()
            '            Return True
            '        End If

            '        If Not USBIO.Break_SW(byteDataBSW) Then
            '            errorProcessContent = "PSoC通信エラー"
            '            Return False
            '        End If

            '        If byteDataBSW = 0 Then
            '            lblInsResult(4).Text = "不合格"
            '            lblInsResult(4).BackColor = Color.Red
            '            errorProcessContent = "LED確認（目視）"
            '            Cv2.DestroyWindow("img")
            '            Me.Refresh()
            '            Return False
            '        End If

            '        My.Application.DoEvents()
            '    Loop
            'End Using


        End If

        lblInstructionDisplay.Text = "LED確認(自動検査)"
        Using cap As VideoCapture = New VideoCapture(0)
            If unitType = "1" Then 'masuter unit flag
                If retryChkMasterUnitLED(cap, colorPatternList) Then
                    lblInsResult(4).Text = "合格"
                    lblInsResult(4).BackColor = Color.Lime
                    Return True
                Else
                    errorProcessContent = "LED確認（自動検査）"
                    Return False
                End If
            Else
                If retryChkSlaveUnitLED(cap, colorPatternList) Then
                    lblInsResult(4).Text = "合格"
                    lblInsResult(4).BackColor = Color.Lime
                    Return True
                Else
                    errorProcessContent = "LED確認（自動検査）"
                    Return False
                End If
            End If

        End Using
    End Function

    Private Function insSetPInspectionFlg(ByVal flg As String) As Boolean
        '------------------------------------------------
        'No16[EX600検査完了フラグセット](ベリファイも)
        '------------------------------------------------

        lblInstructionDisplay.Text = "通常検査完了フラグセット中"

        '省略時
        If flg <> "1" Then
            Return True
        End If


        If Not retryIsConnectNFC() Then
            errorProcessContent = "NFC接続エラー"
            Return False
        End If

        Dim inspectionFlg As Byte()
        inspectionFlg = retryReadModuleInspectionFlag() '検査フラグ読み込み
        inspectionFlg(1) = 1 '製品検査完了フラグをセット

        Dim flgVal As Byte = eightBitFlgsToByteNum(inspectionFlg)

        'フラグ部分の値を更新して&H0ブロックへ書き込み
        renewNormalMemDataArray(&HC, flgVal)
        If Not nfcWriteLine(&H0, normalAreaMemoryArr(0)) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If
        'ベリファイ
        '[OK]<-配列の最初から15文字以上一致（(0)~(14):有効データ, (15):チェックサム
        Dim verifyData As Byte() = nfcReadLine(&H0)
        If verifyData Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        If verifyLineData(normalAreaMemoryArr(&H0), verifyData) < 15 Then
            errorProcessContent = "NFCベリファイエラー"
            Return False
        End If


        Return True

    End Function
    ''' <summary>
    ''' 過電流確認
    ''' </summary>
    ''' <returns></returns>
    Private Function chkOvercurrent(ByVal flg As String) As Boolean

        lblInstructionDisplay.Text = "過電流確認中"

        If flg <> "1" Then
            lblInsResult(2).Text = String.Empty
            lblInsResult(2).BackColor = Color.Gray
            Return True
        End If

        'wait(500)'必要に応じてwait'ワーク立ち上がり待ちのwait(1000)が直前にあるのでCO

        Dim bytData As Byte
        Dim stpWatch As Stopwatch = New Stopwatch
        stpWatch.Start()

        Do
            If Not USBIO.EX600_OverCurrent(bytData) Then
                errorProcessContent = "PSoC通信エラー"
                Return False
            End If

            '過電流状態
            If bytData = 0 Then
                errorProcessContent = "過電流検知"
                Return False
            End If

            If stpWatch.ElapsedMilliseconds > 2000 Then
                lblInsResult(2).Text = "合格"
                lblInsResult(2).BackColor = Color.Lime
                Me.Refresh()
                Return True
            End If

            My.Application.DoEvents()
        Loop

    End Function

    ''' <summary>
    ''' 製品COM確認
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <param name="workType"></param>
    ''' <returns></returns>
    Private Function chkCom(ByVal flg As String, ByVal workType As String) As Boolean

        lblInstructionDisplay.Text = "COM極性確認中"

        If flg <> "1" Then
            lblInsResult(3).Text = String.Empty
            lblInsResult(3).BackColor = Color.Gray
            Return True
        End If

        Dim byteData1 As Byte
        Dim byteData2 As Byte

        If Not USBIO.Com1(byteData1) Then
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If

        If Not USBIO.Com2(byteData2) Then
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If

        If workType = "PNP" Then

            If byteData1 = 0 OrElse byteData2 = 0 Then
                errorProcessContent = String.Format("極性不一致_worktype:{0}_Com1:{1}_Com2{2}", workType, byteData1, byteData2)
                Return False
            End If

        ElseIf workType = "NPN" Then
            If byteData1 = 1 OrElse byteData2 = 1 Then
                errorProcessContent = String.Format("極性不一致_worktype:{0}_Com1:{1}_Com2{2}", workType, byteData1, byteData2)
                Return False
            End If
        Else
            errorProcessContent = String.Format("極性不一致(その他)_worktype:{0}_Com1:{1}_Com2{2}", workType, byteData1, byteData2)
            Return False
        End If

        lblInsResult(3).Text = "合格"
        lblInsResult(3).BackColor = Color.Lime
        Me.Refresh()
        Return True

    End Function
    ''' <summary>
    ''' COM極性確認リトライ版
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <param name="workType"></param>
    ''' <returns></returns>
    Private Function retryChkCom(ByVal flg As String, ByVal workType As String) As Boolean
        Dim retryNum As Integer = 5
        For i As Integer = 0 To retryNum - 1
            '製品内テストプログラムのLED点灯が始まるタイミングでCOM出力がオフとなることがあるため
            If Not chkCom(flg, workType) Then
                wait(300)
                Continue For
            Else
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary>
    ''' EX600製品検査
    ''' </summary>
    ''' <returns></returns>
    Private Function productInspection() As Boolean

        Dim swOverCurrent As Stopwatch = New Stopwatch()
        swOverCurrent.Start()

        '各検査前に検査状態（過電流ワーク検知などを確認）
        Dim insState As String = String.Empty
        If Not chkInspectionState(insState) Then
            lblInsResult(2).BackColor = Color.Red
            lblInsResult(2).Text = "不合格:" & insState
            errorProcessName = "検査前確認"
            errorProcessContent = insState
            Return False
        End If

        '-------------------------------
        'No.6[電源投入]
        '-------------------------------
        lblInstructionDisplay.Text = "電源投入中"

        If Params.ProductType(4) <> "1" Then
            If Not USBIO.Master_Pow(1) Then
                lblInsResult(5).Text = "不合格"
                lblInsResult(5).BackColor = Color.Red
                errorProcessContent = "PSoC通信エラー"
                Return False
            End If
            wait(100)
        Else
            If Not USBIO.Slave_Pow(1) Then
                lblInsResult(5).Text = "不合格"
                lblInsResult(5).BackColor = Color.Red
                errorProcessContent = "PSoC通信エラー"
                Return False
            End If
            wait(100)
        End If


        'ワークには基本24Vが入るようにする
        If Not USBIO.SI_change240V(1) Then
            lblInsResult(2).Text = "不合格"
            lblInsResult(2).BackColor = Color.Red
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If
        wait(100)

        If Not USBIO.EX600_Pow(1) Then
            lblInsResult(2).Text = "不合格"
            lblInsResult(2).BackColor = Color.Red
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If

        wait(1000)


        '過電流確認
        If Not chkOvercurrent(Params.Overcurrent(1)) Then
            lblInsResult(2).Text = "不合格"
            lblInsResult(2).BackColor = Color.Red
            errorProcessName = "過電流確認"
            Return False
        End If

        swOverCurrent.Stop()
        insTimeContent &= "," & swOverCurrent.ElapsedMilliseconds.ToString("0.0")

        Dim swCom As Stopwatch = New Stopwatch()
        swCom.Start()

        'COM確認
        If Not retryChkCom(Params.ComChk(1), Params.ComChk(2)) Then
            lblInsResult(3).Text = "不合格"
            lblInsResult(3).BackColor = Color.Red
            errorProcessName = "COM確認"

            Return False
        End If

        swCom.Stop()
        insTimeContent &= "," & swCom.ElapsedMilliseconds.ToString("0.0")

        Dim swProduct As Stopwatch = New Stopwatch()
        swProduct.Start()
        '--------------------------------------------
        'No.7-13[製品検査(テストプログラム)]
        '--------------------------------------------
        If Not insEX600Nomal(Params.PInspection(1)) Then
            'lblInsResult(4).Text = "不合格"
            lblInsResult(4).BackColor = Color.Red
            errorProcessName = "通常製品検査"
            Return False
        End If
        swProduct.Stop()
        insTimeContent &= "," & swProduct.ElapsedMilliseconds.ToString("0.0")

        Dim swLED As Stopwatch = New Stopwatch()
        swLED.Start()

        '--------------------------------------------
        'No.14[LED確認](画像処理)
        '--------------------------------------------

        If isManuLEDChkMode Then
            If Not insEX600LEDChk(Params.LEDChk1(1), "0", Params.ProductType(4), Params.LEDChk1(3)) Then
                lblInsResult(4).Text = "不合格:LED確認"
                lblInsResult(4).BackColor = Color.Red
                errorProcessName = "通常製品検査LED確認"
                errorProcessContent = "LED確認エラー"
                Return False
            End If
        Else

            Dim bytReserveIn0Data As Byte
            'アクリルカバー開閉確認
            If Not USBIO.Reserve0_In(bytReserveIn0Data) Then
                lblInsResult(4).Text = "不合格:LED確認"
                lblInsResult(4).BackColor = Color.Red
                errorProcessContent = "PSoC通信エラー"
                Return False
            End If

            If bytReserveIn0Data <> 0 Then
                lblInsResult(4).Text = "不合格:LED確認"
                lblInsResult(4).BackColor = Color.Red
                errorProcessContent = "アクリルカバー閉確認"
                Return False
            End If


            If Not insEX600LEDChk(Params.LEDChk1(1), Params.LEDChk1(2), Params.ProductType(4), Params.LEDChk1(3)) Then
                lblInsResult(4).Text = "不合格:LED確認"
                lblInsResult(4).BackColor = Color.Red
                errorProcessName = "通常製品検査"
                errorProcessContent = "LED確認エラー"
                Return False
            End If

        End If


        '--------------------------------------------
        'No.15[電源切断]
        '--------------------------------------------
        lblInstructionDisplay.Text = "電源切断中"

        If Not USBIO.EX600_Pow(0) Then
            lblInsResult(4).Text = "不合格"
            lblInsResult(4).BackColor = Color.Red
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If
        wait(100)


        '------------------------------------------------
        'No16[EX600検査完了フラグセット](ベリファイも)
        '------------------------------------------------
        If Not insSetPInspectionFlg("1") Then
            lblInsResult(4).Text = "不合格"
            lblInsResult(4).BackColor = Color.Red
            errorProcessName = "EX600検査完了フラグセット"
            Return False
        End If

        swLED.Stop()
        insTimeContent &= "," & swLED.ElapsedMilliseconds.ToString("0.0")

        Return True

    End Function


    '--------------------------------------------------------
    '
    '               無線検査用関数
    '
    '--------------------------------------------------------

    ''' <summary>
    ''' 無線検査モードへ変更
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function insChgNWInspectionMode(ByVal flg As String) As Boolean
        '-------------------------------------------------------------------
        'No.17[無線検査モードの変更]
        '-------------------------------------------------------------------
        lblInstructionDisplay.Text = "無線検査モード変更"

        '0x55:EX600製品検査モード, 0xAA:無線製品検査モード, 0x00:通常モード
        renewNormalMemDataArray(&HB, &HAA) '値更新
        If Not nfcWriteLine(&H0, normalAreaMemoryArr(&H0)) Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If
        'ベリファイ
        Dim verifyData As Byte() = nfcReadLine(&H0)
        If verifyData Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        If verifyLineData(normalAreaMemoryArr(&H0), verifyData) < 15 Then
            errorProcessContent = "NFCベリファイエラー"
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' 無線検査完了を確認する
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function insChkNWInspectionDone(ByVal flg As String) As Boolean

        '----------------------------------------------------------------------------
        'No.19[無線通信検査](製品側でマスタースレーブ間での通信)製品側で検査
        '----------------------------------------------------------------------------
        lblInstructionDisplay.Text = "無線通信検査中"
        'master unit
        '1.master -> slave:output
        '2.slave  -> master:input

        'slave unit
        '1.slave  -> master:output
        '2.master -> slave:input
        'wait(10000) '検査終了まで数秒待つ'2017/06/29


        '----------------------------------------------------------------------------
        'No.20[無線検査完了確認](数秒後トンネルモードにて完了確認,コマンドAA0)
        '--------------------l-------------------------------------------------------
        'lblInstructionDisplay.Text = "無線通信検査完了確認中"'2017/06/29
        Dim wirelessTestTimer As Stopwatch = New Stopwatch()
        Dim wirelessTestTO As Integer = 10000 '無線通信検査タイムアウト閾値[ms]
        wirelessTestTimer.Start()


        trans_mode = False

        If Not isConnectNFC() Then
            errorProcessContent = "NFC接続エラー"
            Return False
        End If

        Do

            'データの取得
            Dim tunnelReadData As Byte() = retryNfcTunnelReadLine(&HAA0, 16)

            If wirelessTestTimer.ElapsedMilliseconds > wirelessTestTO Then
                'エラー内容
                If tunnelReadData Is Nothing Then
                    errorProcessContent = "NFC読込みエラー"
                End If
                If tunnelReadData(0) <> &H1 Then
                    errorProcessContent = "無線通信検査未完了"
                End If

                Return False
            End If


            If tunnelReadData Is Nothing Then
                Continue Do
            End If
            '検査終了の確認
            If tunnelReadData(0) <> &H1 Then
                Continue Do
            End If
            '検査正常終了
            If tunnelReadData(0) = &H1 Then
                Exit Do
            End If


            My.Application.DoEvents()
        Loop

        lblInsResult(5).Text = "合格"
        lblInsResult(5).BackColor = Color.Lime
        Return True
    End Function

    ''' <summary>
    ''' 無線検査完了フラグをセットする
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function insSetNWInspectionDoneFlg(ByVal flg As String) As Boolean
        '--------------------------------------------------------
        'No.22[無線検査完了フラグをセット]
        '--------------------------------------------------------
        lblInstructionDisplay.Text = "無線検査完了フラグセット中"
        If Not isConnectNFC() Then
            errorProcessContent = "NFC通信エラー"
            Return False
        End If

        Dim inspectionFlg As Byte()
        inspectionFlg = retryReadModuleInspectionFlag() 'readModuleInspectionFlag()
        inspectionFlg(2) = 1 '無線検査完了フラグをセット

        '通常メモリベリファイ用の配列に0x0Cのフラグを更新した値をセットして再書き込み
        renewNormalMemDataArray(&HC, eightBitFlgsToByteNum(inspectionFlg))
        If Not nfcWriteLine(&H0, normalAreaMemoryArr(&H0)) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If

        'ベリファイ
        Dim verifyData As Byte() = nfcReadLine(&H0)
        If verifyData Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        If verifyLineData(normalAreaMemoryArr(&H0), verifyData) < 15 Then
            errorProcessContent = "NFCベリファイエラー"
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' 通常検査モードに変更
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function insChgNormalInpsctionMode(ByVal flg As String, ByVal unitType As String) As Boolean
        '-----------------------------------------------------------------------
        'No.23[通常モードへ変更]
        '-----------------------------------------------------------------------
        lblInstructionDisplay.Text = "通常モード変更中"

        '通常モード(&H4のb5b4=10)
        renewNormalMemDataArray(&H4, &H10)
        '0x55:EX600製品検査モード, 0xAA:無線製品検査モード, 0x00（その他）:&H4のモード(通常モードなど)
        renewNormalMemDataArray(&HB, &H0)


        '書き込み
        If Not nfcWriteLine(&H0, normalAreaMemoryArr(0)) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If
        'ベリファイ
        Dim verifyData As Byte() = nfcReadLine(&H0)
        If verifyData Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        If verifyLineData(normalAreaMemoryArr(0), verifyData) < 15 Then
            errorProcessContent = "NFCベリファイエラー"
            Return False
        End If

        '登録データの削除(*6(0014~0017h), *14(0011~0013,スレーブのみマスタは元から0x00)に0x00を書き込む)
        If unitType <> "1" Then
            'slave unit
            '書込みデータの更新
            For i As Integer = 0 To 6
                renewNormalMemDataArray(Convert.ToUInt16(&H11 + i), &H0)
            Next
            '書込み
            If Not nfcWriteLine(&H10, normalAreaMemoryArr(1)) Then
                errorProcessContent = "NFC書込みエラー"
                Return False
            End If
        Else
            'master unit
            '書込みデータの更新
            For i As Integer = 0 To 4
                renewNormalMemDataArray(Convert.ToUInt16(&H14 + i), &H0)
            Next
            '書込み
            If Not nfcWriteLine(&H10, normalAreaMemoryArr(1)) Then
                errorProcessContent = "NFC書込みエラー"
                Return False
            End If
        End If

        Return True
    End Function

    ''' <summary>
    ''' 無線製品検査
    ''' </summary>
    ''' <returns></returns>
    Private Function nowireInspection() As Boolean

        Dim swNowire As Stopwatch = New Stopwatch()
        swNowire.Start()

        '各検査前に検査状態（過電流ワーク検知などを確認）
        Dim insState As String = String.Empty
        If Not chkInspectionState(insState) Then
            lblInsResult(5).BackColor = Color.Red
            lblInsResult(5).Text = "不合格:" & insState
            errorProcessName = "検査前確認"
            errorProcessContent = insState
            Return False
        End If

        '-------------------------------------------------------------------
        'No.17[無線検査モードの変更]
        '-------------------------------------------------------------------
        If Not insChgNWInspectionMode("1") Then
            lblInsResult(5).Text = "不合格"
            lblInsResult(5).BackColor = Color.Red
            errorProcessName = "無線検査モード変更"
            Return False
        End If

        '---------------------------------------------------
        'No.18[電源投入]
        '---------------------------------------------------
        '->電源ON
        '->電源ON確認



        If Not USBIO.SI_change240V(1) Then
            lblInsResult(5).Text = "不合格"
            lblInsResult(5).BackColor = Color.Red
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If
        wait(100)

        If Not USBIO.EX600_Pow(1) Then
            lblInsResult(5).Text = "不合格"
            lblInsResult(5).BackColor = Color.Red
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If
        wait(500)

        If Params.ProductType(4) <> "1" Then
            If Not USBIO.Master_Pow(1) Then
                lblInsResult(5).Text = "不合格"
                lblInsResult(5).BackColor = Color.Red
                errorProcessContent = "PSoC通信エラー"
                Return False
            End If
            wait(100)
        Else
            If Not USBIO.Slave_Pow(1) Then
                lblInsResult(5).Text = "不合格"
                lblInsResult(5).BackColor = Color.Red
                errorProcessContent = "PSoC通信エラー"
                Return False
            End If
            wait(100)
        End If

        '-------------------------------------------------------
        'No19, 20[無線通信検査、確認]
        '-------------------------------------------------------
        If Not insChkNWInspectionDone(Params.NNowireCom(1)) Then
            lblInsResult(5).Text = "不合格"
            lblInsResult(5).BackColor = Color.Red
            errorProcessName = "無線製品検査"
            Return False
        End If



        '--------------------------------------------------------
        'No.21[電源切断]
        '--------------------------------------------------------

        If Not USBIO.EX600_Pow(0) Then
            lblInsResult(5).Text = "不合格"
            lblInsResult(5).BackColor = Color.Red
            errorProcessContent = "PSoC通信エラー"
            wait(100)
            Return False
        End If


        If Params.ProductType(4) <> "1" Then
            If Not USBIO.Master_Pow(0) Then
                lblInsResult(5).Text = "不合格"
                lblInsResult(5).BackColor = Color.Red
                errorProcessContent = "PSoC通信エラー"
                Return False
            End If

            wait(100)
        Else
            If Not USBIO.Slave_Pow(0) Then
                lblInsResult(5).Text = "不合格"
                lblInsResult(5).BackColor = Color.Red
                errorProcessContent = "PSoC通信エラー"
                Return False
            End If
            wait(100)
        End If


        '--------------------------------------------------------
        'No.22[無線検査完了フラグをセット]
        '--------------------------------------------------------
        If Not insSetNWInspectionDoneFlg("1") Then
            lblInsResult(5).Text = "不合格"
            lblInsResult(5).BackColor = Color.Red
            errorProcessName = "無線検査完了フラグセット"
            Return False
        End If

        '-----------------------------------------------------------------------
        'No.23[通常モードへ変更]
        '-----------------------------------------------------------------------
        If Not insChgNormalInpsctionMode("1", Params.ProductType(4)) Then
            lblInsResult(5).Text = "不合格"
            lblInsResult(5).BackColor = Color.Red
            errorProcessName = "通常モード変更"
            Return False
        End If

        swNowire.Stop()
        insTimeContent &= "," & swNowire.ElapsedMilliseconds.ToString("0.0")

        Return True
    End Function


    '--------------------------------------------------------
    '
    '               検査後書込用関数
    '
    '--------------------------------------------------------

    ''' <summary>
    ''' ファームウェアソフトVerをFeRAM書込み用に形式変換する
    ''' e.g. string:3.10 -> byte:{3, 1, 0}
    ''' </summary>
    ''' <param name="softverStr"></param>
    ''' <returns></returns>
    Private Function cvtSoftVerStrToByte(ByVal softverStr As String) As Byte()
        Dim verStr As String = softverStr.Replace(".", String.Empty)
        Dim verByte As Byte() = New Byte(verStr.Length - 1) {}
        For i As Integer = 0 To verStr.Length - 1
            verByte(i) = Convert.ToByte(verStr.Substring(i, 1))
        Next

        Return verByte

    End Function
    ''' <summary>
    ''' RX210ver読み出し用関数
    ''' </summary>
    ''' <param name="softVer"></param>
    ''' <returns></returns>
    Private Function readRx210Ver(ByRef softVer As Byte()) As Boolean

        '配列長が不足
        If softVer.Length <= 2 Then
            Return False
        End If

        Dim extendForSoftware As New extend_command()
        If Not extendForSoftware.nfc_connect(True) Then
            Return False
        End If
        If Not extendForSoftware.nfc_send_req(True) Then
            Return False
        End If

        If Not extendForSoftware.read_210_version(True, softVer) Then
            Return False
        End If

        Return True

    End Function
    ''' <summary>
    ''' RX210ver読み出し用関数（リトライ処理あり）
    ''' </summary>
    ''' <param name="softVer"></param>
    ''' <returns></returns>
    Private Function retryReadRx210Ver(ByRef softVer As Byte()) As Boolean

        Dim retryNum As Integer = 5

        For i As Integer = 0 To retryNum - 1
            If readRx210Ver(softVer) Then
                Return True
            End If
        Next

        Return False

    End Function
    ''' <summary>
    ''' RX62ver読み出し用関数
    ''' </summary>
    ''' <param name="softVer"></param>
    ''' <returns></returns>
    Private Function readRx62Ver(ByRef softVer As Byte()) As Boolean
        '配列長が不足
        If softVer.Length <= 2 Then
            Return False
        End If

        Dim extendForSoftware As New extend_command()
        If Not extendForSoftware.nfc_connect(True) Then
            Return False
        End If
        If Not extendForSoftware.nfc_send_req(True) Then
            Return False
        End If

        If Not extendForSoftware.read_62n_version(True, softVer) Then
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' RX62ver読み出し用関数(リトライ処理あり) 
    ''' </summary>
    ''' <param name="softVer"></param>
    ''' <returns></returns>
    Private Function retryReadRx62Ver(ByRef softVer As Byte()) As Boolean
        Dim retryNum As Integer = 5

        For i As Integer = 0 To retryNum - 1
            If readRx62Ver(softVer) Then
                Return True
            End If
        Next

        Return False
    End Function
    ''' <summary>
    ''' NP40Ver読み出し用関数
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <param name="softVer"></param>
    ''' <returns></returns>
    Private Function readNp40Ver(ByVal flg As String, ByRef softVer As Byte()) As Boolean

        'スレーブユニットにはNP40verはないので読み出ししない
        'ワークタイプがマスターでないときは読み出ししない
        If flg <> "1" Then
            Return True
        End If

        '配列長が不足
        If softVer.Length <= 2 Then
            Return False
        End If

        Dim extendForSoftware As New extend_command()
        If Not extendForSoftware.nfc_connect(True) Then
            Return False
        End If
        If Not extendForSoftware.nfc_send_req(True) Then
            Return False
        End If

        If Not extendForSoftware.read_npx0_version(True, softVer) Then
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' NP40Ver読み出し用関数（リトライ処理あり）
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <param name="softVer"></param>
    ''' <returns></returns>
    Private Function retryReadNp40Ver(ByVal flg As String, ByRef softVer As Byte()) As Boolean
        Dim retryNum As Integer = 5

        For i As Integer = 0 To retryNum - 1
            If readNp40Ver(flg, softVer) Then
                Return True
            End If
        Next

        Return False
    End Function

    ''' <summary>
    ''' ソフトウェアバージョンのチェック
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function readSoftVer(ByVal flg As String, ByVal productType As String, ByRef rx210ver As Byte(), ByRef rx62ver As Byte(), ByRef np40ver As Byte(),
                                 ByVal rx210verStr As String, ByVal rx62verStr As String, np40verStr As String) As Boolean
        If flg <> "1" Then
            Return True
        End If

        '配列長チェック
        If rx210ver.Length <= 2 Then Return False
        If rx62ver.Length <= 2 Then Return False
        If np40ver.Length <= 2 Then Return False

        If Not retryIsConnectNFC() Then
            errorProcessContent = "NFC接続エラー"
            Return False
        End If

        'rx210
        If Not retryReadRx210Ver(rx210ver) Then
            errorProcessContent = "ソフトウェアVer読込みエラー(RX210)"
            lblInsResult(7).Text = String.Format("読込みエラー")
            lblInsResult(7).BackColor = Color.Red
            Return False
        End If

        If Not chkSoftVer(rx210ver, rx210verStr) Then
            errorProcessContent = "ソフトウェアVer不一致エラー(RX210)"
            lblInsResult(7).Text = String.Format("{0}.{1}{2}", rx210ver(0).ToString(), rx210ver(1).ToString(), rx210ver(2).ToString())
            lblInsResult(7).BackColor = Color.Red
            Return False
        End If
        lblInsResult(7).Text = String.Format("Ver不一致:{0}.{1}{2}", rx210ver(0).ToString(), rx210ver(1).ToString(), rx210ver(2).ToString())
        lblInsResult(7).BackColor = Color.Lime

        'rx62
        If Not retryReadRx62Ver(rx62ver) Then
            errorProcessContent = "ソフトウェア読込みエラー(RX62)"
            lblInsResult(8).Text = String.Format("読込みエラー")
            lblInsResult(8).BackColor = Color.Red
            Return False
        End If

        If Not chkSoftVer(rx62ver, rx62verStr) Then
            errorProcessContent = "ソフトウェアVer不一致エラー(RX62)"
            lblInsResult(8).Text = String.Format("Ver不一致:{0}.{1}{2}", rx62ver(0).ToString(), rx62ver(1).ToString(), rx62ver(2).ToString())
            lblInsResult(8).BackColor = Color.Red
            Return False
        End If
        lblInsResult(8).Text = String.Format("{0}.{1}{2}", rx62ver(0).ToString(), rx62ver(1).ToString(), rx62ver(2).ToString())
        lblInsResult(8).BackColor = Color.Lime

        'np40
        'NP40verの読込みはマスタユニットのみ
        If Not retryReadNp40Ver(productType, np40ver) Then
            errorProcessContent = "ソフトウェア読込みエラー(NP40)"
            lblInsResult(9).Text = String.Format("読込みエラー")
            lblInsResult(9).BackColor = Color.Red
            Return False
        End If

        If productType = "1" Then
            If Not chkSoftVer(np40ver, np40verStr) Then
                errorProcessContent = "ソフトウェアVer不一致エラー(NP40)"
                lblInsResult(9).Text = String.Format("Ver不一致:{0}.{1}", np40ver(0).ToString(), np40ver(1).ToString())
                lblInsResult(9).BackColor = Color.Red
                'MessageBox.Show("np40ver不一致")
                Return False
            End If
            lblInsResult(9).Text = String.Format("{0}.{1}", np40ver(0).ToString(), np40ver(1).ToString())
            lblInsResult(9).BackColor = Color.Lime
        Else
            lblInsResult(9).Text = String.Empty
            lblInsResult(9).BackColor = Color.Gray
        End If



        Me.Refresh()

        Return True

    End Function

    ''' <summary>
    ''' ソフトウェアVer書込み用関数
    ''' </summary>
    ''' <param name="softVer"></param>
    ''' <param name="rx210ver"></param>
    ''' <param name="rx62ver"></param>
    ''' <param name="np40ver"></param>
    ''' <returns></returns>
    Private Function writeSoftVer(ByVal flg As String, ByVal productType As String, ByVal softVer As Byte(), ByVal rx210ver As Byte(), ByVal rx62ver As Byte(), ByVal np40ver As Byte()) As Boolean

        If flg <> "1" Then
            lblInsResult(10).Text = String.Empty
            lblInsResult(10).BackColor = Color.Gray
            Return True
        End If

        '配列長チェック
        If softVer.Length <= 2 Then Return False
        If rx210ver.Length <= 2 Then Return False
        If rx62ver.Length <= 2 Then Return False
        If np40ver.Length <= 2 Then Return False

        renewNormalMemDataArray(&H70, softVer)
        renewNormalMemDataArray(&H73, rx210ver)
        renewNormalMemDataArray(&H76, rx62ver)
        'マスタユニットの場合はNP40Ver
        If productType = "1" Then
            renewNormalMemDataArray(&H79, np40ver)
        End If

        '->上記4つのソフトバージョンを書き込む
        If Not retryNfcWriteLine(&H70, normalAreaMemoryArr(7)) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If
        '->ベリファイ
        Dim verifyData As Byte() = retryNfcReadLine(&H70)
        If verifyData Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If


        If verifyLineData(normalAreaMemoryArr(7), verifyData) < 15 Then
            errorProcessContent = "NFCベリファイエラー"
            Return False
        End If

        lblInsResult(10).Text = "合格"
        lblInsResult(10).BackColor = Color.Lime
        Me.Refresh()
        Return True

    End Function
    ''' <summary>
    ''' ソフトウェアVer書込用関数（リトライ処理あり）
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <param name="softVer"></param>
    ''' <param name="rx210ver"></param>
    ''' <param name="rx62ver"></param>
    ''' <param name="np40ver"></param>
    ''' <returns></returns>
    Private Function retryWriteSoftVer(ByVal flg As String, ByVal productType As String, ByVal softVer As Byte(), ByVal rx210ver As Byte(), ByVal rx62ver As Byte(), ByVal np40ver As Byte()) As Boolean

        lblInstructionDisplay.Text = "ソフトウェアバージョン書込み中"

        Dim retryNum As Integer = 5
        For i As Integer = 0 To retryNum - 1
            If writeSoftVer(flg, productType, softVer, rx210ver, rx62ver, np40ver) Then
                Return True
            End If
        Next

        Return False

    End Function

    ''' <summary>
    ''' ソフトウェアVerの一致チェック
    ''' </summary>
    ''' <param name="softVer"></param>
    ''' <param name="correctVer"></param>
    ''' <returns></returns>
    Private Function chkSoftVer(ByVal softVer As Byte(), ByVal correctVer As String) As Boolean

        If softVer.Length <= 2 Then Return False

        Dim softVerStr As String = String.Empty
        For i As Integer = 0 To softVer.Length - 1
            softVerStr &= softVer(i).ToString()
        Next
        'NP40Ver読込みの仕様のため変換後の文字列の3文字目までを取り出す
        'e.g. 読み取り値{1,15,0}、正しくは{1,1,5}
        softVerStr = softVerStr.Substring(0, 3)

        '品番リストに登録されたソフトウェアバージョン(X.XX)を比較
        If softVerStr <> correctVer.Replace(".", "") Then
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' PID書込
    ''' </summary>
    ''' <returns></returns>
    Private Function writePID(ByVal flg As String) As Boolean

        If flg <> "1" Then
            lblInsResult(11).Text = lblPID.Text
            lblInsResult(11).BackColor = Color.Gray
            Me.Refresh()
        End If

        '[FeRAMに無線PID(バーコードより取得(文字列))を書き込み(0x0000-0x0003)]
        Dim NowirePID As String = lblPID.Text
        Dim pidStrData As String() = NowirePID.Split("-"c)
        Dim pidByteData As Byte() = New Byte(pidStrData.Length - 1) {}

        For i As Integer = 0 To pidByteData.Length - 1
            pidByteData(i) = Convert.ToByte(pidStrData(i), 16)
        Next


        renewNormalMemDataArray(&H0, pidByteData)
        If Not nfcWriteLine(&H0, normalAreaMemoryArr(&H0)) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If
        'ベリファイ
        Dim verifyData As Byte() = retryNfcReadLine(&H0)

        If verifyData Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        If verifyLineData(normalAreaMemoryArr(&H0), verifyData) < 15 Then
            errorProcessContent = "NFCベリファイエラー"
            Return False
        End If

        lblInsResult(11).Text = "合格" 'lblPID.Text
        lblInsResult(11).BackColor = Color.Lime
        Me.Refresh()

        Return True

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    Private Function retryWritePID(ByVal flg As String) As Boolean

        lblInstructionDisplay.Text = "PID書込み中"

        Dim retryNum As Integer = 5
        For i As Integer = 0 To retryNum - 1
            If writePID(flg) Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary>
    ''' MACアドレスを書き込む(通常、トンネル)
    ''' </summary>
    ''' <returns></returns>
    Private Function writeMac(ByVal flg As String) As Boolean

        If flg <> "1" Then
            lblInsResult(12).Text = String.Empty
            lblInsResult(12).BackColor = Color.Gray
            Return True
        End If

        '-------------------------------------------------------------
        'No.31[FeRAMにEtherNetMACアドレスを書き込み(0x0005-0x000A)]
        '-------------------------------------------------------------
        Dim NowireMAC As String = lblMAC.Text '"00-23-C6-26-00-09"
        Dim macStrData As String() = NowireMAC.Split("-"c)
        Dim macByteData As Byte() = New Byte(macStrData.Length - 1) {}

        For i As Integer = 0 To macByteData.Length - 1
            macByteData(i) = Convert.ToByte(macStrData(i), 16)
        Next

        renewNormalMemDataArray(&H5, macByteData)
        If Not nfcWriteLine(&H0, normalAreaMemoryArr(&H0)) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If
        'ベリファイ
        Dim verifyData As Byte() = nfcReadLine(&H0)
        If verifyData Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        If verifyLineData(normalAreaMemoryArr(&H0), verifyData) < 15 Then
            errorProcessContent = "NFCベリファイエラー"
            Return False
        End If

        '--------------------------------------------------
        'No.32[EtherNetMACアドレス書き込み(トンネル)]
        '--------------------------------------------------
        'MAC書込み（トンネル）
        Dim writeTunnelMacStr As String() = lblMAC.Text.Split("-"c)
        Dim writeTunnelMac As Byte() = New Byte(writeTunnelMacStr.Length - 1) {}
        Dim verifyTunnelMac As Byte() = New Byte(writeTunnelMacStr.Length - 1) {}
        For i As Integer = 0 To verifyTunnelMac.Length - 1
            writeTunnelMac(i) = Convert.ToByte(writeTunnelMacStr(i), 16)
            verifyTunnelMac(i) = Convert.ToByte(writeTunnelMacStr(i), 16)
        Next

        Dim readMac As Byte() = New Byte(writeTunnelMac.Length - 1) {}

        Dim extendMacWrite As New extend_command()
        If Not extendMacWrite.nfc_connect(True) Then
            errorProcessContent = "NFC通信エラー"
            Return False
        End If

        If Not extendMacWrite.nfc_send_req(True) Then
            errorProcessContent = "NFC通信エラー"
            Return False
        End If

        If Not extendMacWrite.write_Fmac(True, writeTunnelMac) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If

        'ベリファイ
        If Not extendMacWrite.read_Fmac(True, readMac) Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        For i As Integer = 0 To writeTunnelMac.Length - 1
            If verifyTunnelMac(i) <> readMac(i) Then
                errorProcessContent = "NFCベリファイエラー"
                Return False
            End If
        Next

        lblInsResult(12).Text = "合格"
        lblInsResult(12).BackColor = Color.Lime
        Me.Refresh()

        Return True
    End Function

    Private Function retryWriteMac(ByVal flg As String) As Boolean

        lblInstructionDisplay.Text = "MACアドレス書込み中"

        Dim retryNum As Integer = 5

        For i As Integer = 0 To retryNum - 1
            If writeMac(flg) Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Function readSerailNo(ByRef serialNoByt As Byte()) As Boolean


        'USBIO.EX600_Pow(1)'呼び出しもとでワーク電源ON
        'wait(1000)

        '配列長不足
        If serialNoByt.Length < 3 Then
            Return False
        End If

        Dim extendSerialNoRead As New extend_command()

        extendSerialNoRead.nfc_disconnection(True)

        If Not extendSerialNoRead.nfc_connect(True) Then
            Return False
        End If

        If Not extendSerialNoRead.nfc_send_req(True) Then
            Return False
        End If


        If Not extendSerialNoRead.read_serialno(True, serialNoByt) Then
            Dim err As Boolean() = Sna.NoWire.ErrorStatus
            Return False
        End If

        extendSerialNoRead.nfc_disconnection(True)

        Return True
    End Function

    ''' <summary>
    ''' シリアル番号書込み
    ''' </summary>
    ''' <returns></returns>
    Private Function writeSerialNo(ByVal flg As String, reInspectionFlg As Boolean) As Boolean
        '---------------------------------------------------
        'No.33[EtherNetシリアルNo書き込み(トンネル)]
        '---------------------------------------------------
        'todo:書込み値は連番、開始番号は別途指示

        If flg <> "1" Then
            lblInsResult(13).Text = String.Empty
            lblInsResult(13).BackColor = Color.Gray
            Me.Refresh()
            Return True
        End If

        '再検査の有無でのシリアル番号の切り替えはID銘板を読んだときに行う
        Dim serialNoStr As String = lblSerialNo.Text

        '書込みデータ作成
        Dim SerialNoByt As Byte() = New Byte(3) {}
        For i As Integer = 0 To 3
            SerialNoByt(i) = Convert.ToByte(serialNoStr.Substring(i * 2, 2), 16)
        Next
        'ベリファイ用データ
        Dim readSerianNo As Byte() = New Byte(SerialNoByt.Length - 1) {}

        '書込み処理開始
        Dim extendSerialNoWrite As New extend_command()

        If Not extendSerialNoWrite.nfc_connect(True) Then
            errorProcessContent = "NFC接続エラー"
            Return False
        End If

        If Not extendSerialNoWrite.nfc_send_req(True) Then
            errorProcessContent = "NFC接続エラー"
            Return False
        End If

        '書込み
        If Not extendSerialNoWrite.write_serialno(True, SerialNoByt) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If

        'ベリファイ
        If Not extendSerialNoWrite.read_serialno(True, readSerianNo) Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        For i As Integer = 0 To SerialNoByt.Length - 1
            If SerialNoByt(i) <> readSerianNo(i) Then
                errorProcessContent = "NFCベリファイエラー"
                Return False
            End If
        Next

        lblInsResult(13).Text = "合格"
        lblInsResult(13).BackColor = Color.Lime
        Me.Refresh()

        Return True

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function retryWriteSerialNo(ByVal flg As String, ByVal reInspectionFlg As Boolean) As Boolean

        lblInstructionDisplay.Text = "シリアルNo書込み中"

        Dim retryNum As Integer = 5

        For i As Integer = 0 To retryNum - 1
            If writeSerialNo(flg, reInspectionFlg) Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary>
    ''' ID書込み完了フラグを書き込む
    ''' </summary>
    ''' <returns></returns>
    Private Function setIDwriteDoneFlg() As Boolean
        '---------------------------------------------------
        'No.34[ID書込み完了フラグ書き込み]
        '---------------------------------------------------
        Dim flg As Byte()
        flg = retryReadModuleInspectionFlag()
        flg(3) = 1 '無線検査完了フラグをセット
        '書き込み処理(flg->1byteでの値に変換, nfcReadLineでデータ取得->必要個所を更新して書き込み)
        renewNormalMemDataArray(&HC, eightBitFlgsToByteNum(flg))
        If Not nfcWriteLine(&H0, normalAreaMemoryArr(&H0)) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If
        'ベリファイ
        Dim verifyData As Byte() = nfcReadLine(&H0)
        If verifyData Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If
        If verifyLineData(normalAreaMemoryArr(&H0), verifyData) < 15 Then
            errorProcessContent = "NFCベリファイエラー"
            Return False
        End If

        Return True

    End Function

    ''' <summary>
    ''' 製品検査完了フラグを書き込む
    ''' </summary>
    ''' <returns></returns>
    Private Function setInspectionDoneFlg() As Boolean

        '--------------------------------------------------------
        'No.35[製品検査完了フラグ書き込み]
        '--------------------------------------------------------
        Dim PIDoneData As Byte() = nfcReadLine(&H0)
        '各ビット情報抽出
        Dim PIDoneFlg As Byte() = byteToEightBitFlag(PIDoneData(&H4))
        '製品検査完了フラグの設定
        PIDoneFlg(6) = 1
        '動作モードを【出荷検査】→【通常】
        PIDoneFlg(4) = 1

        renewNormalMemDataArray(&H4, eightBitFlgsToByteNum(PIDoneFlg))
        If Not nfcWriteLine(&H0, normalAreaMemoryArr(&H0)) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' FeRAMブロック0エリアの書込みプロテクト
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function protectFeRAM(ByVal flg As String) As Boolean
        '--------------------------------------------------------
        'No.36[FeRAMグループ0の書き込みプロテクト]
        'システムメモリエリア(*13:0x01F0, 0x01F4)に0x01を書き込む
        '--------------------------------------------------------
        lblInstructionDisplay.Text = "書込みプロテクト中"

        'プロテクトをかけるため値を更新
        controlAreaMemoryArr(4)(&H0) = &H1
        controlAreaMemoryArr(4)(&H4) = &H1
        controlAreaMemoryArr(4)(&HF) = &H2E '暗号化モードON

        'ワーク電源オフ
        If Not USBIO.EX600_Pow(0) Then
            errorProcessContent = "PSoC通信エラー"
        End If
        wait(500)

        '制御エリア書き込みのため平文モード
        trans_mode = True

        If Not retryIsConnectNFC() Then
            errorProcessContent = "NFC接続エラー"
            Return False
        End If

        If Not Sna.NoWire.Nfc_rf_write_normal_system(&H1F0, controlAreaMemoryArr(4)) Then
            errorProcessContent = "NFC書込みエラー"
            Return False
        End If

        wait(500)

        '制御エリアに書込みを反映させるためNFCの磁気切断→接続処理を行う
        Dim retryNum As Integer = 10
        Dim count As Integer = 0

        '磁気をoff
        For i As Integer = 0 To 3
            If Sna.NoWire.Nfc_turn_off() Then
                wait(500)
                Exit For
            End If
        Next
        '磁気をon
        For i As Integer = 0 To 3
            If Sna.NoWire.Nfc_turn_on() Then
                wait(500)
                Exit For
            End If
        Next

        For i As Integer = 0 To 3
            If Sna.NoWire.Nfc_disconnect() Then
                wait(500)
                Exit For
            End If
        Next

        If Not Sna.NoWire.Nfc_disconnect() Then
            errorProcessContent = "NFC接続エラー"
        End If

        wait(500)

        '暗号化モードに戻す
        trans_mode = False

        If Not USBIO.EX600_Pow(1) Then
            errorProcessContent = "PSoC通信エラー"
        End If
        wait(1000)


        If Not retryIsConnectNFC() Then
            errorProcessContent = "NFC接続エラー"
        End If

        Dim readProtectData As Byte() = retryNfcReadLine(&H0)
        If readProtectData Is Nothing Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        wait(500)

        'ブロック0エリアに書込みができないことを確認
        If retryNfcWriteLine(&H0, readProtectData) Then
            errorProcessContent = "NFC書込みエラー(ブロック0エリア書込み成功)"
            Return False
        End If

        lblInsResult(14).Text = "合格"
        lblInsResult(14).BackColor = Color.Lime
        Me.Refresh()

        Return True
    End Function

    Private Function retryProtectFeRAM(ByVal flg As String) As Boolean
        lblInstructionDisplay.Text = "書込みプロテクト中"

        Dim retryNum As Integer = 5

        For i As Integer = 0 To retryNum - 1
            If protectFeRAM(flg) Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary>
    ''' FeRAMの内容確認（全領域）
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <returns></returns>
    Private Function chkFeRAM(ByVal flg As String) As Boolean
        '--------------------------------------------   
        'No.38[FeRAM読み込み確認(全領域)]
        '--------------------------------------------
        If Not isConnectNFC() Then
            errorProcessContent = "NFC接続エラー"
            Return False
        End If

        'normalMemAreaとcontrolMemAreaのそれぞれでベリファイ

        For i As Integer = 0 To 26

            If Not retryIsConnectNFC() Then
                errorProcessContent = "NFC接続エラー"
                Return False
            End If

            Dim readData As Byte() = retryNfcReadLine(Convert.ToUInt16(16 * i))
            If readData Is Nothing Then
                errorProcessContent = "NFC読込みエラー"
                Return False
            End If

            If verifyLineData(normalAreaMemoryArr(i), readData) < 15 Then
                errorProcessContent = "NFCベリファイエラー"
                Return False
            End If
        Next

        'システムメモリエリア
        '暗号鍵(0x1B0-0x1CF)は読めないので注意
        '0x1D00はNFC通信の試行数？検査中に変化する

        'システムメモリの読み書きの際には暗号化を無効にする
        trans_mode = True
        For i As Integer = 3 To 4

            Dim sysAreaData As Byte() = New Byte(15) {}
            If Not isConnectNFC() Then
                errorProcessContent = "NFC接続エラー"
                Return False
            End If
            '読込み
            If Not Sna.NoWire.Nfc_rf_read(Convert.ToUInt16(16 * i + &H1B0)) Then
                errorProcessContent = "NFC読込みエラー"
                Return False
            End If

            If Not Sna.NoWire.Nfc_rf_DealRecvBuffer_Without_Check() Then
                errorProcessContent = "NFC読込みエラー"
                Return False
            End If

            sysAreaData = RecvData
            'ベリファイ
            If verifyLineData(controlAreaMemoryArr(i), sysAreaData) < 16 Then
                errorProcessContent = "NFCベリファイエラー"
                Return False
            End If

        Next
        trans_mode = False

        lblInsResult(15).Text = "合格"
        lblInsResult(15).BackColor = Color.Lime
        Me.Refresh()

        Return True
    End Function

    Private Function retryChkFeRAM(ByVal flg As String) As Boolean
        lblInstructionDisplay.Text = "FeRAM書込み内容確認中"

        Dim retryNum As Integer = 5

        For i As Integer = 0 To retryNum - 1
            If chkFeRAM(flg) Then
                Return True
            End If
        Next
        Return False

    End Function

    Private Function chkMACSerial(ByVal flg As String, ByVal productType As String) As Boolean
        '-------------------------------------------------
        'No.39[MAC、シリアルNoの読み込み(トンネル)]
        '-------------------------------------------------
        'tunnelReadでMAC、シリアルNoとNormalMemAreaまたはlblMACなどの比較

        If flg <> "1" Then
            lblInsResult(16).Text = String.Empty
            lblInsResult(16).BackColor = Color.Gray
            Me.Refresh()
            Return True
        End If

        'MAC
        Dim extendChkMAC_Serial As New extend_command()
        If Not extendChkMAC_Serial.nfc_connect(True) Then
            errorProcessContent = "NFC接続エラー"
            Return False
        End If

        If Not extendChkMAC_Serial.nfc_send_req(True) Then
            errorProcessContent = "NFC接続エラー"
            Return False
        End If

        Dim verifyMac As Byte() = New Byte(5) {}
        If Not extendChkMAC_Serial.read_Fmac(True, verifyMac) Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        Dim tmpByteMac = lblMAC.Text.Split("-"c)
        For i As Integer = 0 To verifyMac.Length - 1
            If verifyMac(i) <> Convert.ToByte(tmpByteMac(i), 16) Then
                errorProcessContent = "NFCベリファイエラー"
                Return False
            End If
        Next

        'シリアルNo
        Dim verifySerialNo As Byte() = New Byte(3) {}
        If Not extendChkMAC_Serial.read_serialno(True, verifySerialNo) Then
            errorProcessContent = "NFC読込みエラー"
            Return False
        End If

        For i As Integer = 0 To verifySerialNo.Length - 1
            If verifySerialNo(i) <> Convert.ToByte(preIncrementedSerialNo.Substring(i * 2, 2), 16) Then
                errorProcessContent = "NFCベリファイエラー"
                Return False
            End If
        Next

        lblInsResult(16).Text = "合格"
        lblInsResult(16).BackColor = Color.Lime
        Me.Refresh()

        Return True
    End Function

    Private Function retryChkMACSerial(ByVal flg As String, ByVal productType As String) As Boolean
        lblInstructionDisplay.Text = "MAC,シリアルNo確認中"

        Dim retryNum As Integer = 5

        For i As Integer = 0 To retryNum - 1
            If chkMACSerial(flg, productType) Then
                Return True
            End If
        Next
        Return False
    End Function

    '検査終了後のLED目視確認
    Private Function chkLED2(ByVal flg As String) As Boolean
        '-------------------------------------------------
        '40[LED確認(目視)]
        '-------------------------------------------------

        '検査不要の場合
        If flg <> "1" Then
            lblInsResult(17).Text = String.Empty
            lblInsResult(17).BackColor = Color.Gray
            Me.Refresh()
            Return True
        End If

        Using cap As VideoCapture = New VideoCapture(0)
            cap.Set(CaptureProperty.FrameWidth, 640)
            cap.Set(CaptureProperty.FrameHeight, 480)
            cap.Set(CaptureProperty.Fps, 30)
            Dim img As Mat = New Mat()

            Dim winSizeW As Integer = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width
            Dim winSizeH As Integer = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height
            Dim taskbarSizeH As Integer = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height - System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height

            Cv2.NamedWindow("img")
            Cv2.MoveWindow("img", winSizeW - cap.FrameWidth, winSizeH - cap.FrameHeight - taskbarSizeH)

            Do
                lblInstructionDisplay.Text = "LEDの確認を行ってください。" & ControlChars.CrLf &
                                             "合格ならStartSWを押してください。" & ControlChars.CrLf &
                                             "不合格ならBreakSWを押してください。"

                cap.Read(img)
                Cv2.ImShow("img", img)
                Cv2.WaitKey(10)


                Dim byteDataSSW As Byte
                Dim byteDataBSW As Byte
                If Not USBIO.Start_SW(byteDataSSW) Then
                    Return False
                End If

                If byteDataSSW = 0 Then
                    lblInsResult(17).Text = "合格"
                    lblInsResult(17).BackColor = Color.Lime
                    Cv2.DestroyWindow("img")
                    Me.Refresh()
                    Return True
                End If

                If Not USBIO.Break_SW(byteDataBSW) Then
                    Return False
                End If

                If byteDataBSW = 0 Then
                    lblInsResult(17).Text = "不合格"
                    lblInsResult(17).BackColor = Color.Red
                    errorProcessContent = "LED確認2"
                    Cv2.DestroyWindow("img")
                    Me.Refresh()
                    Return False
                End If

                My.Application.DoEvents()
            Loop
        End Using
    End Function

    ''' <summary>
    ''' 検査終了後書込み処理
    ''' </summary>
    ''' <returns></returns>
    Private Function postInspection(ByVal reinspectionFlg As Boolean) As Boolean

        Dim swWEB As Stopwatch = New Stopwatch()
        swWEB.Start()

        '各検査前に検査状態（過電流ワーク検知などを確認）
        Dim insState As String = String.Empty
        If Not chkInspectionState(insState) Then
            lblInsResult(6).BackColor = Color.Red
            lblInsResult(6).Text = "不合格:" & insState
            errorProcessName = "検査前確認"
            errorProcessContent = insState
            Return False
        End If

        '---------------------------------
        '24[電源投入]
        '---------------------------------

        If Not USBIO.EX600_Pow(1) Then
            lblInsResult(6).Text = "不合格"
            lblInsResult(6).BackColor = Color.Red
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If
        wait(1000)

        Dim pstMac As String = String.Empty
        Dim pstIp As String = String.Empty
        Dim pstDeviceName As String = String.Empty

        If Params.PST(1) = "1" Then

            'Profinetマスタの場合にはdevice nameを再度割り当てる
            '(テストプログラム終了時にdevice nameがリセットされネットワーク上で参照できないため)
            If pstBrowse(Params.PST(1)) Then

                lblInstructionDisplay.Text = "Primary Setup Tool:[BROWSE]実行中"

                'Browseの結果を取得
                If Not getPstBrowseResult("EX600", pstMac, pstIp, pstDeviceName) Then
                    lblInsResult(6).Text = "不合格"
                    lblInsResult(6).BackColor = Color.Red
                    errorProcessName = "WEB書込み前BROWSEコマンド"
                    Me.Refresh()
                    Return False
                End If

                lblInstructionDisplay.Text = "Primary Setup Tool:[Name]実行中"
                If Not pstName(pstMac, "test") Then
                    lblInsResult(6).Text = "不合格"
                    lblInsResult(6).BackColor = Color.Red
                    errorProcessName = "WEB書込み前NAMEコマンド"
                    Me.Refresh()
                    Return False
                End If

            Else
                lblInsResult(6).Text = "不合格"
                lblInsResult(6).BackColor = Color.Red
                errorProcessName = "WEB書込み前BROWSEコマンド"
                Me.Refresh()
                Return False
            End If
        End If




        '---------------------------------
        '25[WEBソフトウェア書き込み]
        '---------------------------------
        If Not chkWebWrite2(Params.WEBSoftware(1),
                            Params.WEBSoftware(2),
                            Params.WEBSoftware(3),
                            Params.WEBSoftware(4),
                            Params.WEBSoftware(5),
                            Params.WEBSoftware(6),
                            Params.WEBSoftware(7),
                            Params.WEBSoftware(8)) Then
            lblInsResult(6).Text = "不合格"
            lblInsResult(6).BackColor = Color.Red
            errorProcessName = "WEBソフトウェア書込み"
            Me.Refresh()
            Return False
        End If

        If Params.PST(1) = "1" Then
            lblInstructionDisplay.Text = "Primary Setup Tool:[RESET]実行中"
            If Not pstReset(pstMac) Then
                lblInsResult(6).Text = "不合格"
                lblInsResult(6).BackColor = Color.Red
                errorProcessName = "WEB書込み後RESETコマンド"
                Me.Refresh()
                Return False
            End If

            wait(1000)

            lblInstructionDisplay.Text = "Primary Setup Tool:[BROWSE]実行中"
            If pstBrowse("1") Then
                Dim mac As String = String.Empty
                Dim ip As String = String.Empty
                Dim deviceName As String = String.Empty

                If Not getPstBrowseResult("EX600", mac, ip, deviceName) Then
                    lblInsResult(6).Text = "不合格"
                    lblInsResult(6).BackColor = Color.Red
                    errorProcessName = "WEB書込み後BROWSEコマンド"
                    Me.Refresh()
                    Return False
                End If

                'RESETが正常にできているかを確認
                If deviceName <> "-" OrElse ip <> "0.0.0.0" Then
                    lblInsResult(6).Text = "不合格"
                    lblInsResult(6).BackColor = Color.Red
                    errorProcessName = "WEB書込み後BROWSEコマンド"
                    Me.Refresh()
                    Return False
                End If


            Else
                lblInsResult(6).Text = "不合格"
                lblInsResult(6).BackColor = Color.Red
                errorProcessName = "WEB書込み後BROWSEコマンド"
                Me.Refresh()
                Return False
            End If

        End If

        swWEB.Stop()
        insTimeContent &= "," & swWEB.ElapsedMilliseconds.ToString("0.0")

        Dim swSoftVerRead As Stopwatch = New Stopwatch()
        swSoftVerRead.Start()

        '-------------------------------------------
        '26[ソフトウェアバージョン読み込み]
        '-------------------------------------------
        lblInstructionDisplay.Text = "ソフトウェアバージョン読込み中"

        If Not isConnectNFC() Then
            errorProcessContent = "NFC通信エラー"
            Return False
        End If


        Dim ver_software As Byte() = New Byte(2) {}
        Dim ver_rx210 As Byte() = New Byte(2) {}
        Dim ver_rx62 As Byte() = New Byte(2) {}
        Dim ver_np40 As Byte() = New Byte(2) {}


        If Not readSoftVer(Params.ReadSoftwareVer(1), Params.ProductType(4), ver_rx210, ver_rx62, ver_np40,
                           Params.FormwareVersion(2), Params.FormwareVersion(3), Params.FormwareVersion(4)) Then
            errorProcessName = "ソフトウェアVer読込み"
            Return False
        End If
        '全ての組み合わせが正しければ全体のソフトバージョン決定
        ver_software = cvtSoftVerStrToByte(Params.FormwareVersion(1))
        'NP40verは[1,15,0]などのように読みこまれるため[1,1,5]に変換する
        Dim np40verStr As String = String.Empty
        Dim np40ver As Byte() = New Byte(2) {}
        For i As Integer = 0 To 2
            np40verStr &= ver_np40(i).ToString()
        Next
        '配列が[1,15,0]だと[1150]となるので左から3文字だけ取得する
        np40verStr = np40verStr.Substring(0, 3)
        For i As Integer = 0 To 2
            np40ver(i) = Convert.ToByte(np40verStr.Substring(i, 1))
        Next

        swSoftVerRead.Stop()
        insTimeContent &= "," & swSoftVerRead.ElapsedMilliseconds.ToString("0.0")

        Dim swSoftVerWrite As Stopwatch = New Stopwatch()
        swSoftVerWrite.Start()

        '-----------------------------------------------------
        'No.27[ソフトウェアバージョンFeRAM書き込み]
        '-----------------------------------------------------
        If Not retryWriteSoftVer(Params.WriteSoftVerFeRAM(1), Params.ProductType(4), ver_software, ver_rx210, ver_rx62, np40ver) Then
            lblInsResult(10).Text = "不合格"
            lblInsResult(10).BackColor = Color.Red
            errorProcessName = "ソフトウェアVer書込み"
            Return False
        End If

        swSoftVerWrite.Stop()
        insTimeContent &= "," & swSoftVerWrite.ElapsedMilliseconds.ToString("0.0")


        Dim swPID As Stopwatch = New Stopwatch()
        swPID.Start()

        '---------------------------
        'No.30[PIDの書込み]
        '--------------------------
        '[FeRAMに無線PID(バーコードより取得(文字列))を書き込み(0x0000-0x0003)]
        If Not retryWritePID(Params.WritePID(1)) Then
            lblInsResult(11).Text = "不合格"
            lblInsResult(11).BackColor = Color.Red
            errorProcessName = "PID書込み"
            Return False
        End If

        swPID.Stop()
        insTimeContent &= "," & swPID.ElapsedMilliseconds.ToString("0.0")

        Dim swMAC As Stopwatch = New Stopwatch()
        swMAC.Start()

        '-------------------------------------------------------------
        'No.31[FeRAMにEtherNetMACアドレスを書き込み(0x0005-0x000A)]
        '-------------------------------------------------------------
        'スレーブユニットはMAC書込みをしない
        If Not retryWriteMac(Params.WriteMAC(1)) Then
            lblInsResult(12).Text = "不合格"
            lblInsResult(12).BackColor = Color.Red
            errorProcessName = "MACアドレス書込み"
            Return False
        End If

        swMAC.Stop()
        insTimeContent &= "," & swMAC.ElapsedMilliseconds.ToString("0.0")

        Dim swSerial As Stopwatch = New Stopwatch()
        swSerial.Start()

        '---------------------------------------------------
        'No.33[EtherNetシリアルNo書き込み(トンネル)]
        '---------------------------------------------------
        'シリアル番号が割当てられたワークはシリアル番号を更新しない
        If Not retryWriteSerialNo(Params.WriteSerial(1), hasSerialNo) Then
            lblInsResult(13).Text = "不合格"
            lblInsResult(13).BackColor = Color.Red
            errorProcessName = "シリアルNo書込み"
            Return False
        End If




        'シリアルNoをインクリメントする前の値を保持
        '->No.39シリアルNo確認用
        'インクリメントのタイミングをずらすと書込みプロテクトなどでエラーとなったとき
        '二重でシリアルNoを書き込む可能性があるのでインクリメントはこのタイミングで行う
        preIncrementedSerialNo = lblSerialNo.Text




        'シリアル番号が検査前にすでに割当てられたワークと日常点検ワークではインクリメントしない
        If Params.DailyChk(1) <> "2" AndAlso Not hasSerialNo Then
            If Not incrementSerialNo(Params.WriteSerial(1), Params.WriteSerial(2), lblSerialNo.Text, Params.WriteSerial(3)) Then
                lblInsResult(13).Text = "不合格"
                lblInsResult(13).BackColor = Color.Red
                errorProcessContent = "シリアル番号インクリメントエラー"
                Return False
            End If



            If Not saveSerialNo(Params.WriteSerial(1), Params.WriteSerial(2), lblSerialNo.Text) Then
                lblInsResult(13).Text = "不合格"
                lblInsResult(13).BackColor = Color.Red
                errorProcessContent = "シリアル番号保存エラー"
                Return False
            End If

            'ラベルをここで更新すると履歴に書込み値でなくインクリメント後の値が入るので
            'ラベルの値をインクリメント前にする
            lblSerialNo.Text = preIncrementedSerialNo
            Me.Refresh()

        End If

        swSerial.Stop()
        insTimeContent &= "," & swSerial.ElapsedMilliseconds.ToString("0.0")

        Dim swProtect As Stopwatch = New Stopwatch()
        swProtect.Start()

        '---------------------------------------------------
        'No.34[ID書込み完了フラグ書き込み]
        '---------------------------------------------------
        If Not setIDwriteDoneFlg() Then
            lblInsResult(14).Text = "不合格"
            lblInsResult(14).BackColor = Color.Red
            errorProcessName = "ID書込み完了フラグ書込み"
            Return False
        End If

        '--------------------------------------------------------
        'No.35[製品検査完了フラグ書き込み]
        '--------------------------------------------------------
        If Not setInspectionDoneFlg() Then
            lblInsResult(14).Text = "不合格"
            lblInsResult(14).BackColor = Color.Red
            errorProcessName = "製品検査完了フラグ書込み"
            Return False
        End If

        '--------------------------------------------------------
        'No.36[FeRAMグループ0の書き込みプロテクト]
        '--------------------------------------------------------
        'システムメモリエリア(*13:0x01F0, 0x01F4)に0x01を書き込む
        If Not retryProtectFeRAM(Params.FeRAMProtect(1)) Then
            lblInsResult(14).Text = "不合格"
            lblInsResult(14).BackColor = Color.Red
            errorProcessName = "FeRAMグループ0書込みプロテクト"
            Return False
        End If
        swProtect.Stop()
        insTimeContent &= "," & swProtect.ElapsedMilliseconds.ToString("0.0")

        Dim swFeRAMRead As Stopwatch = New Stopwatch()
        swFeRAMRead.Start()

        '---------------------------------
        '37[電源再投入]
        '---------------------------------
        lblInstructionDisplay.Text = "電源再投入中"

        If Not USBIO.EX600_Pow(0) Then
            lblInsResult(15).Text = "不合格"
            lblInsResult(15).BackColor = Color.Red
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If
        wait(500)


        If Not USBIO.EX600_Pow(1) Then
            lblInsResult(15).Text = "不合格"
            lblInsResult(15).BackColor = Color.Red
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If
        wait(1000)


        '--------------------------------------------   
        'No.38[FeRAM読み込み確認(全領域)]
        '--------------------------------------------
        If Not retryChkFeRAM(Params.ReadFeRAM(1)) Then
            lblInsResult(15).Text = "不合格"
            lblInsResult(15).BackColor = Color.Red
            errorProcessName = "FeRAM読込み確認"
            Return False
        End If
        swFeRAMRead.Stop()
        insTimeContent &= "," & swFeRAMRead.ElapsedMilliseconds.ToString("0.0")

        Dim swMAC_Serial As Stopwatch = New Stopwatch()
        swMAC_Serial.Start()

        '-------------------------------------------------
        'No.39[MAC、シリアルNoの読み込み(トンネル)]
        '-------------------------------------------------
        If Not retryChkMACSerial(Params.ReadMACSerialNo(1), Params.ProductType(4)) Then
            lblInsResult(16).Text = "不合格"
            lblInsResult(16).BackColor = Color.Red
            errorProcessName = "MAC_シリアルNo読込み"
            Return False
        End If

        '--------------------------------------------------
        '41[電源切断]
        '--------------------------------------------------
        lblInstructionDisplay.Text = "電源切断中"

        If Not USBIO.EX600_Pow(0) Then
            lblInsResult(15).Text = "不合格"
            lblInsResult(15).BackColor = Color.Red
            errorProcessContent = "PSoC通信エラー"
            Return False
        End If

        swMAC_Serial.Stop()
        insTimeContent &= "," & swMAC_Serial.ElapsedMilliseconds.ToString("0.0")

        Return True
    End Function

    Private Function inspection(ByVal reinspectionFlg As Boolean) As Boolean

        lblInstructionDisplay.Text = "検査を開始します。"

        If Not setupInspection(reinspectionFlg) Then Return False
        If Not productInspection() Then Return False
        If Not nowireInspection() Then Return False
        If Not postInspection(reinspectionFlg) Then Return False

        Return True

    End Function



    ''' <summary>
    ''' 指定ミリ秒だけウェイト
    ''' </summary>
    ''' <param name="wait_msec"></param>
    ''' <returns></returns>
    Private Function wait(ByVal wait_msec As UInt32) As Boolean

        Dim stpWatch As Stopwatch = New Stopwatch()

        stpWatch.Start()

        Do Until stpWatch.ElapsedMilliseconds > wait_msec
            My.Application.DoEvents()
        Loop

        stpWatch.Stop()

        Return True
    End Function

    ''' <summary>
    ''' 管理PCにアクセスできるかの確認
    ''' 定期的に確認するようにする
    ''' </summary>
    ''' <returns></returns>
    Private Function chkControlPC() As Boolean
        If IO.Directory.Exists(pathControlPC) Then
            Return True
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' NASにアクセスできるかの確認
    ''' 定期的に確認するようにする
    ''' </summary>
    ''' <returns></returns>
    Private Function chkNAS() As Boolean
        If IO.Directory.Exists(pathNAS) Then
            Return True
        Else
            Return False
        End If
    End Function


    ''' <summary>
    ''' 管理PCとNASの存在確認
    ''' </summary>
    Private Function chkPcNas() As Boolean
        If Not IO.Directory.Exists(pathControlPC) Then
            MessageBox.Show(pathControlPC & "にアクセスできません" & ControlChars.CrLf &
                "製造技術担当者に連絡してください" & ControlChars.CrLf &
                "ソフトを終了します", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End If

        If Not IO.Directory.Exists(pathNAS) Then
            MessageBox.Show(pathNAS & "にアクセスできません" & ControlChars.CrLf &
                "製造技術担当者に連絡してください" & ControlChars.CrLf &
                "ソフトを終了します", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End If

        Return True
    End Function

    ''' <summary>
    ''' バーコード①(生産数$品番)の文字列操作
    ''' </summary>
    ''' <param name="bc1"></param>
    ''' <returns></returns>
    Private Function dealBC1(ByVal bc1 As String) As Boolean

        If Not System.Text.RegularExpressions.Regex.IsMatch(bc1, REG_BARCODE_PATTERN_BC1) Then
            MessageBox.Show("バーコード①の書式が不適切です。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        '品番ファイル等の設定読み込みが完了するまでは品番などはラベルに表示しない
        Dim bc1Str As String() = bc1.Split("$"c)
        Dim quantity As String = Convert.ToInt32(bc1Str(0).Trim()).ToString()
        Dim workType As String = bc1Str(1).Trim()
        Dim bytData As Byte


        '品番毎の検査パラメータ取得
        If Not IO.File.Exists(String.Format("{0}{1}{2}.txt", PathSlnFile, pathSettingFolder, workType)) Then
            MessageBox.Show("読み込んだ生産機種の品番リストが存在しません。" & ControlChars.Cr &
                                                      "製造技術担当者に連絡してください。",
                                                      "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If

        If Not readSettingParams(workType) Then
            MessageBox.Show("品番リストの読込みに失敗しました。" & ControlChars.CrLf &
                            "プログラムを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End If

        'ピン治具判定
        If Not USBIO.Pin_Upper(bytData) Then
            MessageBox.Show("PSoC通信エラー" & ControlChars.CrLf &
                            "プログラムを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End If

        If bytData <> Convert.ToByte(Params.PinUpperJudge(1), 16) Then
            MessageBox.Show("使用するピン治具が異なります。" & ControlChars.CrLf &
                            "プログラムを終了します。" & ControlChars.CrLf &
                            "検査機種：" & readJigSetting("pin_upper", Params.PinUpperJudge(1)) & ControlChars.CrLf &
                            "登録機種：" & readJigSetting("pin_upper", bytData.ToString("X")),
                            "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End If


        'ピン変換基板判定
        If Not USBIO.Pin_Config(bytData) Then
            MessageBox.Show("PSoC通信エラー" & ControlChars.CrLf &
                            "プログラムを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End If

        If bytData <> Convert.ToByte(Params.PinBoardJudge(1), 16) Then
            MessageBox.Show("使用するピン変換基板が異なります。" & ControlChars.CrLf &
                            "プログラムを終了します。" & ControlChars.CrLf &
                            "検査機種：" & readJigSetting("connecter", Params.PinBoardJudge(1)) & ControlChars.CrLf &
                            "登録機種：" & readJigSetting("connecter", bytData.ToString("X")),
                            "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End If

        'PLC判別
        If Not USBIO.PLC_Byte(bytData) Then
            MessageBox.Show("PSoC通信エラー" & ControlChars.CrLf &
                            "プログラムを終了します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End If

        If bytData <> Convert.ToByte(Params.PLCJudge(1), 16) Then
            MessageBox.Show("使用するPLCが異なります。" & ControlChars.CrLf &
                            "プログラムを終了します。" & ControlChars.CrLf &
                            "検査機種：" & readJigSetting("plc", Params.PLCJudge(1)) & ControlChars.CrLf &
                            "登録機種：" & readJigSetting("plc", bytData.ToString("X")),
                            "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End If



        lblQuantity.Text = quantity
        lblOrder.Text = quantity
        lblOKWork.Text = String.Empty
        lblWorkType.Text = workType
        lblPaper.Text = String.Empty

        '日常点検ワークを表示する
        lblOkSampleChk.Text = "適合ワーク" & Params.DailyChk(2)
        lblNgSampleChk.Text = "不適合ワーク" & Params.DailyChk(3)


        '日常点検チェック
        '適合サンプル
        If doneDailyChk2(Params.DailyChk(2)) Then
            lblOkSampleChk.BackColor = Color.Lime
        Else
            lblOkSampleChk.BackColor = Color.Red
        End If

        '不適合サンプル
        If doneDailyChk2(Params.DailyChk(3)) Then
            lblNgSampleChk.BackColor = Color.Lime
        Else
            lblNgSampleChk.BackColor = Color.Red
        End If

        Me.Refresh()
        lblSerialNo.Text = loadSerialNo(Params.WriteSerial(1), Params.WriteSerial(2), Params.WriteSerial(3))

        '------------------------------------------------------------------------------------------
        '注意事項の更新
        '------------------------------------------------------------------------------------------

        '日常点検
        If lblOkSampleChk.BackColor <> Color.Lime OrElse lblNgSampleChk.BackColor <> Color.Lime Then
            lblWarningComment.Text = "日常点検が完了していません。"
        Else
            lblWarningComment.Text = String.Empty
        End If

        'シリアル番号
        If lblSerialNo.Text Like "**FFFFFF" Then
            lblWarningComment.Text &= "シリアル番号の残りがありません。"
            MessageBox.Show("シリアル番号が" & lblSerialNo.Text & "です。" & ControlChars.CrLf &
                                        "開発5部に連絡してください。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        ElseIf lblSerialNo.Text Like "**FFFF**" Then
            lblWarningComment.Text &= "シリアル番号が残りわずかです。"
        End If

        If lblWarningComment.Text <> String.Empty Then
            lblWarningComment.BackColor = Color.Yellow
        Else
            lblWarningComment.BackColor = Color.White
        End If



        Return True
    End Function

    ''' <summary>
    ''' PID文字列から各種情報を取得,ラベル表示
    ''' </summary>
    ''' <param name="PID"></param>
    ''' <returns></returns>
    Private Function dealPIDData(ByVal PID As String) As Boolean
        'PID以外の文字列ははじく
        If Not System.Text.RegularExpressions.Regex.IsMatch(PID, REG_BARCODE_PATTERN_PID) Then
            Return False
        End If

        '分解し16進数の数値に変換
        Dim pidStrArr As String() = PID.Split("-"c)
        Dim pidByteArr As Byte() = New Byte(pidStrArr.Length - 1) {}
        For i As Integer = 0 To pidByteArr.Length - 1
            pidByteArr(i) = Convert.ToByte(pidStrArr(i), 16)
        Next

        '[PIDの処理]
        Dim pidFlg As Byte() = New Byte(31) {}
        'pidの頭から順に2進数に変換->32bit
        'インデックスが小さいほど高位ビット
        For i As Integer = 0 To 31
            Select Case i
                Case 0 To 7
                    pidFlg(i) = Convert.ToByte(pidByteArr(0) \ Convert.ToByte(2 ^ ((7 - i) Mod 8)) And &H1)
                Case 8 To 15
                    pidFlg(i) = Convert.ToByte(pidByteArr(1) \ Convert.ToByte(2 ^ ((15 - i) Mod 8)) And &H1)
                Case 16 To 23
                    pidFlg(i) = Convert.ToByte(pidByteArr(2) \ Convert.ToByte(2 ^ ((23 - i) Mod 8)) And &H1)
                Case 24 To 31
                    pidFlg(i) = Convert.ToByte(pidByteArr(3) \ Convert.ToByte(2 ^ ((31 - i) Mod 8)) And &H1)
                Case Else
                    'do nothing
            End Select
        Next

        Dim rotStr As String = String.Empty
        Dim productType1Str As String = String.Empty
        Dim productType2Str As String = String.Empty
        Dim serialNoBitStr As String = String.Empty

        '詳細はID銘板仕様を参照
        For i As Integer = 0 To pidFlg.Length - 1
            Select Case i
                Case 0 To 9
                    rotStr &= pidFlg(i) 'ロット年月
                Case 10 To 11
                    productType1Str &= pidFlg(i) '製品種別1
                Case 12 To 17
                    productType2Str &= pidFlg(i) '製品種別2
                Case 18 To 31
                    serialNoBitStr &= pidFlg(i) 'シリアル番号
                Case Else
                    'do nothing
            End Select
        Next

        Dim rotYear As Integer = Convert.ToInt32(rotStr, 2) \ 12 + 2015
        Dim rotMonth As Integer = Convert.ToInt32(rotStr, 2) Mod 12
        Dim rotDate As New Date(rotYear, rotMonth, 1, 0, 0, 0) '(ロットの基準は2015年1月)

        'ロット番号が未来になっていないことを確認
        If Date.Now.Date < rotDate Then
            MessageBox.Show(String.Format("ロット番号が現在よりも未来となっています。" & ControlChars.Cr &
                                "製造技術担当者に連絡してください。" & ControlChars.Cr &
                                "検査ワークのロット番号：{0}年{1}月" & ControlChars.Cr, rotYear, rotMonth))
            Return False
        End If


        'todo:品番ごとに異なる値、品番リストなどから適切な値を読み込む
        If productType1Str <> Params.ProductType(1) Then
            MessageBox.Show("製品種別1が不適です。" & ControlChars.Cr &
                                "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                "検査ワークの製品種別1：" & productType1Str & ControlChars.NewLine &
                                "製品種別1設定：" & Params.ProductType(1), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If


        If productType2Str <> Params.ProductType(2) Then
            MessageBox.Show("製品種別2が不適です。" & ControlChars.Cr &
                                "製造技術担当者に連絡してください。" & ControlChars.NewLine &
                                "検査ワークの製品種別2：" & productType2Str & ControlChars.NewLine &
                                "製品種別2設定：" & Params.ProductType(2), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If

        'シリアル番号(PIDを生成する際の連番の数字)をビットから16進数に変換
        '今回は利用しない
        Dim serialNo As String = String.Empty
        For i = 0 To serialNoBitStr.Length - 1
            serialNo = serialNo & serialNoBitStr(i)
        Next

        lblPID.Text = PID

        Return True

    End Function

    Private Function dealMACData(ByVal MAC As String) As Boolean

        'MAC以外の文字列ははじく
        If Not System.Text.RegularExpressions.Regex.IsMatch(MAC, REG_BARCODE_PATTERN_MAC) Then
            Return False
        End If
        '[SMC固有MAC-製品固有MAC]が最初の8バイトにない
        If Not MAC.IndexOf(String.Format("{0}-{1}", SMC_MAC_ID, Params.ProductType(3))) = 0 Then
            MessageBox.Show("MACの値が不正です。" & ControlChars.NewLine &
                            "製品固有MAC：" & String.Format("{0}-{1}-**-**", SMC_MAC_ID, Params.ProductType(3)) & ControlChars.NewLine &
                            "読み込んだMAC：" & MAC, "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If

        'PROFINETの場合はMACアドレスを+3ずつインクリメントするのでID銘板MACアドレスは常に3n+mとなる
        'サンプルワークのMAC:00-23-C6-2C-00-0Aなので3n+1で設定
        If Params.ProductType(3) = "2C" Then
            Dim strMAC As String() = MAC.Split("-"c)

            Dim MACLow4 As Long = Convert.ToInt64(strMAC(4) & strMAC(5), 16)
            If MACLow4 Mod 3 <> 1 Then
                MessageBox.Show("MAC下位4桁の値が不正です。" & ControlChars.NewLine &
                                "読み込んだMAC：" & MAC, "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False
            End If

        End If

        lblMAC.Text = MAC

        Return True
    End Function

    ''' <summary>
    ''' 検査合格時のブザー
    ''' 2回ならす
    ''' </summary>
    ''' <returns></returns>
    Private Function OKBuzzer() As Boolean

        If Not USBIO.Buzzer(1) Then
            Return False
        End If

        wait(500)

        If Not USBIO.Buzzer(0) Then
            Return False
        End If

        wait(500)

        If Not USBIO.Buzzer(1) Then
            Return False
        End If

        wait(500)

        If Not USBIO.Buzzer(0) Then
            Return False
        End If

        Return True

    End Function
    ''' <summary>
    ''' 検査不適合時のブザー
    ''' ボタンを押して止めるまで鳴らし続ける
    ''' </summary>
    ''' <returns></returns>
    Private Function NGBuzzer() As Boolean

        lblInstructionDisplay.Text = "ブレークSWを押してブザーを止めてください。"
        Me.Refresh()

        If Not USBIO.Buzzer(1) Then
            MessageBox.Show("PSoC通信エラー(NGブザー)")
            Return False
        End If

        'ブレークSWを押すまで鳴らし続ける
        Do
            Dim bytData As Byte = 0

            If Not USBIO.Break_SW(bytData) Then
                Return False
            End If

            'ブレークSW:ON
            If bytData = 0 Then
                If USBIO.Buzzer(0) Then Return False
                Exit Do
            End If
            '回路などの状態監視用の処理をさらに入れる？
            My.Application.DoEvents()
        Loop


        Return True
    End Function

    ''' <summary>
    ''' Byte型データをフラグ(0)~(7)に分解する
    ''' flg(i):iビット目フラグ
    ''' </summary>
    ''' <param name="byteData"></param>
    ''' <returns></returns>
    Private Function byteToEightBitFlag(ByVal byteData As Byte) As Byte()

        Dim flg As Byte() = New Byte(7) {}

        For i As Integer = 0 To 7
            flg(i) = Convert.ToByte((byteData \ Convert.ToByte(2 ^ i)) And &H1)
        Next

        Return flg

    End Function


    '-------------------------------------------------------------------------------------------------------------
    '
    '                                           画像処理関連の処理
    '
    '-------------------------------------------------------------------------------------------------------------
    ''' <summary>
    ''' LED確認(画像処理)設定ファイル読込み
    ''' </summary>
    ''' <returns></returns>
    Private Function readLEDChkSetting() As Boolean

        Try
            Using sr As IO.StreamReader = New IO.StreamReader(PathSlnFile & pathLEDChkSetting, enc_sj)
                Do While sr.Peek() > 0
                    Dim tmpStr As String = sr.ReadLine()

                    If tmpStr.IndexOf("[明度閾値]") >= 0 Then
                        chkLED_Setting.TH_V = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[彩度閾値]") >= 0 Then
                        chkLED_Setting.TH_S = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[ラベル面積MAX]") >= 0 Then
                        chkLED_Setting.LED_Area_MAX = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[ラベル面積MIN]") >= 0 Then
                        chkLED_Setting.LED_Area_MIN = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[赤色色相閾値MAX]") >= 0 Then
                        chkLED_Setting.TH_H_RED_MAX = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[赤色色相閾値MIN]") >= 0 Then
                        chkLED_Setting.TH_H_RED_MIN = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[緑色色相閾値MAX]") >= 0 Then
                        chkLED_Setting.TH_H_GREEN_MAX = tmpStr.Split(","c)
                    ElseIf tmpStr.IndexOf("[緑色色相閾値MIN]") >= 0 Then
                        chkLED_Setting.TH_H_GREEN_MIN = tmpStr.Split(","c)
                    Else
                        MessageBox.Show("登録されていないパラメータ：" & tmpStr)
                        Return False
                    End If

                    My.Application.DoEvents()
                Loop
            End Using

            LEDColorTh.Red_Max = Integer.Parse(chkLED_Setting.TH_H_RED_MAX(1))
            LEDColorTh.Red_Min = Integer.Parse(chkLED_Setting.TH_H_RED_MIN(1))
            LEDColorTh.Green_Max = Integer.Parse(chkLED_Setting.TH_H_GREEN_MAX(1))
            LEDColorTh.Green_Min = Integer.Parse(chkLED_Setting.TH_H_GREEN_MIN(1))

        Catch ex As Exception
            writeErrorLog(ex)
            Return False
        End Try

        Return True
    End Function


    ''' <summary>
    ''' Cv2.waitkey(msec)の替わり(PictureBoxに画像を表示する場合に使用する)
    ''' </summary>
    ''' <param name="msec"></param>
    Private Sub waitBySW(ByVal msec As Integer)
        Dim sw As Stopwatch = New Stopwatch()
        sw.Start()

        Do Until sw.Elapsed > New TimeSpan(0, 0, 0, 0, msec)
            My.Application.DoEvents()
        Loop

        sw.Stop()
        sw.Reset()
        Me.Refresh()
    End Sub

    ''' <summary>
    ''' 入力した色のパターンに関して循環パターン(Listの配列)を作る
    ''' </summary>
    ''' <param name="colorPattern"></param>
    ''' <returns></returns>

    Private Function createCirculationPattern(ByVal colorPattern As List(Of LED_COLOR)) As List(Of LED_COLOR)()

        Dim circulationPattern As List(Of LED_COLOR)() = New List(Of LED_COLOR)(colorPattern.Count - 1) {}
        For i As Integer = 0 To circulationPattern.Count - 1
            circulationPattern(i) = New List(Of LED_COLOR)
        Next

        For i As Integer = 0 To colorPattern.Count - 1

            For j As Integer = 0 To colorPattern.Count - 1
                circulationPattern(i).Add(colorPattern(j))
            Next
            'パターンの循環を作る(先頭の要素を末尾に持っていく)
            colorPattern.Add(colorPattern(0))
            colorPattern.RemoveAt(0)
        Next

        Return circulationPattern
    End Function

    ''' <summary>
    ''' 指定した矩形領域のHue値の平均を求める
    ''' Sat値でのマスク処理
    ''' </summary>
    ''' <param name="img"></param>
    ''' <param name="rect"></param>
    ''' <param name="satImg"></param>
    ''' <returns></returns>
    Private Function calcHueAveRect(ByRef img As Mat, ByVal rect As Rect, ByRef satImg As Mat) As Double

        If img.Channels <> 1 Then
            Return 0
        End If

        Dim ave As Double = 0
        Dim count As Long = 0
        Dim val As Byte = 0

        For j As Integer = rect.Top To rect.Bottom - 1
            For i As Integer = rect.Left To rect.Right - 1

                '彩度の小さい箇所（LEDの光が強く白く映っているところ）を除く
                If satImg.At(Of Byte)(j, i) >= 30 Then

                    '*2はHueの範囲を[0：360]とするため
                    'ホワイトバランス等の問題で赤(0°(または360°)付近が不安定となるので
                    '色相が紫付近の場合は360°からの距離をとる
                    val = img.At(Of Byte)(j, i)
                    If val < 150 Then
                        ave += 2 * val
                    Else
                        ave += 360 - 2 * val
                    End If
                    count += 1
                End If

            Next
        Next
        ave /= CDbl(count)

        Return ave

    End Function
    ''' <summary>
    ''' 指定した矩形領域のHue値の平均を求める
    ''' Val,Sat値でのマスク処理
    ''' </summary>
    ''' <param name="img"></param>
    ''' <param name="rect"></param>
    ''' <param name="valImg"></param>
    ''' <param name="satImg"></param>
    ''' <returns></returns>
    Private Function calcHueAveRect(ByRef img As Mat, ByVal rect As Rect, ByRef valImg As Mat, ByRef satImg As Mat) As Double

        If img.Channels <> 1 Then
            Return 0
        End If

        Dim ave As Double = 0
        Dim count As Long = 0
        Dim val As Byte = 0

        Dim thV As Integer '明度閾値
        Dim thS As Integer '彩度閾値
        Try
            thV = Integer.Parse(chkLED_Setting.TH_V(1))
            thS = Integer.Parse(chkLED_Setting.TH_S(1))
        Catch ex As Exception
            writeErrorLog(ex)
            'エラーの際にはデフォルト値を入れる
            thV = 50
            thS = 150
        End Try

        For j As Integer = rect.Top To rect.Bottom - 1
            For i As Integer = rect.Left To rect.Right - 1

                '明度、彩度の小さい箇所（LEDの光が強く白く映っているところ）を除く
                'If valImg.At(Of Byte)(j, i) > 50 AndAlso satImg.At(Of Byte)(j, i) > 150 Then
                If valImg.At(Of Byte)(j, i) > thV AndAlso satImg.At(Of Byte)(j, i) > thS Then

                    '*2はHueの範囲を[0:360]とするため
                    'ホワイトバランス等の問題で赤(0°(または360°)付近が不安定となるので
                    '色相が紫付近(青240°,赤360°の中間)の場合は360°からの距離をとる
                    val = img.At(Of Byte)(j, i)
                    If val < 150 Then
                        ave += 2 * val
                    Else
                        ave += 360 - 2 * val
                    End If
                    count += 1
                End If

            Next
        Next
        ave /= CDbl(count)

        Return ave

    End Function
    ''' <summary>
    ''' Hueの値毎に色を返す
    ''' </summary>
    ''' <param name="hVal"></param>
    ''' <returns></returns>
    Private Function judgeColor(ByVal hVal As Double) As LED_COLOR

        If hVal > LEDColorTh.Red_Min AndAlso hVal <= LEDColorTh.Red_Max Then
            Return LED_COLOR.RED
        ElseIf hVal >= LEDColorTh.Green_Min AndAlso hVal <= LEDColorTh.Green_Max Then
            Return LED_COLOR.GREEN
        Else
            Return LED_COLOR.OTHER
        End If

        'If hVal > 0 AndAlso hVal <= 20 Then
        '    Return LED_COLOR.RED
        'ElseIf hVal >= 110 AndAlso hVal <= 130 Then
        '    Return LED_COLOR.GREEN
        'Else
        '    Return LED_COLOR.OTHER
        'End If

    End Function


    ''' <summary>
    ''' マスタユニットLED自動検査リトライ版
    ''' </summary>
    ''' <param name="cap"></param>
    ''' <param name="pattern"></param>
    ''' <returns></returns>
    Private Function retryChkMasterUnitLED(ByRef cap As VideoCapture, ByVal pattern As List(Of LED_COLOR)) As Boolean

        Dim retryNum As Integer = 5

        For i As Integer = 0 To retryNum - 1
            If chkMasterUnitLED(cap, pattern) Then
                Return True
            End If
        Next

        Return False

    End Function

    ''' <summary>
    ''' chkLEDPattern2の引数変更)
    ''' </summary>
    ''' <param name="cap"></param>
    ''' <param name="pattern"></param>
    ''' <returns></returns>
    Private Function chkMasterUnitLED(ByRef cap As VideoCapture, ByVal pattern As List(Of LED_COLOR)) As Boolean

        Dim colorPattern As New List(Of LED_COLOR) '検出した色パターン
        Dim cycleTimer As Stopwatch = New Stopwatch() 'サイクルタイム
        Dim timeout_msec As Integer = 4000 'サイクルタイムオーバー[msec] 
        Dim resultImg As Mat() = New Mat(pattern.Count - 1) {} '結果保存用


        For i As Integer = 0 To pattern.Count - 1
            resultImg(i) = New Mat()
        Next

        Dim img As Mat = New Mat()
        Dim color As New List(Of LED_COLOR) '各取得画像に対してラベル領域の色を保持
        Cv2.NamedWindow("chkLED")

        'パラメータ読込み
        Dim thLEDAreaMax As Integer 'LEDラベル面積(MAX)
        Dim thLEDAreaMin As Integer 'LEDラベル面積(MIN)
        Dim maskThS As Integer '彩度閾値
        Dim maskThV As Integer '明度閾値
        Try
            thLEDAreaMax = Integer.Parse(chkLED_Setting.LED_Area_MAX(1))
            thLEDAreaMin = Integer.Parse(chkLED_Setting.LED_Area_MIN(1))
            maskThS = Integer.Parse(chkLED_Setting.TH_S(1))
            maskThV = Integer.Parse(chkLED_Setting.TH_V(1))
        Catch ex As Exception
            thLEDAreaMax = 1000
            thLEDAreaMin = 30
            maskThS = 150
            maskThV = 50
            writeErrorLog(ex)
        End Try


        cycleTimer.Start()

        Do While colorPattern.Count < pattern.Count

            '[初期化]
            color.Clear()
            '[画像のキャプチャ]
            cap.Read(img)


            If cycleTimer.ElapsedMilliseconds > timeout_msec Then
                'gColorList = colorPattern
                Cv2.PutText(img, "Cycletime Over", New Point(img.Width / 10, img.Height / 10), HersheyFonts.HersheyComplex, 1, New Scalar(255, 0, 255))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                Cv2.DestroyAllWindows()
                Return False
            End If

            '[HSV変換]
            Dim imgHSV As Mat = New Mat()
            Cv2.CvtColor(img, imgHSV, ColorConversionCodes.BGR2HSV)
            'HとVの値をそれぞれ仕様するため分割
            Dim imgSepHSV() As Mat = New Mat(imgHSV.Channels - 1) {}
            Cv2.Split(imgHSV, imgSepHSV)


            '[輝度値(V)で2値化]
            Dim mask As Mat = New Mat()
            Cv2.Threshold(imgSepHSV(2), mask, maskThV, 255, ThresholdTypes.Binary)
            'Cv2.ImShow("mask", mask)
            'Cv2.WaitKey(10)

            '[マスク画像のフィルタリング]
            mask.MedianBlur(5)
            'Cv2.Erode(mask, mask, Mat.Ones(5, 5, MatType.CV_8UC1))
            Cv2.Dilate(mask, mask, Mat.Ones(5, 5, MatType.CV_8UC1))
            'Cv2.ImShow("mask", mask)
            'Cv2.WaitKey(10)

            '[ラベリング]
            Dim cc As ConnectedComponents = Cv2.ConnectedComponentsEx(mask)
            '[LED領域のフィルタリング]
            'ラベリングされている外枠部分を除外
            'todo:値の調整
            Dim filterCC = From ft In cc.Blobs Where ft.Area < thLEDAreaMax AndAlso ft.Area > thLEDAreaMin

            For Each fc In filterCC
                img.Rectangle(fc.Rect, New Scalar(0, 255, 255), 1)
            Next
            'Cv2.ImShow("fc", img)
            'Cv2.WaitKey(15)

            '[画像中のLEDの色の登録->color]

            'LEDが消灯の場合
            If filterCC.Count = 0 Then

                If colorPattern.Count = 0 Then
                    img.CopyTo(resultImg(color.Count))
                    colorPattern.Add(LED_COLOR.OFF)

                    Cv2.ImShow("chkLED", img)
                    waitBySW(20)

                    Continue Do
                End If
                'LED状態の切り替わりが起きた場合
                If colorPattern.Last <> LED_COLOR.OFF Then
                    img.CopyTo(resultImg(colorPattern.Count))
                    colorPattern.Add(LED_COLOR.OFF)

                    Cv2.ImShow("chkLED", img)
                    waitBySW(20)

                End If
                Continue Do
            End If


            'LEDが全て点灯していない状態でキャプチャを行った場合
            Dim maxLEDNum As Integer = 8
            'todo:ラベルの面積閾値の調整
            If filterCC.Count <> maxLEDNum Then
                Continue Do
            End If

            'Hue画像から各ラベル領域の色を算出
            For Each blb In filterCC
                color.Add(judgeColor(calcHueAveRect(imgSepHSV(0), blb.Rect, imgSepHSV(2), imgSepHSV(1))))
                img.Rectangle(blb.Rect, New Scalar(0, 0, 255))
            Next

            '全ラベル領域の色の一致確認
            For Each i In color
                If i <> color.First() Then
                    'MessageBox.Show("all colors are not same")
                    Continue Do
                End If
            Next

            '消灯状態でなく全てのラベル領域が同色
            '初回
            If colorPattern.Count = 0 Then
                img.CopyTo(resultImg(colorPattern.Count))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                colorPattern.Add(color.First())
                Continue Do
            End If
            '検出した色の切り替わりが起きた時
            If colorPattern.Last() <> color.First() Then
                img.CopyTo(resultImg(colorPattern.Count))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                colorPattern.Add(color.First())
            End If

            My.Application.DoEvents()
        Loop



        '[パターン比較]
        Dim isMatch As Boolean = True

        '循環パターンの作成(パターン数 = pattern.length)
        Dim ciculationPattern As List(Of LED_COLOR)() = createCirculationPattern(pattern)
        '循環パターンのいずれかとの一致を確認
        For i As Integer = 0 To ciculationPattern.Count - 1

            isMatch = True '初期化
            For j As Integer = 0 To colorPattern.Count - 1
                isMatch = isMatch AndAlso colorPattern.Item(j) = ciculationPattern(i).Item(j)
            Next
            If isMatch Then
                Exit For
            End If
        Next


        '結果画像作成＆表示
        Dim combinedImg As Mat = New Mat(New Size(img.Width, img.Height), MatType.CV_8UC3)
        For i As Integer = 0 To resultImg.Count - 1
            Cv2.Resize(resultImg(i), resultImg(i), New Size(0.5 * combinedImg.Width, 0.5 * combinedImg.Height))
            Cv2.PutText(resultImg(i), "No." & i.ToString & ":" & colorPattern.Item(i).ToString(), New Point(resultImg(i).Width / 10, resultImg(i).Height / 10), HersheyFonts.HersheyComplex, 1, New Scalar(255, 0, 255))
        Next
        resultImg(0).CopyTo(combinedImg(New Rect(New Point(0, 0), New Size(resultImg(0).Width, resultImg(0).Height))))
        resultImg(1).CopyTo(combinedImg(New Rect(New Point(320, 0), New Size(resultImg(1).Width, resultImg(1).Height))))
        resultImg(2).CopyTo(combinedImg(New Rect(New Point(0, 240), New Size(resultImg(2).Width, resultImg(2).Height))))
        resultImg(3).CopyTo(combinedImg(New Rect(New Point(320, 240), New Size(resultImg(3).Width, resultImg(3).Height))))

        Dim cpStr As String = String.Empty
        Dim correctPattern As String = String.Empty
        '取得した色のパターン
        For i As Integer = 0 To colorPattern.Count - 1
            cpStr = cpStr & colorPattern.Item(i).ToString() & "->"
        Next
        '検出したいの色パターン
        For i As Integer = 0 To pattern.Count - 1
            correctPattern = correctPattern & pattern.Item(i).ToString() & "->"
        Next

        combinedImg.PutText("Result:" & If(isMatch, "[OK]", "[NG]") & " " & cpStr, New Point(combinedImg.Width * 1 / 10, combinedImg.Height * 9 / 10), HersheyFonts.HersheyComplex, 0.5, New Scalar(255, 0, 255))
        combinedImg.PutText("input pattern:" & correctPattern, New Point(combinedImg.Width * 1 / 10, combinedImg.Height * 9.5 / 10), HersheyFonts.HersheyComplex, 0.5, New Scalar(255, 0, 255))
        Cv2.ImShow("chkLED", combinedImg)
        waitBySW(20)
        combinedImg.SaveImage(String.Format("{0}\LED検査結果画像(master).jpg", PathSlnFile))
        combinedImg.Release()
        Cv2.DestroyWindow("chkLED")

        '入力パターンとの一致するか
        If isMatch Then
            Return True
        Else
            Return False
        End If

    End Function


    ''' <summary>
    ''' スレーブユニットLED検査リトライ版
    ''' </summary>
    ''' <param name="cap"></param>
    ''' <param name="pattern"></param>
    ''' <returns></returns>
    Private Function retryChkSlaveUnitLED(ByRef cap As VideoCapture, ByVal pattern As List(Of LED_COLOR)) As Boolean

        Dim retryNum As Integer = 5

        For i As Integer = 0 To retryNum - 1
            If chkSlaveUnitLED(cap, pattern) Then
                Return True
            End If
        Next

        Return False

    End Function

    Private Function chkSlaveUnitLED(ByRef cap As VideoCapture, ByVal pattern As List(Of LED_COLOR)) As Boolean

        Dim colorPattern As New List(Of LED_COLOR) '検出した色パターン
        Dim cycleTimer As Stopwatch = New Stopwatch() 'サイクルタイム
        Dim timeout_msec As Integer = 4000 'サイクルタイムオーバー[msec] 
        Dim resultImg As Mat() = New Mat(pattern.Count - 1) {} '結果保存用

        For i As Integer = 0 To resultImg.Length - 1
            resultImg(i) = New Mat()
        Next

        Dim img As Mat = New Mat()
        Dim color As New List(Of LED_COLOR) '各取得画像に対してラベル領域の色を保持
        Cv2.NamedWindow("chkLED")

        'パラメータ読込み
        Dim thLEDAreaMax As Integer 'LEDラベル面積(MAX)
        Dim thLEDAreaMin As Integer 'LEDラベル面積(MIN)
        Dim maskThS As Integer '彩度閾値
        Dim maskThV As Integer '明度閾値
        Try
            thLEDAreaMax = Integer.Parse(chkLED_Setting.LED_Area_MAX(1))
            thLEDAreaMin = Integer.Parse(chkLED_Setting.LED_Area_MIN(1))
            maskThS = Integer.Parse(chkLED_Setting.TH_S(1))
            maskThV = Integer.Parse(chkLED_Setting.TH_V(1))
        Catch ex As Exception
            thLEDAreaMax = 1000
            thLEDAreaMin = 30
            maskThS = 150
            maskThV = 50
            writeErrorLog(ex)
        End Try

        cycleTimer.Start()

        Do While colorPattern.Count < pattern.Count

            '[初期化]
            color.Clear()
            '[画像のキャプチャ]
            cap.Read(img)


            '[サイクルタイムチェック]
            If cycleTimer.ElapsedMilliseconds > timeout_msec Then
                Cv2.PutText(img, "Cycletime Over", New Point(img.Width / 10, img.Height / 10), HersheyFonts.HersheyComplex, 1, New Scalar(255, 0, 255))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                Cv2.DestroyAllWindows()
                Return False
            End If

            '[HSV変換]
            Dim imgHSV As Mat = New Mat()
            Cv2.CvtColor(img, imgHSV, ColorConversionCodes.BGR2HSV)
            'HとVの値をそれぞれ仕様するため分割
            Dim imgSepHSV() As Mat = New Mat(imgHSV.Channels - 1) {}
            Cv2.Split(imgHSV, imgSepHSV)


            '[輝度値(V)で2値化]
            Dim mask As Mat = New Mat()
            Cv2.Threshold(imgSepHSV(2), mask, maskThV, 255, ThresholdTypes.Binary)
            'Cv2.ImShow("mask", mask)
            'Cv2.ImShow("image", img)
            'Cv2.WaitKey(10)

            '[マスク画像のフィルタリング]
            mask.MedianBlur(5)
            'Cv2.Erode(mask, mask, Mat.Ones(5, 5, MatType.CV_8UC1))
            Cv2.Dilate(mask, mask, Mat.Ones(5, 5, MatType.CV_8UC1))


            '[ラベリング]
            Dim cc As ConnectedComponents = Cv2.ConnectedComponentsEx(mask)
            '[LED領域のフィルタリング]
            'ラベリングされている外枠部分を除外
            Dim filterCC = From ft In cc.Blobs Where ft.Area < thLEDAreaMax AndAlso ft.Area > thLEDAreaMin

            '[画像中のLEDの色の登録->color]

            'LEDが消灯の場合
            If filterCC.Count = 0 Then

                If colorPattern.Count = 0 Then
                    img.CopyTo(resultImg(color.Count))
                    colorPattern.Add(LED_COLOR.OFF)

                    Cv2.ImShow("chkLED", img)
                    waitBySW(20)

                    Continue Do
                End If
                'LED状態の切り替わりが起きた場合
                If colorPattern.Last <> LED_COLOR.OFF Then
                    img.CopyTo(resultImg(colorPattern.Count))
                    colorPattern.Add(LED_COLOR.OFF)

                    Cv2.ImShow("chkLED", img)
                    waitBySW(20)

                End If
                Continue Do
            End If

            'LEDが全て点灯していない状態でキャプチャを行った場合
            Dim maxLEDNum As Integer = 4
            'todo:ラベルの面積閾値の調整
            If filterCC.Count <> maxLEDNum Then
                Continue Do
            End If

            'Hue画像から各ラベル領域の色を算出
            For Each blb In filterCC
                color.Add(judgeColor(calcHueAveRect(imgSepHSV(0), blb.Rect, imgSepHSV(2), imgSepHSV(1))))
                img.Rectangle(blb.Rect, New Scalar(0, 0, 255))
            Next

            '全ラベル領域の色の一致確認
            For Each i In color
                If i <> color.First() Then
                    'MessageBox.Show("all colors are not same")
                    Continue Do
                End If
            Next

            '--------------------------------------
            '           位置合わせ処理
            '--------------------------------------
            Dim centerPoint As Point2d = New Point2d(0, 0) '全ラベル重心を加重平均した点
            Dim centerLabel As Point2d
            Dim leftPoint As New Point2d(img.Width, img.Height) '画面の右端で初期化
            Dim rightPoint As New Point2d(0, 0) '画面の左端で初期化
            Dim topPoint As New Point2d(0, 0) '画面の下端で初期化
            Dim bottomPoint As New Point2d(img.Width, img.Height) '画面の上端で初期化


            '[中央のラベルを求める]
            '[各ラベル重心の平均を算出]
            For Each lbl In filterCC
                centerPoint.X += lbl.Centroid.X / (filterCC.Count)
                centerPoint.Y += lbl.Centroid.Y / (filterCC.Count)
            Next

            '全てのラベル重心に最も近いラベルを求める
            '[距離を計算]
            Dim minDistance As Double = Math.Sqrt(img.Width ^ 2 + img.Height ^ 2) '初期値(画像内の最大距離)

            For Each lbl In filterCC
                Dim distance As Double = Math.Sqrt((lbl.Centroid.X - centerPoint.X) ^ 2 + (lbl.Centroid.Y - centerPoint.Y) ^ 2)

                '最も距離の近いラベルを探索
                If distance < minDistance Then
                    minDistance = distance
                    centerLabel.X = lbl.Centroid.X
                    centerLabel.Y = lbl.Centroid.Y
                End If
                '左端(x=0)に近いラベルを探索
                If lbl.Centroid.X < leftPoint.X Then
                    leftPoint.X = lbl.Centroid.X
                    leftPoint.Y = lbl.Centroid.Y
                End If
                '右端(x=img.width)に近いラベルを探索
                If lbl.Centroid.X > rightPoint.X Then
                    rightPoint.X = lbl.Centroid.X
                    rightPoint.Y = lbl.Centroid.Y
                End If
                '下端(y=img.height)に近いラベルを探索
                If lbl.Centroid.Y > topPoint.Y Then
                    topPoint.X = lbl.Centroid.X
                    topPoint.Y = lbl.Centroid.Y
                End If
                '上端(y=0)に近いラベルを探索
                If lbl.Centroid.Y < bottomPoint.Y Then
                    bottomPoint.X = lbl.Centroid.X
                    bottomPoint.Y = lbl.Centroid.Y
                End If

            Next

            '[中央の左ラベル位置を求める]
            minDistance = Math.Sqrt(img.Width ^ 2 + img.Height ^ 2) '初期値(画像内の最大距離)

            Dim leftCenter As New Point2d()
            For Each lbl In filterCC
                'X座標の基準は左端ラベルのX座標
                'Y座標の基準は中央ラベルのY座標
                Dim distance As Double = Math.Sqrt((lbl.Centroid.X - leftPoint.X) ^ 2 + (lbl.Centroid.Y - centerLabel.Y) ^ 2)
                If distance < minDistance Then
                    minDistance = distance
                    leftCenter.X = lbl.Centroid.X
                    leftCenter.Y = lbl.Centroid.Y
                End If
            Next

            '[中央の右ラベル位置を求める]
            minDistance = Math.Sqrt(img.Width ^ 2 + img.Height ^ 2) '初期値(画像内の最大距離)

            'Dim rightCenter As New Point2d(0, centerLabel.Y)
            Dim rightCenter As New Point2d()
            For Each lbl In filterCC
                'X座標の基準は右端ラベルのX座標
                'Y座標の基準は中央ラベルのY座標
                Dim distance As Double = Math.Sqrt((lbl.Centroid.X - rightPoint.X) ^ 2 + (lbl.Centroid.Y - centerLabel.Y) ^ 2)
                If distance < minDistance Then
                    minDistance = distance
                    rightCenter.X = lbl.Centroid.X
                    rightCenter.Y = lbl.Centroid.Y
                End If
            Next

            '[ラベル重心を用いてθ方向の補正]

            '[中央行の左右ラベル重心からθ方向の傾きを算出]
            Dim disX As Double = -leftCenter.X + rightCenter.X
            Dim disY As Double = -leftCenter.Y + rightCenter.Y
            Dim theta As Double = If(disX <> 0, Math.Atan(disY / disX) * 180 / Math.PI, 0) 'disXでZeroDivisionErrorの可能性
            '[中央ラベルを回転中心]とした回転行列算出
            Dim rotMat As Mat = Cv2.GetRotationMatrix2D(New Point2f(Convert.ToSingle(centerLabel.X), Convert.ToSingle(centerLabel.Y)), theta, 1.0)

            Cv2.WarpAffine(img, img, rotMat, New Size(img.Width, img.Height))
            'Cv2.ImShow("warping", img)
            'Cv2.WaitKey(10)


            '位置補正のための座標を算出(中央ラベル座標（回転中心）からLEDのX,Y方向間隔だけずれた位置に他のLED)

            '[LEDの配置間隔を算出]

            '   LEDの並び
            '   o   o   o
            '   o   o   o
            '     o     o


            'img.Circle(New Point(leftCenter.X, leftCenter.Y), 20, New Scalar(255, 0, 0))
            'img.Circle(New Point(centerLabel.X, centerLabel.Y), 20, New Scalar(0, 255, 0))
            'img.Circle(New Point(bottomPoint.X, bottomPoint.Y), 20, New Scalar(0, 0, 255))
            'Cv2.ImShow("base", img)
            'Cv2.WaitKey(0)


            'x方向のLED間隔は中央左端と中央ラベルの距離
            Dim xBase As Double = Math.Sqrt((leftCenter.X - centerLabel.X) ^ 2 + (leftCenter.Y - centerLabel.Y) ^ 2)
            'todo:
            'y方向のLED間隔は下段右ラベルと中央右ラベルの距離
            Dim yBase As Double = Math.Sqrt((leftCenter.X - bottomPoint.X) ^ 2 + (leftCenter.Y - bottomPoint.Y) ^ 2)

            Dim width As Integer = 15 '色を計算する際の矩形幅
            Dim height As Integer = 15 '色を計算する際の矩形高さ

            '全てのLEDがあるはずの場所へ円を描画し、矩形領域内の色を算出
            '領域内の一定以上の輝度値を持つピクセルのみ計算

            '[左上]
            img.Circle(New Point(centerLabel.X - xBase, centerLabel.Y - yBase), 15, New Scalar(0, 255, 255)) 'leftTop

            Dim colorLeftTop As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X - xBase - width), Convert.ToInt32(centerLabel.Y - yBase - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorLeftTop.ToString())

            If colorLeftTop = LED_COLOR.OFF OrElse colorLeftTop = LED_COLOR.OTHER Then
                'MessageBox.Show("error:lefttop")
                'Cv2.ImShow("test", img)
                'Cv2.WaitKey(0)
                'Return False
                Continue Do
            Else
                color.Add(colorLeftTop)
            End If


            '[中央上]
            img.Circle(New Point(centerLabel.X, centerLabel.Y - yBase), 15, New Scalar(0, 255, 255)) 'centerTop

            Dim colorCenterTop As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X - width), Convert.ToInt32(centerLabel.Y - yBase - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorCenterTop.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorCenterTop = LED_COLOR.OFF OrElse colorCenterTop = LED_COLOR.OTHER Then
                'do nothing
            Else
                'MessageBox.Show("error:centertop")
                'Cv2.ImShow("test", img)
                'Cv2.WaitKey(0)
                'Return False
                Continue Do
            End If




            '[右上]
            img.Circle(New Point(centerLabel.X + xBase, centerLabel.Y - yBase), 15, New Scalar(0, 255, 255)) 'rightTop

            Dim colorRightTop As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X + xBase - width), Convert.ToInt32(centerLabel.Y - yBase - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorRightTop.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorRightTop = LED_COLOR.OFF OrElse colorRightTop = LED_COLOR.OTHER Then
                'do nothing
            Else
                'MessageBox.Show("error:righttop")
                'Cv2.ImShow("test", img)
                'Cv2.WaitKey(0)
                'Return False
                Continue Do
            End If

            '[中央左]
            img.Circle(New Point(centerLabel.X - xBase, centerLabel.Y), 15, New Scalar(0, 255, 255)) 'leftCenter

            Dim colorLeftCenter As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X - xBase - width), Convert.ToInt32(centerLabel.Y - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorLeftCenter.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorLeftCenter = LED_COLOR.OFF OrElse colorLeftCenter = LED_COLOR.OTHER Then
                'MessageBox.Show("error:leftcenter")
                'Cv2.ImShow("test", img)
                'Cv2.WaitKey(0)
                'Return False
                Continue Do
            Else
                color.Add(colorLeftCenter)
            End If



            '[中央]
            img.Circle(New Point(centerLabel.X, centerLabel.Y), 15, New Scalar(0, 255, 255)) 'center

            Dim colorCenter As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X - width), Convert.ToInt32(centerLabel.Y - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorCenter.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorCenter = LED_COLOR.OFF OrElse colorCenter = LED_COLOR.OTHER Then
                'MessageBox.Show("error:center")
                'Cv2.ImShow("test", img)
                'Cv2.WaitKey(0)
                'Return False
                Continue Do
            Else
                color.Add(colorCenter)
            End If


            '[中央右]
            img.Circle(New Point(centerLabel.X + xBase, centerLabel.Y), 15, New Scalar(0, 255, 255)) 'rightCenter

            Dim colorRightCenter As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X + xBase - width), Convert.ToInt32(centerLabel.Y - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorRightCenter.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorRightCenter = LED_COLOR.OFF OrElse colorRightCenter = LED_COLOR.OTHER Then
                'MessageBox.Show("error:rightcenter")
                'Cv2.ImShow("test", img)
                'Cv2.WaitKey(0)
                'Return False
                Continue Do
            Else
                color.Add(colorRightCenter)
            End If


            '左下
            img.Circle(New Point(centerLabel.X - xBase / 2, centerLabel.Y + yBase), 15, New Scalar(0, 255, 255)) 'leftBottom

            Dim colorLeftBottom As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X - xBase / 2 - width), Convert.ToInt32(centerLabel.Y + yBase - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorLeftBottom.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)


            If colorLeftBottom = LED_COLOR.OFF OrElse colorLeftBottom = LED_COLOR.OTHER Then
                'do nothing
            Else
                'MessageBox.Show("error:leftbottom")
                'Cv2.ImShow("test", img)
                'Cv2.WaitKey(0)
                'Return False
                Continue Do
            End If


            '[右下]
            img.Circle(New Point(centerLabel.X + xBase, centerLabel.Y + yBase), 15, New Scalar(0, 255, 255)) 'rightBottom

            Dim colorRightBottom As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X + xBase - width), Convert.ToInt32(centerLabel.Y + yBase - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorRightBottom.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorRightBottom = LED_COLOR.OFF OrElse colorRightBottom = LED_COLOR.OTHER Then
                'do nothing
            Else
                'MessageBox.Show("error:rightbottom")
                'Cv2.ImShow("test", img)
                'Cv2.WaitKey(0)
                'Return False
                Continue Do
            End If

            'Cv2.ImShow("result", img)
            'Cv2.WaitKey(10)

            '消灯状態でなく全てのラベル領域が同色
            '初回
            If colorPattern.Count = 0 Then
                img.CopyTo(resultImg(colorPattern.Count))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                colorPattern.Add(color.First())
                Continue Do
            End If
            '検出した色の切り替わりが起きた時
            If colorPattern.Last() <> color.First() Then
                img.CopyTo(resultImg(colorPattern.Count))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                colorPattern.Add(color.First())
            End If

            My.Application.DoEvents()
        Loop

        '[パターン比較]
        Dim isMatch As Boolean = True

        '循環パターンの作成(パターン数 = pattern.length)
        Dim ciculationPattern As List(Of LED_COLOR)() = createCirculationPattern(pattern)
        '循環パターンのいずれかとの一致を確認
        For i As Integer = 0 To ciculationPattern.Count - 1

            isMatch = True '初期化
            For j As Integer = 0 To colorPattern.Count - 1
                isMatch = isMatch AndAlso colorPattern.Item(j) = ciculationPattern(i).Item(j)
            Next
            If isMatch Then
                Exit For
            End If
        Next


        '結果画像作成＆表示
        Dim combinedImg As Mat = New Mat(New Size(img.Width, img.Height), MatType.CV_8UC3)
        For i As Integer = 0 To resultImg.Count - 1
            Cv2.Resize(resultImg(i), resultImg(i), New Size(0.5 * combinedImg.Width, 0.5 * combinedImg.Height))
            Cv2.PutText(resultImg(i), "No." & i.ToString & ":" & colorPattern.Item(i).ToString(), New Point(resultImg(i).Width / 10, resultImg(i).Height / 10), HersheyFonts.HersheyComplex, 1, New Scalar(255, 0, 255))
        Next
        resultImg(0).CopyTo(combinedImg(New Rect(New Point(0, 0), New Size(resultImg(0).Width, resultImg(0).Height))))
        resultImg(1).CopyTo(combinedImg(New Rect(New Point(320, 0), New Size(resultImg(1).Width, resultImg(1).Height))))
        resultImg(2).CopyTo(combinedImg(New Rect(New Point(0, 240), New Size(resultImg(2).Width, resultImg(2).Height))))
        resultImg(3).CopyTo(combinedImg(New Rect(New Point(320, 240), New Size(resultImg(3).Width, resultImg(3).Height))))

        Dim cpStr As String = String.Empty
        Dim correctPattern As String = String.Empty
        '取得した色のパターン
        For i As Integer = 0 To colorPattern.Count - 1
            cpStr = cpStr & colorPattern.Item(i).ToString() & "->"
        Next
        '検出したいの色パターン
        For i As Integer = 0 To pattern.Count - 1
            correctPattern = correctPattern & pattern.Item(i).ToString() & "->"
        Next

        combinedImg.PutText("Result:" & If(isMatch, "[OK]", "[NG]") & " " & cpStr, New Point(combinedImg.Width * 1 / 10, combinedImg.Height * 9 / 10), HersheyFonts.HersheyComplex, 0.5, New Scalar(255, 0, 255))
        combinedImg.PutText("input pattern:" & correctPattern, New Point(combinedImg.Width * 1 / 10, combinedImg.Height * 9.5 / 10), HersheyFonts.HersheyComplex, 0.5, New Scalar(255, 0, 255))
        Cv2.ImShow("chkLED", combinedImg)
        waitBySW(20)
        combinedImg.SaveImage(String.Format("{0}\LED検査結果画像(slave).jpg", PathSlnFile))
        combinedImg.Release()
        Cv2.DestroyWindow("chkLED")

        '入力パターンとの一致するか
        If isMatch Then
            Return True
        Else
            Return False
        End If

    End Function
    ''' <summary>
    ''' 色の文字列をLED_COLOR型に変換
    ''' </summary>
    ''' <param name="strColor"></param>
    ''' <returns></returns>
    Private Function transStrToColor(ByVal strColor As String) As LED_COLOR
        Select Case strColor
            Case "赤"
                Return LED_COLOR.RED
            Case "緑"
                Return LED_COLOR.GREEN
            Case "オレンジ"
                Return LED_COLOR.ORANGE
            Case "消灯"
                Return LED_COLOR.OFF
            Case Else
                Return LED_COLOR.OTHER
        End Select
    End Function

    ''' <summary>
    ''' 品番リストから取得したLED点灯パターンを変換
    ''' </summary>
    ''' <param name="patternStr"></param>
    ''' <returns></returns>
    Private Function makeColorPattern(ByVal patternStr As String) As List(Of LED_COLOR)

        Dim patternList As List(Of LED_COLOR) = New List(Of LED_COLOR) From {}
        Dim tmpStr As String() = patternStr.Split("-"c)

        For i As Integer = 0 To tmpStr.Length - 1
            patternList.Add(transStrToColor(tmpStr(i)))
        Next

        Return patternList

    End Function

    Private Sub Button10_Click(sender As Object, e As EventArgs)
        '----------------------------------
        'FeRAMチェックサム計算
        '----------------------------------
        'Dim tmpStrArry As String() = New String() {"3", "D0", "BF", "FE", "50", "F"}
        'Dim sum As Integer = 0
        'For Each n In tmpStrArry
        '    sum += Convert.ToByte(n, 16)ii
        'Next
        'Dim numStr As String = (Not sum).ToString("X")
        'Dim chkSum As Byte = Convert.ToByte(numStr.Substring(numStr.Length - 2, 2), 16)
        'MessageBox.Show(chkSum.ToString("X"))



        '----------------------------------
        'PIDの生成
        '----------------------------------
        'Dim rotYear As Integer = 2017
        'Dim rotYearBase As Integer = 201ii5
        'Dim rotMonth As Integer = 3
        'Dim type1 As Integer = 2
        'Dim type2 As Integer = 1
        'Dim serialNum As Integer = 100

        'Dim rotStr As String = Convert.ToString((rotYear - rotYearBase) + (rotMonth - 1) Mod 12, 2).PadLeft(10, "0"c)
        'Dim type1Str As String = Convert.ToString(type1, 2).PadLeft(2, "0"c)
        'Dim type2Str As String = Convert.ToString(type2, 2).PadLeft(6, "0"c)
        'Dim serialNumStr As String = Convert.ToString(serialNum, 2).PadLeft(14, "0"c)

        'Dim pidStr As String = rotStr & type1Str & type2Str & serialNumStr
        'Dim pid As String() = New String(3) {}
        'For i As Integer = 0 To 3
        '    pid(i) = Convert.ToUInt32(pidStr.Substring(8 * i, 8), 2).ToString("X2")
        'Next
        'MessageBox.Show(String.Format("{0}-{1}-{2}-{3}", pid(0), pid(1), pid(2), pid(3)))

    End Sub


    '-------------------------------------------------------------
    '引数indexに番号を受取って、その番号が付いたlblPreInspectionを返す
    Private Function lblInsResult(ByVal index As Integer) As Label
        Return DirectCast(Me.Controls("lblInsResult" & index.ToString), Label)
    End Function

    'PSoCマニュアルモード起動
    Private Sub PSoCToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PSoCToolStripMenuItem.Click
        'passwordbox.setMode(Manual_PSoC)
        passwordbox.setMode(PSoCManu)
        passwordbox.Show()
    End Sub

    'Cameraマニュアルモード起動
    Private Sub CameraToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CameraToolStripMenuItem.Click
        passwordbox.setMode(Manual_Camera)
        passwordbox.Show()
    End Sub
    'WebBrowserマニュアルモード起動
    Private Sub WebBrowserToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WebBrowserToolStripMenuItem.Click
        passwordbox.setMode(Manual_WebBrowser)
        passwordbox.Show()
    End Sub

    ''' <summary>
    ''' エラー情報をログファイルに記録する
    ''' </summary>
    ''' <param name="errorInfo"></param>
    ''' <returns></returns>
    Private Function writeErrorLog(ByVal errorInfo As Exception) As Boolean
        Try
            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}\errorLog.txt", PathSlnFile), True, enc_sj)
                sw.WriteLine(Date.Now & ControlChars.CrLf &
                             "Message:" & errorInfo.Message & ControlChars.CrLf &
                             "StackTrace:" & errorInfo.StackTrace)
            End Using
        Catch ex As Exception
            MessageBox.Show("エラー情報の書込みに失敗しました。" & ControlChars.CrLf &
                            Date.Now & ControlChars.CrLf &
                            "Message:" & errorInfo.Message & ControlChars.CrLf &
                            "StackTrace:" & errorInfo.StackTrace,
                            "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End Try

        Return True
    End Function

    ''' <summary>
    ''' ワークごとのシリアルNoの読出し
    ''' </summary>
    ''' <param name="worktype"></param>
    ''' <returns></returns>
    Private Function loadSerialNo(ByVal worktype As String) As String

        Try
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}{1}{2}.txt", PathSlnFile, pathSerialNoFolder, worktype), enc_sj)
                Return sr.ReadLine()
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            'MessageBox.Show("シリアル番号の読み出しができません。")
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' ワークごとのシリアル番号の読み出し
    ''' </summary>
    ''' <param name="flg">シリアル番号書込み有無</param>
    ''' <param name="filePath">シリアル番号保存ファイルパス</param>
    ''' <returns>
    ''' シリアル番号文字列(書込みあり),string.empty(書込みなし),
    ''' nothing:読込み失敗
    ''' </returns>
    Private Function loadSerialNo(ByVal flg As String, ByVal filePath As String) As String


        If flg <> "1" Then
            Return String.Empty
        End If

        Try
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}\{1}", PathSlnFile, filePath), enc_sj)
                Return sr.ReadLine()
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            'MessageBox.Show("シリアル番号の読み出しができません")
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <param name="filePath"></param>
    ''' <param name="IDNo"></param>
    ''' <returns></returns>
    Private Function loadSerialNo(ByVal flg As String, ByVal filePath As String, ByVal IDNo As String) As String

        If flg <> "1" Then
            Return String.Empty
        End If

        Dim tmpSerialNo As String = String.Empty
        Dim tmpIDNo As String = String.Empty

        Try
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}\{1}", PathSlnFile, filePath), enc_sj)
                tmpSerialNo = sr.ReadLine()
                tmpIDNo = tmpSerialNo.Substring(0, 2)
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            'MessageBox.Show("シリアル番号の読み出しができません")
            Return Nothing
        End Try

        '固有番号の確認
        If tmpIDNo <> IDNo Then
            MessageBox.Show(String.Format("シリアルNoの型式が異なります。" & ControlChars.CrLf &
                                          "ファイルが壊れている可能性があります。" & ControlChars.CrLf &
                                          "読み込んだシリアルNo：{0}" & ControlChars.CrLf &
                                          "登録されているシリアルNoの型式：{1}******", tmpSerialNo, IDNo), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return Nothing
        End If
        'シリアル番号の超過確認
        Dim tmpNo As Long = Convert.ToInt64(tmpSerialNo.Substring(2, 6), 16)
        If tmpNo > Convert.ToInt64("FFFFFF", 16) Then
            MessageBox.Show(String.Format("シリアルNo{0}です。" & ControlChars.CrLf &
                                          "シリアルNoの追加採番が必要です。" & ControlChars.CrLf &
                                          "開発５部に連絡してください。", tmpSerialNo), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return Nothing
        End If

        Return tmpSerialNo
    End Function

    ''' <summary>
    ''' シリアルNoテキストファイルの保存
    ''' </summary>
    ''' <param name="workType"></param>
    ''' <param name="serialNo"></param>
    ''' <returns></returns>
    Private Function saveSerialNo(ByVal workType As String, ByVal serialNo As String) As Boolean
        Try
            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}{2}.txt", PathSlnFile, pathSerialNoFolder, workType), False, enc_sj)
                sw.WriteLine(serialNo)
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            Me.Close()
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' シリアルNoテキストファイルの保存
    ''' </summary>
    ''' <param name="flg">シリアル番号書込み有無</param>
    ''' <param name="filePath">シリアル番号保存ファイルパス</param>
    ''' <param name="serialNo">保存するシリアルNo</param>
    ''' <returns></returns>
    Private Function saveSerialNo(ByVal flg As String, ByVal filePath As String, ByVal serialNo As String) As Boolean

        If flg <> "1" Then
            Return True
        End If

        Try
            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}\{1}", PathSlnFile, filePath), False, enc_sj)
                sw.WriteLine(serialNo)
            End Using
        Catch ex As Exception
            writeErrorLog(ex)
            Me.Close()
            Return False
        End Try

        Return True
    End Function



    ''' <summary>
    ''' シリアルNoのインクリメント
    ''' </summary>
    ''' <param name="workType"></param>
    ''' <returns></returns>
    Private Function incrementSerialNo(ByVal workType As String, ByRef serialNo As String) As Boolean
        Dim tmpNo As String = loadSerialNo(workType)

        If tmpNo Is Nothing Then
            Return False
        End If

        serialNo = (Convert.ToInt64(tmpNo, 16) + 1).ToString("X8")
        Return True
    End Function

    ''' <summary>
    ''' シリアルNoのインクリメント
    ''' 保存ファイルパス指定
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <param name="filePath"></param>
    ''' <param name="serialNo"></param>
    ''' <returns></returns>
    Private Function incrementSerialNo(ByVal flg As String, ByVal filePath As String, ByRef serialNo As String) As Boolean

        'シリアルNo書込みを行わない場合はインクリメントしない
        If flg <> "1" Then
            Return True
        End If

        Dim tmpNo As String = loadSerialNo(flg, filePath)

        If tmpNo Is Nothing Then
            Return False
        End If

        serialNo = (Convert.ToInt64(tmpNo, 16) + 1).ToString("X8")

        Return True
    End Function

    Private Function incrementSerialNo(ByVal flg As String, ByVal filePath As String, ByRef serialNo As String, ByVal idNo As String) As Boolean

        'シリアルNo書込みを行わない場合はインクリメントしない
        If flg <> "1" Then
            Return True
        End If

        Dim tmpNo As String = loadSerialNo(flg, filePath)

        If tmpNo Is Nothing Then
            Return False
        End If

        Dim tmpIDNo As String = tmpNo.Substring(0, 2)
        '固有番号の確認
        If tmpIDNo <> idNo Then
            MessageBox.Show(String.Format("シリアルNoの型式が異なります。" & ControlChars.CrLf &
                                          "ファイルが壊れている可能性があります。" & ControlChars.CrLf &
                                          "読み込んだシリアルNo：{0}" & ControlChars.CrLf &
                                          "登録されているシリアルNoの型式：{1}******", tmpNo, idNo), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End If
        'シリアル番号の超過確認
        If Convert.ToInt64(Convert.ToInt64(tmpNo.Substring(2, 6), 16)) > Convert.ToInt64("FFFFFF", 16) Then
            MessageBox.Show(String.Format("シリアルNo{0}です。" & ControlChars.CrLf &
                                          "シリアルNoの追加採番が必要です。" & ControlChars.CrLf &
                                          "開発５部に連絡してください。", tmpNo), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End If
        serialNo = (Convert.ToInt64(tmpNo, 16) + 1).ToString("X8")

        Return True
    End Function

    ''' <summary>
    ''' シリアルNoの残り状態をチェック
    ''' </summary>
    ''' <returns></returns>
    Private Function chkRemainningSerialNo(ByVal workType As String) As Boolean

        Dim tmpSerialNo As String = loadSerialNo(workType)

        If tmpSerialNo Like "**FFFF**" Then
            MessageBox.Show("シリアル番号が残りわずかです")
            Return False
        End If

        Return True
    End Function
    ''' <summary>
    ''' シリアル番号の残りをチェック残りがなければプログラムを終了
    ''' </summary>
    ''' <param name="flg"></param>
    ''' <param name="filePath"></param>
    ''' <returns></returns>
    Private Function chkRemainningSerialNo(ByVal flg As String, ByVal filePath As String) As Boolean

        Dim tmpSerialNo As String = loadSerialNo(flg, filePath)

        If tmpSerialNo Like "**FFFFFF" Then
            MessageBox.Show(String.Format("シリアル番号が{0}です。" & ControlChars.CrLf &
                                          "開発5部に連絡してください。", tmpSerialNo), "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
            Return False
        End If

        If tmpSerialNo Like "**FFFF**" Then
            'MessageBox.Show("シリアル番号が残りわずかです")
            'lblWarningComment.Text = "シリアル番号が残りわずかです。"
            Return True
        End If

        Return True
    End Function

    Private Function chkInspectionState(ByRef errorStr As String) As Boolean

        Dim bytData As Byte
        errorStr = String.Empty '引数に値を必ず持たせるためここで初期化

        '過電流チェック
        If Not USBIO.EX600_OverCurrent(bytData) Then
            errorStr = "PSoC通信エラー"
            Return False
        End If

        If bytData = 0 Then
            wait(1000)
            '突入電流の場合があるので
            '継続して過電流検知がONなら突入電流でなく過電流と判定する
            If Not USBIO.EX600_OverCurrent(bytData) Then
                errorStr = "PSoC通信エラー"
                Return False
            End If

            If bytData <> 0 Then
                errorStr = "過電流検知"
                Return False
            End If
        End If

        'ワークセンサチェック
        If Not USBIO.Work_Sens(bytData) Then
            errorStr = "PSoC通信エラー"
            Return False
        End If

        If bytData <> 0 Then
            errorStr = "ワークセンサオフ"
            Return False
        End If

        'アクリルカバー開閉確認
        If Not USBIO.Reserve0_In(bytData) Then
            errorStr = "PSoC通信エラー"
            Return False
        End If
        'LED目視確認モードの際にはエラーとしない
        If bytData <> 0 AndAlso Not isManuLEDChkMode Then
            errorStr = "アクリルカバー開"
            Return False
        End If

        '管理PCの存在確認
        If Not IO.Directory.Exists(pathControlPC) Then
            errorStr = "管理PC接続エラー"
            Return False
        End If
        'NASの存在確認
        If Not IO.Directory.Exists(pathNAS) Then
            errorStr = "NAS接続エラー"
            Return False
        End If

        Return True

    End Function


    ''' <summary>
    ''' LED目視検査モードボタンのON/OFF切替
    ''' ON:アクリルカバーを使用せずLED検査を目視で行う
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnManuLEDChk_Click(sender As Object, e As EventArgs) Handles btnManuLEDChk.Click
        isManuLEDChkMode = Not isManuLEDChkMode
        If isManuLEDChkMode Then
            btnManuLEDChk.BackColor = Color.Lime
        Else
            btnManuLEDChk.BackColor = Color.Gray
        End If

    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs)

        Sna.NoWire.trans_mode = False

        If Not isConnectNFC() Then
            MessageBox.Show("connection error")
        End If

        'FeRAM初期値書込
        If Not insFeRAMInitWrite("1", "EX600-WSV2", False) Then

            MessageBox.Show("error")
        Else
            MessageBox.Show("finished")
        End If

        Sna.NoWire.Nfc_disconnect()

    End Sub


End Class
