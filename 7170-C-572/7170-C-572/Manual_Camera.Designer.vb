﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Manual_Camera
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Manual_Camera))
        Me.btnCalcHSV = New System.Windows.Forms.Button()
        Me.btnCapture = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.cbEXX600Pow = New System.Windows.Forms.CheckBox()
        Me.cbHubPow = New System.Windows.Forms.CheckBox()
        Me.cbPLCPow = New System.Windows.Forms.CheckBox()
        Me.cbEX260Pow = New System.Windows.Forms.CheckBox()
        Me.btnInsMode = New System.Windows.Forms.Button()
        Me.btnNormalMode = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnProtect = New System.Windows.Forms.Button()
        Me.btnResetProtect = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbMaskTHV = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbRectW = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbRectH = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnChkLED_Master = New System.Windows.Forms.Button()
        Me.btnChkLED_Slave = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tbMaskTH_V_chkLED = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbMaskTH_S_chkLED = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.gbGreenLED = New System.Windows.Forms.GroupBox()
        Me.tbGreenHueMin = New System.Windows.Forms.TextBox()
        Me.tbGreenHueMax = New System.Windows.Forms.TextBox()
        Me.gbRedLED = New System.Windows.Forms.GroupBox()
        Me.tbRedHueMin1 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbRedHueMax1 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.tbLedAreaTH_chkLED = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.tbHsvItrNum = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.tbMaskTHS = New System.Windows.Forms.TextBox()
        Me.btnResetPID_MAC = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.btnSaveImg = New System.Windows.Forms.Button()
        Me.btnImgSelect = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.tbV_TH = New System.Windows.Forms.TrackBar()
        Me.tbS_TH = New System.Windows.Forms.TrackBar()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblSVal = New System.Windows.Forms.Label()
        Me.lblVVal = New System.Windows.Forms.Label()
        Me.lblHVal = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.tbH_TH = New System.Windows.Forms.TrackBar()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.btnSerialRead = New System.Windows.Forms.Button()
        Me.btnMACRead = New System.Windows.Forms.Button()
        Me.gbGreenLED.SuspendLayout()
        Me.gbRedLED.SuspendLayout()
        CType(Me.tbV_TH, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbS_TH, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbH_TH, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCalcHSV
        '
        resources.ApplyResources(Me.btnCalcHSV, "btnCalcHSV")
        Me.btnCalcHSV.Name = "btnCalcHSV"
        Me.btnCalcHSV.UseVisualStyleBackColor = True
        '
        'btnCapture
        '
        resources.ApplyResources(Me.btnCapture, "btnCapture")
        Me.btnCapture.Name = "btnCapture"
        Me.btnCapture.UseVisualStyleBackColor = True
        '
        'Button1
        '
        resources.ApplyResources(Me.Button1, "Button1")
        Me.Button1.Name = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        resources.ApplyResources(Me.Button2, "Button2")
        Me.Button2.Name = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'cbEXX600Pow
        '
        resources.ApplyResources(Me.cbEXX600Pow, "cbEXX600Pow")
        Me.cbEXX600Pow.Name = "cbEXX600Pow"
        Me.cbEXX600Pow.UseVisualStyleBackColor = True
        '
        'cbHubPow
        '
        resources.ApplyResources(Me.cbHubPow, "cbHubPow")
        Me.cbHubPow.Name = "cbHubPow"
        Me.cbHubPow.UseVisualStyleBackColor = True
        '
        'cbPLCPow
        '
        resources.ApplyResources(Me.cbPLCPow, "cbPLCPow")
        Me.cbPLCPow.Name = "cbPLCPow"
        Me.cbPLCPow.UseVisualStyleBackColor = True
        '
        'cbEX260Pow
        '
        resources.ApplyResources(Me.cbEX260Pow, "cbEX260Pow")
        Me.cbEX260Pow.Name = "cbEX260Pow"
        Me.cbEX260Pow.UseVisualStyleBackColor = True
        '
        'btnInsMode
        '
        resources.ApplyResources(Me.btnInsMode, "btnInsMode")
        Me.btnInsMode.Name = "btnInsMode"
        Me.btnInsMode.UseVisualStyleBackColor = True
        '
        'btnNormalMode
        '
        resources.ApplyResources(Me.btnNormalMode, "btnNormalMode")
        Me.btnNormalMode.Name = "btnNormalMode"
        Me.btnNormalMode.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        resources.ApplyResources(Me.TextBox1, "TextBox1")
        Me.TextBox1.Name = "TextBox1"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'btnProtect
        '
        resources.ApplyResources(Me.btnProtect, "btnProtect")
        Me.btnProtect.Name = "btnProtect"
        Me.btnProtect.UseVisualStyleBackColor = True
        '
        'btnResetProtect
        '
        resources.ApplyResources(Me.btnResetProtect, "btnResetProtect")
        Me.btnResetProtect.Name = "btnResetProtect"
        Me.btnResetProtect.UseVisualStyleBackColor = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'tbMaskTHV
        '
        resources.ApplyResources(Me.tbMaskTHV, "tbMaskTHV")
        Me.tbMaskTHV.Name = "tbMaskTHV"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'tbRectW
        '
        resources.ApplyResources(Me.tbRectW, "tbRectW")
        Me.tbRectW.Name = "tbRectW"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'tbRectH
        '
        resources.ApplyResources(Me.tbRectH, "tbRectH")
        Me.tbRectH.Name = "tbRectH"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'btnChkLED_Master
        '
        resources.ApplyResources(Me.btnChkLED_Master, "btnChkLED_Master")
        Me.btnChkLED_Master.Name = "btnChkLED_Master"
        Me.btnChkLED_Master.UseVisualStyleBackColor = True
        '
        'btnChkLED_Slave
        '
        resources.ApplyResources(Me.btnChkLED_Slave, "btnChkLED_Slave")
        Me.btnChkLED_Slave.Name = "btnChkLED_Slave"
        Me.btnChkLED_Slave.UseVisualStyleBackColor = True
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'tbMaskTH_V_chkLED
        '
        resources.ApplyResources(Me.tbMaskTH_V_chkLED, "tbMaskTH_V_chkLED")
        Me.tbMaskTH_V_chkLED.Name = "tbMaskTH_V_chkLED"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'tbMaskTH_S_chkLED
        '
        resources.ApplyResources(Me.tbMaskTH_S_chkLED, "tbMaskTH_S_chkLED")
        Me.tbMaskTH_S_chkLED.Name = "tbMaskTH_S_chkLED"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'gbGreenLED
        '
        Me.gbGreenLED.Controls.Add(Me.tbGreenHueMin)
        Me.gbGreenLED.Controls.Add(Me.Label9)
        Me.gbGreenLED.Controls.Add(Me.tbGreenHueMax)
        Me.gbGreenLED.Controls.Add(Me.Label10)
        resources.ApplyResources(Me.gbGreenLED, "gbGreenLED")
        Me.gbGreenLED.Name = "gbGreenLED"
        Me.gbGreenLED.TabStop = False
        '
        'tbGreenHueMin
        '
        resources.ApplyResources(Me.tbGreenHueMin, "tbGreenHueMin")
        Me.tbGreenHueMin.Name = "tbGreenHueMin"
        '
        'tbGreenHueMax
        '
        resources.ApplyResources(Me.tbGreenHueMax, "tbGreenHueMax")
        Me.tbGreenHueMax.Name = "tbGreenHueMax"
        '
        'gbRedLED
        '
        Me.gbRedLED.Controls.Add(Me.tbRedHueMin1)
        Me.gbRedLED.Controls.Add(Me.Label11)
        Me.gbRedLED.Controls.Add(Me.tbRedHueMax1)
        Me.gbRedLED.Controls.Add(Me.Label12)
        resources.ApplyResources(Me.gbRedLED, "gbRedLED")
        Me.gbRedLED.Name = "gbRedLED"
        Me.gbRedLED.TabStop = False
        '
        'tbRedHueMin1
        '
        resources.ApplyResources(Me.tbRedHueMin1, "tbRedHueMin1")
        Me.tbRedHueMin1.Name = "tbRedHueMin1"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'tbRedHueMax1
        '
        resources.ApplyResources(Me.tbRedHueMax1, "tbRedHueMax1")
        Me.tbRedHueMax1.Name = "tbRedHueMax1"
        '
        'Label12
        '
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.Name = "Label12"
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.Name = "Label15"
        '
        'tbLedAreaTH_chkLED
        '
        resources.ApplyResources(Me.tbLedAreaTH_chkLED, "tbLedAreaTH_chkLED")
        Me.tbLedAreaTH_chkLED.Name = "tbLedAreaTH_chkLED"
        '
        'Label18
        '
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.Name = "Label18"
        '
        'tbHsvItrNum
        '
        resources.ApplyResources(Me.tbHsvItrNum, "tbHsvItrNum")
        Me.tbHsvItrNum.Name = "tbHsvItrNum"
        '
        'Button3
        '
        resources.ApplyResources(Me.Button3, "Button3")
        Me.Button3.Name = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label19
        '
        resources.ApplyResources(Me.Label19, "Label19")
        Me.Label19.Name = "Label19"
        '
        'tbMaskTHS
        '
        resources.ApplyResources(Me.tbMaskTHS, "tbMaskTHS")
        Me.tbMaskTHS.Name = "tbMaskTHS"
        '
        'btnResetPID_MAC
        '
        resources.ApplyResources(Me.btnResetPID_MAC, "btnResetPID_MAC")
        Me.btnResetPID_MAC.Name = "btnResetPID_MAC"
        Me.btnResetPID_MAC.UseVisualStyleBackColor = True
        '
        'Button4
        '
        resources.ApplyResources(Me.Button4, "Button4")
        Me.Button4.Name = "Button4"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'btnSaveImg
        '
        resources.ApplyResources(Me.btnSaveImg, "btnSaveImg")
        Me.btnSaveImg.Name = "btnSaveImg"
        Me.btnSaveImg.UseVisualStyleBackColor = True
        '
        'btnImgSelect
        '
        resources.ApplyResources(Me.btnImgSelect, "btnImgSelect")
        Me.btnImgSelect.Name = "btnImgSelect"
        Me.btnImgSelect.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'tbV_TH
        '
        resources.ApplyResources(Me.tbV_TH, "tbV_TH")
        Me.tbV_TH.Maximum = 255
        Me.tbV_TH.Name = "tbV_TH"
        '
        'tbS_TH
        '
        resources.ApplyResources(Me.tbS_TH, "tbS_TH")
        Me.tbS_TH.Maximum = 255
        Me.tbS_TH.Name = "tbS_TH"
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.Name = "Label13"
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'lblSVal
        '
        resources.ApplyResources(Me.lblSVal, "lblSVal")
        Me.lblSVal.Name = "lblSVal"
        '
        'lblVVal
        '
        resources.ApplyResources(Me.lblVVal, "lblVVal")
        Me.lblVVal.Name = "lblVVal"
        '
        'lblHVal
        '
        resources.ApplyResources(Me.lblHVal, "lblHVal")
        Me.lblHVal.Name = "lblHVal"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.Name = "Label17"
        '
        'tbH_TH
        '
        resources.ApplyResources(Me.tbH_TH, "tbH_TH")
        Me.tbH_TH.Maximum = 360
        Me.tbH_TH.Name = "tbH_TH"
        '
        'Button5
        '
        resources.ApplyResources(Me.Button5, "Button5")
        Me.Button5.Name = "Button5"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'btnSerialRead
        '
        resources.ApplyResources(Me.btnSerialRead, "btnSerialRead")
        Me.btnSerialRead.Name = "btnSerialRead"
        Me.btnSerialRead.UseVisualStyleBackColor = True
        '
        'btnMACRead
        '
        resources.ApplyResources(Me.btnMACRead, "btnMACRead")
        Me.btnMACRead.Name = "btnMACRead"
        Me.btnMACRead.UseVisualStyleBackColor = True
        '
        'Manual_Camera
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnMACRead)
        Me.Controls.Add(Me.btnSerialRead)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.lblHVal)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.tbH_TH)
        Me.Controls.Add(Me.lblVVal)
        Me.Controls.Add(Me.lblSVal)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.tbS_TH)
        Me.Controls.Add(Me.tbV_TH)
        Me.Controls.Add(Me.btnImgSelect)
        Me.Controls.Add(Me.btnSaveImg)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.btnResetPID_MAC)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.tbMaskTHS)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.tbHsvItrNum)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.tbLedAreaTH_chkLED)
        Me.Controls.Add(Me.gbRedLED)
        Me.Controls.Add(Me.gbGreenLED)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.tbMaskTH_S_chkLED)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tbMaskTH_V_chkLED)
        Me.Controls.Add(Me.btnChkLED_Slave)
        Me.Controls.Add(Me.btnChkLED_Master)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tbRectH)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tbRectW)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbMaskTHV)
        Me.Controls.Add(Me.btnResetProtect)
        Me.Controls.Add(Me.btnProtect)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.btnNormalMode)
        Me.Controls.Add(Me.btnInsMode)
        Me.Controls.Add(Me.cbEX260Pow)
        Me.Controls.Add(Me.cbPLCPow)
        Me.Controls.Add(Me.cbHubPow)
        Me.Controls.Add(Me.cbEXX600Pow)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnCalcHSV)
        Me.Controls.Add(Me.btnCapture)
        Me.Name = "Manual_Camera"
        Me.gbGreenLED.ResumeLayout(False)
        Me.gbGreenLED.PerformLayout()
        Me.gbRedLED.ResumeLayout(False)
        Me.gbRedLED.PerformLayout()
        CType(Me.tbV_TH, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbS_TH, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbH_TH, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnCalcHSV As Button
    Friend WithEvents btnCapture As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents cbEXX600Pow As CheckBox
    Friend WithEvents cbHubPow As CheckBox
    Friend WithEvents cbPLCPow As CheckBox
    Friend WithEvents cbEX260Pow As CheckBox
    Friend WithEvents btnInsMode As Button
    Friend WithEvents btnNormalMode As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnProtect As Button
    Friend WithEvents btnResetProtect As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents tbMaskTHV As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents tbRectW As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents tbRectH As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents btnChkLED_Master As Button
    Friend WithEvents btnChkLED_Slave As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents tbMaskTH_V_chkLED As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents tbMaskTH_S_chkLED As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents gbGreenLED As GroupBox
    Friend WithEvents tbGreenHueMin As TextBox
    Friend WithEvents tbGreenHueMax As TextBox
    Friend WithEvents gbRedLED As GroupBox
    Friend WithEvents tbRedHueMin1 As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents tbRedHueMax1 As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents tbLedAreaTH_chkLED As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents tbHsvItrNum As TextBox
    Friend WithEvents Button3 As Button
    Friend WithEvents Label19 As Label
    Friend WithEvents tbMaskTHS As TextBox
    Friend WithEvents btnResetPID_MAC As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents btnSaveImg As Button
    Friend WithEvents btnImgSelect As Button
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents tbV_TH As TrackBar
    Friend WithEvents tbS_TH As TrackBar
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents lblSVal As Label
    Friend WithEvents lblVVal As Label
    Friend WithEvents lblHVal As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents tbH_TH As TrackBar
    Friend WithEvents Button5 As Button
    Friend WithEvents btnSerialRead As Button
    Friend WithEvents btnMACRead As Button
End Class
