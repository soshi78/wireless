﻿Imports OpenCvSharp
Imports Sna.NoWire
Public Class Manual_Camera

    'ソリューションファイルパス
    Private PathSlnFile As String = System.IO.Path.GetDirectoryName(
                                    System.IO.Path.GetDirectoryName(
                                    System.IO.Path.GetDirectoryName(Application.StartupPath)))

    'カメラマニュアルモード用保存パス
    Private Const pathCameraManu As String = "\camera_manu\"
    Private Const pathHSVVal As String = "\camera_manu\HSV_Val\"
    'Encoding
    Private enc_sj As System.Text.Encoding = System.Text.Encoding.GetEncoding("shift_jis")

    ''' <summary>
    ''' LED色
    ''' </summary>
    Enum LED_COLOR
        RED
        ORANGE
        GREEN
        OFF
        OTHER
    End Enum

    'LED色相閾値
    Dim RED_MAX As Integer = 10
    Dim RED_MIN As Integer = 0
    Dim GREEN_MAX As Integer = 140
    Dim GREEN_MIN As Integer = 80


    'マウスコールバック用のグローバル変数
    Dim mousePoints As Point() = New Point(1) {}
    Dim isDraw As Boolean = False


    'マニュアルモード起動時には他のフォームを使用できないようにする
    Private Sub Manual_Camera_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        frmMain.Enabled = False
        frmWebBrowser.Enabled = False
    End Sub
    'マニュアルモード終了時に他のフォームの使用制限を解除
    Private Sub Manual_Camera_Closed(sender As Object, e As EventArgs) Handles MyBase.Closed
        frmMain.Enabled = True
        frmWebBrowser.Enabled = True

        cbEXX600Pow.Checked = False
        cbHubPow.Checked = False
        cbPLCPow.Checked = False
        cbEX260Pow.Checked = False

        USBIO.EX600_Pow(0)
        USBIO.EX260_Pow(0)
        USBIO.Hub_Pow(0)
        USBIO.AC_Relay(0)

    End Sub

    Private Sub btnCapture_Click(sender As Object, e As EventArgs) Handles btnCapture.Click

        Dim camNo As Integer
        If Not Integer.TryParse(TextBox1.Text, camNo) Then
            MessageBox.Show("数字を入力してください")
            Exit Sub
        End If

        Using cap As VideoCapture = New VideoCapture(CaptureDevice.Any, camNo)

            If Not cap.IsOpened() Then
                MessageBox.Show("カメラが開けません")
                Exit Sub
            End If

            Dim img As Mat = New Mat()

            Do
                Dim isReadSuccess As Boolean = False

                isReadSuccess = cap.Read(img)
                If img.Empty() Then
                    Continue Do
                End If

                Cv2.ImShow("img", img)
                '検証用２値画像
                '明度、再度
                Dim maskThV As Integer = Integer.Parse(tbMaskTHV.Text)
                Dim maskThS As Integer = Integer.Parse(tbMaskTHS.Text)
                Dim imgHSV As Mat = New Mat()
                Cv2.CvtColor(img, imgHSV, ColorConversionCodes.BGR2HSV)
                'HとVの値をそれぞれ仕様するため分割
                Dim imgSepHSV() As Mat = New Mat(imgHSV.Channels - 1) {}
                Cv2.Split(imgHSV, imgSepHSV)
                '[輝度値(V)で2値化]
                Dim maskV As Mat = New Mat()
                Dim maskS As Mat = New Mat()
                Dim mask As Mat = New Mat()


                Cv2.Threshold(imgSepHSV(2), maskV, maskThV, 255, ThresholdTypes.Binary)
                Cv2.Threshold(imgSepHSV(1), maskS, maskThS, 255, ThresholdTypes.Binary)
                mask = maskV.Mul(maskS) 'maskV and maskS
                Cv2.ImShow("v", maskV)
                Cv2.ImShow("s", maskS)
                Cv2.ImShow("mask", mask)
                Cv2.WaitKey(10)

                If Cv2.WaitKey(15) = 27 Then
                    Exit Do
                End If

                If Cv2.WaitKey(15) = Asc("s") Then
                    If Not IO.Directory.Exists("img_test") Then
                        IO.Directory.CreateDirectory("img_test")
                    End If

                    Dim currentTime As String = Now.ToString("yyMMdd_hhmmss")

                    img.SaveImage(String.Format("{0}{1}img_test\" & currentTime & "_img.jpg", PathSlnFile, pathCameraManu))
                    maskS.SaveImage(String.Format("{0}{1}img_test\" & currentTime & "_s.jpg", PathSlnFile, pathCameraManu))
                    maskV.SaveImage(String.Format("{0}{1}img_test\" & currentTime & "_v.jpg", PathSlnFile, pathCameraManu))
                    mask.SaveImage(String.Format("{0}{1}img_test\" & currentTime & "_mask.jpg", PathSlnFile, pathCameraManu))
                End If

                My.Application.DoEvents()
            Loop

            Cv2.DestroyAllWindows()

        End Using
    End Sub



    ''' <summary>
    ''' Cv2.waitkey(msec)の替わり(PictureBoxに画像を表示する場合に使用する)
    ''' </summary>
    ''' <param name="msec"></param>
    Private Sub waitBySW(ByVal msec As Integer)
        Dim sw As Stopwatch = New Stopwatch()

        sw.Start()

        Do Until sw.Elapsed > New TimeSpan(0, 0, 0, 0, msec)
            My.Application.DoEvents()
        Loop

        sw.Stop()
        sw.Reset()

        Me.Refresh()

    End Sub
    ''' <summary>
    ''' 矩形領域のHSV値を計算
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnCalcHSV_Click(sender As Object, e As EventArgs) Handles btnCalcHSV.Click

        Dim maskThV As Integer = 50 '明度閾値(V)
        Dim maskThS As Integer = 150 '彩度閾値(S)


        Dim camNo As Integer
        If Not Integer.TryParse(TextBox1.Text, camNo) Then
            MessageBox.Show("数字を入力してください")
            Exit Sub
        End If

        Using cap As VideoCapture = New VideoCapture(CaptureDevice.Any, camNo)
            'パラメータ設定
            If IsNumeric(tbMaskTHV.Text) Then
                maskTh = Integer.Parse(tbMaskTHV.Text)
            Else
                MessageBox.Show("invalid maskTHV, use default value:" & maskTh.ToString())
            End If

            If IsNumeric(tbMaskTHS.Text) Then
                maskThS = Integer.Parse(tbMaskTHV.Text)
            Else
                MessageBox.Show("invalid maskTHS, use default value:" & maskThS.ToString())
            End If

            '座標指定待ち
            Dim mc As CvMouseCallback = New CvMouseCallback(AddressOf mouseCB)

            Dim window As Window = New Window("img")
            Cv2.SetMouseCallback("img", mc)


            Dim img As New Mat()
            cap.Read(img)
            Cv2.ImShow("img", img)


            Do
                'ESCキーで終了
                If Cv2.WaitKey(10) = 27 Then
                    Exit Do
                End If

                '画像の再取得
                If Cv2.WaitKey(10) = Asc("r"c) Then
                    cap.Read(img)
                    Cv2.ImShow("img", img)
                End If

                '画像の表示と座標指定待ち
                If isDraw Then


                    Dim rect As New Rect(mousePoints(0).X, mousePoints(0).Y, mousePoints(1).X - mousePoints(0).X, mousePoints(1).Y - mousePoints(0).Y)


                    'HSV値の計算
                    Dim imgHSV As Mat = New Mat()
                    Cv2.CvtColor(img, imgHSV, ColorConversionCodes.BGR2HSV)
                    'HとVの値をそれぞれ仕様するため分割
                    Dim imgSepHSV() As Mat = New Mat(imgHSV.Channels - 1) {}
                    Cv2.Split(imgHSV, imgSepHSV)
                    '[輝度値(V)で2値化]
                    Dim mask As Mat = New Mat()
                    Cv2.Threshold(imgSepHSV(2), mask, maskThV, 255, ThresholdTypes.Binary)
                    'Cv2.ImShow("mask", mask)
                    Cv2.WaitKey(100)

                    Dim aveH As Double = 0
                    Dim aveS As Double = 0
                    Dim aveV As Double = 0
                    Dim color As Vec3b = New Vec3b()
                    Dim counter As Integer = 0


                    Dim hHist As Integer() = New Integer(359) {}
                    Dim hHistF As Integer() = New Integer(359) {} 'S,Vでフィルタリングした際のヒストグラム
                    Dim sHist As Integer() = New Integer(255) {}
                    Dim vHist As Integer() = New Integer(255) {}


                    '矩形領域のHSV値の平均値を算出
                    For j As Integer = rect.Top To rect.Bottom - 1

                        For i As Integer = rect.Left To rect.Right

                            Dim histColor As Vec3b = imgHSV.At(Of Vec3b)(j, i)
                            hHist(2 * histColor(0)) += Convert.ToByte(1)
                            sHist(histColor(1)) += Convert.ToByte(1)
                            vHist(histColor(2)) += Convert.ToByte(1)

                            If imgSepHSV(2).At(Of Byte)(j, i) >= maskThV AndAlso imgSepHSV(1).At(Of Byte)(j, i) > maskThS Then '彩度について追加
                                color = imgHSV.At(Of Vec3b)(j, i)

                                If 2 * color(0) < 300 Then
                                    aveH += color(0) * 2
                                    hHistF(Convert.ToInt32(color(0) * 2)) += 1
                                Else
                                    aveV += 360 - color(0) * 2
                                    hHistF(Convert.ToInt32(360 - color(0) * 2)) += 1
                                End If
                                aveS += color(1)
                                aveV += color(2)
                                counter += 1
                            End If
                        Next
                    Next
                    If counter <> 0 Then
                        aveH /= counter
                        aveS /= counter
                        aveV /= counter
                    End If

                    Dim imgHistH As Mat = Mat.Zeros(New Size(hHist.Length, hHist.Max), MatType.CV_8UC1)


                    For i As Integer = 0 To imgHistH.Width - 1
                        For j As Integer = imgHistH.Height - hHist(i) To imgHistH.Height - 1
                            imgHistH.Set(Of Byte)(j, i, 255)
                        Next
                    Next

                    Dim imgHistS As Mat = Mat.Zeros(New Size(sHist.Length, sHist.Max), MatType.CV_8UC1)
                    For i As Integer = 0 To imgHistS.Width - 1
                        For j As Integer = imgHistS.Height - sHist(i) To imgHistS.Height - 1
                            imgHistS.Set(Of Byte)(j, i, 255)
                        Next
                    Next

                    Dim imgHistV As Mat = Mat.Zeros(New Size(vHist.Length, vHist.Max), MatType.CV_8UC1)
                    For i As Integer = 0 To imgHistV.Width - 1
                        For j As Integer = imgHistV.Height - vHist(i) To imgHistV.Height - 1
                            imgHistV.Set(Of Byte)(j, i, 255)
                        Next
                    Next

                    Dim imgHistHF As Mat = Mat.Zeros(New Size(hHist.Length, hHist.Max), MatType.CV_8UC1)


                    For i As Integer = 0 To imgHistHF.Width - 1
                        For j As Integer = imgHistHF.Height - hHistF(i) To imgHistHF.Height - 1
                            imgHistHF.Set(Of Byte)(j, i, 255)
                        Next
                    Next

                    Cv2.ImShow("imgHistH", imgHistH)
                    Cv2.ImShow("imgHistS", imgHistS)
                    Cv2.ImShow("imgHistV", imgHistV)
                    Cv2.ImShow("imgHistHF", imgHistHF)
                    Cv2.WaitKey(30)

                    '計算結果表示
                    MessageBox.Show(String.Format("H:{0}_S:{1}_V:{2}_Area:{3}", aveH.ToString("0.0"), aveS.ToString("0.0"), aveV.ToString("0.0"), counter))

                    isDraw = False
                    Continue Do
                End If

                My.Application.DoEvents()
            Loop
        End Using

        Cv2.DestroyAllWindows()
    End Sub


    ''' <summary>
    ''' マウスコールバック、矩形領域の座標を取得
    ''' </summary>
    ''' <param name="e"></param>
    ''' <param name="x"></param>
    ''' <param name="y"></param>
    ''' <param name="flag"></param>
    ''' <returns></returns>
    Private Function mouseCB(ByVal e As MouseEvent, ByVal x As Integer, ByVal y As Integer, ByVal flag As OpenCvSharp.MouseEvent) As CvMouseCallback

        Select Case e
            Case MouseEvent.LButtonDown
                mousePoints(0).X = x
                mousePoints(0).Y = y
                isDraw = False

            Case MouseEvent.LButtonUp
                mousePoints(1).X = x
                mousePoints(1).Y = y

                If mousePoints(0).X > mousePoints(1).X Then
                    Dim tmp As Integer = mousePoints(0).X
                    mousePoints(0).X = mousePoints(1).X
                    mousePoints(1).X = tmp
                End If

                If mousePoints(0).Y > mousePoints(1).Y Then
                    Dim tmp As Integer = mousePoints(0).Y
                    mousePoints(0).Y = mousePoints(1).Y
                    mousePoints(1).Y = tmp
                End If
                isDraw = True

            Case Else
                'do nothing
        End Select
    End Function

    ''' <summary>
    ''' 矩形部分の座標をテキストファイルに保存
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim width As Integer = 30
        Dim height As Integer = 30
        Dim pointList As List(Of Rect) = New List(Of Rect)

        Dim img As Mat = New Mat()
        Dim win As Window = New Window("win")
        Cv2.SetMouseCallback("win", AddressOf mouseCB)

        Dim camNo As Integer
        If Not Integer.TryParse(TextBox1.Text, camNo) Then
            MessageBox.Show("数字を入力してください")
            Exit Sub
        End If

        Using cap As VideoCapture = New VideoCapture(camNo)

            If Not cap.IsOpened() Then
                MessageBox.Show("カメラが開けません")
                Exit Sub
            End If

            cap.Read(img)
            win.ShowImage(img)

            Do

                Dim key As Integer = Cv2.WaitKey(30)
                Select Case key
                    'キャプチャ終了
                    Case 27
                        Exit Do
                    '画像の再取得
                    Case Asc("r"c)
                        cap.Read(img)
                        win.ShowImage(img)
                        pointList.Clear()
                End Select



                If isDraw Then
                    Dim tmpRect As Rect = New Rect(mousePoints(0).X, mousePoints(0).Y, width, height)
                    pointList.Add(tmpRect)
                    For i As Integer = 0 To pointList.Count - 1
                        img.Rectangle(pointList.Item(i), New Scalar(255, 0, 255))
                    Next
                    win.ShowImage(img)
                    Cv2.WaitKey(10)
                    isDraw = False
                End If
                My.Application.DoEvents()

            Loop
        End Using
        Try
            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}pointList.txt", PathSlnFile, pathCameraManu), False, enc_sj)
                sw.WriteLine("x,y,width,height")
                For i As Integer = 0 To pointList.Count - 1
                    sw.WriteLine(String.Format("{0},{1},{2},{3}", pointList.Item(i).X, pointList.Item(i).Y, pointList.Item(i).Width, pointList.Item(i).Height))
                Next
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Cv2.DestroyAllWindows()

    End Sub

    ''' <summary>
    ''' 矩形ファイル情報をテキストファイルから読み出し
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Dim rectList As List(Of Rect) = New List(Of Rect)

        Try
            Using sr As IO.StreamReader = New IO.StreamReader(String.Format("{0}\pointList.txt", PathSlnFile), enc_sj)
                '題目の除去
                sr.ReadLine()
                Do While sr.Peek() >= 0
                    Dim tmpStrArry As String() = sr.ReadLine().Split(","c)
                    Dim tmpRect As Rect = New Rect()
                    tmpRect.X = Convert.ToInt32(tmpStrArry(0))
                    tmpRect.Y = Convert.ToInt32(tmpStrArry(1))
                    tmpRect.Width = Convert.ToInt32(tmpStrArry(2))
                    tmpRect.Height = Convert.ToInt32(tmpStrArry(3))
                    rectList.Add(tmpRect)

                    My.Application.DoEvents()
                Loop
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim img As Mat = New Mat()
        Using cap As VideoCapture = New VideoCapture(1)

            Do While Cv2.WaitKey(30) = -1
                cap.Read(img)

                For Each rl In rectList
                    img.Rectangle(rl, New Scalar(0, 0, 255))
                Next
                Cv2.ImShow("img", img)

                My.Application.DoEvents()
            Loop


        End Using


    End Sub

    ''' <summary>
    ''' 指定した矩形領域のHue値の平均を求める
    ''' Sat値でのマスク処理
    ''' </summary>
    ''' <param name="img"></param>
    ''' <param name="rect"></param>
    ''' <returns></returns>
    Private Function calcHueAveRect(ByRef img As Mat, ByVal rect As Rect) As Double

        If img.Channels <> 1 Then
            MessageBox.Show("")
            Return 0
        End If

        Dim ave As Double = 0
        Dim count As Long = 0
        Dim val As Byte = 0

        For j As Integer = rect.Top To rect.Bottom - 1
            For i As Integer = rect.Left To rect.Right - 1

                '彩度の小さい箇所（LEDの光が強く白く映っているところ）を除く

                '*2はHueの範囲を[0:360]とするため
                'ホワイトバランス等の問題で赤(0°(または360°)付近が不安定となるので
                '色相が紫付近の場合は360°からの距離をとる
                val = img.At(Of Byte)(j, i)
                If val < 150 Then
                    ave += 2 * val
                Else
                    ave += 360 - 2 * val
                End If
                count += 1

            Next
        Next
        ave /= CDbl(count)

        Return ave

    End Function
    ''' <summary>
    ''' 製品ワーク電源切替
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cbEXX600Pow_CheckedChanged(sender As Object, e As EventArgs) Handles cbEXX600Pow.CheckedChanged
        If Not cbEXX600Pow.Checked Then
            USBIO.EX600_Pow(1)
            cbEXX600Pow.BackColor = Color.Lime
        Else
            USBIO.EX600_Pow(0)
            cbEXX600Pow.BackColor = Color.Gray
        End If
    End Sub
    ''' <summary>
    ''' HUB電源切替
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cbHubPow_CheckedChanged(sender As Object, e As EventArgs) Handles cbHubPow.CheckedChanged
        If Not cbHubPow.Checked Then
            USBIO.Hub_Pow(1)
            cbHubPow.BackColor = Color.Lime
        Else
            USBIO.Hub_Pow(0)
            cbHubPow.BackColor = Color.Gray
        End If
    End Sub
    ''' <summary>
    ''' PLC電源切替
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cbPLCPow_CheckedChanged(sender As Object, e As EventArgs) Handles cbPLCPow.CheckedChanged
        If Not cbPLCPow.Checked Then
            USBIO.AC_Relay(1)
            cbPLCPow.BackColor = Color.Lime
        Else
            USBIO.AC_Relay(0)
            cbPLCPow.BackColor = Color.Gray
        End If
    End Sub
    ''' <summary>
    ''' EX260電源切替
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cbEX260Pow_CheckedChanged(sender As Object, e As EventArgs) Handles cbEX260Pow.CheckedChanged
        If Not cbEX260Pow.Checked Then
            USBIO.EX260_Pow(1)
            cbEX260Pow.BackColor = Color.Lime
        Else
            USBIO.EX260_Pow(0)
            cbEX260Pow.BackColor = Color.Gray
        End If
    End Sub

    ''' <summary>
    ''' NFC接続
    ''' </summary>
    ''' <returns></returns>
    Private Function isConnectNFC() As Boolean

        If Not Sna.NoWire.Nfc_connect() Then
            Sna.NoWire.Nfc_disconnect()
            Return False
        End If

        If Not Sna.NoWire.Nfc_req() Then
            Sna.NoWire.Nfc_disconnect()
            Return False
        End If

        Return True

    End Function
    ''' <summary>
    ''' NFC１行読込み
    ''' </summary>
    ''' <param name="block"></param>
    ''' <returns></returns>
    Private Function nfcReadLine(ByVal block As UInt16) As Byte()

        trans_mode = False

        '通信確認
        If Not isConnectNFC() Then
            Return Nothing
        End If

        '読み込み
        If Not Sna.NoWire.Nfc_rf_read(Convert.ToUInt16(block)) Then
            Dim err As Boolean() = Sna.NoWire.ErrorStatus
            Return Nothing
        End If
        '有効データの抽出
        If Not Sna.NoWire.Nfc_rf_DealRecvBuffer() Then
            Return Nothing
        End If

        Dim data(Sna.NoWire.RecvData.Length - 1) As Byte

        '値のみコピーしてreturn
        'return RecvDataだとRecvDataへの参照が帰る？
        For i As Integer = 0 To Sna.NoWire.RecvData.Length - 1
            data(i) = Sna.NoWire.RecvData(i)
        Next

        Return data

    End Function
    ''' <summary>
    ''' NFC１行書込み
    ''' </summary>
    ''' <param name="block"></param>
    ''' <param name="data"></param>
    ''' <returns></returns>
    Private Function nfcWriteLine(ByVal block As UInt16, ByVal data As Byte()) As Boolean
        '通信確認
        If Not isConnectNFC() Then Return False

        If Not Sna.NoWire.Nfc_rf_write(block, data) Then
            Return False
        End If

        Return True '書き込み成功

    End Function

    ''' <summary>
    ''' ワークを検査モードに変更
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnInsMode_Click(sender As Object, e As EventArgs) Handles btnInsMode.Click

        Sna.NoWire.trans_mode = False

        If Not isConnectNFC() Then
            MessageBox.Show("error:nfcConnect")
        End If

        Dim readData As Byte() = nfcReadLine(&H0)
        If readData Is Nothing Then
            MessageBox.Show("error:nfcReadLine")
        End If

        Try
            readData(&H4) = &H40
            readData(&HB) = &H55 '検査モード

            If Not nfcWriteLine(&H0, readData) Then
                MessageBox.Show("error:nfcWriteLine")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        MessageBox.Show("finished")

    End Sub
    ''' <summary>
    ''' ワークを通常モードに変更する
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnNormalMode_Click(sender As Object, e As EventArgs) Handles btnNormalMode.Click
        Sna.NoWire.trans_mode = False

        If Not isConnectNFC() Then
            MessageBox.Show("error:nfcConnect")
        End If

        Dim readData As Byte() = nfcReadLine(&H0)
        If readData Is Nothing Then
            MessageBox.Show("error:nfcReadLine")
        End If

        Try
            readData(&H4) = &H50
            readData(&HB) = &H0 '通常モード

            If Not nfcWriteLine(&H0, readData) Then
                MessageBox.Show("error:nfcWriteLine")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        MessageBox.Show("finished")

    End Sub
    ''' <summary>
    ''' FeRAMブロック０書込みプロテクト
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnProtect_Click(sender As Object, e As EventArgs) Handles btnProtect.Click
        '--------------------------------------------------------
        '[FeRAMグループ0の書き込みプロテクト]
        'システムメモリエリア(*13:0x01F0, 0x01F4)に0x01を書き込む
        '--------------------------------------------------------
        Sna.NoWire.Nfc_disconnect()

        'ワーク電源オフ
        If Not USBIO.EX600_Pow(0) Then
            MessageBox.Show("error:PSoC")
        End If
        Wait(500)

        '制御エリア書き込みのため平文モード
        trans_mode = True

        If Not isConnectNFC() Then
            MessageBox.Show("error:nfcConnection")
            Exit Sub
        End If

        Dim readSystemData As Byte()
        If Not Sna.NoWire.Nfc_rf_read(&H1F0) Then
            MessageBox.Show("error:readSytemMemory")
            Exit Sub
        End If

        If Not Sna.NoWire.Nfc_rf_DealRecvBuffer_Without_Check() Then
            MessageBox.Show("error:dealRecvBuf_WithoutChck")
            Exit Sub
        End If

        readSystemData = Sna.NoWire.RecvData
        If readSystemData.Length < 16 Then
            MessageBox.Show("readDataLength < 16")
            Exit Sub
        End If
        'プロテクトをかけるため値を更新
        readSystemData(&H0) = &H1
        readSystemData(&H4) = &H1
        readSystemData(&HF) = &H2E '暗号化モードON

        If Not Sna.NoWire.Nfc_rf_write_normal_system(&H1F0, readSystemData) Then
            MessageBox.Show("error:nfcWrite(System memory area)")
            Exit Sub
        End If

        Wait(500)

        '制御エリアに書込みを反映させるためNFCの磁気切断→接続処理を行う
        Dim retryNum As Integer = 10
        Dim count As Integer = 0

        '磁気をoff
        For i As Integer = 0 To 3
            If Sna.NoWire.Nfc_turn_off() Then
                Wait(500)
                Exit For
            End If
        Next
        '磁気をon
        For i As Integer = 0 To 3
            If Sna.NoWire.Nfc_turn_on() Then
                Wait(500)
                Exit For
            End If
        Next

        For i As Integer = 0 To 3
            If Sna.NoWire.Nfc_disconnect() Then
                Wait(500)
                Exit For
            End If
        Next



        Wait(1000)

        '暗号化モードに戻す
        trans_mode = False

        If Not USBIO.EX600_Pow(1) Then
            MessageBox.Show("error:PSoC")
            Exit Sub
        End If
        Wait(2000)


        If Not isConnectNFC() Then
            MessageBox.Show("error:nfcConnection")
            Exit Sub
        End If

        Dim readProtectData As Byte() = nfcReadLine(&H0)
        If readProtectData Is Nothing Then
            MessageBox.Show("error:nfcReadLine")
            Exit Sub
        End If

        Wait(500)

        'ブロック0エリアに書込みができないことを確認
        If nfcWriteLine(&H0, readProtectData) Then
            MessageBox.Show("NFC書込みエラー(ブロック０エリア書込み成功)")
            Exit Sub
        End If

        MessageBox.Show("finished")

    End Sub

    Private Sub Manual_Camera_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Sna.NoWire.Nfc_disconnect()
    End Sub

    ''' <summary>
    ''' FeRAMブロック０書込みプロテクト解除
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnResetProtect_Click(sender As Object, e As EventArgs) Handles btnResetProtect.Click
        Sna.NoWire.Nfc_disconnect()

        'ワーク電源オフ
        If Not USBIO.EX600_Pow(0) Then
            MessageBox.Show("error:PSoC")
        End If
        Wait(500)

        '制御エリア書き込みのため平文モード
        trans_mode = True

        If Not isConnectNFC() Then
            MessageBox.Show("error:nfcConnection")
            Exit Sub
        End If

        Dim readSystemData As Byte()
        If Not Sna.NoWire.Nfc_rf_read(&H1F0) Then
            MessageBox.Show("error:readSytemMemory")
            Exit Sub
        End If

        If Not Sna.NoWire.Nfc_rf_DealRecvBuffer_Without_Check() Then
            MessageBox.Show("error:dealRecvBuf_WithoutChck")
            Exit Sub
        End If

        readSystemData = Sna.NoWire.RecvData
        If readSystemData.Length < 16 Then
            MessageBox.Show("readDataLength < 16")
            Exit Sub
        End If
        'プロテクトをかけるため値を更新
        readSystemData(&H0) = &H0
        readSystemData(&H4) = &H0
        readSystemData(&HF) = &H2E '暗号化モードON

        If Not Sna.NoWire.Nfc_rf_write_normal_system(&H1F0, readSystemData) Then
            MessageBox.Show("error:nfcWrite(System memory area)")
            Exit Sub
        End If

        Wait(500)

        '制御エリアに書込みを反映させるためNFCの磁気切断→接続処理を行う
        Dim retryNum As Integer = 10
        Dim count As Integer = 0

        '磁気をoff
        For i As Integer = 0 To 3
            If Sna.NoWire.Nfc_turn_off() Then
                Wait(500)
                Exit For
            End If
        Next
        '磁気をon
        For i As Integer = 0 To 3
            If Sna.NoWire.Nfc_turn_on() Then
                Wait(500)
                Exit For
            End If
        Next

        For i As Integer = 0 To 3
            If Sna.NoWire.Nfc_disconnect() Then
                Wait(500)
                Exit For
            End If
        Next



        Wait(1000)

        '暗号化モードに戻す
        trans_mode = False

        'If Not USBIO.EX600_Pow(1) Then
        '    MessageBox.Show("error:PSoC")
        '    Exit Sub
        'End If
        'Wait(2000)


        If Not isConnectNFC() Then
            MessageBox.Show("error:nfcConnection")
            Exit Sub
        End If

        Dim readProtectData As Byte() = nfcReadLine(&H0)
        If readProtectData Is Nothing Then
            MessageBox.Show("error:nfcReadLine")
            Exit Sub
        End If

        Wait(500)

        'ブロック0エリアに書込みができないことを確認
        If Not nfcWriteLine(&H0, readProtectData) Then
            MessageBox.Show("NFC書込みエラー(ブロック０エリア書込み失敗)")
            Exit Sub
        End If

        MessageBox.Show("finished")
    End Sub

    ''' <summary>
    ''' Hueの値毎に色を返す
    ''' </summary>
    ''' <param name="hVal"></param>
    ''' <returns></returns>
    Private Function judgeColor(ByVal hVal As Double) As LED_COLOR

        Select Case hVal

            'Case 0.01 To 10 ', 340 To 360 '［Hueが大きい場合は360からの距離取る]処理であれば350 to 360は不要
            '    Return LED_COLOR.RED
            'Case 80 To 140
            '    Return LED_COLOR.GREEN

            '+0.01:彩度、明度のフィルタリング漏れ対策
            Case RED_MIN + 0.01 To RED_MAX ', 340 To 360 '［Hueが大きい場合は360からの距離取る]処理であれば350 to 360は不要
                Return LED_COLOR.RED
            Case GREEN_MIN To GREEN_MAX
                Return LED_COLOR.GREEN

        End Select

        Return LED_COLOR.OTHER '該当なし
    End Function


    ''' <summary>
    ''' 指定した矩形領域のHue値の平均を求める
    ''' Sat値でのマスク処理
    ''' </summary>
    ''' <param name="img"></param>
    ''' <param name="rect"></param>
    ''' <param name="satImg"></param>
    ''' <returns></returns>
    Private Function calcHueAveRect(ByRef img As Mat, ByVal rect As Rect, ByRef satImg As Mat) As Double

        If img.Channels <> 1 Then
            Return 0
        End If

        Dim ave As Double = 0
        Dim count As Long = 0
        Dim val As Byte = 0

        For j As Integer = rect.Top To rect.Bottom - 1
            For i As Integer = rect.Left To rect.Right - 1

                '彩度の小さい箇所（LEDの光が強く白く映っているところ）を除く
                If satImg.At(Of Byte)(j, i) >= 50 Then

                    '*2はHueの範囲を[0:360]とするため
                    'ホワイトバランス等の問題で赤(0°(または360°)付近が不安定となるので
                    '色相が紫付近の場合は360°からの距離をとる
                    val = img.At(Of Byte)(j, i)
                    If val < 150 Then
                        ave += 2 * val
                    Else
                        ave += 360 - 2 * val
                    End If
                    count += 1
                End If

            Next
        Next
        ave /= CDbl(count)

        Return ave

    End Function
    ''' <summary>
    ''' 指定した矩形領域のHue値の平均を求める
    ''' Val,Sat値でのマスク処理
    ''' </summary>
    ''' <param name="img"></param>
    ''' <param name="rect"></param>
    ''' <param name="valImg"></param>
    ''' <param name="satImg"></param>
    ''' <returns></returns>
    Private Function calcHueAveRect(ByRef img As Mat, ByVal rect As Rect, ByRef valImg As Mat, ByRef satImg As Mat) As Double

        'パラメータ取得
        Dim maskThV As Integer = 50 '明度閾値
        Dim maskThS As Integer = 150 '彩度閾値
        Dim LEDAreaTh As Integer = 30 'LED部分ラベル面積閾値
        If IsNumeric(maskThV) Then maskThV = Integer.Parse(tbMaskTH_V_chkLED.Text)
        If IsNumeric(maskThS) Then maskThS = Integer.Parse(tbMaskTH_S_chkLED.Text)
        If IsNumeric(LEDAreaTh) Then LEDAreaTh = Integer.Parse(tbLedAreaTH_chkLED.Text)


        If img.Channels <> 1 Then
            Return 0
        End If

        Dim ave As Double = 0
        Dim count As Long = 0
        Dim val As Byte = 0


        For j As Integer = rect.Top To rect.Bottom - 1
            For i As Integer = rect.Left To rect.Right - 1

                '明度、彩度の小さい箇所（LEDの光が強く白く映っているところ）を除く
                'If valImg.At(Of Byte)(j, i) > 50 AndAlso satImg.At(Of Byte)(j, i) > 150 Then
                If valImg.At(Of Byte)(j, i) > maskThV AndAlso satImg.At(Of Byte)(j, i) > maskThS Then

                    '*2はHueの範囲を[0:360]とするため
                    'ホワイトバランス等の問題で赤(0°(または360°)付近が不安定となるので
                    '色相が紫付近(青240°,赤360°の中間)の場合は360°からの距離をとる
                    val = img.At(Of Byte)(j, i)
                    If val < 150 Then
                        ave += 2 * val
                    Else
                        ave += 360 - 2 * val
                    End If
                    count += 1
                End If

            Next
        Next
        ave /= CDbl(count)

        Return ave

    End Function
    ''' <summary>
    ''' 入力した色のパターンに関して循環パターン(Listの配列)を作る
    ''' </summary>
    ''' <param name="colorPattern"></param>
    ''' <returns></returns>

    Private Function createCirculationPattern(ByVal colorPattern As List(Of LED_COLOR)) As List(Of LED_COLOR)()

        Dim circulationPattern As List(Of LED_COLOR)() = New List(Of LED_COLOR)(colorPattern.Count - 1) {}
        For i As Integer = 0 To circulationPattern.Count - 1
            circulationPattern(i) = New List(Of LED_COLOR)
        Next

        For i As Integer = 0 To colorPattern.Count - 1

            For j As Integer = 0 To colorPattern.Count - 1
                circulationPattern(i).Add(colorPattern(j))
            Next
            'パターンの循環を作る(先頭の要素を末尾に持っていく)
            colorPattern.Add(colorPattern(0))
            colorPattern.RemoveAt(0)
        Next

        Return circulationPattern
    End Function

    ''' <summary>
    ''' 色の文字列をLED_COLOR型に変換
    ''' </summary>
    ''' <param name="strColor"></param>
    ''' <returns></returns>
    Private Function transStrToColor(ByVal strColor As String) As LED_COLOR
        Select Case strColor
            Case "赤"
                Return LED_COLOR.RED
            Case "緑"
                Return LED_COLOR.GREEN
            Case "オレンジ"
                Return LED_COLOR.ORANGE
            Case "消灯"
                Return LED_COLOR.OFF
            Case Else
                Return LED_COLOR.OTHER
        End Select
    End Function

    ''' <summary>
    ''' 品番リストから取得したLED点灯パターンを変換
    ''' </summary>
    ''' <param name="patternStr"></param>
    ''' <returns></returns>
    Private Function makeColorPattern(ByVal patternStr As String) As List(Of LED_COLOR)

        Dim patternList As List(Of LED_COLOR) = New List(Of LED_COLOR) From {}
        Dim tmpStr As String() = patternStr.Split("-"c)

        For i As Integer = 0 To tmpStr.Length - 1
            patternList.Add(transStrToColor(tmpStr(i)))
        Next

        Return patternList

    End Function


    ''' <summary>
    ''' master unit
    ''' </summary>
    ''' <param name="cap"></param>
    ''' <param name="pattern"></param>
    ''' <returns></returns>
    Private Function chkMasterUnitLED(ByRef cap As VideoCapture, ByVal pattern As List(Of LED_COLOR)) As Boolean

        Dim colorPattern As New List(Of LED_COLOR) '検出した色パターン
        Dim cycleTimer As Stopwatch = New Stopwatch() 'サイクルタイム
        Dim timeout_msec As Integer = 4000 'サイクルタイムオーバー[msec] 
        Dim resultImg As Mat() = New Mat(pattern.Count - 1) {} '結果保存用

        'パラメータ取得
        Dim maskThV As Integer = 50 '明度閾値
        Dim maskThS As Integer = 150 '彩度閾値
        Dim LEDAreaTh As Integer = 30 'LED部分ラベル面積閾値

        If IsNumeric(maskThV) Then maskThV = Integer.Parse(tbMaskTH_V_chkLED.Text)
        If IsNumeric(maskThS) Then maskThS = Integer.Parse(tbMaskTH_S_chkLED.Text)
        If IsNumeric(LEDAreaTh) Then LEDAreaTh = Integer.Parse(tbLedAreaTH_chkLED.Text)


        For i As Integer = 0 To pattern.Count - 1
            resultImg(i) = New Mat()
        Next

        Dim img As Mat = New Mat()
        Dim color As New List(Of LED_COLOR) '各取得画像に対してラベル領域の色を保持
        Cv2.NamedWindow("chkLED")

        cycleTimer.Start()

        Do While colorPattern.Count < pattern.Count

            '[初期化]
            color.Clear()
            '[画像のキャプチャ]
            cap.Read(img)


            If cycleTimer.ElapsedMilliseconds > timeout_msec Then
                'gColorList = colorPattern
                Cv2.PutText(img, "Cycletime Over", New Point(img.Width / 10, img.Height / 10), HersheyFonts.HersheyComplex, 1, New Scalar(255, 0, 255))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                Cv2.DestroyAllWindows()
                Return False
            End If

            '[HSV変換]
            Dim imgHSV As Mat = New Mat()
            Cv2.CvtColor(img, imgHSV, ColorConversionCodes.BGR2HSV)
            'HとVの値をそれぞれ仕様するため分割
            Dim imgSepHSV() As Mat = New Mat(imgHSV.Channels - 1) {}
            Cv2.Split(imgHSV, imgSepHSV)


            '[輝度値(V)で2値化]
            Dim mask As Mat = New Mat()
            Cv2.Threshold(imgSepHSV(2), mask, maskThV, 255, ThresholdTypes.Binary)
            'Cv2.ImShow("mask", mask)
            'Cv2.WaitKey(10)

            '[マスク画像のフィルタリング]
            mask.MedianBlur(5)
            'Cv2.Erode(mask, mask, Mat.Ones(5, 5, MatType.CV_8UC1))
            Cv2.Dilate(mask, mask, Mat.Ones(5, 5, MatType.CV_8UC1))
            'Cv2.ImShow("mask", mask)
            'Cv2.WaitKey(10)

            '[ラベリング]
            Dim cc As ConnectedComponents = Cv2.ConnectedComponentsEx(mask)
            '[LED領域のフィルタリング]
            'ラベリングされている外枠部分を除外
            'todo:値の調整
            Dim filterCC = From ft In cc.Blobs Where ft.Area < 1000 AndAlso ft.Area > LEDAreaTh

            For Each fc In filterCC
                img.Rectangle(fc.Rect, New Scalar(0, 255, 255), 1)
            Next
            'Cv2.ImShow("fc", img)
            'Cv2.WaitKey(15)

            '[画像中のLEDの色の登録->color]

            'LEDが消灯の場合
            If filterCC.Count = 0 Then

                If colorPattern.Count = 0 Then
                    img.CopyTo(resultImg(color.Count))
                    colorPattern.Add(LED_COLOR.OFF)

                    Cv2.ImShow("chkLED", img)
                    waitBySW(20)

                    Continue Do
                End If
                'LED状態の切り替わりが起きた場合
                If colorPattern.Last <> LED_COLOR.OFF Then
                    img.CopyTo(resultImg(colorPattern.Count))
                    colorPattern.Add(LED_COLOR.OFF)

                    Cv2.ImShow("chkLED", img)
                    waitBySW(20)

                End If
                Continue Do
            End If


            'LEDが全て点灯していない状態でキャプチャを行った場合
            Dim maxLEDNum As Integer = 8
            'todo:ラベルの面積閾値の調整
            If filterCC.Count <> maxLEDNum Then
                Continue Do
            End If

            'Hue画像から各ラベル領域の色を算出
            For Each blb In filterCC
                color.Add(judgeColor(calcHueAveRect(imgSepHSV(0), blb.Rect, imgSepHSV(2), imgSepHSV(1))))
                img.Rectangle(blb.Rect, New Scalar(0, 0, 255))
            Next

            '全ラベル領域の色の一致確認
            For Each i In color
                If i <> color.First() Then
                    'MessageBox.Show("all colors are not same")
                    Continue Do
                End If
            Next

            '消灯状態でなく全てのラベル領域が同色
            '初回
            If colorPattern.Count = 0 Then
                img.CopyTo(resultImg(colorPattern.Count))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                colorPattern.Add(color.First())
                Continue Do
            End If
            '検出した色の切り替わりが起きた時
            If colorPattern.Last() <> color.First() Then
                img.CopyTo(resultImg(colorPattern.Count))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                colorPattern.Add(color.First())
            End If

            My.Application.DoEvents()
        Loop



        '[パターン比較]
        Dim isMatch As Boolean = True

        '循環パターンの作成(パターン数 = pattern.length)
        Dim ciculationPattern As List(Of LED_COLOR)() = createCirculationPattern(pattern)
        '循環パターンのいずれかとの一致を確認
        For i As Integer = 0 To ciculationPattern.Count - 1

            isMatch = True '初期化
            For j As Integer = 0 To colorPattern.Count - 1
                isMatch = isMatch AndAlso colorPattern.Item(j) = ciculationPattern(i).Item(j)
            Next
            If isMatch Then
                Exit For
            End If
        Next


        '結果画像作成＆表示
        Dim combinedImg As Mat = New Mat(New Size(img.Width, img.Height), MatType.CV_8UC3)
        For i As Integer = 0 To resultImg.Count - 1
            Cv2.Resize(resultImg(i), resultImg(i), New Size(0.5 * combinedImg.Width, 0.5 * combinedImg.Height))
            Cv2.PutText(resultImg(i), "No." & i.ToString & ":" & colorPattern.Item(i).ToString(), New Point(resultImg(i).Width / 10, resultImg(i).Height / 10), HersheyFonts.HersheyComplex, 1, New Scalar(255, 0, 255))
        Next
        resultImg(0).CopyTo(combinedImg(New Rect(New Point(0, 0), New Size(resultImg(0).Width, resultImg(0).Height))))
        resultImg(1).CopyTo(combinedImg(New Rect(New Point(320, 0), New Size(resultImg(1).Width, resultImg(1).Height))))
        resultImg(2).CopyTo(combinedImg(New Rect(New Point(0, 240), New Size(resultImg(2).Width, resultImg(2).Height))))
        resultImg(3).CopyTo(combinedImg(New Rect(New Point(320, 240), New Size(resultImg(3).Width, resultImg(3).Height))))

        Dim cpStr As String = String.Empty
        Dim correctPattern As String = String.Empty
        '取得した色のパターン
        For i As Integer = 0 To colorPattern.Count - 1
            cpStr = cpStr & colorPattern.Item(i).ToString() & "->"
        Next
        '検出したいの色パターン
        For i As Integer = 0 To pattern.Count - 1
            correctPattern = correctPattern & pattern.Item(i).ToString() & "->"
        Next

        combinedImg.PutText("Result:" & If(isMatch, "[OK]", "[NG]") & " " & cpStr, New Point(combinedImg.Width * 1 / 10, combinedImg.Height * 9 / 10), HersheyFonts.HersheyComplex, 0.5, New Scalar(255, 0, 255))
        combinedImg.PutText("input pattern:" & correctPattern, New Point(combinedImg.Width * 1 / 10, combinedImg.Height * 9.5 / 10), HersheyFonts.HersheyComplex, 0.5, New Scalar(255, 0, 255))
        Cv2.ImShow("chkLED", combinedImg)
        'waitBySW(20)
        Cv2.WaitKey(0) 'マニュアルモード起動時にはキー入力を待つ
        combinedImg.SaveImage(String.Format("{0}\LED検査結果画像(master).jpg", PathSlnFile))
        combinedImg.Release()
        Cv2.DestroyWindow("chkLED")

        '入力パターンとの一致するか
        If isMatch Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function chkSlaveUnitLED(ByRef cap As VideoCapture, ByVal pattern As List(Of LED_COLOR)) As Boolean

        Dim colorPattern As New List(Of LED_COLOR) '検出した色パターン
        Dim cycleTimer As Stopwatch = New Stopwatch() 'サイクルタイム
        Dim timeout_msec As Integer = 4000 'サイクルタイムオーバー[msec] 
        Dim resultImg As Mat() = New Mat(pattern.Count - 1) {} '結果保存用

        For i As Integer = 0 To resultImg.Length - 1
            resultImg(i) = New Mat()
        Next

        Dim img As Mat = New Mat()
        Dim color As New List(Of LED_COLOR) '各取得画像に対してラベル領域の色を保持
        Cv2.NamedWindow("chkLED")

        'パラメータ取得
        Dim maskThV As Integer = 50 '明度閾値
        Dim maskThS As Integer = 150 '彩度閾値
        Dim LEDAreaTh As Integer = 30 'LED部分ラベル面積閾値
        If IsNumeric(maskThV) Then maskThV = Integer.Parse(tbMaskTH_V_chkLED.Text)
        If IsNumeric(maskThS) Then maskThS = Integer.Parse(tbMaskTH_S_chkLED.Text)
        If IsNumeric(LEDAreaTh) Then LEDAreaTh = Integer.Parse(tbLedAreaTH_chkLED.Text)


        cycleTimer.Start()

        Do While colorPattern.Count < pattern.Count

            '[初期化]
            color.Clear()
            '[画像のキャプチャ]
            cap.Read(img)


            '[サイクルタイムチェック]
            If cycleTimer.ElapsedMilliseconds > timeout_msec Then
                Cv2.PutText(img, "Cycletime Over", New Point(img.Width / 10, img.Height / 10), HersheyFonts.HersheyComplex, 1, New Scalar(255, 0, 255))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                Cv2.DestroyAllWindows()
                Return False
            End If

            '[HSV変換]
            Dim imgHSV As Mat = New Mat()
            Cv2.CvtColor(img, imgHSV, ColorConversionCodes.BGR2HSV)
            'HとVの値をそれぞれ仕様するため分割
            Dim imgSepHSV() As Mat = New Mat(imgHSV.Channels - 1) {}
            Cv2.Split(imgHSV, imgSepHSV)


            '[輝度値(V)で2値化]
            Dim mask As Mat = New Mat()
            Cv2.Threshold(imgSepHSV(2), mask, maskThV, 255, ThresholdTypes.Binary)
            'Cv2.ImShow("mask", mask)
            'Cv2.ImShow("image", img)
            'Cv2.WaitKey(10)

            '[マスク画像のフィルタリング]
            mask.MedianBlur(5)
            'Cv2.Erode(mask, mask, Mat.Ones(5, 5, MatType.CV_8UC1))
            Cv2.Dilate(mask, mask, Mat.Ones(5, 5, MatType.CV_8UC1))


            '[ラベリング]
            Dim cc As ConnectedComponents = Cv2.ConnectedComponentsEx(mask)
            '[LED領域のフィルタリング]
            'ラベリングされている外枠部分を除外
            Dim filterCC = From ft In cc.Blobs Where ft.Area < 1000 AndAlso ft.Area > LEDAreaTh

            '[画像中のLEDの色の登録->color]

            'LEDが消灯の場合
            If filterCC.Count = 0 Then

                If colorPattern.Count = 0 Then
                    img.CopyTo(resultImg(color.Count))
                    colorPattern.Add(LED_COLOR.OFF)

                    Cv2.ImShow("chkLED", img)
                    waitBySW(20)

                    Continue Do
                End If
                'LED状態の切り替わりが起きた場合
                If colorPattern.Last <> LED_COLOR.OFF Then
                    img.CopyTo(resultImg(colorPattern.Count))
                    colorPattern.Add(LED_COLOR.OFF)

                    Cv2.ImShow("chkLED", img)
                    waitBySW(20)

                End If
                Continue Do
            End If

            'LEDが全て点灯していない状態でキャプチャを行った場合
            Dim maxLEDNum As Integer = 4
            'todo:ラベルの面積閾値の調整
            If filterCC.Count <> maxLEDNum Then
                Continue Do
            End If

            'Hue画像から各ラベル領域の色を算出
            For Each blb In filterCC
                color.Add(judgeColor(calcHueAveRect(imgSepHSV(0), blb.Rect, imgSepHSV(2), imgSepHSV(1))))
                img.Rectangle(blb.Rect, New Scalar(0, 0, 255))
            Next

            '全ラベル領域の色の一致確認
            For Each i In color
                If i <> color.First() Then
                    'MessageBox.Show("all colors are not same")
                    Continue Do
                End If
            Next

            '--------------------------------------
            '           位置合わせ処理
            '--------------------------------------
            Dim centerPoint As Point2d = New Point2d(0, 0) '全ラベル重心を加重平均した点
            Dim centerLabel As Point2d
            Dim leftPoint As New Point2d(img.Width, img.Height) '画面の右端で初期化
            Dim rightPoint As New Point2d(0, 0) '画面の左端で初期化
            Dim topPoint As New Point2d(0, 0) '画面の下端で初期化
            Dim bottomPoint As New Point2d(img.Width, img.Height) '画面の上端で初期化


            '[中央のラベルを求める]
            '[各ラベル重心の平均を算出]
            For Each lbl In filterCC
                centerPoint.X += lbl.Centroid.X / (filterCC.Count)
                centerPoint.Y += lbl.Centroid.Y / (filterCC.Count)
            Next

            '全てのラベル重心に最も近いラベルを求める
            '[距離を計算]
            Dim minDistance As Double = Math.Sqrt(img.Width ^ 2 + img.Height ^ 2) '初期値(画像内の最大距離)

            For Each lbl In filterCC
                Dim distance As Double = Math.Sqrt((lbl.Centroid.X - centerPoint.X) ^ 2 + (lbl.Centroid.Y - centerPoint.Y) ^ 2)

                '最も距離の近いラベルを探索
                If distance < minDistance Then
                    minDistance = distance
                    centerLabel.X = lbl.Centroid.X
                    centerLabel.Y = lbl.Centroid.Y
                End If
                '左端(x=0)に近いラベルを探索
                If lbl.Centroid.X < leftPoint.X Then
                    leftPoint.X = lbl.Centroid.X
                    leftPoint.Y = lbl.Centroid.Y
                End If
                '右端(x=img.width)に近いラベルを探索
                If lbl.Centroid.X > rightPoint.X Then
                    rightPoint.X = lbl.Centroid.X
                    rightPoint.Y = lbl.Centroid.Y
                End If
                '下端(y=img.height)に近いラベルを探索
                If lbl.Centroid.Y > topPoint.Y Then
                    topPoint.X = lbl.Centroid.X
                    topPoint.Y = lbl.Centroid.Y
                End If
                '上端(y=0)に近いラベルを探索
                If lbl.Centroid.Y < bottomPoint.Y Then
                    bottomPoint.X = lbl.Centroid.X
                    bottomPoint.Y = lbl.Centroid.Y
                End If

            Next

            '[中央の左ラベル位置を求める]
            minDistance = Math.Sqrt(img.Width ^ 2 + img.Height ^ 2) '初期値(画像内の最大距離)

            Dim leftCenter As New Point2d()
            For Each lbl In filterCC
                'X座標の基準は左端ラベルのX座標
                'Y座標の基準は中央ラベルのY座標
                Dim distance As Double = Math.Sqrt((lbl.Centroid.X - leftPoint.X) ^ 2 + (lbl.Centroid.Y - centerLabel.Y) ^ 2)
                If distance < minDistance Then
                    minDistance = distance
                    leftCenter.X = lbl.Centroid.X
                    leftCenter.Y = lbl.Centroid.Y
                End If
            Next

            '[中央の右ラベル位置を求める]
            minDistance = Math.Sqrt(img.Width ^ 2 + img.Height ^ 2) '初期値(画像内の最大距離)

            'Dim rightCenter As New Point2d(0, centerLabel.Y)
            Dim rightCenter As New Point2d()
            For Each lbl In filterCC
                'X座標の基準は右端ラベルのX座標
                'Y座標の基準は中央ラベルのY座標
                Dim distance As Double = Math.Sqrt((lbl.Centroid.X - rightPoint.X) ^ 2 + (lbl.Centroid.Y - centerLabel.Y) ^ 2)
                If distance < minDistance Then
                    minDistance = distance
                    rightCenter.X = lbl.Centroid.X
                    rightCenter.Y = lbl.Centroid.Y
                End If
            Next

            '[ラベル重心を用いてθ方向の補正]

            '[中央行の左右ラベル重心からθ方向の傾きを算出]
            Dim disX As Double = -leftCenter.X + rightCenter.X
            Dim disY As Double = -leftCenter.Y + rightCenter.Y
            Dim theta As Double = If(disX <> 0, Math.Atan(disY / disX) * 180 / Math.PI, 0) 'disXでZeroDivisionErrorの可能性
            '[中央ラベルを回転中心]とした回転行列算出
            Dim rotMat As Mat = Cv2.GetRotationMatrix2D(New Point2f(Convert.ToSingle(centerLabel.X), Convert.ToSingle(centerLabel.Y)), theta, 1.0)

            Cv2.WarpAffine(img, img, rotMat, New Size(img.Width, img.Height))
            'Cv2.ImShow("warping", img)
            'Cv2.WaitKey(10)


            '位置補正のための座標を算出(中央ラベル座標（回転中心）からLEDのX,Y方向間隔だけずれた位置に他のLED)

            '[LEDの配置間隔を算出]

            '   LEDの並び
            '   o   o   o
            '   o   o   o
            '     o     o


            'img.Circle(New Point(leftCenter.X, leftCenter.Y), 20, New Scalar(255, 0, 0))
            'img.Circle(New Point(centerLabel.X, centerLabel.Y), 20, New Scalar(0, 255, 0))
            'img.Circle(New Point(bottomPoint.X, bottomPoint.Y), 20, New Scalar(0, 0, 255))
            'Cv2.ImShow("base", img)
            'Cv2.WaitKey(0)


            'x方向のLED間隔は中央左端と中央ラベルの距離
            Dim xBase As Double = Math.Sqrt((leftCenter.X - centerLabel.X) ^ 2 + (leftCenter.Y - centerLabel.Y) ^ 2)
            'todo:
            'y方向のLED間隔は下段右ラベルと中央右ラベルの距離
            Dim yBase As Double = Math.Sqrt((leftCenter.X - bottomPoint.X) ^ 2 + (leftCenter.Y - bottomPoint.Y) ^ 2)

            Dim width As Integer = 15 '色を計算する際の矩形幅
            Dim height As Integer = 15 '色を計算する際の矩形高さ

            '全てのLEDがあるはずの場所へ円を描画し、矩形領域内の色を算出
            '領域内の一定以上の輝度値を持つピクセルのみ計算

            '[左上]
            img.Circle(New Point(centerLabel.X - xBase, centerLabel.Y - yBase), 15, New Scalar(0, 255, 255)) 'leftTop

            Dim colorLeftTop As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X - xBase - width), Convert.ToInt32(centerLabel.Y - yBase - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorLeftTop.ToString())

            If colorLeftTop = LED_COLOR.OFF OrElse colorLeftTop = LED_COLOR.OTHER Then
                'MessageBox.Show("error:lefttop")
                Return False
            Else
                color.Add(colorLeftTop)
            End If


            '[中央上]
            img.Circle(New Point(centerLabel.X, centerLabel.Y - yBase), 15, New Scalar(0, 255, 255)) 'centerTop

            Dim colorCenterTop As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X - width), Convert.ToInt32(centerLabel.Y - yBase - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorCenterTop.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorCenterTop = LED_COLOR.OFF OrElse colorCenterTop = LED_COLOR.OTHER Then
                'do nothing
            Else
                'MessageBox.Show("error:centertop")
                Return False
            End If




            '[右上]
            img.Circle(New Point(centerLabel.X + xBase, centerLabel.Y - yBase), 15, New Scalar(0, 255, 255)) 'rightTop

            Dim colorRightTop As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X + xBase - width), Convert.ToInt32(centerLabel.Y - yBase - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorRightTop.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorRightTop = LED_COLOR.OFF OrElse colorRightTop = LED_COLOR.OTHER Then
                'do nothing
            Else
                'MessageBox.Show("error:righttop")
                Return False
            End If

            '[中央左]
            img.Circle(New Point(centerLabel.X - xBase, centerLabel.Y), 15, New Scalar(0, 255, 255)) 'leftCenter

            Dim colorLeftCenter As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X - xBase - width), Convert.ToInt32(centerLabel.Y - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorLeftCenter.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorLeftCenter = LED_COLOR.OFF OrElse colorLeftCenter = LED_COLOR.OTHER Then
                'MessageBox.Show("error:leftcenter")
                Return False
            Else
                color.Add(colorLeftCenter)
            End If



            '[中央]
            img.Circle(New Point(centerLabel.X, centerLabel.Y), 15, New Scalar(0, 255, 255)) 'center

            Dim colorCenter As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X - width), Convert.ToInt32(centerLabel.Y - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorCenter.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorCenter = LED_COLOR.OFF OrElse colorCenter = LED_COLOR.OTHER Then
                'MessageBox.Show("error:center")
                Return False
            Else
                color.Add(colorCenter)
            End If


            '[中央右]
            img.Circle(New Point(centerLabel.X + xBase, centerLabel.Y), 15, New Scalar(0, 255, 255)) 'rightCenter

            Dim colorRightCenter As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X + xBase - width), Convert.ToInt32(centerLabel.Y - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorRightCenter.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorRightCenter = LED_COLOR.OFF OrElse colorRightCenter = LED_COLOR.OTHER Then
                'MessageBox.Show("error:rightcenter")
                Return False
            Else
                color.Add(colorRightCenter)
            End If


            '左下
            img.Circle(New Point(centerLabel.X - xBase / 2, centerLabel.Y + yBase), 15, New Scalar(0, 255, 255)) 'leftBottom

            Dim colorLeftBottom As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X - xBase / 2 - width), Convert.ToInt32(centerLabel.Y + yBase - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorLeftBottom.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)


            If colorLeftBottom = LED_COLOR.OFF OrElse colorLeftBottom = LED_COLOR.OTHER Then
                'do nothing
            Else
                'MessageBox.Show("error:leftbottom")
                Return False
            End If


            '[右下]
            img.Circle(New Point(centerLabel.X + xBase, centerLabel.Y + yBase), 15, New Scalar(0, 255, 255)) 'rightBottom

            Dim colorRightBottom As LED_COLOR = judgeColor(calcHueAveRect(imgSepHSV(0), New Rect(Convert.ToInt32(centerLabel.X + xBase - width), Convert.ToInt32(centerLabel.Y + yBase - height), width, height), imgSepHSV(2), imgSepHSV(1)))
            'MessageBox.Show(colorRightBottom.ToString())
            'Cv2.ImShow("img", img)
            'Cv2.WaitKey(0)

            If colorRightBottom = LED_COLOR.OFF OrElse colorRightBottom = LED_COLOR.OTHER Then
                'do nothing
            Else
                'MessageBox.Show("error:rightbottom")
                Return False
            End If


            '消灯状態でなく全てのラベル領域が同色
            '初回
            If colorPattern.Count = 0 Then
                img.CopyTo(resultImg(colorPattern.Count))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                colorPattern.Add(color.First())
                Continue Do
            End If
            '検出した色の切り替わりが起きた時
            If colorPattern.Last() <> color.First() Then
                img.CopyTo(resultImg(colorPattern.Count))
                Cv2.ImShow("chkLED", img)
                waitBySW(20)
                colorPattern.Add(color.First())
            End If

            My.Application.DoEvents()
        Loop

        '[パターン比較]
        Dim isMatch As Boolean = True

        '循環パターンの作成(パターン数 = pattern.length)
        Dim ciculationPattern As List(Of LED_COLOR)() = createCirculationPattern(pattern)
        '循環パターンのいずれかとの一致を確認
        For i As Integer = 0 To ciculationPattern.Count - 1

            isMatch = True '初期化
            For j As Integer = 0 To colorPattern.Count - 1
                isMatch = isMatch AndAlso colorPattern.Item(j) = ciculationPattern(i).Item(j)
            Next
            If isMatch Then
                Exit For
            End If
        Next


        '結果画像作成＆表示
        Dim combinedImg As Mat = New Mat(New Size(img.Width, img.Height), MatType.CV_8UC3)
        For i As Integer = 0 To resultImg.Count - 1
            Cv2.Resize(resultImg(i), resultImg(i), New Size(0.5 * combinedImg.Width, 0.5 * combinedImg.Height))
            Cv2.PutText(resultImg(i), "No." & i.ToString & ":" & colorPattern.Item(i).ToString(), New Point(resultImg(i).Width / 10, resultImg(i).Height / 10), HersheyFonts.HersheyComplex, 1, New Scalar(255, 0, 255))
        Next
        resultImg(0).CopyTo(combinedImg(New Rect(New Point(0, 0), New Size(resultImg(0).Width, resultImg(0).Height))))
        resultImg(1).CopyTo(combinedImg(New Rect(New Point(320, 0), New Size(resultImg(1).Width, resultImg(1).Height))))
        resultImg(2).CopyTo(combinedImg(New Rect(New Point(0, 240), New Size(resultImg(2).Width, resultImg(2).Height))))
        resultImg(3).CopyTo(combinedImg(New Rect(New Point(320, 240), New Size(resultImg(3).Width, resultImg(3).Height))))

        Dim cpStr As String = String.Empty
        Dim correctPattern As String = String.Empty
        '取得した色のパターン
        For i As Integer = 0 To colorPattern.Count - 1
            cpStr = cpStr & colorPattern.Item(i).ToString() & "->"
        Next
        '検出したいの色パターン
        For i As Integer = 0 To pattern.Count - 1
            correctPattern = correctPattern & pattern.Item(i).ToString() & "->"
        Next

        combinedImg.PutText("Result:" & If(isMatch, "[OK]", "[NG]") & " " & cpStr, New Point(combinedImg.Width * 1 / 10, combinedImg.Height * 9 / 10), HersheyFonts.HersheyComplex, 0.5, New Scalar(255, 0, 255))
        combinedImg.PutText("input pattern:" & correctPattern, New Point(combinedImg.Width * 1 / 10, combinedImg.Height * 9.5 / 10), HersheyFonts.HersheyComplex, 0.5, New Scalar(255, 0, 255))
        Cv2.ImShow("chkLED", combinedImg)
        'waitBySW(20)
        Cv2.WaitKey(0) 'マニュアルモード起動時にはキー入力を待つ
        combinedImg.SaveImage(String.Format("{0}\LED検査結果画像(slave).jpg", PathSlnFile))
        combinedImg.Release()
        Cv2.DestroyWindow("chkLED")

        '入力パターンとの一致するか
        If isMatch Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub btnChkLED_Master_Click(sender As Object, e As EventArgs) Handles btnChkLED_Master.Click

        Dim colorPatten As String = "緑-消灯-赤-消灯"

        Dim colorPatternList As List(Of LED_COLOR) = makeColorPattern(colorPatten)

        Using cap As VideoCapture = New VideoCapture(0)
            If Not chkMasterUnitLED(cap, colorPatternList) Then
                MessageBox.Show("LED確認不合格(master)")
            End If
        End Using
    End Sub

    Private Sub btnChkLED_Slave_Click(sender As Object, e As EventArgs) Handles btnChkLED_Slave.Click

        Dim colorPatten As String = "緑-消灯-赤-消灯"
        Dim colorPatternList As List(Of LED_COLOR) = makeColorPattern(colorPatten)

        Using cap As VideoCapture = New VideoCapture(0)
            If Not chkSlaveUnitLED(cap, colorPatternList) Then
                MessageBox.Show("LED確認不合格(slave)")
            End If
        End Using
    End Sub
    ''' <summary>
    ''' 矩形領域のHSV値を指定回数だけテキストファイルに保存
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim img As New Mat()

        '座標指定待ち
        Dim mc As CvMouseCallback = New CvMouseCallback(AddressOf mouseCB)

        Dim window As Window = New Window("img")
        Cv2.SetMouseCallback("img", mc)

        'パラメータ設定
        '連続取得中には値を変えられないようにする
        Dim maskThV As Integer = 50 '明度閾値(V)
        Dim maskThS As Integer = 150 '彩度閾値(S)

        If IsNumeric(tbMaskTHV.Text) Then
            maskThV = Integer.Parse(tbMaskTHV.Text)
        Else
            MessageBox.Show("invalid maskTHV, use default value:" & maskThV.ToString())
        End If

        If IsNumeric(tbMaskTHS.Text) Then
            maskThS = Integer.Parse(tbMaskTHS.Text)
        Else
            MessageBox.Show("invalid maskTHS, use default value:" & maskThS.ToString())
        End If

        Using cap As VideoCapture = New VideoCapture(0)
            cap.Set(CaptureProperty.FrameWidth, 640)
            cap.Set(CaptureProperty.FrameHeight, 480)
            Do While Cv2.WaitKey(1) = -1

                cap.Read(img)
                Cv2.ImShow("img", img)
                Cv2.WaitKey(1)

                If isDraw Then

                    Dim dateStr As String = Date.Now.ToString("yyMMmm_hhmm")
                    Dim retryNum As Integer = 100
                    Dim rect As New Rect(mousePoints(0).X, mousePoints(0).Y, mousePoints(1).X - mousePoints(0).X, mousePoints(1).Y - mousePoints(0).Y)
                    Dim rectImg As Mat = New Mat()
                    img.CopyTo(rectImg)
                    rectImg.Rectangle(rect, New Scalar(255, 255, 255))
                    rectImg.SaveImage(String.Format("{0}{1}hsvImg_{2}.jpg", PathSlnFile, pathHSVVal, dateStr))
                    '履歴ファイル項目作成
                    Try
                        Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}LED_HSV_Val_{2}.txt", PathSlnFile, pathHSVVal, dateStr), False)
                            sw.WriteLine(String.Format("{0},{1},{2},{3},{4},{5}", "画像No", "明度閾値", "彩度閾値", "H", "S", "V"))
                        End Using
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try


                    For k As Integer = 0 To retryNum - 1

                        cap.Read(img)
                        Cv2.ImShow("img", img)
                        Cv2.WaitKey(10)
                        img.SaveImage(String.Format("{0}{1}img_{2}_{3}.jpg", PathSlnFile, pathHSVVal, dateStr, k.ToString("000")))

                        'HSV値の計算
                        Dim imgHSV As Mat = New Mat()
                        Cv2.CvtColor(img, imgHSV, ColorConversionCodes.BGR2HSV)
                        'HとVの値をそれぞれ仕様するため分割
                        Dim imgSepHSV() As Mat = New Mat(imgHSV.Channels - 1) {}
                        Cv2.Split(imgHSV, imgSepHSV)
                        '[輝度値(V)で2値化]
                        Dim mask As Mat = New Mat()
                        Cv2.Threshold(imgSepHSV(2), mask, maskThV, 255, ThresholdTypes.Binary)
                        'Cv2.Threshold(imgSepHSV(2), mask, 50, 255, ThresholdTypes.Binary)
                        Cv2.ImShow("mask", mask)
                        Cv2.WaitKey(10)
                        mask.SaveImage(String.Format("{0}{1}mask_{2}_{3}.jpg", PathSlnFile, pathHSVVal, dateStr, k.ToString("000")))

                        Dim aveH As Double = 0
                        Dim aveS As Double = 0
                        Dim aveV As Double = 0
                        Dim color As Vec3b = New Vec3b()
                        Dim counter As Integer = 0

                        '矩形領域のHSV値の平均値を算出
                        For j As Integer = rect.Top To rect.Bottom - 1

                            For i As Integer = rect.Left To rect.Right
                                If imgSepHSV(2).At(Of Byte)(j, i) >= maskThV AndAlso imgSepHSV(1).At(Of Byte)(j, i) >= maskThS Then
                                    color = imgHSV.At(Of Vec3b)(j, i)

                                    If 2 * color(0) < 300 Then
                                        aveH += color(0) * 2
                                    Else
                                        aveV += 360 - color(0) * 2
                                    End If
                                    aveS += color(1)
                                    aveV += color(2)
                                    counter += 1
                                End If
                            Next
                        Next
                        If counter <> 0 Then
                            aveH /= counter
                            aveS /= counter
                            aveV /= counter
                        End If
                        'MessageBox.Show(String.Format("H:{0}_S:{1}_V:{2}", aveH.ToString("0.0"), aveS.ToString("0.0"), aveV.ToString("0.0")))
                        Try
                            Using sw As IO.StreamWriter = New IO.StreamWriter(String.Format("{0}{1}LED_HSV_Val_{2}.txt", PathSlnFile, pathHSVVal, dateStr), True)
                                sw.WriteLine(String.Format("{0},{1},{2},{3},{4},{5}", k.ToString("000"), maskThV, maskThS, aveH.ToString("0.0"), aveS.ToString("0.0"), aveV.ToString("0.0")))
                            End Using
                        Catch ex As Exception
                            MessageBox.Show(ex.Message)
                        End Try


                    Next

                    isDraw = False
                    MessageBox.Show("終了")
                    Exit Do
                End If

                My.Application.DoEvents()
            Loop
        End Using
        Cv2.DestroyAllWindows()
    End Sub

    ''' <summary>
    ''' PIDとMACの値をリセットする
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnResetPID_MAC_Click(sender As Object, e As EventArgs) Handles btnResetPID_MAC.Click
        If Not isConnectNFC() Then
            MessageBox.Show("error:nfcConnection")
            Exit Sub
        End If

        Dim readData As Byte() = nfcReadLine(&H0)
        If readData Is Nothing Then
            MessageBox.Show("error:nfcReadLine")
            Exit Sub
        End If

        Try
            'PID
            readData(&H0) = 0
            readData(&H1) = 0
            readData(&H2) = 0
            readData(&H3) = 0
            'MAC
            readData(&H5) = 0
            readData(&H6) = 0
            readData(&H7) = 0
            readData(&H8) = 0
            readData(&H9) = 0
            readData(&HA) = 0

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        If Not nfcWriteLine(&H0, readData) Then
            MessageBox.Show("error:nfcWriteLine")
        End If

        MessageBox.Show("finished")

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

        Try
            GREEN_MAX = Integer.Parse(tbGreenHueMax.Text)
            GREEN_MIN = Integer.Parse(tbGreenHueMin.Text)
            RED_MAX = Integer.Parse(tbRedHueMax1.Text)
            RED_MIN = Integer.Parse(tbRedHueMin1.Text)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            GREEN_MAX = 140
            GREEN_MIN = 80
            RED_MAX = 10
            GREEN_MIN = 0

            tbGreenHueMax.Text = "140"
            tbGreenHueMin.Text = "80"
            tbRedHueMax1.Text = "10"
            tbRedHueMin1.Text = "0"

        End Try


    End Sub

    Private Sub btnSaveImg_Click(sender As Object, e As EventArgs) Handles btnSaveImg.Click
        Dim camNo As Integer
        If Not Integer.TryParse(TextBox1.Text, camNo) Then
            MessageBox.Show("数字を入力してください")
            Exit Sub
        End If

        Using cap As VideoCapture = New VideoCapture(CaptureDevice.Any, camNo)

            If Not cap.IsOpened() Then
                MessageBox.Show("カメラが開けません")
                Exit Sub
            End If

            Dim img As Mat = New Mat()

            Do
                Dim isReadSuccess As Boolean = False

                isReadSuccess = cap.Read(img)
                If img.Empty() Then
                    Continue Do
                End If

                Cv2.ImShow("img", img)
                If Cv2.WaitKey(10) = 27 Then
                    Exit Do
                End If

                If Cv2.WaitKey(20) = Asc("s") Then
                    MessageBox.Show("画像の保存を開始します。")
                    Dim saveFolderName As String = "img_save_" & Now.ToString("yyMMdd_hhmmss")
                    If Not IO.Directory.Exists(saveFolderName) Then
                        IO.Directory.CreateDirectory(saveFolderName)
                    End If

                    Dim count As Integer = 0
                    Do
                        If count > 200 Then
                            Exit Do
                        End If

                        isReadSuccess = cap.Read(img)
                        If img.Empty() Then
                            Continue Do
                        End If
                        Cv2.ImShow("saveImg", img)
                        img.SaveImage(String.Format(saveFolderName & "\" & count.ToString("000") & ".jpg"))

                        count += 1
                        My.Application.DoEvents()
                    Loop

                    Cv2.DestroyWindow("saveImg")


                End If

                My.Application.DoEvents()
            Loop

            Cv2.DestroyAllWindows()

        End Using
    End Sub

    '選択した画像を表示
    Private Sub btnImgSelect_Click(sender As Object, e As EventArgs) Handles btnImgSelect.Click
        Dim ofd As New OpenFileDialog
        ofd.InitialDirectory = "F:\7170-C-572\7170-C-572\bin\Debug"
        ofd.Title = "開くファイルを選択してください"
        ofd.RestoreDirectory = True
        ofd.CheckFileExists = True
        ofd.CheckFileExists = True

        If Not ofd.ShowDialog() = DialogResult.OK Then
            Exit Sub
        End If

        Dim img As Mat = New Mat()
        Dim hsvImg As Mat() = New Mat() {}
        Dim h_mask As Mat = New Mat()
        Dim s_mask As Mat = New Mat()
        Dim v_mask As Mat = New Mat()
        Dim sv_mask As Mat = New Mat()



        Try
            img = New Mat(ofd.FileName)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        img.CvtColor(ColorConversionCodes.BGR2HSV)
        hsvImg = New Mat(img.Channels - 1) {}
        Cv2.Split(img, hsvImg)



        'ウインドウ作成
        Cv2.NamedWindow("img", WindowMode.AutoSize)
        Cv2.NamedWindow("h_mask", WindowMode.AutoSize)
        Cv2.NamedWindow("v_mask", WindowMode.AutoSize)
        Cv2.NamedWindow("s_mask", WindowMode.AutoSize)
        Cv2.NamedWindow("sv_mask", WindowMode.AutoSize)

        Do
            If Cv2.WaitKey(30) = 27 Then
                Exit Do
            End If

            hsvImg(0).CopyTo(h_mask)
            hsvImg(1).CopyTo(s_mask)
            hsvImg(2).CopyTo(v_mask)

            Dim val As Byte = 0
            'For j As Integer = 0 To h_mask.Height - 1

            '    For i As Integer = 0 To h_mask.Width - 1
            '        val = h_mask.Get(Of Byte)(j, i)
            '        If val < 200 Then
            '            h_mask.Set(Of Byte)(j, i, Convert.ToByte(val))
            '        Else
            '            h_mask.Set(Of Byte)(j, i, Convert.ToByte(255 - val))
            '        End If
            '    Next
            'Next





            lblHVal.Text = tbH_TH.Value.ToString()
            lblSVal.Text = tbS_TH.Value.ToString()
            lblVVal.Text = tbV_TH.Value.ToString()

            Cv2.Threshold(h_mask, h_mask, tbH_TH.Value, 255, ThresholdTypes.Binary)
            Cv2.Threshold(s_mask, s_mask, tbS_TH.Value, 255, ThresholdTypes.Binary)
            Cv2.Threshold(v_mask, v_mask, tbV_TH.Value, 255, ThresholdTypes.Binary)

            's,v共通領域
            sv_mask = s_mask.Mul(v_mask)

            Cv2.ImShow("img", img)
            Cv2.ImShow("h_mask", h_mask)
            Cv2.ImShow("v_mask", v_mask)
            Cv2.ImShow("s_mask", s_mask)
            Cv2.ImShow("sv_mask", sv_mask)
            My.Application.DoEvents()
        Loop

        Cv2.DestroyAllWindows()



    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        '座標指定待ち
        Dim mc As CvMouseCallback = New CvMouseCallback(AddressOf mouseCB)

        Dim window As Window = New Window("img")
        Cv2.SetMouseCallback("img", mc)


        Dim img As New Mat()
        Dim ofd As New OpenFileDialog
        ofd.InitialDirectory = "F:\7170-C-572\7170-C-572\bin\Debug"
        ofd.Title = "開くファイルを選択してください"
        ofd.RestoreDirectory = True
        ofd.CheckFileExists = True
        ofd.CheckFileExists = True

        If Not ofd.ShowDialog() = DialogResult.OK Then
            Exit Sub
        End If


        Try
            img = New Mat(ofd.FileName)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try



        Cv2.ImShow("img", img)

        Do
            'ESCキーで終了
            If Cv2.WaitKey(30) = 27 Then
                Exit Do
            End If

            lblHVal.Text = tbH_TH.Value.ToString()
            lblSVal.Text = tbS_TH.Value.ToString()
            lblVVal.Text = tbV_TH.Value.ToString()

            '画像の表示と座標指定待ち
            If isDraw Then


                Dim rect As New Rect(mousePoints(0).X, mousePoints(0).Y, mousePoints(1).X - mousePoints(0).X, mousePoints(1).Y - mousePoints(0).Y)


                'HSV値の計算
                Dim imgHSV As Mat = New Mat()
                Cv2.CvtColor(img, imgHSV, ColorConversionCodes.BGR2HSV)
                'HとVの値をそれぞれ仕様するため分割
                Dim imgSepHSV() As Mat = New Mat(imgHSV.Channels - 1) {}
                Cv2.Split(imgHSV, imgSepHSV)
                '[輝度値(V)で2値化]
                Dim mask As Mat = New Mat()
                Cv2.Threshold(imgSepHSV(2), mask, tbV_TH.Value, 255, ThresholdTypes.Binary)
                '[マスク画像のフィルタリング]
                'mask.MedianBlur(5)
                'Cv2.Dilate(mask, mask, Mat.Ones(5, 5, MatType.CV_8UC1))
                'Cv2.ImShow("mask", mask)

                'ラベリング
                Dim cc As ConnectedComponents = Cv2.ConnectedComponentsEx(mask)
                Dim lblImg As Mat = New Mat()
                img.CopyTo(lblImg)
                For Each blb In cc.Blobs
                    lblImg.Rectangle(blb.Rect, New Scalar(255, 0, 255))
                Next
                'Cv2.ImShow("lblImg", lblImg)

                Dim aveH As Double = 0
                Dim aveS As Double = 0
                Dim aveV As Double = 0
                Dim color As Vec3b = New Vec3b()
                Dim counter As Integer = 0




                '矩形領域のHSV値の平均値を算出
                For j As Integer = rect.Top To rect.Bottom - 1

                    For i As Integer = rect.Left To rect.Right



                        If imgSepHSV(2).At(Of Byte)(j, i) >= tbV_TH.Value AndAlso imgSepHSV(1).At(Of Byte)(j, i) > tbS_TH.Value Then '彩度について追加
                            color = imgHSV.At(Of Vec3b)(j, i)

                            If 2 * color(0) < 300 Then
                                aveH += color(0) * 2

                            Else
                                aveV += 360 - color(0) * 2

                            End If
                            aveS += color(1)
                            aveV += color(2)
                            counter += 1
                        End If

                        'hist(Convert.ToInt32(imgSepHSV(0).At(Of Byte)(j, i))) += 1

                    Next
                Next
                If counter <> 0 Then
                    aveH /= counter
                    aveS /= counter
                    aveV /= counter
                End If

                '計算結果表示
                MessageBox.Show(String.Format("H:{0}_S:{1}_V:{2}_Area:{3}", aveH.ToString("0.0"), aveS.ToString("0.0"), aveV.ToString("0.0"), counter))

                isDraw = False
                Continue Do
            End If

            My.Application.DoEvents()
        Loop
    End Sub

    'MACアドレスのトンネルリード
    Private Sub btnSerialRead_Click(sender As Object, e As EventArgs) Handles btnSerialRead.Click
        Dim flg1 As Boolean = False
        Dim flg2 As Boolean = False
        Dim flg3 As Boolean = False
        Dim flg4 As Boolean = False

        Dim rxdata(5) As Byte

        'クラスのインスタンス化
        Dim extend As New extend_command

        Sna.NoWire.trans_mode = False

        flg1 = extend.nfc_connect(True)
        flg2 = extend.nfc_send_req(flg1)

        flg3 = extend.read_Fmac(flg2, rxdata)
        flg4 = Sna.NoWire.Nfc_disconnect()

        Dim strMAC As String = String.Empty
        For i As Integer = 0 To rxdata.Length - 1

            If i = rxdata.Length - 1 Then
                strMAC &= rxdata(i).ToString("X2")
            Else
                strMAC &= rxdata(i).ToString("X2") & "-"
            End If

        Next

        MessageBox.Show(strMAC)

    End Sub

    Private Sub btnMACRead_Click(sender As Object, e As EventArgs) Handles btnMACRead.Click
        Dim flg1 As Boolean = False
        Dim flg2 As Boolean = False
        Dim flg3 As Boolean = False
        Dim flg4 As Boolean = False

        Dim rxdata(3) As Byte

        'クラスのインスタンス化
        Dim extend As New extend_command

        'Dim extendTest As New extend_command
        'Dim flg00 As Boolean = extendTest.nfc_connect(True)
        'Dim flg01 As Boolean = extendTest.nfc_send_req(flg00)

        Sna.NoWire.trans_mode = False

        flg1 = extend.nfc_connect(True)
        flg2 = extend.nfc_send_req(flg1)

        flg3 = extend.read_serialno(flg2, rxdata)
        flg4 = Sna.NoWire.Nfc_disconnect()

        Dim strSerial As String = String.Empty

        For i As Integer = 0 To rxdata.Length - 1

            If i = rxdata.Length - 1 Then
                strSerial &= rxdata(i).ToString("X2")
            Else
                strSerial &= rxdata(i).ToString("X2") & "-"
            End If

        Next

        MessageBox.Show(strSerial)

    End Sub
End Class