﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Manual_WebBrowser
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.nudRB = New System.Windows.Forms.NumericUpDown()
        Me.nudLB = New System.Windows.Forms.NumericUpDown()
        Me.nudRT = New System.Windows.Forms.NumericUpDown()
        Me.nudLT = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.tbReadFileName = New System.Windows.Forms.TextBox()
        Me.tbWriteFileName = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        CType(Me.nudRB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudRT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(39, 49)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(103, 34)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "ブラウザ起動"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(63, 25)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(195, 19)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Text = "http://192.168.0.1/"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(134, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 12)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "アドレス"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(172, 50)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(95, 33)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "画面キャプチャ"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'nudRB
        '
        Me.nudRB.Location = New System.Drawing.Point(249, 145)
        Me.nudRB.Maximum = New Decimal(New Integer() {750, 0, 0, 0})
        Me.nudRB.Name = "nudRB"
        Me.nudRB.Size = New System.Drawing.Size(62, 19)
        Me.nudRB.TabIndex = 73
        Me.nudRB.Value = New Decimal(New Integer() {400, 0, 0, 0})
        '
        'nudLB
        '
        Me.nudLB.Location = New System.Drawing.Point(172, 145)
        Me.nudLB.Maximum = New Decimal(New Integer() {750, 0, 0, 0})
        Me.nudLB.Name = "nudLB"
        Me.nudLB.Size = New System.Drawing.Size(62, 19)
        Me.nudLB.TabIndex = 72
        '
        'nudRT
        '
        Me.nudRT.Location = New System.Drawing.Point(249, 106)
        Me.nudRT.Maximum = New Decimal(New Integer() {1300, 0, 0, 0})
        Me.nudRT.Name = "nudRT"
        Me.nudRT.Size = New System.Drawing.Size(62, 19)
        Me.nudRT.TabIndex = 71
        Me.nudRT.Value = New Decimal(New Integer() {650, 0, 0, 0})
        '
        'nudLT
        '
        Me.nudLT.Location = New System.Drawing.Point(172, 108)
        Me.nudLT.Maximum = New Decimal(New Integer() {1300, 0, 0, 0})
        Me.nudLT.Name = "nudLT"
        Me.nudLT.Size = New System.Drawing.Size(62, 19)
        Me.nudLT.TabIndex = 70
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(248, 130)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 12)
        Me.Label4.TabIndex = 69
        Me.Label4.Text = "rightbottom"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(170, 130)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 12)
        Me.Label3.TabIndex = 68
        Me.Label3.Text = "leftbottom"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(247, 94)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 12)
        Me.Label2.TabIndex = 67
        Me.Label2.Text = "righttop"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(170, 93)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(38, 12)
        Me.Label5.TabIndex = 66
        Me.Label5.Text = "lefttop"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(39, 106)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(103, 47)
        Me.Button9.TabIndex = 65
        Me.Button9.Text = "WEB比較画像" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "生成"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'tbReadFileName
        '
        Me.tbReadFileName.Location = New System.Drawing.Point(42, 188)
        Me.tbReadFileName.Name = "tbReadFileName"
        Me.tbReadFileName.Size = New System.Drawing.Size(119, 19)
        Me.tbReadFileName.TabIndex = 74
        Me.tbReadFileName.Text = "EX600-WEN1.bmp"
        '
        'tbWriteFileName
        '
        Me.tbWriteFileName.Location = New System.Drawing.Point(187, 188)
        Me.tbWriteFileName.Name = "tbWriteFileName"
        Me.tbWriteFileName.Size = New System.Drawing.Size(114, 19)
        Me.tbWriteFileName.TabIndex = 75
        Me.tbWriteFileName.Text = "result.bmp"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(49, 173)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(74, 12)
        Me.Label6.TabIndex = 76
        Me.Label6.Text = "読込みファイル"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(190, 173)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(74, 12)
        Me.Label7.TabIndex = 77
        Me.Label7.Text = "書込みファイル"
        '
        'Manual_WebBrowser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(324, 219)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tbWriteFileName)
        Me.Controls.Add(Me.tbReadFileName)
        Me.Controls.Add(Me.nudRB)
        Me.Controls.Add(Me.nudLB)
        Me.Controls.Add(Me.nudRT)
        Me.Controls.Add(Me.nudLT)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Manual_WebBrowser"
        Me.Text = "Manual_WebBrowser"
        CType(Me.nudRB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudRT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents nudRB As NumericUpDown
    Friend WithEvents nudLB As NumericUpDown
    Friend WithEvents nudRT As NumericUpDown
    Friend WithEvents nudLT As NumericUpDown
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Button9 As Button
    Friend WithEvents tbReadFileName As TextBox
    Friend WithEvents tbWriteFileName As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
End Class
