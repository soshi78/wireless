﻿Imports OpenCvSharp

Public Class Manual_WebBrowser
    'ソリューションファイルパス
    Private PathSlnFile As String = System.IO.Path.GetDirectoryName(
                                    System.IO.Path.GetDirectoryName(
                                    System.IO.Path.GetDirectoryName(Application.StartupPath)))


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            frmWebBrowser.Show()
            frmWebBrowser.Activate()
            Wait(1000)
            Dim bmpImg As New Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height)
            Dim g As Graphics = Graphics.FromImage(bmpImg)
            g.CopyFromScreen(New Drawing.Point(0, 0), New Drawing.Point(0, 0), bmpImg.Size)
            g.Dispose()

            bmpImg.Save(String.Format("{0}\WEBブラウザキャプチャ画面{1}.bmp", PathSlnFile, Now.ToString("yyMMdd_hhmmss")))
            MessageBox.Show("画像保存完了" & ControlChars.CrLf &
                            "ファイルパス:" & String.Format("{0}\WEBブラウザキャプチャ画面{1}.bmp", PathSlnFile, Now.ToString("yyMMdd_hhmmss")))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try



    End Sub

    Private Sub Manual_WebBrowser_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        frmMain.Enabled = True
        USBIO.EX600_Pow(0)
        Wait(500)
        USBIO.Hub_Pow(0)
        Wait(500)
    End Sub

    Private Sub Manual_WebBrowser_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        frmMain.Enabled = False
        USBIO.EX600_Pow(1)
        Wait(1000)
        USBIO.Hub_Pow(1)
        Wait(1000)
        Me.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            'URLをIEで開く
            frmWebBrowser.WindowState = FormWindowState.Maximized

            frmWebBrowser.WebBrowser1.Navigate(New Uri(TextBox1.Text))
            frmWebBrowser.Focus()
            frmWebBrowser.Visible = True
            frmWebBrowser.Show()
            '表示まで待機
            Do
                If frmWebBrowser.WebBrowser1.ReadyState = WebBrowserReadyState.Complete AndAlso
                        frmWebBrowser.WebBrowser1.IsBusy = False Then
                    Wait(3000) 'toppage->wireless systemへと変わるのに時間がかかるので
                    Exit Do
                End If
                My.Application.DoEvents()
            Loop
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' WEB確認比較用マスク画像生成
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click

        Try
            Dim img As New Mat(tbReadFileName.Text)
            Dim resultImg As New Mat(img.Height, img.Width, MatType.CV_8UC3)
            Dim mask As Mat = New Mat(img.Height, img.Width, MatType.CV_8UC1)

            'スピンボタンで指定した範囲のマスクを生成
            For i As Decimal = nudLT.Value To nudRT.Value
                For j As Decimal = nudLB.Value To nudRB.Value
                    mask.Set(Of Byte)(Convert.ToInt32(j), Convert.ToInt32(i), 255)
                Next
            Next

            img.CopyTo(resultImg, mask)
            Cv2.ImShow("result", resultImg)
            Cv2.WaitKey(0)
            resultImg.SaveImage(tbWriteFileName.Text)
            Cv2.DestroyAllWindows()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class