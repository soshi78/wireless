﻿Module Module_timer


    Public Declare Function timeGetTime Lib "winmm.dll" () As Long
    Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
    '-----------------------------------------------------------------------------
    '【機能】msecウェイト
    '【引数】waittime:ウェイト時間
    '【戻り値】
    '-----------------------------------------------------------------------------
    Public Sub Wait(ByVal waitTime As Long)
        Dim startTime As Long

        startTime = timeGetTime()
        Do While timeGetTime() - startTime < waitTime
            My.Application.DoEvents()
        Loop
    End Sub
End Module
