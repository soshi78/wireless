﻿
'--------------------
'   Manual操作
'--------------------
Option Explicit On
Option Strict On
Option Compare Binary

Public Class PSoCManu

    Dim bytRet As Boolean = False
    Dim doloopFlg As Boolean = True


    Private Sub PSoCManu_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        frmMain.Enabled = False 'add_nishimura

        'PSoC設定
        Try
            With frmMain.PSoC
                If .IsOpen = False Then
                    .PortName = "COM7"
                    .BaudRate = 38400
                    .Parity = IO.Ports.Parity.None
                    .DataBits = 8
                    .StopBits = IO.Ports.StopBits.One
                    .ReadBufferSize = 100
                    .WriteBufferSize = 100
                    '.NewLine = vbCrLf
                    .ReadTimeout = 200
                    .WriteTimeout = 200
                    .Open()
                    .DiscardInBuffer()
                    .DiscardOutBuffer()
                End If
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message, "確認事項",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)

            Call Unload()
        Finally
        End Try

        Me.Show()
        Call Main()
    End Sub

    Private Sub Main()

        Dim Text_no As Byte = 0

        '画面初期化
        For Text_no = 1 To 15 Step 1
            With TextBox(Text_no)
                .Text = vbNullString
            End With
            My.Application.DoEvents()
        Next Text_no

        CheckBox1.CheckState = CheckState.Unchecked
        CheckBox2.CheckState = CheckState.Unchecked
        CheckBox3.CheckState = CheckState.Unchecked
        CheckBox4.CheckState = CheckState.Unchecked
        CheckBox5.CheckState = CheckState.Unchecked
        CheckBox6.CheckState = CheckState.Unchecked
        CheckBox7.CheckState = CheckState.Unchecked
        CheckBox8.CheckState = CheckState.Unchecked
        CheckBox9.CheckState = CheckState.Unchecked
        CheckBox10.CheckState = CheckState.Unchecked
        CheckBox11.CheckState = CheckState.Unchecked
        CheckBox12.CheckState = CheckState.Unchecked
        CheckBox13.CheckState = CheckState.Unchecked
        CheckBox14.CheckState = CheckState.Unchecked
        CheckBox15.CheckState = CheckState.Unchecked

        bytRet = Master_Pow(0)
        Call RetChk()

        bytRet = Hub_Pow(0)
        Call RetChk()

        bytRet = Reserve0_Out(0)
        Call RetChk()

        bytRet = AC_Relay(0)
        Call RetChk()

        bytRet = EX260_Pow(0)
        Call RetChk()

        bytRet = EX600_Pow(0)
        Call RetChk()

        bytRet = Slave_Pow(0)
        Call RetChk()

        bytRet = Reserve1_Out(0)
        Call RetChk()

        bytRet = Buzzer(0)
        Call RetChk()

        bytRet = SI_change170V(0)
        Call RetChk()

        bytRet = SI_change240V(0)
        Call RetChk()

        bytRet = SI_change223V(0)
        Call RetChk()

        bytRet = Reserve2_Out(0)
        Call RetChk()

        bytRet = Reserve3_Out(0)
        Call RetChk()

        bytRet = Reserve4_Out(0)
        Call RetChk()

        'Call Doloop()'co_nishimura
    End Sub

    'パスワードボックスの消去のため
    Private Sub PSoCManu_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Call Doloop()
    End Sub

    Private Sub Doloop()

        '終了処理のため
        If Not doloopFlg Then
            Exit Sub
        End If

        Dim intCnt As Byte = 0
        Dim BytinData As Byte = 0
        Dim Bytstr As String = Nothing

        '--------------------
        '       入力
        '--------------------
        Do

            For intCnt = 1 To 15 Step 1


                Select Case (intCnt)
                    Case 1
                        'P0[0],P0[1],P0[2],P0[3](ピン上部判別、4bitまとめて判別)
                        bytRet = Pin_Upper(BytinData)
                    Case 2
                        'P0[4],P0[5],P0[6],P0[7](PLC判別、4bitまとめて判別)
                        bytRet = PLC_Byte(BytinData)
                    Case 3
                        'P1[0]:Start_SW(0bit目のみ)
                        bytRet = Start_SW(BytinData)
                    Case 4
                        'P1[1]:Break_SW(1bit目のみ)
                        bytRet = Break_SW(BytinData)
                    Case 5
                        'P1[2]:Work_Sens(2bit目のみ)
                        bytRet = Work_Sens(BytinData)
                    Case 6
                        'P1[3]:EX600過電流検知(3bit目のみ)
                        bytRet = EX600_OverCurrent(BytinData)
                    Case 7
                        'P1[4]:予約(4bit目のみ)
                        bytRet = Reserve0_In(BytinData)
                    Case 8
                        'P1[5],P1[6],P1[7]:ピン変換基板(3bitまとめて判別)
                        bytRet = Pin_Config(BytinData)
                    Case 9
                        'P3[0],P3[1],P3[2],P3[3]:7SEG入力(4bitまとめて判別)
                        bytRet = Seg7(BytinData)
                    Case 10
                        'P3[4]:COM検知1(4bit目のみ)
                        bytRet = Com1(BytinData)
                    Case 11
                        'P3[5]:COM検知2(5bit目のみ)
                        bytRet = Com2(BytinData)
                    Case 12
                        'P3[6]:予備入力(PLC出力ユニットから出力)(6bit目のみ)
                        bytRet = Reserve1_In(BytinData)
                    Case 13
                        'P3[7]:予備入力(PLC出力ユニットから出力)(7bit目のみ)
                        bytRet = Reserve2_In(BytinData)
                    Case 14
                        'P5[2]:予備入力(2bit目のみ)
                        bytRet = Reserve3_In(BytinData)
                    Case 15
                        'P5[3]:予備入力(3bit目のみ)
                        bytRet = Reserve4_In(BytinData)
                End Select

                '終了時の割り込み処理によるエラー対策
                If Not doloopFlg Then
                    Exit Do
                End If
                If bytRet = True Then
                    TextBox(intCnt).Text = Convert.ToString(BytinData, 16)
                Else
                    TextBox(intCnt).Text = "PSoC Error"
                    Call RetChk()
                End If
                My.Application.DoEvents()
            Next intCnt

            '--------------------
            '       出力
            '--------------------
            'P2[0]:治具用マスタユニット電源(0bit目のみ)
            If CheckBox1.CheckState = CheckState.Checked Then
                bytRet = Master_Pow(1)  'ON
                CheckBox1.BackColor = Color.Lime
            Else
                bytRet = Master_Pow(0)  'OFF
                CheckBox1.BackColor = Color.Gray
            End If
            Call RetChk()

            'P2[1]:HUB電源(1bit目のみ)
            If CheckBox2.CheckState = CheckState.Checked Then
                bytRet = Hub_Pow(1)  'ON
                CheckBox2.BackColor = Color.Lime
            Else
                bytRet = Hub_Pow(0)  'OFF
                CheckBox2.BackColor = Color.Gray
            End If
            Call RetChk()

            'P2[2]:予備出力(PLC入力ユニットへ出力)(2bit目のみ)
            If CheckBox3.CheckState = CheckState.Checked Then
                bytRet = Reserve0_Out(1) 'ON
                CheckBox3.BackColor = Color.Lime
            Else
                bytRet = Reserve0_Out(0) 'OFF
                CheckBox3.BackColor = Color.Gray
            End If
            Call RetChk()

            'P2[3]:AC切替え(3bit目のみ)
            If CheckBox4.CheckState = CheckState.Checked Then
                bytRet = AC_Relay(1) 'ON
                CheckBox4.BackColor = Color.Lime
            Else
                bytRet = AC_Relay(0) 'OFF
                CheckBox4.BackColor = Color.Gray
            End If
            Call RetChk()

            'P2[4]:EX260電源(4bit目のみ)
            If CheckBox5.CheckState = CheckState.Checked Then
                bytRet = EX260_Pow(1) 'ON
                CheckBox5.BackColor = Color.Lime
            Else
                bytRet = EX260_Pow(0) 'OFF
                CheckBox5.BackColor = Color.Gray
            End If
            Call RetChk()

            'P2[5]:EX600電源(5bit目のみ)
            If CheckBox6.CheckState = CheckState.Checked Then
                bytRet = EX600_Pow(1) 'ON
                CheckBox6.BackColor = Color.Lime
            Else
                bytRet = EX600_Pow(0) 'OFF
                CheckBox6.BackColor = Color.Gray
            End If
            Call RetChk()

            'P2[6]:治具用スレーブユニット電源(6bit目のみ)
            If CheckBox7.CheckState = CheckState.Checked Then
                bytRet = Slave_Pow(1) 'ON
                CheckBox7.BackColor = Color.Lime
            Else
                bytRet = Slave_Pow(0) 'OFF
                CheckBox7.BackColor = Color.Gray
            End If
            Call RetChk()

            'P2[7]:予備出力(PLC入力ユニットへ出力)(7bit目のみ)
            If CheckBox8.CheckState = CheckState.Checked Then
                bytRet = Reserve1_Out(1) 'ON
                CheckBox8.BackColor = Color.Lime
            Else
                bytRet = Reserve1_Out(0) 'OFF
                CheckBox8.BackColor = Color.Gray
            End If
            Call RetChk()

            'P4[0]:ブザー出力(0bit目のみ)
            If CheckBox9.CheckState = CheckState.Checked Then
                bytRet = Buzzer(1) 'ON
                CheckBox9.BackColor = Color.Lime
            Else
                bytRet = Buzzer(0) 'OFF
                CheckBox9.BackColor = Color.Gray
            End If
            Call RetChk()



            'P4[1],P4[2],P4[3],P4[4]:ワーク電圧17.0V出力(4bitまとめて判別)
            If CheckBox10.CheckState = CheckState.Checked Then
                bytRet = SI_change170V(1) 'ON
                CheckBox10.BackColor = Color.Lime
            Else
                'bytRet = SI_change170V(0) 'OFF
                CheckBox10.BackColor = Color.Gray
            End If
            Call RetChk()

            'P4[1],P4[2],P4[3],P4[4]:ワーク電圧24.0V出力(4bitまとめて判別)
            If CheckBox11.CheckState = CheckState.Checked Then
                bytRet = SI_change240V(1) 'ON
                CheckBox11.BackColor = Color.Lime
            Else
                'bytRet = SI_change240V(0) 'off
                CheckBox11.BackColor = Color.Gray
            End If
            Call RetChk()

            'P4[1],P4[2],P4[3],P4[4]:ワーク電圧22.3V出力(4bitまとめて判別)
            If CheckBox12.CheckState = CheckState.Checked Then
                bytRet = SI_change223V(1) 'ON
                CheckBox12.BackColor = Color.Lime
            Else
                'bytRet = SI_change223V(0) 'OFF
                CheckBox12.BackColor = Color.Gray
            End If
            Call RetChk()

            '電圧切替にチェックが入っていなければ初期値(24V)にする
            If Not (CheckBox10.CheckState = CheckState.Checked OrElse CheckBox11.CheckState = CheckState.Checked OrElse CheckBox12.CheckState = CheckState.Checked) Then
                bytRet = SI_change170V(0) 'OFF
                CheckBox10.BackColor = Color.Gray
                bytRet = SI_change240V(1) 'OFF
                CheckBox11.BackColor = Color.Gray
                bytRet = SI_change223V(0) 'OFF
                CheckBox12.BackColor = Color.Gray
            End If
            Call RetChk()

            'P4[5]:予備出力(5bit目のみ)
            If CheckBox13.CheckState = CheckState.Checked Then
                bytRet = Reserve2_Out(1) 'ON
                CheckBox13.BackColor = Color.Lime
            Else
                bytRet = Reserve2_Out(0) 'OFF
                CheckBox13.BackColor = Color.Gray
            End If
            Call RetChk()

            'P4[6]:予備出力(6bit目のみ)
            If CheckBox14.CheckState = CheckState.Checked Then
                bytRet = Reserve3_Out(1) 'ON
                CheckBox14.BackColor = Color.Lime
            Else
                bytRet = Reserve3_Out(0) 'OFF
                CheckBox14.BackColor = Color.Gray
            End If
            Call RetChk()

            'P4[7]:予備出力(7bit目のみ)
            If CheckBox15.CheckState = CheckState.Checked Then
                bytRet = Reserve4_Out(1) 'ON
                CheckBox15.BackColor = Color.Lime
            Else
                bytRet = Reserve4_Out(0) 'OFF
                CheckBox15.BackColor = Color.Gray
            End If
            Call RetChk()
            My.Application.DoEvents()
        Loop
    End Sub

    Private Sub Init()

        Call Master_Pow(0)
        Call Hub_Pow(0)
        Call Reserve0_Out(0)
        Call AC_Relay(0)
        Call EX260_Pow(0)
        Call EX600_Pow(0)
        Call Slave_Pow(0)
        Call Reserve1_Out(0)
        Call Buzzer(0)
        Call SI_change170V(0)
        Call SI_change240V(0)
        Call SI_change223V(0)
        Call Reserve2_Out(0)
        Call Reserve3_Out(0)
        Call Reserve4_Out(0)

        Do While True
            bytRet = Master_Pow(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = Hub_Pow(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = Reserve0_Out(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = AC_Relay(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = EX260_Pow(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = EX600_Pow(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = Slave_Pow(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = Reserve1_Out(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = Buzzer(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = SI_change170V(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = SI_change240V(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = SI_change223V(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = Reserve2_Out(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = Reserve3_Out(0)
            If bytRet = False Then
                Exit Do
            End If

            bytRet = Reserve4_Out(0)
            If bytRet = False Then
                Exit Do
            End If

            Exit Do
            My.Application.DoEvents()
        Loop

    End Sub

    Private Sub RetChk()
        If bytRet = False Then
            MessageBox.Show("PSoC異常" & vbCrLf & "ソフト終了します", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Call Unload()
        End If
    End Sub

    'Doloop()で終了時にエラーが起きるのを防止
    Private Sub PSoCManu_FormClsing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        doloopFlg = False
        Wait(1000)
        Call Init()
    End Sub

    Private Sub PSoCManu_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        frmMain.Enabled = True
        Unload()
    End Sub



    Private Sub Unload()
        'Call Init()
        'frmMain.PSoC.Dispose()
        'frmMain.PSoC.Close()
        'frmMain.Close() 'add:nishimura

    End Sub

    '-------------------------------------------------------------
    '引数indexに番号を受取って、その番号が付いたTextBoxを返す
    '-------------------------------------------------------------
    Private Function TextBox(ByVal index As Byte) As TextBox
        Return DirectCast(Controls("TextBox" & index.ToString), TextBox)
    End Function
    '-------------------------------------------------------------
    '引数indexに番号を受取って、その番号が付いたCheckBoxを返す
    '-------------------------------------------------------------
    Private Function CheckBox(ByVal index As Byte) As CheckBox
        Return DirectCast(Me.Controls("CheckBox" & index.ToString), CheckBox)
    End Function


End Class
