﻿Public Class passwordbox
    'パスワード入力ボックス

    'ソリューションファイルパス
    Private PathSlnFile As String = System.IO.Path.GetDirectoryName(
                                    System.IO.Path.GetDirectoryName(
                                    System.IO.Path.GetDirectoryName(Application.StartupPath)))
    Private Const pathPassword As String = "\TestData\password\"
    Private Const defaultPassword As String = "dengi"
    'Encoding
    Private enc_sj As System.Text.Encoding = System.Text.Encoding.GetEncoding("shift_jis")
    Dim password As String = String.Empty
    Private form As Form

    '設定
    Private Sub passwordbox_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "password"
        TextBox1.PasswordChar = "*"c
        TextBox1.MaxLength = 10 '入力文字数：10文字
        TextBox1.ImeMode = ImeMode.Alpha '入力モード：半角英数字
        TextBox1.Text = String.Empty
        TextBox1.Multiline = False
        lblPassMessage.Text = "パスワードを入力してください"
        Me.Focus()
        Me.TopMost = True
        Me.MinimizeBox = False
        frmMain.Enabled = False

        Try
            Using sr As New IO.StreamReader(String.Format("{0}{1}password.txt", PathSlnFile, pathPassword), enc_sj)
                password = sr.ReadLine()
            End Using
        Catch ex As Exception
            MessageBox.Show("パスワード設定ファイルが読み込めません。" & ControlChars.CrLf &
                            "デフォルトパスワードを設定します。", "確認事項", MessageBoxButtons.OK, MessageBoxIcon.Error)
            password = defaultPassword
        End Try
    End Sub

    'パスワード入力判定(キーが押されて上がった時に判定)
    Public Sub TextBox1_KeyUp(sender As Object, e As KeyEventArgs) Handles TextBox1.KeyUp
        If e.KeyData = Keys.Enter Then
            If TextBox1.Text = password Then
                'パスワードOK
                changeMode()
                Me.Close()
            Else
                'パスワードNG
                lblPassMessage.Text = "パスワードが異なります"
            End If
        End If

    End Sub


    ''' <summary>
    ''' パスワード入力後変更するモードを設定
    ''' </summary>
    Public Sub setMode(ByVal mode As Form)
        If mode Is Nothing Then
            MessageBox.Show("form is nothing")
            Exit Sub
        End If
        form = mode
    End Sub


    ''' <summary>
    ''' パスワード入力成功後モード変更(所定のフォームを開く)
    ''' </summary>
    Private Sub changeMode()
        If form Is Nothing Then
            MessageBox.Show("form is nothing")
            Exit Sub
        End If
        form.Show() '変更,Closedで呼び出す
        'Me.Close()
    End Sub


    '終了ボタン
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub passwordbox_Closed(sender As Object, e As EventArgs) Handles Me.Closed

        If form Is Nothing Then
            frmMain.Enabled = True
            Exit Sub
        End If

        'モードの変更を行っていない場合はメインフォームを使用できるように戻す
        If Not form.Visible Then
            frmMain.Enabled = True
        End If
        'formの初期化
        form = Nothing
    End Sub
End Class