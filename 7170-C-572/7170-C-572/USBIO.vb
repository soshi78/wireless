﻿Option Explicit On
Option Strict On
Option Compare Binary

Module USBIO

    Private Const RS232cioTIMEOUT As Integer = 200  'frmMain.RS232cio受信タイムオーバー時間(msec)
    Private Const RS232cioRETRYCOUNT As Integer = 5 'frmMain.RS232cio送受信通信エラーのリトライ回数
    Private Port2OutData As Byte = 0                'PSoC P[2]出力用
    Private Port4OutData As Byte = 0                'PSoC P[4]出力用

    '---------------------------------------------------------------------------------
    '【機能】P0[0],P0[1],P0[2],P0[3]:Byte型(ピン上部判別、4bitまとめて判別)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True：正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Pin_Upper(ByRef data As Byte) As Boolean

        '初期設定
        Pin_Upper = False

        Pin_Upper = pinp(&H0, data)
        If Pin_Upper = False Then
            Exit Function
        End If
        '下位4bitのみマスク
        data = data And CByte(&HF)'下位4ビットを取り出す
        Pin_Upper = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P0[4],P0[5],P0[6],P0[7]:Byte型(PLC判別、4bitまとめて判別)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function PLC_Byte(ByRef data As Byte) As Boolean

        '初期設定
        PLC_Byte = False
        '文字格納用
        Dim StrTmp As String = vbNullString

        PLC_Byte = pinp(&H0, data)
        If PLC_Byte = False Then
            Exit Function
        End If
        '上位4bitのみマスク(数字→文字変換)
        StrTmp = Strings.Left(UCase(Convert.ToString(data, 16)), 1)
        '文字→数値変換
        data = CByte(Convert.ToInt32(StrTmp, 16))

        PLC_Byte = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P1[0]:Start_SW(0bit目のみ)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Start_SW(ByRef data As Byte) As Boolean

        '初期設定
        Start_SW = False

        Start_SW = pinp(&H1, data)
        If Start_SW = False Then
            Exit Function
        End If
        'P1[0]のみマスク
        data = (data >> 0) And CByte(2 ^ 0)'右シフト0ビット(=x/2^0)
        Start_SW = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P1[1]:Break_SW(1bit目のみ)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Break_SW(ByRef data As Byte) As Boolean

        '初期設定
        Break_SW = False

        Break_SW = pinp(&H1, data)
        If Break_SW = False Then
            Exit Function
        End If
        'P1[1]のみマスク
        data = (data >> 1) And CByte(2 ^ 0)
        Break_SW = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P1[2]:Work_Sens(2bit目のみ)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Work_Sens(ByRef data As Byte) As Boolean

        '初期設定
        Work_Sens = False

        Work_Sens = pinp(&H1, data)
        If Work_Sens = False Then
            Exit Function
        End If
        'P1[2]のみマスク
        data = (data >> 2) And CByte(2 ^ 0)
        Work_Sens = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P1[3]:EX600過電流検知(3bit目のみ)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function EX600_OverCurrent(ByRef data As Byte) As Boolean

        '初期設定
        EX600_OverCurrent = False

        EX600_OverCurrent = pinp(&H1, data)
        If EX600_OverCurrent = False Then
            Exit Function
        End If
        'P1[3]のみマスク
        data = (data >> 3) And CByte(2 ^ 0)
        EX600_OverCurrent = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P1[4]:予約(4bit目のみ)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Reserve0_In(ByRef data As Byte) As Boolean

        '初期設定
        Reserve0_In = False

        Reserve0_In = pinp(&H1, data)
        If Reserve0_In = False Then
            Exit Function
        End If
        'P1[4]のみマスク
        data = (data >> 4) And CByte(2 ^ 0)
        Reserve0_In = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P1[5],P1[6],P1[7]:ピン変換基板(3bitまとめて判別)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Pin_Config(ByRef data As Byte) As Boolean

        '初期設定
        Pin_Config = False

        Pin_Config = pinp(&H1, data)
        If Pin_Config = False Then
            Exit Function
        End If
        'P1[5],P1[6],P1[7]のみマスク
        data = (data >> 5) And CByte(&H7)
        Pin_Config = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P2[0]:治具用マスタユニット電源(0bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Master_Pow(ByVal data As Byte) As Boolean

        '初期設定
        Master_Pow = False

        If data = &H1 Then
            Port2OutData = Port2OutData Or CByte(2 ^ 0)
        Else
            Port2OutData = Port2OutData And Not CByte(2 ^ 0)'0bit目だけOff/Nishi
        End If
        'Port出力
        Master_Pow = poutp(&H2, Port2OutData)'マスタユニット電源の状態を変更する/Nishi
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P2[1]:HUB電源(1bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Hub_Pow(ByVal data As Byte) As Boolean

        Dim Ret As Boolean = False
        '初期設定
        Hub_Pow = False

        If data = &H1 Then
            Port2OutData = Port2OutData Or CByte(2 ^ 1)
        Else
            Port2OutData = Port2OutData And Not CByte(2 ^ 1)
        End If
        'Port出力
        Hub_Pow = poutp(&H2, Port2OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P2[2]:予備出力(PLC入力ユニットへ出力)(2bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Reserve0_Out(ByVal data As Byte) As Boolean

        '初期設定
        Reserve0_Out = False

        If data = &H1 Then
            Port2OutData = Port2OutData Or CByte(2 ^ 2)
        Else
            Port2OutData = Port2OutData And Not CByte(2 ^ 2)
        End If
        'Port出力
        Reserve0_Out = poutp(&H2, Port2OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P2[3]:AC切替え(3bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function AC_Relay(ByVal data As Byte) As Boolean

        '初期設定
        AC_Relay = False

        If data = &H1 Then
            Port2OutData = Port2OutData Or CByte(2 ^ 3)
        Else
            Port2OutData = Port2OutData And Not CByte(2 ^ 3)
        End If
        'Port出力
        AC_Relay = poutp(&H2, Port2OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P2[4]:EX260電源(4bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function EX260_Pow(ByVal data As Byte) As Boolean

        '初期設定
        EX260_Pow = False

        If data = &H1 Then
            Port2OutData = Port2OutData Or CByte(2 ^ 4)
        Else
            Port2OutData = Port2OutData And Not CByte(2 ^ 4)
        End If
        'Port出力
        EX260_Pow = poutp(&H2, Port2OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P2[5]:EX600電源(5bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function EX600_Pow(ByVal data As Byte) As Boolean

        '初期設定
        EX600_Pow = False

        If data = &H1 Then
            Port2OutData = Port2OutData Or CByte(2 ^ 5)
        Else
            Port2OutData = Port2OutData And Not CByte(2 ^ 5)
        End If
        'Port出力
        EX600_Pow = poutp(&H2, Port2OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P2[6]:治具用スレーブユニット電源(6bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Slave_Pow(ByVal data As Byte) As Boolean

        '初期設定
        Slave_Pow = False

        If data = &H1 Then
            Port2OutData = Port2OutData Or CByte(2 ^ 6)
        Else
            Port2OutData = Port2OutData And Not CByte(2 ^ 6)
        End If
        'Port出力
        Slave_Pow = poutp(&H2, Port2OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P2[7]:予備出力(PLC入力ユニットへ出力)(7bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Reserve1_Out(ByVal data As Byte) As Boolean

        '初期設定
        Reserve1_Out = False

        If data = &H1 Then
            Port2OutData = Port2OutData Or CByte(2 ^ 7)
        Else
            Port2OutData = Port2OutData And Not CByte(2 ^ 7)
        End If
        'Port出力
        Reserve1_Out = poutp(&H2, Port2OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P3[0],P3[1],P3[2],P3[3]:7SEG入力(4bitまとめて判別)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Seg7(ByRef data As Byte) As Boolean

        '初期設定
        Seg7 = False

        Seg7 = pinp(&H3, data)
        If Seg7 = False Then
            Exit Function
        End If
        'P3[0],P3[1],P3[2],P3[3]のみマスク
        'Active Lなのでビット反転
        data = (Not data) And CByte(&HF)
        Seg7 = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P3[4]:COM検知1(4bit目のみ)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Com1(ByRef data As Byte) As Boolean

        '初期設定
        Com1 = False

        Com1 = pinp(&H3, data)
        If Com1 = False Then
            Exit Function
        End If
        'P3[4]のみマスク
        data = (data >> 4) And CByte(2 ^ 0)
        Com1 = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P3[5]:COM検知2(5bit目のみ)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Com2(ByRef data As Byte) As Boolean

        '初期設定
        Com2 = False

        Com2 = pinp(&H3, data)
        If Com2 = False Then
            Exit Function
        End If
        'P3[5]のみマスク
        data = (data >> 5) And CByte(2 ^ 0)
        Com2 = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P3[6]:予備入力(PLC出力ユニットから出力)(6bit目のみ)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Reserve1_In(ByRef data As Byte) As Boolean

        '初期設定
        Reserve1_In = False

        Reserve1_In = pinp(&H3, data)
        If Reserve1_In = False Then
            Exit Function
        End If
        'P3[6]のみマスク
        data = (data >> 6) And CByte(2 ^ 0)
        Reserve1_In = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P3[7]:予備入力(PLC出力ユニットから出力)(7bit目のみ)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Reserve2_In(ByRef data As Byte) As Boolean

        '初期設定
        Reserve2_In = False

        Reserve2_In = pinp(&H3, data)
        If Reserve2_In = False Then
            Exit Function
        End If
        'P3[7]のみマスク
        data = (data >> 7) And CByte(2 ^ 0)
        Reserve2_In = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P4[0]:ブザー出力(0bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Buzzer(ByVal data As Byte) As Boolean

        '初期設定
        Buzzer = False

        If data = &H1 Then
            Port4OutData = Port4OutData Or CByte(2 ^ 0)
        Else
            Port4OutData = Port4OutData And Not CByte(2 ^ 0)
        End If
        'Port出力
        Buzzer = poutp(&H4, Port4OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P4[1],P4[2],P4[3],P4[4]:ワーク電圧17.0V出力(4bitまとめて判別)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function SI_change170V(ByVal data As Byte) As Boolean

        '初期設定
        SI_change170V = False

        If data = &H1 Then
            Port4OutData = Port4OutData Or CByte(&H6)
            'Port4OutData = Port4OutData And Not CByte(&H18)
            Port4OutData = Port4OutData And CByte(&HE7)
        Else
            Port4OutData = Port4OutData Or CByte(2 ^ 2)
            'Port4OutData = Port4OutData And Not CByte(&H1A)
            Port4OutData = Port4OutData And CByte(&HE5)
        End If
        'Port出力
        SI_change170V = poutp(&H4, Port4OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P4[1],P4[2],P4[3],P4[4]:ワーク電圧24.0V出力(4bitまとめて判別)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function SI_change240V(ByVal data As Byte) As Boolean

        '初期設定
        SI_change240V = False

        If data = &H1 Then
            'Port4OutData = Port4OutData And Not CByte(&HE)
            Port4OutData = Port4OutData And CByte(&HE1)
            'Port4OutData = Port4OutData And Not CByte(2 ^ 0)
        Else
            Port4OutData = Port4OutData Or CByte(2 ^ 2)
            'Port4OutData = Port4OutData And Not CByte(&H1A)
            Port4OutData = Port4OutData And CByte(&HE5)
        End If
        'Port出力
        SI_change240V = poutp(&H4, Port4OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P4[1],P4[2],P4[3],P4[4]:ワーク電圧22.3V出力(4bitまとめて判別)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function SI_change223V(ByVal data As Byte) As Boolean

        '初期設定
        SI_change223V = False

        If data = &H1 Then
            Port4OutData = Port4OutData Or CByte(&HC)
            'Port4OutData = Port4OutData And Not CByte(&H18)
            Port4OutData = Port4OutData And CByte(&HED)
        Else
            Port4OutData = Port4OutData Or CByte(2 ^ 2)
            'Port4OutData = Port4OutData And Not CByte(&H12)
            Port4OutData = Port4OutData And CByte(&HE5)
        End If
        'Port出力
        SI_change223V = poutp(&H4, Port4OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P4[5]:予備出力(5bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Reserve2_Out(ByVal data As Byte) As Boolean

        '初期設定
        Reserve2_Out = False

        If data = &H1 Then
            Port4OutData = Port4OutData Or CByte(2 ^ 5)
        Else
            Port4OutData = Port4OutData And Not CByte(2 ^ 5)
        End If
        'Port出力
        Reserve2_Out = poutp(&H4, Port4OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P4[6]:予備出力(6bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Reserve3_Out(ByVal data As Byte) As Boolean

        '初期設定
        Reserve3_Out = False

        If data = &H1 Then
            Port4OutData = Port4OutData Or CByte(2 ^ 6)
        Else
            Port4OutData = Port4OutData And Not CByte(2 ^ 6)
        End If
        'Port出力
        Reserve3_Out = poutp(&H4, Port4OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P4[7]:予備出力(7bit目のみ)
    '【引数】data 出力データ 1:ON状態 0:OFF状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Reserve4_Out(ByVal data As Byte) As Boolean

        '初期設定
        Reserve4_Out = False

        If data = &H1 Then
            Port4OutData = Port4OutData Or CByte(2 ^ 7)
        Else
            Port4OutData = Port4OutData And Not CByte(2 ^ 7)
        End If
        'Port出力
        Reserve4_Out = poutp(&H4, Port4OutData)
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P5[2]:予備入力(2bit目のみ)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Reserve3_In(ByRef data As Byte) As Boolean

        '初期設定
        Reserve3_In = False

        Reserve3_In = pinp(&H5, data)
        If Reserve3_In = False Then
            Exit Function
        End If
        'P5[2]のみマスク
        data = (data >> 2) And CByte(2 ^ 0)
        Reserve3_In = True
    End Function

    '---------------------------------------------------------------------------------
    '【機能】P5[3]:予備入力(3bit目のみ)
    '【引数】data 入力データ 1:pullup状態 0:ON状態
    '【戻り値】True:正常   False:通信異常
    '---------------------------------------------------------------------------------
    Public Function Reserve4_In(ByRef data As Byte) As Boolean

        '初期設定
        Reserve4_In = False

        Reserve4_In = pinp(&H5, data)
        If Reserve4_In = False Then
            Exit Function
        End If
        'P5[3]のみマスク
        data = (data >> 3) And CByte(2 ^ 0)
        Reserve4_In = True
    End Function

    '-----------------------------------------------------------------------------
    '【機能】Port汎用出力
    '【引数】address:portNo(1～4) 1:port1 ・・・4:port4
    '        data:出力ビット 1:1bit目・・・・・・・0xff:全ビット
    '【戻り値】True：正常   False:通信異常
    '-----------------------------------------------------------------------------
    Private Function poutp(ByVal address As Byte, ByVal data As Byte) As Boolean

        '初期設定
        poutp = False

        Dim TxBuff(5) As Byte           '送信バッファ
        Dim RxBuff(16) As Byte          '受信バッファ
        Dim Buff(16) As Byte            '受信データ
        Dim CheckSum As Byte            'チェックサム値
        Dim i As Byte                   'Forループカウント
        Dim TimeCount As Long = 0       '時間計測用
        Dim RetryCount As Integer = 0   '通信エラー時のリトライ回数

RetryPosition:
        TxBuff(0) = &HFF                'ヘッダ
        TxBuff(1) = &H4                 'データ長
        TxBuff(2) = &H1                 'コマンド
        TxBuff(3) = address             'データ1
        TxBuff(4) = data                'データ2
        TxBuff(5) = &H0                 'チェックサム

        '送信するチェックサム計算
        For i = 1 To TxBuff(1)
            TxBuff(TxBuff(1) + 1) = TxBuff(TxBuff(1) + 1) Xor TxBuff(i)
            My.Application.DoEvents()
        Next i

        '受信バッファ初期化
        frmMain.PSoC.DiscardInBuffer()
        '送信バッファ初期化
        frmMain.PSoC.DiscardOutBuffer()
        'データ書込み
        '(引数1:書込みを行う配列名、引数2:開始配列番号、引数3:配列数)
        frmMain.PSoC.Write(TxBuff, 0, TxBuff(1) + 2)
        'Call wait(100)
        Call wait(10)
        TimeCount = timeGetTime
        Do
            'タイムアウト処理
            If timeGetTime() - TimeCount >= RS232cioTIMEOUT Then
                If RetryCount <= RS232cioRETRYCOUNT Then
                    RetryCount = RetryCount + 1
                    GoTo RetryPosition
                End If
                Exit Function
            End If

            Try
                'データ読込み
                '(引数1:読取りを行う配列名、引数2:開始配列番号、引数3:配列数)
                frmMain.PSoC.Read(RxBuff, 0, 17)

            Catch ex As Exception
                For i = 0 To 16
                    RxBuff(i) = 0
                    My.Application.DoEvents()
                Next i
            Finally
            End Try

            'RxBuffデータがごみデータじゃなかったら
            If RxBuff(0) = &HFF Then
                For i = 0 To RxBuff(1) + CByte(1)
                    Buff(i) = RxBuff(i)
                    My.Application.DoEvents()
                Next i
                Exit Do
            End If
            My.Application.DoEvents()
        Loop
        '受信バッファ初期化
        frmMain.PSoC.DiscardInBuffer()
        '送信バッファ初期化
        frmMain.PSoC.DiscardOutBuffer()

        CheckSum = 0
        '受信データからチェックサム値計算
        For i = 1 To Buff(1)
            CheckSum = CheckSum Xor Buff(i)
            My.Application.DoEvents()
        Next i

        'チェックサム比較
        If CheckSum <> Buff(Buff(1) + 1) Then
            If RetryCount <= RS232cioRETRYCOUNT Then
                RetryCount = RetryCount + 1
                GoTo RetryPosition
            End If
            Exit Function
        End If

        '戻り値確認
        If Buff(0) <> &HFF Or Buff(1) <> &H2 Or Buff(2) <> &H1 Then
            If RetryCount <= RS232cioRETRYCOUNT Then
                RetryCount = RetryCount + 1
                GoTo RetryPosition
            End If
            Exit Function
        End If

        poutp = True
    End Function

    '-----------------------------------------------------------------------------
    '【機能】Port汎用入力
    '【引数】address:portNo(0～4) 0:port0 ・・・4:port4
    '        data:入力データ 0x00：全ビットON状態、 0xff:全ビットOFF状態(pullup状態)
    '【戻り値】True：正常   False:通信異常
    '-----------------------------------------------------------------------------
    Private Function pinp(ByVal address As Byte, ByRef data As Byte) As Boolean

        '初期設定
        pinp = False

        Dim TxBuff(4) As Byte           '送信バッファ
        Dim RxBuff(16) As Byte          '受信バッファ
        Dim Buff(16) As Byte            '受信データ
        Dim CheckSum As Byte            'チェックサム値
        Dim i As Byte                   'Forループカウント
        Dim TimeCount As Long = 0       '時間計測用
        Dim RetryCount As Integer = 0   '通信エラー時のリトライ回数

RetryPosition:
        TxBuff(0) = &HFF                'ヘッダ
        TxBuff(1) = &H3                 'データ長
        TxBuff(2) = &H2                 'コマンド
        TxBuff(3) = address             'データ1'PSoCかPLCの仕様で決まっている送信先？/Nishi
        TxBuff(4) = &H0                 'チェックサム

        '送信するチェックサム計算
        For i = 1 To TxBuff(1)
            TxBuff(TxBuff(1) + 1) = TxBuff(TxBuff(1) + 1) Xor TxBuff(i)
            My.Application.DoEvents()
        Next i

        '受信バッファ初期化
        frmMain.PSoC.DiscardInBuffer()
        '送信バッファ初期化
        frmMain.PSoC.DiscardOutBuffer()
        'データ書込み
        '(引数1:書込みを行う配列名、引数2:開始配列番号、引数3:配列数)
        frmMain.PSoC.Write(TxBuff, 0, TxBuff(1) + 2)
        'Call wait(100)
        Call wait(10)
        TimeCount = timeGetTime()
        Do
            'タイムアウト処理
            If timeGetTime() - TimeCount >= RS232cioTIMEOUT Then
                If RetryCount <= RS232cioRETRYCOUNT Then
                    RetryCount = RetryCount + 1
                    GoTo RetryPosition
                End If
                Exit Function
            End If

            Try
                'データ読込み
                '(引数1:読取りを行う配列名、引数2:開始配列番号、引数3:配列数)
                'Call wait(300)
                frmMain.PSoC.Read(RxBuff, 0, 17)
            Catch ex As Exception
                For i = 0 To 16
                    RxBuff(i) = 0
                    My.Application.DoEvents()
                Next i
            Finally
            End Try

            'RxBuffデータがごみデータじゃなかったら
            '条件Buff(1) <= &HFを追加
            If RxBuff(0) = &HFF AndAlso Buff(1) <= &HF Then
                For i = 0 To RxBuff(1) + CByte(1)
                    Buff(i) = RxBuff(i)
                    My.Application.DoEvents()
                Next i
                Exit Do
            End If

            My.Application.DoEvents()
        Loop
        '受信バッファ初期化
        frmMain.PSoC.DiscardInBuffer()
        '送信バッファ初期化
        frmMain.PSoC.DiscardOutBuffer()

        CheckSum = 0
        '受信データからチェックサム値計算
        For i = 1 To Buff(1)
            CheckSum = CheckSum Xor Buff(i)
            My.Application.DoEvents()
        Next i

        'チェックサム比較
        If CheckSum <> Buff(Buff(1) + 1) Then
            If RetryCount <= RS232cioRETRYCOUNT Then
                RetryCount = RetryCount + 1
                GoTo RetryPosition
            End If
            Exit Function
        End If

        '戻り値確認
        If Buff(0) <> &HFF Or Buff(1) <> &H3 Or Buff(2) <> &H2 Then
            If RetryCount <= RS232cioRETRYCOUNT Then
                RetryCount = RetryCount + 1
                GoTo RetryPosition
            End If
            Exit Function
        End If

        '入力データをセット
        data = Buff(3)
        pinp = True
    End Function

End Module
